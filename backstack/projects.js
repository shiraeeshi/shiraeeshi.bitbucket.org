
var projects = (function() {
	var projectsMap = {};
	var instance = {
		add: function(projectName) {
			if (projectsMap[projectName]) {
				throw "project with name '" + projectName + "' already exists.";
			}
			var project = new Project(projectName);
			projectsMap[projectName] = project;
			this.runListeners();
		},
		getProject: function(name) {return projectsMap[name]},
		getNames: function() {
			return Object.keys(projectsMap);
		}
	};
	(function() {
		var listenerFunctions = [];
		instance.addListenerFunction = function(listenerFunction) {
			listenerFunctions.push(listenerFunction);
		};
		instance.runListeners = function() {
			for (var i=0, len=listenerFunctions.length; i<len; i++) {
				var listenerFunction = listenerFunctions[i];
				listenerFunction.apply(null, [instance]);
			}
		}
	 })();
	return instance;
})();
