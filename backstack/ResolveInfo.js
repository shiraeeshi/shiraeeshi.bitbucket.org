var ResolveInfo = (function() {
	var initFunction = function ResolveInfoInitFunction() {
		this.specificIndex = -1; // int
		this.activityInfo = null; // ActivityInfo
		this.serviceInfo = null; // ServiceInfo
		this.providerInfo = null; // ProviderInfo
		this.filter = null; // IntentFilter
		this.priority = 0; // int
		this.preferredOrder = 0; // int
		this.match = 0; // int
		this.isDefault = false; // boolean
		this.labelRes = 0; // int
		this.nonLocalizedLabel = null; // CharSequence
		this.icon = 0; // int
		this.resolvePackageName = null; // String
		this.targetUserId = -2; // int UserHandle.USER_CURRENT
		this.noResourceId = false; // boolean
		this.system = false; // boolean

		if (orig) {
			this.activityInfo = orig.activityInfo;
			this.serviceInfo = orig.serviceInfo;
			this.providerInfo = orig.providerInfo;
			this.filter = orig.filter;
			this.priority = orig.priority;
			this.preferredOrder = orig.preferredOrder;
			this.match = orig.match;
			this.specificIndex = orig.specificIndex;
			this.labelRes = orig.labelRes;
			this.nonLocalizedLabel = orig.nonLocalizedLabel;
			this.icon = orig.icon;
			this.resolvePackageName = orig.resolvePackageName;
			this.system = orig.system;
			this.targetUserId = orig.targetUserId;
		}


	};
	initFunction.prototype = {
		getComponentInfo: function() {
			if (this.activityInfo != null) return this.activityInfo;
			if (this.serviceInfo != null) return this.serviceInfo;
			if (this.providerInfo != null) return this.providerInfo;
			throw new Error("Missing ComponentInfo!");
		},
		loadLabel: function(pm) {
			if (this.nonLocalizedLabel != null) {
			    return this.nonLocalizedLabel;
			}
			var label;
			if (this.resolvePackageName != null && this.labelRes != 0) {
			    label = pm.getText(this.resolvePackageName, this.labelRes, null);
			    if (label != null) {
				return label.toString().trim();
			    }
			}
			var ci = this.getComponentInfo();
			var ai = ci.applicationInfo;
			if (this.labelRes != 0) {
			    label = pm.getText(ci.packageName, this.labelRes, ai);
			    if (label != null) {
				return label.toString().trim();
			    }
			}

			var data = ci.loadLabel(pm);
			// Make the data safe
			if (data != null) data = data.toString().trim();
			return data;
		},
    
		loadIcon: function(pm) {
			var dr;
			if (this.resolvePackageName != null && this.icon != 0) {
			    dr = pm.getDrawable(this.resolvePackageName, this.icon, null);
			    if (dr != null) {
				return dr;
			    }
			}
			var ci = this.getComponentInfo();
			var ai = ci.applicationInfo;
			if (this.icon != 0) {
			    dr = pm.getDrawable(ci.packageName, this.icon, ai);
			    if (dr != null) {
				return dr;
			    }
			}
			return ci.loadIcon(pm);
		},
    
		getIconResource: function() {
			if (this.noResourceId) return 0;
			if (this.icon != 0) return this.icon;
			var ci = this.getComponentInfo();
			if (ci != null) {
			    return ci.getIconResource();
			}
			return 0;
		}
	};
	return initFunction;
})();
