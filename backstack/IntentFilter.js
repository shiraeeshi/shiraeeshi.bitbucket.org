var IntentFilter = (function() {
	var self = {};
	self.__construct = function(action_or_filter, dataType) {
		this.mPriority = 0; // int
		this.mActions = []; // ArrayList<String>
		this.mCategories = null; // ArrayList<String>
		this.mDataSchemes = null; // ArrayList<String>
		this.mDataSchemeSpecificParts = null; // ArrayList<PatternMatcher>
		this.mDataAuthorities = null; // ArrayList<AuthorityEntry>
		this.mDataPaths = null; // ArrayList<PatternMatcher>
		this.mDataTypes = null; // ArrayList<String>
		this.mHasPartialTypes = false; // boolean

		if (arguments.length == 0) {
			this.mPriority = 0;
			this.mActions = [];
		} else if (arguments.length == 1) {
			if (action_or_filter instanceof IntentFilter) {
				var filter = action_or_filter;
				var stateIndex = filter._getState();
				var state = stateBank['key'+stateIndex];
				delete stateBank['key'+stateIndex];

				this.mPriority = state.mPriority;
				this.mActions = state.mActions.slice();
				if (state.mCategories) {
					this.mCategories = state.mCategories.slice();
				}
				if (state.mDataSchemes) {
					this.mDataSchemes = state.mDataSchemes.slice();
				}
				if (state.mDataSchemeSpecificParts) {
					this.mDataSchemeSpecificParts = state.mDataSchemeSpecificParts.slice();
				}
				if (state.mDataAuthorities) {
					this.mDataAuthorities = state.mDataAuthorities.slice();
				}
				if (state.mDataPaths) {
					this.mDataPaths = state.mDataPaths.slice();
				}
				if (state.mDataTypes) {
					this.mDataTypes = state.mDataTypes.slice();
				}
				this.mHasPartialTypes = state.mHasPartialTypes;
			} else {
				this.mPriority = 0;
				this.mActions = [];
				this.addAction(action_or_filter);
			}
		} else {
			this.mPriority = 0;
			this.mActions = [];
			this.addAction(action_or_filter);
			this.addDataType(dataType);
		}
	};
	self._getState = function() {
		var state = {};
		state.mPriority = this.mPriority;
		state.mActions = this.mActions;
		state.mCategories = this.mCategories;
		state.mDataSchemes = this.mDataSchemes;
		state.mDataSchemeSpecificParts = this.mDataSchemeSpecificParts;
		state.mDataAuthorities = this.mDataAuthorities;
		state.mDataPaths = this.mDataPaths;
		state.mDataTypes = this.mDataTypes;
		state.mHasPartialTypes = this.mHasPartialTypes;
		var ind = Object.keys(stateBank).length;
		stateBank['key'+ind] = state;
		return ind;
	};
	self.setPriority = function( priority) {
		this.mPriority = priority;
	};

	self.getPriority = function() {
		return this.mPriority;
	};

	self.addAction = function( action) {
		if (this.mActions.indexOf(action) < 0) {
		    this.mActions.push(action);
		}
	};

	self.countActions = function() {
		return this.mActions.length;
	};

	self.getAction = function( index) {
		return this.mActions[index];
	};

	self.hasAction = function( action) {
		return action != null && (this.mActions.indexOf(action) >= 0);
	};

	self.matchAction = function( action) {
		return self.hasAction(action);
	};

	self.actionsIterator = function() {
		return this.mActions != null ? new ArrayIterator(this.mActions) : null;
	};

	self.addDataType = function(type) {
		var slashpos = type.indexOf('/');
		var typelen = type.length;
		if (slashpos > 0 && typelen >= slashpos+2) {
		    if (this.mDataTypes == null) this.mDataTypes = [];
		    if (typelen == slashpos+2 && type.charAt(slashpos+1) == '*') {
			var str = type.substring(0, slashpos);
			if (this.mDataTypes.indexOf(str) < 0) {
			    this.mDataTypes.push(str);
			}
			this.mHasPartialTypes = true;
		    } else {
			if (this.mDataTypes.indexOf(type) < 0) {
			    this.mDataTypes.push(type);
			}
		    }
		    return;
		}

		throw new initFunction.MalformedMimeTypeException(type);
	};

	self.hasDataType = function(type) {
		return this.mDataTypes != null && findMimeType(type);
	};

	self.hasExactDataType = function(type) {
		return this.mDataTypes != null && (this.mDataTypes.indexOf(type) >= 0);
	};

	self.countDataTypes = function() {
		return this.mDataTypes != null ? this.mDataTypes.length : 0;
	};

	self.getDataType = function(index) {
		return this.mDataTypes[index];
	};

	self.typesIterator = function() {
		return this.mDataTypes != null ? new ArrayIterator(this.mDataTypes) : null;
	};

	self.addDataScheme = function(scheme) {
		if (this.mDataSchemes == null) this.mDataSchemes = [];
		if (this.mDataSchemes.indexOf(scheme) < 0) {
		    this.mDataSchemes.push(scheme);
		}
	};

	self.countDataSchemes = function() {
		return this.mDataSchemes != null ? this.mDataSchemes.length : 0;
	};

	self.getDataScheme = function(index) {
		return this.mDataSchemes[index];
	};

	self.hasDataScheme = function(scheme) {
		return this.mDataSchemes != null && (this.mDataSchemes.indexOf(scheme) >= 0);
	};

	self.schemesIterator = function() {
		return this.mDataSchemes != null ? new ArrayIterator(this.mDataSchemes) : null;
	};
	self.addDataSchemeSpecificPart = function(ssp_or_matcher, type) {
		var matcher;
		if (ssp_or_matcher instanceof PatternMatcher) {
			matcher = ssp_or_matcher;
		} else {
			var ssp = ssp_or_matcher;
			matcher = new PatternMatcher(ssp, type);
		}
		if (this.mDataSchemeSpecificParts == null) {
		    this.mDataSchemeSpecificParts = [];
		}
		this.mDataSchemeSpecificParts.push(matcher);
	};

	self.countDataSchemeSpecificParts = function() {
		return this.mDataSchemeSpecificParts != null ? this.mDataSchemeSpecificParts.length : 0;
	};

	self.getDataSchemeSpecificPart = function(index) {
		return this.mDataSchemeSpecificParts[index];
	}

	self.hasDataSchemeSpecificPart = function(data) {
		if (data instanceof PatternMatcher) {
			var ssp = data;
			if (this.mDataSchemeSpecificParts == null) {
			    return false;
			}
			var numDataSchemeSpecificParts = this.mDataSchemeSpecificParts.size();
			for (var i = 0; i < numDataSchemeSpecificParts; i++) {
			    var pe = this.mDataSchemeSpecificParts[i];
			    if (pe.getType() == ssp.getType() && pe.getPath() == ssp.getPath()) {
				return true;
			    }
			}
			return false;
		} else {
			if (this.mDataSchemeSpecificParts == null) {
			    return false;
			}
			var numDataSchemeSpecificParts = this.mDataSchemeSpecificParts.length;
			for (var i = 0; i < numDataSchemeSpecificParts; i++) {
			    var pe = this.mDataSchemeSpecificParts[i];
			    if (pe.match(data)) {
				return true;
			    }
			}
			return false;
		}
	};

	self.schemeSpecificPartsIterator = function() {
		return this.mDataSchemeSpecificParts != null ? new ArrayIterator(this.mDataSchemeSpecificParts) : null;
	};

	self.addDataAuthority = function(host_or_entry, port) {
		var ent;
		if (host_or_entry instanceof initFunction.AuthorityEntry) {
			ent = host_or_entry;
		} else {
			var host = host_or_entry;
			ent = new initFunction.AuthorityEntry(host, port);
		}
		if (this.mDataAuthorities == null) this.mDataAuthorities = [];
		this.mDataAuthorities.push(ent);
	};

	self.countDataAuthorities = function() {
		return this.mDataAuthorities != null ? this.mDataAuthorities.length : 0;
	};

	self.getDataAuthority = function(index) {
		return this.mDataAuthorities[index];
	};

	self.hasDataAuthority = function(data) {
		return self.matchDataAuthority(data) >= 0;
	};

	self.hasDataAuthority = function(auth) {
		if (this.mDataAuthorities == null) {
		    return false;
		}
		var numDataAuthorities = this.mDataAuthorities.length;
		for (var i = 0; i < numDataAuthorities; i++) {
		    if (this.mDataAuthorities[i].match(auth)) {
			return true;
		    }
		}
		return false;
	};

	self.authoritiesIterator = function() {
		return this.mDataAuthorities != null ? new ArrayIterator(this.mDataAuthorities) : null;
	};

	self.addDataPath = function(path_or_matcher, type) {
		var patternMatcher;
		if (path instanceof PatternMatcher) {
			patternMatcher = path_or_matcher;
		} else {
			var path = path_or_matcher;
			patternMatcher = new PatternMatcher(path, type);
		}
		if (this.mDataPaths == null) this.mDataPaths = [];
		this.mDataPaths.push(patternMatcher);
	};

	self.countDataPaths = function() {
		return this.mDataPaths != null ? this.mDataPaths.length : 0;
	};

	self.getDataPath = function(index) {
		return this.mDataPaths[index];
	};

	self.hasDataPath = function(data_or_matcher) {
		if (data_or_matcher instanceof PatternMatcher) {
			var path = data_or_matcher;
			if (this.mDataPaths == null) {
			    return false;
			}
			var numDataPaths = this.mDataPaths.length;
			for (var i = 0; i < numDataPaths; i++) {
			    var pe = this.mDataPaths[i];
			    if (pe.getType() == path.getType() && (pe.getPath() == path.getPath()) ) {
				return true;
			    }
			}
			return false;
		} else {
			var data = data_or_matcher;
			if (this.mDataPaths == null) {
			    return false;
			}
			var numDataPaths = this.mDataPaths.length;
			for (var i = 0; i < numDataPaths; i++) {
			    var pe = this.mDataPaths[i];
			    if (pe.match(data)) {
				return true;
			    }
			}
			return false;
		}
	};

	self.pathsIterator = function() {
		return this.mDataPaths != null ? new ArrayIterator(this.mDataPaths) : null;
	};

	self.matchDataAuthority = function(data) {
		if (this.mDataAuthorities == null) {
		    return initFunction.NO_MATCH_DATA;
		}
		var numDataAuthorities = this.mDataAuthorities.length;
		for (var i = 0; i < numDataAuthorities; i++) {
		    var ae = this.mDataAuthorities[i];
		    var match = ae.match(data);
		    if (match >= 0) {
			return match;
		    }
		}
		return initFunction.NO_MATCH_DATA;
	};

	self.matchData = function(type, scheme, data) {
		var types = this.mDataTypes;
		var schemes = this.mDataSchemes;

		var match = initFunction.MATCH_CATEGORY_EMPTY;

		if (types == null && schemes == null) {
		    return ((type == null && data == null)
			? (initFunction.MATCH_CATEGORY_EMPTY+initFunction.MATCH_ADJUSTMENT_NORMAL) : initFunction.NO_MATCH_DATA);
		}

		if (schemes != null) {
		    if (schemes.indexOf(scheme != null ? scheme : "") >= 0) {
			match = initFunction.MATCH_CATEGORY_SCHEME;
		    } else {
			return initFunction.NO_MATCH_DATA;
		    }

		    var schemeSpecificParts = this.mDataSchemeSpecificParts;
		    if (schemeSpecificParts != null) {
			match = self.hasDataSchemeSpecificPart(data.getSchemeSpecificPart())
				? initFunction.MATCH_CATEGORY_SCHEME_SPECIFIC_PART : initFunction.NO_MATCH_DATA;
		    }
		    if (match != initFunction.MATCH_CATEGORY_SCHEME_SPECIFIC_PART) {
			// If there isn't any matching ssp, we need to match an authority.
			var authorities = this.mDataAuthorities;
			if (authorities != null) {
			    var authMatch = self.matchDataAuthority(data);
			    if (authMatch >= 0) {
				var paths = this.mDataPaths;
				if (paths == null) {
				    match = authMatch;
				} else if (self.hasDataPath(data.getPath())) {
				    match = initFunction.MATCH_CATEGORY_PATH;
				} else {
				    return initFunction.NO_MATCH_DATA;
				}
			    } else {
				return initFunction.NO_MATCH_DATA;
			    }
			}
		    }
		    // If neither an ssp nor an authority matched, we're done.
		    if (match == initFunction.NO_MATCH_DATA) {
			return initFunction.NO_MATCH_DATA;
		    }
		} else {
		    // Special case: match either an Intent with no data URI,
		    // or with a scheme: URI.  This is to give a convenience for
		    // the common case where you want to deal with data in a
		    // content provider, which is done by type, and we don't want
		    // to force everyone to say they handle content: or file: URIs.
		    if (scheme != null && scheme.length > 0
			    && "content" != scheme
			    && "file" != scheme) {
			return initFunction.NO_MATCH_DATA;
		    }
		}

		if (types != null) {
		    if (findMimeType(type)) {
			match = initFunction.MATCH_CATEGORY_TYPE;
		    } else {
			return initFunction.NO_MATCH_TYPE;
		    }
		} else {
		    // If no MIME types are specified, then we will only match against
		    // an Intent that does not have a MIME type.
		    if (type != null) {
			return initFunction.NO_MATCH_TYPE;
		    }
		}

		return match + initFunction.MATCH_ADJUSTMENT_NORMAL;
	};

	self.addCategory = function(category) {
		if (this.mCategories == null) this.mCategories = [];
		if (this.mCategories.indexOf(category) < 0) {
		    this.mCategories.push(category);
		}
	};

	self.countCategories = function() {
		return this.mCategories != null ? this.mCategories.length : 0;
	};

	self.getCategory = function(index) {
		return this.mCategories[index];
	};

	self.hasCategory = function(category) {
		return this.mCategories != null && (this.mCategories.indexOf(category) >= 0);
	};

	self.categoriesIterator = function() {
		return this.mCategories != null ? new ArrayIterator(this.mCategories) : null;
	};

	self.matchCategories = function(categories) {
		if (categories == null) {
		    return null;
		}

		var it = new ArrayIterator(categories);

		if (this.mCategories == null) {
			return categories.length > 0 ? categories[0] : null;
		}

		while (true) {
		    var category = it.next();
		    if (it.isDone()) {
			    break;
		    }
		    if (this.mCategories.indexOf(category) < 0) {
			return category;
		    }
		}

		return null;
	};

	self.match = function(action_or_resolver, type_or_intent, scheme_or_resolve, data, categories) {
		var action, type, scheme;
		if (action_or_resolver instanceof ContentResolver) {
			var resolver = action_or_resolver;
			var intent = type_or_intent;
			var resolve = scheme_or_resolve;
			type = resolve ? intent.resolveType(resolver) : intent.getType();
			action = intent.getAction();
			scheme = intent.getScheme();
			data = intent.getData();
			categories = intent.getCategories();
		} else {
			action = action_or_resolver;
			type = type_or_intent;
			scheme = scheme_or_resolve;
		}
		if (action != null && !self.matchAction(action)) {
		    return initFunction.NO_MATCH_ACTION;
		}

		var dataMatch = self.matchData(type, scheme, data);
		if (dataMatch < 0) {
		    return dataMatch;
		}

		var categoryMismatch = self.matchCategories(categories);
		if (categoryMismatch != null) {
		    return initFunction.NO_MATCH_CATEGORY;
		}

		return dataMatch;
	};
	var initFunction = klass(Object, self);
	var ArrayIterator = function(array) {
		var currentIndex = -1;
		var removedOnce = false;
		this.next = function() {
			if (removedOnce) {
				removedOnce = false;
			}
			currentIndex++;
			return array[currentIndex];
		};
		this.isDone = function() {
			if (currentIndex < 0) {
				throw new RangeError('iterator "isDone" must be invoked after "next" method invokation');
			}
			return currentIndex >= array.length;
		};
		this.remove = function() {
			if (removedOnce) {
				return;
			}
			if (this.isDone()) {
				return;
			}
			if (currentIndex < 0) {
				throw new RangeError('iterator "remove" must be invoked after "next" method invokation');
			}

			removedOnce = true;
			array.splice(currentIndex, 1);
			currentIndex--;
		};
	};
	var findMimeType = function(type) {
		//final ArrayList<String> t = this.mDataTypes;
		t = this.mDataTypes;

		if (type == null) {
		    return false;
		}

		if (t.indexOf(type) >= 0) {
		    return true;
		}

		// Deal with an Intent wanting to match every type in the IntentFilter.
		var typeLength = type.length;
		if (typeLength == 3 && type.equals("*/*")) {
		    return t.length > 0;
		}

		// Deal with this IntentFilter wanting to match every Intent type.
		if (this.mHasPartialTypes && t.indexOf("*") >= 0) {
		    return true;
		}

		var slashpos = type.indexOf('/');
		if (slashpos > 0) {
		    if (this.mHasPartialTypes && (t.indexOf(type.substring(0, slashpos)) >= 0) ) {
			return true;
		    }
		    if (typeLength == slashpos+2 && type.charAt(slashpos+1) == '*') {
			// Need to look through all types for one that matches
			// our base...
			var numTypes = t.length;
			for (var i = 0; i < numTypes; i++) {
			    var v = t[i];
			    //if (type.regionMatches(0, v, 0, slashpos+1)) {
			    if (type.substring(0, slashpos+1) == v.substring(0, slashpos+1)) {
				return true;
			    }
			}
		    }
		}

		return false;
	};
	var SGLOB_STR = "sglob";
	var PREFIX_STR = "prefix";
	var LITERAL_STR = "literal";
	var PATH_STR = "path";
	var PORT_STR = "port";
	var HOST_STR = "host";
	var AUTH_STR = "auth";
	var SSP_STR = "ssp";
	var SCHEME_STR = "scheme";
	var TYPE_STR = "type";
	var CAT_STR = "cat";
	var NAME_STR = "name";
	var ACTION_STR = "action";

	// These functions are the start of more optimized code for managing
	// the string sets...  not yet implemented.

	var findStringInSet = function(array, string, lengths, lenPos) {
		if (array == null || array == undefined) return -1;
		var N = lengths[lenPos];
		for (var i=0; i<N; i++) {
		    if (array[i] == string) return i;
		}
		return -1;
	};

	var addStringToSet = function(array, string, lengths, lenPos) {
		if (findStringInSet(array, string, lengths, lenPos) >= 0) return array;
		if (array == null) {
		    array = new Array(2);
		    array[0] = string;
		    lengths[lenPos] = 1;
		    return array;
		}
		var N = lengths[lenPos];
		if (N < array.length) {
		    array[N] = string;
		    lengths[lenPos] = N+1;
		    return array;
		}

		var newSet = array.slice(0, N);
		array = newSet;
		array[N] = string;
		lengths[lenPos] = N+1;
		return array;
	};

	var removeStringFromSet = function(array, string, lengths, lenPos) {
		var pos = findStringInSet(array, string, lengths, lenPos);
		if (pos < 0) return array;
		var N = lengths[lenPos];
		if (N > (array.length/4)) {
		    var copyLen = N-(pos+1);
		    if (copyLen > 0) {
			//System.arraycopy(array, pos+1, array, pos, copyLen);
			array.splice(pos, 1);
		    }
		    array[N-1] = null;
		    lengths[lenPos] = N-1;
		    return array;
		}

		var newSet = [];
		if (pos > 0) {
			//System.arraycopy(array, 0, newSet, 0, pos);
			Array.prototype.splice.apply(newSet, [0,0].concat(array.slice(0, pos)));
		}
		if ((pos+1) < N) {
			//System.arraycopy(array, pos+1, newSet, pos, N-(pos+1));
			Array.prototype.splice.apply(newSet, [pos,N-pos].concat(array.slice(pos+1, N)));
		}
		return newSet;
	};

	initFunction.MalformedMimeTypeException = function(msg) {this.name=msg;};
	initFunction.MalformedMimeTypeException.prototype = Object.create(Error.prototype);
	initFunction.MalformedMimeTypeException.prototype.constructor = initFunction.MalformedMimeTypeException;

	initFunction.create = function(action, dataType) {
		try {
		    return new IntentFilter(action, dataType);
		} catch (e) {
			if (e instanceof initFunction.MalformedMimeTypeException) {
				throw new initFunction.MalformedMimeTypeException('Bad MIME type. ' + e.name);
			}
		}
	};
	initFunction.AuthorityEntry = function(host, port) {
		this.mOrigHost = host;
		this.mWild = host.length > 0 && host.charAt(0) == '*';
		this.mHost = this.mWild ? host.substring(1) : host;
		this.mPort = port != null ? parseInt(port) : -1;
	};
	initFunction.AuthorityEntry.prototype = {
		getHost: function() {
			return this.mOrigHost;
		},
		getPort: function() {
			return this.mPort;
		},
		match: function(other_or_data) {
			if (other_or_data instanceof initFunction.AuthorityEntry) {
				var other = other_or_data;
				if (this.mWild != other.mWild) {
					return false;
				}
				if (this.mHost != other.mHost) {
					return false;
				}
				if (this.mPort != other.mPort) {
					return false;
				}
				return true;
			}
			var data = other_or_data;
			var host = data.getHost();
			if (host == null) {
				return initFunction.NO_MATCH_DATA;
			}
			if (this.mWild) {
				if (host.length < this.mHost.length) {
					return initFunction.NO_MATCH_DATA;
				}
				host = host.substring(host.length - this.mHost.length);
			}
			if (host.toLowerCase() != this.mHost.toLowerCase()) {
				return initFunction.NO_MATCH_DATA;
			}
			if (this.mHost >= 0) {
				if (this.mPort != data.getPort()) {
					return initFunction.NO_MATCH_DATA;
				}
				return initFunction.MATCH_CATEGORY_PORT;
			}
			return initFunction.MATCH_CATEGORY_HOST;
		}
	};
	initFunction.SYSTEM_HIGH_PRIORITY = 1000;
	initFunction.SYSTEM_LOW_PRIORITY = -1000;
	initFunction.MATCH_CATEGORY_MASK = 0xfff0000;
	initFunction.MATCH_ADJUSTMENT_MASK = 0x000ffff;
	initFunction.MATCH_ADJUSTMENT_NORMAL = 0x8000;
	initFunction.MATCH_CATEGORY_EMPTY = 0x0100000;
	initFunction.MATCH_CATEGORY_SCHEME = 0x0200000;
	initFunction.MATCH_CATEGORY_HOST = 0x0300000;
	initFunction.MATCH_CATEGORY_PORT = 0x0400000;
	initFunction.MATCH_CATEGORY_PATH = 0x0500000;
	initFunction.MATCH_CATEGORY_SCHEME_SPECIFIC_PART = 0x0580000;
	initFunction.MATCH_CATEGORY_TYPE = 0x0600000;
	initFunction.NO_MATCH_TYPE = -1;
	initFunction.NO_MATCH_DATA = -2;
	initFunction.NO_MATCH_ACTION = -3;
	initFunction.NO_MATCH_CATEGORY = -4;
	var stateBank = {};
	return initFunction;
})();
