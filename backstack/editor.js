
var Editor = (function() {

	var initFunction = function EditorInitFunction() {
		var self = this;
		var buildElement = function() {
			var editorMarkupElement = document.getElementById('editor-layout');
			var clone = editorMarkupElement.cloneNode(true);
			clone.id = '';
			return clone;
		};
		var element = buildElement();
		var textarea = element.getElementsByTagName('textarea')[0];
		var mainScreen = new EditorMainScreen(textarea);
		element.getElementsByClassName('close-button')[0].addEventListener('click', function() {
				if (self.project) {
					if ( ! bufferIsEmpty()) {
						if ( ! confirm('This editor has unsaved data in buffer. \nClose it and lose all unsaved data?')) {return;}
					}
					self.project.removeManifestListener(manifestListener);
					self.project.removeCodeListener(codeListener);
					self.project.removeLayoutListener(layoutListener);
					self.project.removeLayoutListListener(layoutListListener);
				}
				element.parentNode.removeChild(element);
				var index = editors.indexOf(self);
				editors.splice(index,1);
				if (editors.length < 3) {document.getElementById('main-toolbox').getElementsByClassName('add-editor-button')[0].disabled = false;}
				recalculateWidths();
			}, false);
		var bufferIsEmpty = function() {
			self.saveTextareaValue();
			return (Object.keys(self.buffer).length == 1) &&
				(Object.keys(self.buffer.layout).length == 0);
		};
		textarea.addEventListener('keyup', function() {
			self.saveTextareaValue();
			resetStars();
			resetActionSelectorOptions();
		}, false);
		textarea.addEventListener('keydown', function(eve) {
			if (eve.keyCode == 9) {
				var text = textarea.value;
				var cursorPosition = textarea.selectionStart;
				textarea.value = text.substr(0, cursorPosition) + '    ' + text.substr(cursorPosition);
				textarea.selectionStart = cursorPosition + 4;
				textarea.selectionEnd = textarea.selectionStart;
				eve.preventDefault();
			}
		}, false);
		textarea.addEventListener('keydown', function(eve) {
			if (eve.keyCode == 13) {
				var text = textarea.value;
				var cursorPosition = textarea.selectionStart;
				var prevLineStart = text.lastIndexOf('\n', cursorPosition-1);
				if (prevLineStart < 0) {
					prevLineStart = 0;
				} else {
					prevLineStart++;
				}
				var prevLine = text.substring(prevLineStart, cursorPosition);
				var firstNonWhitespaceIndex = prevLine.search(/\S/);
				if (firstNonWhitespaceIndex < 0) {
					firstNonWhitespaceIndex = prevLine.length;
				}
				var spacesCount = firstNonWhitespaceIndex;
				var prevIndentsCount = spacesCount / 4;
				var indentsCount = prevIndentsCount;
				if (text[cursorPosition-1] == '{') {
					indentsCount++;
				}
				var indents = (new Array(indentsCount+1)).join('    ');
				textarea.value = text.substr(0, cursorPosition)+ '\n' + indents + text.substr(cursorPosition);
				textarea.selectionStart = cursorPosition + indents.length + 1;
				textarea.selectionEnd = textarea.selectionStart;
				eve.preventDefault();
			}
		}, false);
		textarea.addEventListener('keydown', function(eve) {
			if (eve.keyCode == 8) {
				var text = textarea.value;
				var cursorPosition = textarea.selectionStart;
				var lineStart = text.lastIndexOf('\n', cursorPosition-1) + 1;
				if (text.substring(lineStart, cursorPosition).search(/\S/) >= 0) {
					return;
				}
				if (textarea.selectionEnd != textarea.selectionStart) {
					return;
				}
				var lineEnd = text.indexOf('\n', cursorPosition-1);
				if (lineEnd < 0) {
					lineEnd = text.length;
				}
				var line = text.substring(lineStart, lineEnd);
				var firstNonWhitespaceIndex = line.search(/\S/);
				if (firstNonWhitespaceIndex < 0) {
					firstNonWhitespaceIndex = line.length;
				}
				var spacesCount = firstNonWhitespaceIndex;
				if (spacesCount % 4 == 0) {
					var indentsCount = spacesCount / 4;
					indentsCount--;
					var indents = (new Array(indentsCount+1)).join('    ');
					textarea.value = text.substr(0, lineStart) + indents + text.substr(cursorPosition);
					textarea.selectionStart = lineStart + indents.length;
					textarea.selectionEnd = textarea.selectionStart;
					eve.preventDefault();
				}
			}
		}, false);
		var projectsSelector = element.getElementsByClassName('project-selector')[0];
		var actionSelector = element.getElementsByClassName('action-selector')[0];
		var fillProjectsSelector = function(projects) {
			var names = projects.getNames();
			var currentValue = projectsSelector.value;
			projectsSelector.innerHTML = '';
			if (!currentValue) {
				var opt = document.createElement('option');
				opt.value = '';
				opt.disabled = true;
				opt.innerHTML = 'select project';
				opt.selected = true;
				projectsSelector.appendChild(opt);
			}
			for (var i=0, len=names.length; i<len; i++) {
				var name = names[i];
				var opt = document.createElement('option');
				opt.value = name;
				opt.innerHTML = name;
				projectsSelector.appendChild(opt);
			}
			projectsSelector.value = currentValue;
		};
		fillProjectsSelector(projects);
		projectsSelector.addEventListener('change', 
			(function() {
				var dontRun = false;
				projects.addListenerFunction(function(projects) {
					if (dontRun) {dontRun = false;return;}
					fillProjectsSelector(projects);
				});
				return function(eve) {
					var value = eve.target.value;
					dontRun = true;
					self.setProject(value);
				}
			 })(), false);
		var typeSelector = element.getElementsByClassName('type-selector')[0];
		var manifestListener = {
			update: function(proj) {
				if (self.type == initFunction.Type.MANIFEST) {
					textarea.value = proj.getManifest();
					if (self.buffer.manifest) {
						delete self.buffer.manifest;
						resetStars();
					}
				}
			}
		};
		var codeListener = {
			update: function(proj) {
				if (self.type == initFunction.Type.CODE) {
					textarea.value = proj.getCode();
					if (self.buffer.code) {
						delete self.buffer.code;
						resetStars();
					}
				}
			}
		};
		var layoutListener = {
			update: function(proj, changedLayoutName) {
				if (self.type == initFunction.Type.LAYOUT) {
					if (self.currentLayoutName != changedLayoutName) {return;}
					textarea.value = proj.getLayout(self.currentLayoutName);
					if (viewBuilder.isBuilt()) {
						viewBuilder.rebuild();
					}
					if (self.buffer.layout[self.currentLayoutName]) {
						delete self.buffer.layout[self.currentLayoutName];
						resetStars();
					}
				}
			}
		};
		var layoutListListener = {
			update: function(proj) {
				typeSelector.innerHTML = '';
				typeSelector.innerHTML = document.getElementById('editor-layout').getElementsByClassName('type-selector')[0].innerHTML;
				var lastOption = typeSelector.lastElementChild;
				//if (lastOption.innerHTML.startsWith(initFunction.Type.LAYOUT.value)) {
				var layoutConstantValue = initFunction.Type.LAYOUT.value;
				if (lastOption.innerHTML.substring(0, layoutConstantValue.length) == layoutConstantValue) {
					typeSelector.removeChild(lastOption);
				}
				var layoutNames = proj.getLayoutNames();
				for (var i=0, len=layoutNames.length; i<len; i++) {
					var name = layoutNames[i];
					name = initFunction.Type.LAYOUT.value + ' ' + name;
					var option = document.createElement('option');
					option.value = i;
					option.text = name;
					typeSelector.appendChild(option);
				}
				resetStars();
				if (self.currentLayoutName) {
					if (layoutNames.indexOf(self.currentLayoutName) < 0) {
						self.setType(initFunction.Type.MANIFEST.value);
					} else {
						selectLayout(self.currentLayoutName);
					}
				}
			}
		};
		typeSelector.addEventListener('change', 
			function(eve) {
				var value = eve.target.value;
				typeSelectorValueChangeHandler.call(null, value);
			}, false);
		var typeSelectorValueChangeHandler = function(newValue) {
			self.saveTextareaValue();
			self.setType(newValue);
		};
		self.type = initFunction.Type.MANIFEST;
		self.getElement = function() {return element;};
		self.buffer = {layout:{}};
		self.saveBufferValue = function() {
			if (self.type == initFunction.Type.MANIFEST) {
				self.project.setManifest(self.buffer.manifest);
				delete self.buffer.manifest;
			} else if (self.type == initFunction.Type.LAYOUT) {
				self.project.setLayout(self.currentLayoutName, self.buffer.layout[self.currentLayoutName]);
				delete self.buffer.layout[self.currentLayoutName];
			} else if (self.type == initFunction.Type.CODE) {
				self.project.setCode(self.buffer.code);
				delete self.buffer.code;
			}
		};
		self.saveTextareaValue = function() {
			if (self.type == initFunction.Type.MANIFEST) {
				var projectManifest = self.project.getManifest();
				if (projectManifest != textarea.value) {
					self.buffer.manifest = textarea.value;
				} else if (self.buffer.manifest) {
					delete self.buffer.manifest;
				}
			} else if (self.type == initFunction.Type.LAYOUT) {
				var projectLayout = self.project.getLayout(self.currentLayoutName);
				if (projectLayout != textarea.value) {
					self.buffer.layout[self.currentLayoutName] = textarea.value;
				} else if (self.buffer.layout[self.currentLayoutName]) {
					delete self.buffer.layout[self.currentLayoutName];
				}
			} else if (self.type == initFunction.Type.CODE) {
				var projectCode = self.project.getCode();
				if (projectCode != textarea.value) {
					self.buffer.code = textarea.value;
				} else if (self.buffer.code) {
					delete self.buffer.code;
				}
			}
			resetStars();
		};

		var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

		var resetStars = function() {
			var manifestValue = initFunction.Type.MANIFEST.value;
			var codeValue = initFunction.Type.CODE.value;
			var layoutValue = initFunction.Type.LAYOUT.value;

			var manifestOption = typeSelector.querySelector('option[value=' + manifestValue + ']');
			var manifestInnerHTML = manifestOption.innerText;
			var isBuffered = self.buffer.hasOwnProperty('manifest');
			var hasStar = (manifestInnerHTML.charAt(manifestInnerHTML.length-1) == '*');
			if (isBuffered && ! hasStar) {
				manifestOption.text = manifestInnerHTML + '*';
			} else if (hasStar && ! isBuffered) {
				manifestOption.text = manifestInnerHTML.substr(0, manifestInnerHTML.length-1)
			}

			var codeOption = typeSelector.querySelector('option[value=' + codeValue + ']');
			var codeInnerHTML = codeOption.innerText;
			isBuffered = self.buffer.hasOwnProperty('code');
			hasStar = (codeInnerHTML.charAt(codeInnerHTML.length-1) == '*');
			if (isBuffered && ! hasStar) {
				codeOption.text = codeInnerHTML + '*';
			} else if (hasStar && ! isBuffered) {
				codeOption.text = codeInnerHTML.substr(0, codeInnerHTML.length-1);
			}

			var bufferedLayoutNames = Object.keys(self.buffer.layout);
			var options = typeSelector.querySelectorAll('option:not([value=' + manifestValue +']):not([value=' + codeValue + '])');
			for (var i=0, len=options.length; i<len; i++) {
				var option = options[i];
				var optionLayout = option.innerText.substr(layoutValue.length+1);
				var hasStar = optionLayout.charAt(optionLayout.length-1) == '*';
				if (hasStar) {
					optionLayout = optionLayout.substr(0, optionLayout.length-1);
				}
				var isBuffered = (bufferedLayoutNames.indexOf(optionLayout) >= 0);
				if (isBuffered && ! hasStar) {
					option.text = option.innerText + '*';
				} else if (hasStar && ! isBuffered) {
					option.text = option.innerText.substr(0, option.innerText.length-1);
				}
			}
			if (isIE) {
				forseRedrawForIE(typeSelector);
			}
		};
		var forseRedrawForIE = function(node) {
			var dis = node.style.display;
			node.style.display = 'none';
			node.style.display = dis;
		};
		var recalculateWidths = function() {
			var bodyWidth = document.getElementsByTagName('body')[0].clientWidth;
			var width = 0;
			if (editors.length > 2) {
				width = bodyWidth / 3;
			} else {
				width = bodyWidth / editors.length;
			}
			width += 'px';
			for (var i=editors.length-1; i>=0; i--) {
				editors[i].setWidth(width);
			}
		};
		self.show = function() {
			element.style.display = 'inline-block';
			recalculateWidths();
		};
		self.setWidth = function(width) {
			element.style.width = width;
		};
		(function(instance) {
			var typeChangeListeners = [];
			instance.addTypeChangeListener = function(listener) {
				console.log('typechl added');
				typeChangeListeners.push(listener);
			};
			instance.removeTypeChangeListener = function(listener) {
				var index2remove = typeChangeListeners.indexOf(listener);
				if (index2remove < 0) {return;}
				typeChangeListeners.splice(index2remove, 1);
				console.log('typechl removed');
			};
			instance.notifyTypeChangeListeners = function() {
				for (var i=0, len=typeChangeListeners.length; i<len; i++) {
					var listener = typeChangeListeners[i];
					listener.update(instance);
				}
			};
		 })(self);
		var viewBuilder = (function() {
			var toShowMarkup = false;
			var typeChangeListenerAdded = false;
			var builtLayout = null;
			var option = null;
			var removeBuiltLayout = function() {
				mainScreen.showTextarea();
				option.innerHTML = 'build view';
				builtLayout = null;
				self.removeTypeChangeListener(typeChangeListener);
				typeChangeListenerAdded = false;
			};
			var typeChangeListener = {
				update: function() {
					toShowMarkup = false;
					typeChangeListenerAdded = false;
					removeBuiltLayout();
				}
			};
			var buildAndShowLayout = function() {
				builtLayout = document.createElement('div');
				builtLayout.style.width = '100%';
				builtLayout.style.height = '100%';
				builtLayout.style.backgroundColor = '#999999';
				builtLayout.style.boxSizing = 'border-box';
				builtLayout.style.display = 'inline-block';
				builtLayout.style.float = 'left';
				builtLayout.style.whiteSpace = 'normal';
				builtLayout.innerHTML = textarea.value;
				mainScreen.show(builtLayout);
				option.innerHTML = 'show markup';
				if ( ! typeChangeListenerAdded) {
					typeChangeListenerAdded = true;
					self.addTypeChangeListener(typeChangeListener);
				}
			};
			var listener = function() {
				option = actionSelector.querySelector('option[value=build-view]');
				if (toShowMarkup) {
					removeBuiltLayout();
				} else {
					buildAndShowLayout();
				}
				toShowMarkup = !toShowMarkup;
			};
			return {
				getListener: function() {return listener;},
				isBuilt: function() {return (builtLayout != null);},
				isShowing: function() {
					return this.isBuilt() && mainScreen.isShowing(builtLayout);
				},
				rebuild: function() {builtLayout.innerHTML = textarea.value;}
			};
		})();
		var layoutActionListeners = {};
		layoutActionListeners.listenerViewBuild = viewBuilder.getListener();
		layoutActionListeners.listenerElementAdd = function() {
			var element2insert = document.getElementById('action-add').cloneNode(true);
			element2insert.id = '';
			var button = element2insert.getElementsByClassName('ok-button')[0];
			button.addEventListener('click', function() {
				var what2addSelect = element2insert.getElementsByClassName('what2add-select')[0];
				var options = null;
				what2addSelect.addEventListener('change', function(eve) {
					var value = eve.target.value;
					if (value == 'select') {
					}
				}, false);
				var what2add = what2addSelect.value;
				if ( ! what2add) {
					alert("You haven't selected what to add");
					return;
				}

				var parentId = element2insert.getElementsByClassName('parentid-txt')[0].value;
				var id = element2insert.getElementsByClassName('id-txt')[0].value;
				if ( ! parentId || (! id && what2add != 'span')) {
					alert("You haven't entered parentId or id value");
					return;
				}
				var text = element2insert.getElementsByClassName('text-txt')[0].value;
				var startLine = element2insert.getElementsByClassName('add-element-start-line')[0].checked;
				var element = null;
				var label = null;
				if (what2add == 'button') {
					element = document.createElement('button');
					element.innerHTML = text || id;
				} else if (what2add == 'textfield') {
					element = document.createElement('input');
					element.type = 'text';
				} else if (what2add == 'checkbox') {
					element = document.createElement('input');
					element.type = 'checkbox';
					label = document.createElement('label');
					label.htmlFor = id;
					label.innerHTML = text;
				} else if (what2add == 'span') {
					element = document.createElement('span');
					element.innerHTML = text;
				}
				element.id = id;
				var tempParent = document.createElement('div');
				tempParent.innerHTML = textarea.value;
				var parent = tempParent.querySelector('#'+parentId);
				if ( ! parent) {
					alert("There's no element with id '" + parentId + "' in markup");
					return;
				}
				if (startLine) {
					parent.appendChild(document.createElement('br'));
				}
				parent.appendChild(element);
				if (label) {parent.appendChild(label);}
				var value2insert = tempParent.innerHTML;
				value2insert = value2insert.replace(/></g, '>\n<');
				textarea.value = value2insert;
				if (viewBuilder.isBuilt()) {
					viewBuilder.rebuild();
				}
				self.saveTextareaValue();
				mainScreen.destroy();
				resetActionSelectorOptions();
			}, false);
			var cancelButton = element2insert.getElementsByClassName('cancel-button')[0];
			cancelButton.addEventListener('click', function() {
				mainScreen.destroy();
			}, false);
			element2insert.style.display = 'inline-block';
			element2insert.style.float = 'left';
			element2insert.style.whiteSpace = 'normal';
			mainScreen.show(element2insert);
		};
		layoutActionListeners.listenerLayoutAdd = function() {
			typeSelector.disabled = true;
			projectsSelector.disabled = true;
			actionSelector.disabled = true;
			var element2insert = document.getElementById('action-add-layout').cloneNode(true);
			element2insert.id = '';
			var textfield = element2insert.querySelector('input[type=text]');
			var button = element2insert.getElementsByClassName('add-layout-button')[0];
			button.addEventListener('click', function() {
				var layoutName = textfield.value;
				if (layoutName.length == 0) {
					alert('cannot add layout: layout name is empty');
					return;
				}
				if (self.project.hasLayout(layoutName)) {
					alert('cannot add layout: layout with entered name already exists.');
					return;
				}
				if ( ! /[a-zA-Z0-9]+/.test(layoutName)) {
					alert('cannot add layout: layout name contains non-alphanumeric characters');
					return;
				}
				self.project.addLayout(layoutName, Project.defaultValues.layout); 
				typeSelector.disabled = false;
				projectsSelector.disabled = false;
				actionSelector.disabled = false;
				mainScreen.showTextarea();
				selectLayout(layoutName, true);
			}, false);

			var cancelButton = element2insert.getElementsByClassName('cancel-button')[0];
			cancelButton.addEventListener('click', function() {
				typeSelector.disabled = false;
				projectsSelector.disabled = false;
				actionSelector.disabled = false;
				mainScreen.destroy();
			}, false);
			element2insert.style.display = 'inline-block';
			element2insert.style.float = 'left';
			element2insert.style.whiteSpace = 'normal';
			mainScreen.show(element2insert);
		};
		layoutActionListeners.listenerLayoutRemove = function() {
			if (self.project.getLayoutFilesCount() == 1) {
				alert('cannot remove last layout file');
				return;
			}
			typeSelector.disabled = true;
			projectsSelector.disabled = true;
			actionSelector.disabled = true;
			var element2insert = document.getElementById('action-remove-layout').cloneNode(true);
			element2insert.id = '';
			var layoutSelect = element2insert.querySelector('select');
			var layoutNames = self.project.getLayoutNames();
			for (var i=0, len=layoutNames.length; i<len; i++) {
				var opt = document.createElement('option');
				opt.value = i;
				opt.innerHTML = layoutNames[i];
				layoutSelect.appendChild(opt);
			}
			var button = element2insert.getElementsByClassName('remove-layout-button')[0];
			button.addEventListener('click', function() {
				var layoutSelectValue = layoutSelect.value;
				var layoutName = layoutSelect.querySelector('option[value="' + layoutSelectValue + '"]').innerHTML;
				if (layoutName.length == 0) {
					alert('cannot remove layout: layout name is empty');
					return;
				}
				self.project.removeLayout(layoutName);
				typeSelector.disabled = false;
				projectsSelector.disabled = false;
				actionSelector.disabled = false;
				mainScreen.destroy();
				//element2insert.parentNode.removeChild(element2insert);
				//textarea.style.display = 'inline';
			}, false);

			var cancelButton = element2insert.getElementsByClassName('cancel-button')[0];
			cancelButton.addEventListener('click', function() {
				typeSelector.disabled = false;
				projectsSelector.disabled = false;
				actionSelector.disabled = false;
				mainScreen.destroy();
				//element2insert.parentNode.removeChild(element2insert);
				//textarea.style.display = 'inline';
			}, false);
			element2insert.style.display = 'inline-block';
			element2insert.style.float = 'left';
			element2insert.style.whiteSpace = 'normal';
			mainScreen.show(element2insert);
			//textarea.parentNode.appendChild(element2insert);
			//textarea.style.display = 'none';
		};

		actionSelector.addEventListener('change', function(eve) {
			var value = eve.target.value;
			if (value == 'save') {
				self.saveBufferValue();
				resetStars();
				resetActionSelectorOptions();
			} else if (value == 'build-view') {
				layoutActionListeners.listenerViewBuild.apply(null);
			} else if (value == 'add-element') {
				layoutActionListeners.listenerElementAdd.apply(null);
			} else if (value == 'add-layout') {
				layoutActionListeners.listenerLayoutAdd.apply(null);
			} else if (value == 'remove-layout') {
				layoutActionListeners.listenerLayoutRemove.apply(null);
			} 
			selectOption(actionSelector, 'action');
		}, false);
		var selectLayout = function(layoutName, toSetType) {
			var value2search = initFunction.Type.LAYOUT.value + ' ' + layoutName;
			var option = selectOption(typeSelector, value2search);
			if (toSetType) {
				typeSelectorValueChangeHandler.call(null, option.value);
			}
		};

		var selectOption = function(selectorElement, optionText2Search) {
			var options = selectorElement.querySelectorAll('option');
			for (var i=0, len=options.length; i<len; i++) {
				var option = options[i];
				if (option.text != optionText2Search && option.text != (optionText2Search + '*')) {continue;}
				option.selected = true;
				return option;
			}
		}
		
		var resetActionSelectorOptions = function() {
			var appendOption = function(value, html) {
				var option = createOption(value, html);
				actionSelector.appendChild(option);
			};
			var createOption = function(value, html) {
				var option = document.createElement('option');
				option.value = value;
				option.innerHTML = html;
				return option;
			};
			actionSelector.innerHTML = '';
			var firstOption = createOption('', 'action');
			firstOption.selected = true;
			firstOption.disabled = true;
			actionSelector.appendChild(firstOption);
			if (self.type == initFunction.Type.MANIFEST) {
				if (self.buffer.manifest) {
					appendOption('save', 'save');
				}
			} else if (self.type == initFunction.Type.LAYOUT) {
				if (self.buffer.layout[self.currentLayoutName]) {
					appendOption('save', 'save');
				}
				appendOption('build-view', viewBuilder.isShowing() ? 'show markup' : 'build view');
				appendOption('add-element', 'add...');
				appendOption('add-layout', 'add layout...');
				appendOption('remove-layout', 'remove layout...');
			} else if (self.type == initFunction.Type.CODE) {
				if (self.buffer.code) {
					appendOption('save', 'save');
				}
			}

			if (actionSelector.childElementCount > 1) {
				actionSelector.style.display = 'inline';
			} else {
				actionSelector.style.display = 'none';
			}
		};

		self.setType = function(value) {
			if (self.type == initFunction.Type.LAYOUT) {
				self.currentLayoutName = null;
			}
			var editorType = initFunction.Type[value.toUpperCase()];
			var layoutName = null;
			if (!editorType) {
				var testValue = typeSelector.querySelector('option[value="'+value+'"]').innerHTML;
				var layoutConstantValue = initFunction.Type.LAYOUT.value;
				//if (testValue.startsWith(initFunction.Type.LAYOUT.value)) {
				if (testValue.substring(0, layoutConstantValue.length) == layoutConstantValue) {
					editorType = initFunction.Type.LAYOUT;
					var substringIndex = initFunction.Type.LAYOUT.value.length;
					substringIndex++; //whitespace
					layoutName = testValue.substr(substringIndex);
					if (layoutName.charAt(layoutName.length-1) == '*') {
						layoutName = layoutName.substr(0, layoutName.length-1);
					}
				}
			}

			var oldType = self.type;
			self.type = editorType;

			if (editorType == initFunction.Type.MANIFEST) {
				textarea.value = self.buffer.manifest || self.project.getManifest();
			} else if (editorType == initFunction.Type.LAYOUT) {
				self.currentLayoutName = layoutName;
				textarea.value = self.buffer.layout[layoutName] || self.project.getLayout(layoutName);
			} else if (editorType == initFunction.Type.CODE) {
				textarea.value = self.buffer.code || self.project.getCode();
			}
			mainScreen.showTextarea();
			self.notifyTypeChangeListeners();
			resetActionSelectorOptions();
		};
		self.clearBuffer = function() {
			self.buffer = {layout:{}};
		};
		self.setProject = function(projectName) {
			if (self.project) {
				if ( ! bufferIsEmpty()) {
					if ( ! confirm("This editor has unsaved data in buffer. \nChange it's project and lose all unsaved data?")) {
						selectOption(projectsSelector, self.project.name);
						return;
					}
				}
				self.project.removeManifestListener(manifestListener);
				self.project.removeCodeListener(codeListener);
				self.project.removeLayoutListener(layoutListener);
				self.project.removeLayoutListListener(layoutListListener);
				self.clearBuffer();
			}
			self.project = projects.getProject(projectName);
			self.project.addManifestListener(manifestListener);
			self.project.addCodeListener(codeListener);
			self.project.addLayoutListener(layoutListener);
			self.project.addLayoutListListener(layoutListListener);
			layoutListListener.update(self.project);
			self.setType(initFunction.Type.MANIFEST.value);
			selectOption(typeSelector, initFunction.Type.MANIFEST.value);
			mainScreen.showTextarea();

			textarea.disabled = false;
			typeSelector.disabled = false;
			actionSelector.disabled = false;
			var firstOption = projectsSelector.querySelector('[disabled]');
			if (firstOption) {
				projectsSelector.removeChild(firstOption);
			}
		};
	};
	var EditorType = function EditorType(value) {this.value=value;};
	initFunction.Type = {
		MANIFEST: new EditorType('manifest'),
		LAYOUT: new EditorType('layout'),
		CODE: new EditorType('code')
	};
	return initFunction;
})();
