var Manifest = (function() {
	var initFunction = function ManifestInitFunction(config) {
		this.config = config;
	};
	initFunction.isManifestAttr = function(attr) {return com_android_internal_R_styleable.MANIFEST_ATTR_NAMES.indexOf(attr.name) >= 0;};
	return initFunction;
})();
