var ApplicationPackageManager = (function() {
	var initFunction = function ApplicationPackageManagerInitFunction(projects) {
		var self = this;
		var mPM = new PackageManagerService();
		self.resolveActivity = function(intent, flags) {
			return mPM.resolveIntent(
				intent, 
				intent.resolveTypeIfNeeded(mContext.getContentResolver()),
				flags);
		}
		self.getActivityInfo = function(componentName, flags) {
			var ai = mPM.getActivityInfo(componentName, flags);
			if (ai == null) {
				throw new Error('name not found: ' + componentName.toString());
			}
			return ai;
		};
		self._js_run = function(projects, configParser) {
			mPM._js_run(projects, configParser);
		};
	};
	return initFunction;
})();
