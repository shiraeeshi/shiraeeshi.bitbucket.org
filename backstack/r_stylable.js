var com_android_internal_R_styleable = (function() {
	var instance = {};

	var prefix = function(attrNames, objectName, prefixValue) {
		if (prefixValue == undefined) {
			prefixValue = 'android:';
		}
		var obj = instance[objectName] || {};
		for (var i=0, len=attrNames.length; i<len; i++) {
			var attrName = attrNames[i];
			var attrNamePrefixed = prefixValue + attrName;
			attrNames[i] = attrNamePrefixed;
			obj[attrName] = attrNamePrefixed;
		}
		instance[objectName] = obj;
	};
	var manifestAttrNames = ["sharedUserId", "sharedUserLabel", "versionCode", "versionName", "installLocation"];
	prefix(manifestAttrNames, 'AndroidManifest');
	var manifestAttrNamesWithoutPrefix = ["package"];
	prefix(manifestAttrNamesWithoutPrefix, 'AndroidManifest', '');
	manifestAttrNames = manifestAttrNames.concat(manifestAttrNamesWithoutPrefix);
	instance.MANIFEST_ATTR_NAMES = manifestAttrNames;

	var applicationAttrNames = ["allowTaskReparenting", "allowBackup", "backupAgent", "banner", "debuggable", "description", "enabled", "hasCode", "hardwareAccelerated", "icon", "isGame", "killAfterRestore", "largeHeap", "label", "logo", "manageSpaceActivity", "name", "permission", "persistent", "process", "restoreAnyVersion", "requiredAccountType", "restrictedAccountType", "supportsRtl", "taskAffinity", "testOnly", "theme", "uiOptions", "vmSafeMode"];
	prefix(applicationAttrNames, 'AndroidManifestApplication');
	instance.APPLICATION_ATTR_NAMES = applicationAttrNames;
	var activityAttrNames = [ 'allowEmbedded', 'allowTaskReparenting', 'alwaysRetainTaskState', 'autoRemoveFromRecents', 'banner', 'clearTaskOnLaunch', 'configChanges', 'description', 'documentLaunchMode', 'enabled', 'excludeFromRecents', 'exported', 'finishOnCloseSystemDialogs', 'finishOnTaskLaunch', 'hardwareAccelerated', 'icon', 'immersive', 'label', 'launchMode', 'logo', 'maxRecents', 'multiprocess', 'name', 'noHistory', 'parentActivityName', 'permission', 'persistableMode', 'process', 'relinquishTaskIdentity', 'resumeWhilePausing', 'screenOrientation', 'showOnLockScreen', 'singleUser', 'stateNotNeeded', 'taskAffinity', 'theme', 'uiOptions', 'windowSoftInputMode'
	];
	prefix(activityAttrNames, 'AndroidManifestActivity');
	instance.ACTIVITY_ATTR_NAMES = activityAttrNames;

	var intentFilterAttrNames = [ 'banner', 'icon', 'label', 'logo', 'priority' ];
	prefix(intentFilterAttrNames, 'AndroidManifestIntentFilter');
	instance.INTENT_FILTER_ATTR_NAMES = intentFilterAttrNames;

	var dataAttrNames = [ 'host', 'mimeType', 'path', 'pathPattern', 'pathPrefix', 'port', 'scheme', 'ssp', 'sspPattern', 'sspPrefix' ];
	prefix(dataAttrNames, 'AndroidManifestData');
	instance.DATA_ATTR_NAMES = dataAttrNames;
	return instance;
})();
