var ComponentInfo = (function() {
	var initFunction = function ComponentInfoInitFunction(orig) {
		this.applicationInfo = null; // ApplicationInfo
		this.processName = null; // String
		this.descriptionRes = 0; // int
		this.enabled = true; // boolean
		this.exported = false; // boolean
		if (orig) {
			this.applicationInfo = orig.applicationInfo;
			this.processName = orig.processName;
			this.descriptionRes = orig.descriptionRes;
			this.enabled = orig.enabled;
			this.exported = orig.exported;
		}
		var supper = new PackageItemInfo(orig);
		this.prototype = supper;

		this.loadLabel = function(pm) {
			if (this.nonLocalizedLabel != null) {
			    return this.nonLocalizedLabel;
			}
			var ai = this.applicationInfo;
			var label;
			if (this.labelRes != 0) {
			    label = pm.getText(this.packageName, this.labelRes, ai);
			    if (label != null) {
				return label;
			    }
			}
			if (ai.nonLocalizedLabel != null) {
			    return ai.nonLocalizedLabel;
			}
			if (ai.labelRes != 0) {
			    label = pm.getText(this.packageName, ai.labelRes, ai);
			    if (label != null) {
				return label;
			    }
			}
			return this.name;
		};
		this.isEnabled = function() {
			return this.enabled && this.applicationInfo.enabled;
		};

		this.getIconResource = function() {
			return this.icon != 0 ? this.icon : this.applicationInfo.icon;
		};

		this.getLogoResource = function() {
			return this.logo != 0 ? this.logo : this.applicationInfo.logo;
		};

		this.getBannerResource = function() {
			return this.banner != 0 ? this.banner : this.applicationInfo.banner;
		};
		this.loadDefaultIcon = function(pm) {
			return this.applicationInfo.loadIcon(pm);
		};

		this.loadDefaultBanner = function(pm) {
			return this.applicationInfo.loadBanner(pm);
		};

		this.loadDefaultLogo = function(pm) {
			return this.applicationInfo.loadLogo(pm);
		};

		this.getApplicationInfo = function() {
			return this.applicationInfo;
		};
	};
	return initFunction;
})();
