
var Project = (function() {
	var initFunction = function ProjectInitFunction(projectName) {
		var manifest = initFunction.defaultValues.manifest;
		var layout = {main: initFunction.defaultValues.layout};
		var code = initFunction.defaultValues.code;

		var instance = {
			name: projectName,
			getManifest: function() {return manifest;},
			setManifest: function(value) {
				manifest = value;
				this.notifyManifestListeners();
			},
			getCode: function() {return code;},
			setCode: function(value) {
				code = value;
				this.notifyCodeListeners();
			},
			hasLayout: function(name) {return layout.hasOwnProperty(name);},
			getLayoutFilesCount: function() {return Object.keys(layout).length;},
			getLayoutNames: function() {return Object.keys(layout);},
			getLayout: function(layoutName) {return layout[layoutName];},
			setLayout: function(layoutName, value) {
				layout[layoutName] = value;
				this.notifyLayoutListeners(layoutName);
			},
			addLayout: function(layoutName, value) {
				layout[layoutName] = value;
				this.notifyLayoutListListeners();
			},
			removeLayout: function(layoutName) {
				delete layout[layoutName];
				this.notifyLayoutListListeners();
				this.notifyLayoutListeners(layoutName);
			},
			copyLayouts: function() {
				var result = {};
				for (var i in layout) {
					if ( ! layout.hasOwnProperty(i)) {continue;}
					result[i] = layout[i];
				}
				return result;
			}
		};
		(function() {
			var layoutListeners = [];
			instance.addLayoutListener = function(listener) {
				layoutListeners.push(listener);
			};
			instance.removeLayoutListener = function(layoutName, listener) {
				var index2remove = layoutListeners.indexOf(listener);
				if (index2remove<0) {return;}
				layoutListeners.splice(index2remove, 1);
			};
			instance.notifyLayoutListeners = function(layoutName) {
				for (var i=0, len=layoutListeners.length; i<len; i++) {
					var listener = layoutListeners[i];
					listener.update(instance, layoutName);
				}
			};
		 })();
		(function() {
			var codeListeners = [];
			instance.addCodeListener = function(listener) {
				codeListeners.push(listener);
			};
			instance.removeCodeListener = function(listener) {
				var index2remove = codeListeners.indexOf(listener);
				if (index2remove<0) {return;}
				codeListeners.splice(index2remove, 1);
			};
			instance.notifyCodeListeners = function() {
				for (var i=0, len=codeListeners.length; i<len; i++) {
					var listener = codeListeners[i];
					listener.update(instance);
				}
			};
		 })();
		(function() {
			var manifestListeners = [];
			instance.addManifestListener = function(listener) {
				manifestListeners.push(listener);
			};
			instance.removeManifestListener = function(listener) {
				var index2remove = manifestListeners.indexOf(listener);
				if (index2remove<0) {return;}
				manifestListeners.splice(index2remove, 1);
			};
			instance.notifyManifestListeners = function() {
				for (var i=0, len=manifestListeners.length; i<len; i++) {
					var listener = manifestListeners[i];
					listener.update(instance);
				}
			};
		 })();
		(function() {
			var layoutListListeners = [];
			instance.addLayoutListListener = function(listener) {
				layoutListListeners.push(listener);
			};
			instance.removeLayoutListListener = function(listener) {
				var index2remove = layoutListListeners.indexOf(listener);
				if (index2remove<0) {return;}
				layoutListListeners.splice(index2remove, 1);
			};
			instance.notifyLayoutListListeners = function() {
				for (var i=0, len=layoutListListeners.length; i<len; i++) {
					var listener = layoutListListeners[i];
					listener.update(instance);
				}
			};
		 })();
		return instance;
	};
	var defaultValues = {
		manifest: '<manifest xmlns:android="http://schemas.android.com/apk/res/android" package="com.sample" android:versionCode="1" android:versionName="1.0">\n    <application android:allowBackup="true" android:icon="@mipmap/launcher" android:label="@string/app_name" android:theme="@style/AppTheme">\n        <activity android:taskAffinity="sdf.sdf" android:launchMode="singleTop" android:allowTaskReparenting="false" android:clearTaskOnLaunch="true" android:name="com.example.app.ChildActivity" android:label="@string/title_child_activity" android:parentActivityName="com.example.myfirstapp.MainActivity" >\n            <intent-filter>\n                <action android:name="android.intent.action.SEND"/>\n                <category android:name="android.intent.category.DEFAULT"/>\n                <data android:mimeType="text/plain"/>\n            </intent-filter>\n        </activity>\n    </application>\n</manifest>',
		layout: '<div id="main" style="width:100%;height:100%;padding:20px;box-sizing: border-box;">\n    <div style="width: 100%; padding: 5px; box-sizing: border-box;">\n        <button id="button">button</button>\n    </div>\n</div>',
		code: '{\n    onCreate: function(savedInstanceState) {\n        super.onCreate(savedInstanceState);\n        setContentView(R.layout.main);\n        var button = findViewById("button");\n        button.addEventListener("click", \n            function() {\n                var intent = new Intent(this, "someActivityId");\n                startActivity(intent);\n            }, false);\n    }\n}'
	};
	initFunction.defaultValues = defaultValues;
	return initFunction;
})();
