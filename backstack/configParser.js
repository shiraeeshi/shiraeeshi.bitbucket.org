
var configParser = (function() {
	var parseXml;

	if (typeof window.DOMParser != "undefined") {
	    parseXml = function(xmlStr) {
		return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
	    };
	} else if (typeof window.ActiveXObject != "undefined" &&
	       new window.ActiveXObject("Microsoft.XMLDOM")) {
	    parseXml = function(xmlStr) {
		var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = "false";
		xmlDoc.loadXML(xmlStr);
		return xmlDoc;
	    };
	} else {
	    throw new Error("No XML parser found");
	}

	var readIntentChild = function(intentChild, intentFilter) {
		var intentChildNodeName = intentChild.nodeName;
		if (intentChildNodeName == 'action') {
			var actionName = intentChild.getAttribute('android:name');
			intentFilter.actions.push(actionName);
		} else if (intentChildNodeName == 'category') {
			var categoryName = intentChild.getAttribute('android:name');
			intentFilter.categories.push(categoryName);
		} else if (intentChildNodeName == 'data') {
			var data = {};
			var attrs = intentChild.attributes;
			for (var i=0, len=attrs.length; i<len; i++) {
				var attr = attrs[i];
				data[attr.name] = attr.value;
			}
			//intentFilter.datas.push('data');
			intentFilter.datas.push(data);
		}
	};

	var readActivityChild = function(activityChild, intentFilters) {
		if (activityChild.nodeName == '#text') {return;}
		if (activityChild.nodeName != 'intent-filter') {
			throw new Error('activity tag can contain "intent-filter" or "meta-data". (in simulator "intent-filter" only) wrong tag: ' + activityChild.nodeName);
		}
		var intentFilterNode = activityChild;
		var intentFilter = {
			actions: [],
			categories: [],
			datas: []
		};
		var intentFilterNodeChildren = intentFilterNode.childNodes;
		for (var intentChildIndex=0, intentChildrenLength=intentFilterNodeChildren.length; intentChildIndex < intentChildrenLength; intentChildIndex++) {
			var intentChild = intentFilterNodeChildren[intentChildIndex];
			readIntentChild(intentChild, intentFilter);
		}
		intentFilters.push(intentFilter);
	};

	var parseManifest = function(manifestNode) {
		if (manifestNode.nodeName != 'manifest') {console.log('root node is not manifest. nodeName: ' + manifestNode.nodeName);}
		var manifestConfig = {};
		var manifestAttrs = {};
		for (var attrIndex=0, attrsLen=manifestNode.attributes.length; attrIndex < attrsLen; attrIndex++) {
			var attr = manifestNode.attributes[attrIndex];
			if (Manifest.isManifestAttr(attr)) {
				manifestAttrs[attr.name] = attr.value;
			}
		}
		manifestConfig.attrs = manifestAttrs;
		var apps = [];
		manifestConfig.apps = apps;
		var manifest = new Manifest(manifestConfig);
		if (manifestNode.childElementCount == 0) {return app;}
		var manifestNodeChildren = manifestNode.childNodes;
		for (var childIndex=0, childrenLength=manifestNodeChildren.length; childIndex < childrenLength; childIndex++) {
			var manifestChild = manifestNodeChildren[childIndex];
			if (manifestChild.nodeName != 'application') {continue;}
			var app = readManifestChild(manifestChild);
			manifest.config.apps.push(app);
			app.manifest = manifest;
		}
		return manifest;
	};

	var readManifestChild = function(manifestChild) {
		var applicationNode = manifestChild;
		var applicationConfig = {};
		var applicationAttrs = {};
		for (var attrIndex=0, attrsLen=applicationNode.attributes.length; attrIndex < attrsLen; attrIndex++) {
			var attr = applicationNode.attributes[attrIndex];
			if (Application.isApplicationAttr(attr)) {
				applicationAttrs[attr.name] = attr.value;
			}
		}
		applicationConfig.attrs = applicationAttrs;
		var activities = [];
		applicationConfig.activities = activities;
		var app = new Application(applicationConfig);
		if (applicationNode.childElementCount == 0) {return app;}
		var appNodeChildren = applicationNode.childNodes;
		for (var childIndex=0, childrenLength=appNodeChildren.length; childIndex < childrenLength; childIndex++) {
			var appChild = appNodeChildren[childIndex];
			if (appChild.nodeName != 'activity') {continue;}
			readAppChild(appChild, app);
		}
		return app;
	};

	var readAppChild = function(appChild, app) {
		var activityNode = appChild;
		var activityConfig = {};
		var activityAttrs = {};
		for (var attrIndex=0, attrsLen=activityNode.attributes.length; attrIndex < attrsLen; attrIndex++) {
			var attr = activityNode.attributes[attrIndex];
			if (Activity.isActivityAttr(attr)) {
				activityAttrs[attr.name] = attr.value;
			}
		}
		var intentFilters = [];
		activityConfig.attrs = activityAttrs;
		activityConfig.intentFilters = intentFilters;
		var activity = new Activity(activityConfig);
		app.config.activities.push(activity);
		activity.app = app;
		if (activityNode.childElementCount == 0) {return;}
		var activityNodeChildren = activityNode.childNodes;
		for (var childIndex=0, childrenLength=activityNodeChildren.length; childIndex < childrenLength; childIndex++) {
			var activityChild = activityNodeChildren[childIndex];
			readActivityChild(activityChild, intentFilters);
		}
	};
	return {
		parseConfig: function(input) {
			try {
				var xml = parseXml(input);
				var manifestNode = xml.childNodes[0];
				return parseManifest(manifestNode);
			} catch (e) {
				console.log(e);
			}
		}
	};
})();
