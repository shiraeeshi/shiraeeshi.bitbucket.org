var Configuration = (function() {
	var initFunction = function ConfigurationInitFunction() {};
	initFunction.NATIVE_CONFIG_MCC = 0x0001;
	initFunction.NATIVE_CONFIG_MNC = 0x0002;
	initFunction.NATIVE_CONFIG_LOCALE = 0x0004;
	initFunction.NATIVE_CONFIG_TOUCHSCREEN = 0x0008;
	initFunction.NATIVE_CONFIG_KEYBOARD = 0x0010;
	initFunction.NATIVE_CONFIG_KEYBOARD_HIDDEN = 0x0020;
	initFunction.NATIVE_CONFIG_NAVIGATION = 0x0040;
	initFunction.NATIVE_CONFIG_ORIENTATION = 0x0080;
	initFunction.NATIVE_CONFIG_DENSITY = 0x0100;
	initFunction.NATIVE_CONFIG_SCREEN_SIZE = 0x0200;
	initFunction.NATIVE_CONFIG_VERSION = 0x0400;
	initFunction.NATIVE_CONFIG_SCREEN_LAYOUT = 0x0800;
	initFunction.NATIVE_CONFIG_UI_MODE = 0x1000;
	initFunction.NATIVE_CONFIG_SMALLEST_SCREEN_SIZE = 0x2000;
	initFunction.NATIVE_CONFIG_LAYOUTDIR = 0x4000;
	return initFunction;
})();
