var AbstractCollection = klass(Object, {
	isEmpty: function() {
		return this.size() == 0;
	},
    	contains: function(o) {
		var it = this.iterator();
		if (o == null) {
			while (true) {
				var n = it.next();
				if (it.isDone()) {
					break;
				}
				if (n == null) {
					return true;
				}
			}
		} else {
			while (true) {
				var n = it.next();
				if (it.isDone()) {
					break;
				}
				if (o.equals(n)) {
					return true;
				}
			}
		}
		return false;
	},
	toArray: function(a) {
		if (!a) {
			var size = this.size()
			var arr = a.length >= size ? a : new Array(size);
			var it = this.iterator();
			for (var i=0; i<r.length; i++) {
				var n = it.next();
				if (it.isDone()) {
					return arr.slice(0, i);
				}
				arr[i] = n;
			}
			return AbstractCollection.finishToArray(arr, it);
		} else {
			var size = this.size()
			var arr = a.length >= size ? a : new Array(size);
			var it = this.iterator();
			for (var i=0; i<r.length; i++) {
				var n = it.next();
				if (it.isDone()) {
					if (a == r) {
						r[i] = null;
					} else if (a.length < i) {
						return r.slice(0, i+1);
					} else {
						Array.prototype.splice.apply(a, [0,0].concat(r.slice(0,i)));
						if (a.length > i) {
							a[i] = null;
						}
					}
					return a;
				}
				arr[i] = n;
			}
			// more elements than expected
			return AbstractCollection.finishToArray(arr, it);
		}
	},
	remove: function(o) {
		var it = this.iterator();
		if (o == null) {
			while (true) {
				var n = it.next();
				if (it.isDone()) {break;}
				if (n == null) {
					it.remove();
					return true;
				}
			}
		} else {
			while (true) {
				var n = it.next();
				if (it.isDone()) {break;}
				if (o.equals(n)) {
					it.remove();
					return true;
				}
			}
		}
		return false;
	},
	containsAll: function(c) {
		var cit = c.iterator();
		while (true) {
			var n = cit.next();
			if (cit.isDone()) {break;}
			if ( ! this.contains(n) ) {
				return false;
			}
		}
		return true;
	},
	addAll: function(c) {
		var modified = false;
		var cit = c.iterator();
		while (true) {
			var n = cit.next();
			if (cit.isDone()) {break;}
			if (this.add(n)) {
				modified = true;
			}
		}
		return modified;
	},
	removeAll: function(c) {
		var modified = false;
		var it = this.iterator();
		while (true) {
			var n = it.next();
			if (it.isDone()) {break;}
			if (c.contains(n)) {
				it.remove();
				modified = true;
			}
		}
		return modified;
	},
	retainAll: function(c) {
		var modified = false;
		var it = this.iterator();
		while (true) {
			var n = it.next();
			if (it.isDone()) {break;}
			if ( ! c.contains(n)) {
				it.remove();
				modified = true;
			}
		}
		return modified;
	},
	clear: function() {
		var it = this.iterator();
		while (true) {
			it.next();
			if (it.isDone()) {break;}
			it.remove();
		}
	},
	toString: function() {
		var it = this.iterator();
		var sb = '[';
		while (true) {
			var n = it.next();
			if (it.isDone()) {
				sb += ']';
				return sb;
			}
			if (n == this) {
				sb += '(this Collection)';
			} else {
				if (n['toString']) {
					sb += n.toString();
				} else {
					sb += n;
				}
			}
			sb += ', ';
		}
	}
});
AbstractCollection.finishToArray = function(arr, it) {
	var i = arr.length;
	while (true) {
		var n = it.next();
		if (it.isDone()) {
			break;
		}
		arr[i++] = n;
	}
	return arr;
};
