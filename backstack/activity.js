
var Activity = (function() {
	var initFunction = function ActivityInitFunction(config) {
		this.config = config;
	};
	var isLauncherFilter = function(filter) {
		return (filter.actions.indexOf(Intent.ACTION_MAIN) >= 0) &&
			(filter.categories.indexOf(Intent.CATEGORY_LAUNCHER) >= 0);
	};
	initFunction.prototype.isLauncher = function() {
		var filters = this.config.intentFilters;
		for (var i=0, len=filters.length; i<len; i++) {
			if (isLauncherFilter(filters[i])) {return true;}
		}
		return false;
	};
	initFunction.isActivityAttr = function(attr) {

		var result = (com_android_internal_R_styleable.ACTIVITY_ATTR_NAMES.indexOf(attr.name) >= 0);
		return result;
	};
	return initFunction;
})();

