var AbstractList = (function() {
	var initFunction = klass(Object, {
		add: function(e) {
			this.add(this.size(), e);
			return true;
		},
	    	indexOf: function(o) {
			var it = this.listIterator();
			if (o == null) {
				while (it.hasNext()) {
					if (it.next() == null) {
						return it.previousIndex();
					}
				}
			} else {
				while (it.hasNext()) {
					if (o.equals(it.next())) {
						return it.previousIndex();
					}
				}
			}
			return -1;
		},
	    	lastIndexOf: function(o) {
			var it = this.listIterator(this.size());
			if (o == null) {
				while (it.hasPrevious()) {
					if (it.previous() == null) {
						return it.nextIndex();
					}
				}
			} else {
				while (it.hasPrevious()) {
					if (o.equals(it.previous())) {
						return it.nextIndex();
					}
				}
			}
			return -1;
		},
		clear: function() {
			this.removeRange(0, this.size());
		},
		addAll: function(index, c) {
			this.rangeCheckForAdd(index);
			var modified = false;
			var cit = c.iterator();
			while (cit.hasNext()) {
				this.add(index++, cit.next());
				modified = true;
			}
			return modified;
		},
		iterator: function() {
			return new Itr(this);
		},
		listIterator: function(index) {
			index = index || 0;
			this.rangeCheckForAdd(index);
			return new ListItr(this, index);
		},
	    	subList: function(fromIndex, toIndex) {
			return new SubList(this, fromIndex, toIndex);
		},
	    	equals: function(o) {
			if (o == this) {return true;}
			if ( ! (o instanceof AbstractList)) {return false;}

			var e1 = this.listIterator();
			var e2 = o.listIterator();

			while (e1.hasNext() && e2.hasNext()) {
				var o1 = e1.next();
				var o2 = e2.next();

				if ( ! (o1==null ? o2==null : o1.equals(o2))) {
					return false;
				}
			}
			return !(e1.hasNext() || e2.hasNext());
		},
		hashCode: function() {
			return 1; // TODO: simplified when ported to js.
		},
		removeRange: function(fromIndex, toIndex) {
			var it = this.listIterator(fromIndex, toIndex);
			for (var i=0, n=toIndex-fromIndex; i<n; i++) {
				it.next();
				it.remove();
			}
		},
		rangeCheckForAdd: function(index) {
			if (index < 0 || index > this.size()) {
				throw new RangeError('index: ' + index + ', size: ' + this.size());
			}
		},
	});
	var Itr = klass(Object, {
		__construct: function(list) {
			this.cursor = 0;
			this.lastRet = -1;
			this.list = list;
		},
	    	hasNext: function() {
			return this.cursor != this.list.size();
		},
	    	next: function() {
			try {
				var i = this.cursor;
				var next = this.list.get(i);
				this.lastRet = i;
				this.cursor = i+1;
				return next;
			} catch (e) {
				throw new RangeError('no such element');
			}
		},
	    	remove: function() {
			if (this.lastRet < 0) {
				throw new Error('illegal state');
			}
			this.list.remove(this.lastRet);
			if (this.lastRet < this.cursor) {
				this.cursor--;
			}
			this.lastRet = -1;
		},
	});
	var ListItr = klass(Itr, {
		__construct: function(list, index) {
			if (ListItr.uber && ListItr.uber.hasOwnProperty('__construct')) {
				ListItr.uber.__construct.apply(this, [list]);
			}
			this.cursor = index;
		},
	    	hasPrevious: function() {
			return this.cursor != 0;
		},
	    	previous: function() {
			try {
				var i = this.cursor - 1;
				var previous = this.list.get(i);
				this.lastRet = this.cursor - 1;
				return previous;
			} catch (e) {
				throw new RangeError('no such element');
			}
		},
	    	nextIndex: function() {
			return this.cursor;
		},
	    	previousIndex: function() {
			return this.cursor - 1;
		},
		set: function(e) {
			if (this.lastRet < 0) {
				throw new Error('illegal state');
			}
			this.list.set(this.lastRet, e)
		},
		add: function(e) {
			var i = this.cursor;
			this.list.add(i, e);
			this.lastRet = -1;
			this.cursor = i + 1;
		}
	});
	var SubList = klass(AbstractList, {
		__construct: function(list, fromIndex, toIndex) {
			if (fromIndex < 0) {throw new RangeError('fromIndex=' + fromIndex);}
			if (toIndex > list.size()) {throw new RangeError('toIndex=' + toIndex);}
			if (fromIndex > toIndex) {throw new Error('fromIndex('+fromIndex+') > toIndex('+toIndex+')');}

			this.l = list;
			this.offset = fromIndex;
			this.size = toIndex - fromIndex;
		},
		set: function(index, element) {
			this.rangeCheck(index);
			return l.set(index+offset, element);
		},
		get: function(index) {
			this.rangeCheck(index);
			return l.get(index+offset);
		},
		size: function() {
			return size;
		},
		add: function(index, element) {
			this.rangeCheckForAdd(index);
			l.add(index+offset, element);
			this.size++;
		},
		remove: function(index) {
			this.rangeCheck(index);
			var result = l.remove(index+offset);
			this.size--;
			return result;
		},
		removeRange: function(fromIndex, toIndex) {
			l.removeRange(fromIndex+offset, toIndex+offset);
			this.size -= (toIndex-fromIndex);
		},
		addAll: function(a, b) {
			var index, c;
			if (arguments.length == 1) {
				index = this.size;
				c = a;
			} else {
				index = a;
				c = b;
			}
			this.rangeCheckForAdd(index);
			var csize = c.size();
			if (csize == 0) {
				return false;
			}
			l.addAll(this.offset+index, c);
			this.size += csize;
			return true;
		},
		iterator: function() {
			return this.listIterator();
		},
		listIterator: function(index) {
			this.rangeCheckForAdd(index);
			var self = this;
			return (function() {
				var i = self.l.listIterator(index + self.offset);
				return {
					hasNext: function() {
						this.nextIndex() - self.size;
					},
			       		next: function() {
						if (this.hasNext()) {
							return i.next();
						} else {
							throw new RangeError('no such element');
						}
					},
			       		hasPrevious: function() {
						return this.previousIndex() >= 0;
					},
			       		previous: function() {
						if (this.hasPrevious()) {
							return i.previous();
						} else {
							throw new RangeError('no such element');
						}
					},
					nextIndex: function() {
						return i.nextIndex() - self.offset;
					},
					previousIndex: function() {
						return i.previousIndex() - self.offset;
					},
					remove: function() {
						i.remove();
						self.size--;
					},
					set: function(e) {
						i.set(e);
					},
					add: function(e) {
						i.add(e);
						self.size++;
					}
				};
			})();
		},
		subList: function(fromIndex, toIndex) {
			return new SubList(this, fromIndex, toIndex);
		},
		rangeCheck: function(index) {
			if (index < 0 || index >= this.size) {
				throw new RangeError('index: ' + index + ', size: ' + this.size);
			}
		},
		rangeCheckForAdd: function(index) {
			if (index < 0 || index > this.size) {
				throw new RangeError('index: ' + index + ', size: ' + this.size);
			}
		},
	});
	return initFunction;
})();
