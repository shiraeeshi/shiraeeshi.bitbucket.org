var PackageManager = {};
PackageManager.GET_ACTIVITIES              = 0x00000001;
PackageManager.GET_RECEIVERS               = 0x00000002;
PackageManager.GET_SERVICES                = 0x00000004;
PackageManager.GET_PROVIDERS               = 0x00000008;
PackageManager.GET_INSTRUMENTATION         = 0x00000010;
PackageManager.GET_INTENT_FILTERS          = 0x00000020;
PackageManager.GET_SIGNATURES          = 0x00000040;
PackageManager.GET_RESOLVED_FILTER         = 0x00000040;
PackageManager.GET_META_DATA               = 0x00000080;
PackageManager.GET_GIDS                    = 0x00000100;
PackageManager.GET_DISABLED_COMPONENTS     = 0x00000200;
PackageManager.GET_SHARED_LIBRARY_FILES    = 0x00000400;
PackageManager.GET_URI_PERMISSION_PATTERNS  = 0x00000800;
PackageManager.GET_PERMISSIONS               = 0x00001000;
PackageManager.GET_UNINSTALLED_PACKAGES = 0x00002000;
PackageManager.GET_CONFIGURATIONS = 0x00004000;
PackageManager.GET_DISABLED_UNTIL_USED_COMPONENTS = 0x00008000;
PackageManager.MATCH_DEFAULT_ONLY   = 0x00010000;
PackageManager.SKIP_CURRENT_PROFILE = 0x00000002;
PackageManager.PERMISSION_GRANTED = 0;
PackageManager.PERMISSION_DENIED = -1;
PackageManager.SIGNATURE_MATCH = 0;
PackageManager.SIGNATURE_NEITHER_SIGNED = 1;
PackageManager.SIGNATURE_FIRST_NOT_SIGNED = -1;
PackageManager.SIGNATURE_SECOND_NOT_SIGNED = -2;
PackageManager.SIGNATURE_NO_MATCH = -3;
PackageManager.SIGNATURE_UNKNOWN_PACKAGE = -4;
PackageManager.COMPONENT_ENABLED_STATE_DEFAULT = 0;
PackageManager.COMPONENT_ENABLED_STATE_ENABLED = 1;
PackageManager.COMPONENT_ENABLED_STATE_DISABLED = 2;
PackageManager.COMPONENT_ENABLED_STATE_DISABLED_USER = 3;
PackageManager.COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED = 4;
PackageManager.INSTALL_FORWARD_LOCK = 0x00000001;
PackageManager.INSTALL_REPLACE_EXISTING = 0x00000002;
PackageManager.INSTALL_ALLOW_TEST = 0x00000004;
PackageManager.INSTALL_EXTERNAL = 0x00000008;
PackageManager.INSTALL_INTERNAL = 0x00000010;
PackageManager.INSTALL_FROM_ADB = 0x00000020;
PackageManager.INSTALL_ALL_USERS = 0x00000040;
PackageManager.INSTALL_ALLOW_DOWNGRADE = 0x00000080;
PackageManager.DONT_KILL_APP = 0x00000001;
PackageManager.INSTALL_SUCCEEDED = 1;
PackageManager.INSTALL_FAILED_ALREADY_EXISTS = -1;
PackageManager.INSTALL_FAILED_INVALID_APK = -2;
PackageManager.INSTALL_FAILED_INVALID_URI = -3;
PackageManager.INSTALL_FAILED_INSUFFICIENT_STORAGE = -4;
PackageManager.INSTALL_FAILED_DUPLICATE_PACKAGE = -5;
PackageManager.INSTALL_FAILED_NO_SHARED_USER = -6;
PackageManager.INSTALL_FAILED_UPDATE_INCOMPATIBLE = -7;
PackageManager.INSTALL_FAILED_SHARED_USER_INCOMPATIBLE = -8;
PackageManager.INSTALL_FAILED_MISSING_SHARED_LIBRARY = -9;
PackageManager.INSTALL_FAILED_REPLACE_COULDNT_DELETE = -10;
PackageManager.INSTALL_FAILED_DEXOPT = -11;
PackageManager.INSTALL_FAILED_OLDER_SDK = -12;
PackageManager.INSTALL_FAILED_CONFLICTING_PROVIDER = -13;
PackageManager.INSTALL_FAILED_NEWER_SDK = -14;
PackageManager.INSTALL_FAILED_TEST_ONLY = -15;
PackageManager.INSTALL_FAILED_CPU_ABI_INCOMPATIBLE = -16;
PackageManager.INSTALL_FAILED_MISSING_FEATURE = -17;
PackageManager.INSTALL_FAILED_CONTAINER_ERROR = -18;
PackageManager.INSTALL_FAILED_INVALID_INSTALL_LOCATION = -19;
PackageManager.INSTALL_FAILED_MEDIA_UNAVAILABLE = -20;
PackageManager.INSTALL_FAILED_VERIFICATION_TIMEOUT = -21;
PackageManager.INSTALL_FAILED_VERIFICATION_FAILURE = -22;
PackageManager.INSTALL_FAILED_PACKAGE_CHANGED = -23;
PackageManager.INSTALL_FAILED_UID_CHANGED = -24;
PackageManager.INSTALL_FAILED_VERSION_DOWNGRADE = -25;
PackageManager.INSTALL_PARSE_FAILED_NOT_APK = -100;
PackageManager.INSTALL_PARSE_FAILED_BAD_MANIFEST = -101;
PackageManager.INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION = -102;
PackageManager.INSTALL_PARSE_FAILED_NO_CERTIFICATES = -103;
PackageManager.INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES = -104;
PackageManager.INSTALL_PARSE_FAILED_CERTIFICATE_ENCODING = -105;
PackageManager.INSTALL_PARSE_FAILED_BAD_PACKAGE_NAME = -106;
PackageManager.INSTALL_PARSE_FAILED_BAD_SHARED_USER_ID = -107;
PackageManager.INSTALL_PARSE_FAILED_MANIFEST_MALFORMED = -108;
PackageManager.INSTALL_PARSE_FAILED_MANIFEST_EMPTY = -109;
PackageManager.INSTALL_FAILED_INTERNAL_ERROR = -110;
PackageManager.INSTALL_FAILED_USER_RESTRICTED = -111;
PackageManager.INSTALL_FAILED_DUPLICATE_PERMISSION = -112;
PackageManager.INSTALL_FAILED_NO_MATCHING_ABIS = -113;
PackageManager.NO_NATIVE_LIBRARIES = -114;
PackageManager.INSTALL_FAILED_ABORTED = -115;
PackageManager.DELETE_KEEP_DATA = 0x00000001;
PackageManager.DELETE_ALL_USERS = 0x00000002;
PackageManager.DELETE_SYSTEM_APP = 0x00000004;
PackageManager.DELETE_SUCCEEDED = 1;
PackageManager.DELETE_FAILED_INTERNAL_ERROR = -1;
PackageManager.DELETE_FAILED_DEVICE_POLICY_MANAGER = -2;
PackageManager.DELETE_FAILED_USER_RESTRICTED = -3;
PackageManager.DELETE_FAILED_OWNER_BLOCKED = -4;
PackageManager.DELETE_FAILED_ABORTED = -5;
PackageManager.MOVE_SUCCEEDED = 1;
PackageManager.MOVE_FAILED_INSUFFICIENT_STORAGE = -1;
PackageManager.MOVE_FAILED_DOESNT_EXIST = -2;
PackageManager.MOVE_FAILED_SYSTEM_PACKAGE = -3;
PackageManager.MOVE_FAILED_FORWARD_LOCKED = -4;
PackageManager.MOVE_FAILED_INVALID_LOCATION = -5;
PackageManager.MOVE_FAILED_INTERNAL_ERROR = -6;
PackageManager.MOVE_FAILED_OPERATION_PENDING = -7;
PackageManager.MOVE_INTERNAL = 0x00000001;
PackageManager.MOVE_EXTERNAL_MEDIA = 0x00000002;
PackageManager.VERIFICATION_ALLOW_WITHOUT_SUFFICIENT = 2;
PackageManager.VERIFICATION_ALLOW = 1;
PackageManager.VERIFICATION_REJECT = -1;
PackageManager.MAXIMUM_VERIFICATION_TIMEOUT = 60*60*1000;
PackageManager.FEATURE_AUDIO_LOW_LATENCY = "android.hardware.audio.low_latency";
PackageManager.FEATURE_AUDIO_OUTPUT = "android.hardware.audio.output";
PackageManager.FEATURE_BLUETOOTH = "android.hardware.bluetooth";
PackageManager.FEATURE_BLUETOOTH_LE = "android.hardware.bluetooth_le";
PackageManager.FEATURE_CAMERA = "android.hardware.camera";
PackageManager.FEATURE_CAMERA_AUTOFOCUS = "android.hardware.camera.autofocus";
PackageManager.FEATURE_CAMERA_ANY = "android.hardware.camera.any";
PackageManager.FEATURE_CAMERA_EXTERNAL = "android.hardware.camera.external";
PackageManager.FEATURE_CAMERA_FLASH = "android.hardware.camera.flash";
PackageManager.FEATURE_CAMERA_FRONT = "android.hardware.camera.front";
PackageManager.FEATURE_CAMERA_LEVEL_FULL = "android.hardware.camera.level.full";
PackageManager.FEATURE_CAMERA_CAPABILITY_MANUAL_SENSOR =
PackageManager.FEATURE_CAMERA_CAPABILITY_MANUAL_POST_PROCESSING =
PackageManager.FEATURE_CAMERA_CAPABILITY_RAW =
PackageManager.FEATURE_CONSUMER_IR = "android.hardware.consumerir";
PackageManager.FEATURE_LOCATION = "android.hardware.location";
PackageManager.FEATURE_LOCATION_GPS = "android.hardware.location.gps";
PackageManager.FEATURE_LOCATION_NETWORK = "android.hardware.location.network";
PackageManager.FEATURE_MICROPHONE = "android.hardware.microphone";
PackageManager.FEATURE_NFC = "android.hardware.nfc";
PackageManager.FEATURE_NFC_HCE = "android.hardware.nfc.hce";
PackageManager.FEATURE_NFC_HOST_CARD_EMULATION = "android.hardware.nfc.hce";
PackageManager.FEATURE_OPENGLES_EXTENSION_PACK = "android.hardware.opengles.aep";
PackageManager.FEATURE_SENSOR_ACCELEROMETER = "android.hardware.sensor.accelerometer";
PackageManager.FEATURE_SENSOR_BAROMETER = "android.hardware.sensor.barometer";
PackageManager.FEATURE_SENSOR_COMPASS = "android.hardware.sensor.compass";
PackageManager.FEATURE_SENSOR_GYROSCOPE = "android.hardware.sensor.gyroscope";
PackageManager.FEATURE_SENSOR_LIGHT = "android.hardware.sensor.light";
PackageManager.FEATURE_SENSOR_PROXIMITY = "android.hardware.sensor.proximity";
PackageManager.FEATURE_SENSOR_STEP_COUNTER = "android.hardware.sensor.stepcounter";
PackageManager.FEATURE_SENSOR_STEP_DETECTOR = "android.hardware.sensor.stepdetector";
PackageManager.FEATURE_SENSOR_HEART_RATE = "android.hardware.sensor.heartrate";
PackageManager.FEATURE_SENSOR_HEART_RATE_ECG =
PackageManager.FEATURE_SENSOR_RELATIVE_HUMIDITY =
PackageManager.FEATURE_SENSOR_AMBIENT_TEMPERATURE =
PackageManager.FEATURE_TELEPHONY = "android.hardware.telephony";
PackageManager.FEATURE_TELEPHONY_CDMA = "android.hardware.telephony.cdma";
PackageManager.FEATURE_TELEPHONY_GSM = "android.hardware.telephony.gsm";
PackageManager.FEATURE_USB_HOST = "android.hardware.usb.host";
PackageManager.FEATURE_USB_ACCESSORY = "android.hardware.usb.accessory";
PackageManager.FEATURE_SIP = "android.software.sip";
PackageManager.FEATURE_SIP_VOIP = "android.software.sip.voip";
PackageManager.FEATURE_CONNECTION_SERVICE = "android.software.connectionservice";
PackageManager.FEATURE_TOUCHSCREEN = "android.hardware.touchscreen";
PackageManager.FEATURE_TOUCHSCREEN_MULTITOUCH = "android.hardware.touchscreen.multitouch";
PackageManager.FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT = "android.hardware.touchscreen.multitouch.distinct";
PackageManager.FEATURE_TOUCHSCREEN_MULTITOUCH_JAZZHAND = "android.hardware.touchscreen.multitouch.jazzhand";
PackageManager.FEATURE_FAKETOUCH = "android.hardware.faketouch";
PackageManager.FEATURE_FAKETOUCH_MULTITOUCH_DISTINCT = "android.hardware.faketouch.multitouch.distinct";
PackageManager.FEATURE_FAKETOUCH_MULTITOUCH_JAZZHAND = "android.hardware.faketouch.multitouch.jazzhand";
PackageManager.FEATURE_SCREEN_PORTRAIT = "android.hardware.screen.portrait";
PackageManager.FEATURE_SCREEN_LANDSCAPE = "android.hardware.screen.landscape";
PackageManager.FEATURE_LIVE_WALLPAPER = "android.software.live_wallpaper";
PackageManager.FEATURE_APP_WIDGETS = "android.software.app_widgets";
PackageManager.FEATURE_VOICE_RECOGNIZERS = "android.software.voice_recognizers";
PackageManager.FEATURE_HOME_SCREEN = "android.software.home_screen";
PackageManager.FEATURE_INPUT_METHODS = "android.software.input_methods";
PackageManager.FEATURE_DEVICE_ADMIN = "android.software.device_admin";
PackageManager.FEATURE_LEANBACK = "android.software.leanback";
PackageManager.FEATURE_LEANBACK_ONLY = "android.software.leanback_only";
PackageManager.FEATURE_LIVE_TV = "android.software.live_tv";
PackageManager.FEATURE_WIFI = "android.hardware.wifi";
PackageManager.FEATURE_WIFI_DIRECT = "android.hardware.wifi.direct";
PackageManager.FEATURE_TELEVISION = "android.hardware.type.television";
PackageManager.FEATURE_WATCH = "android.hardware.type.watch";
PackageManager.FEATURE_PRINTING = "android.software.print";
PackageManager.FEATURE_BACKUP = "android.software.backup";
PackageManager.FEATURE_MANAGED_USERS = "android.software.managed_users";
PackageManager.FEATURE_MANAGED_PROFILES = "android.software.managed_users";
PackageManager.FEATURE_VERIFIED_BOOT = "android.software.verified_boot";
PackageManager.FEATURE_SECURELY_REMOVES_USERS = "android.software.securely_removes_users";
PackageManager.FEATURE_WEBVIEW = "android.software.webview";
PackageManager.FEATURE_ETHERNET = "android.hardware.ethernet";
PackageManager.FEATURE_HDMI_CEC = "android.hardware.hdmi.cec";
PackageManager.FEATURE_GAMEPAD = "android.hardware.gamepad";
PackageManager.ACTION_CLEAN_EXTERNAL_STORAGE = "android.content.pm.CLEAN_EXTERNAL_STORAGE";
PackageManager.EXTRA_VERIFICATION_URI = "android.content.pm.extra.VERIFICATION_URI";
PackageManager.EXTRA_VERIFICATION_ID = "android.content.pm.extra.VERIFICATION_ID";
PackageManager.EXTRA_VERIFICATION_INSTALLER_PACKAGE = "android.content.pm.extra.VERIFICATION_INSTALLER_PACKAGE";
PackageManager.EXTRA_VERIFICATION_INSTALL_FLAGS = "android.content.pm.extra.VERIFICATION_INSTALL_FLAGS";
PackageManager.EXTRA_VERIFICATION_INSTALLER_UID = "android.content.pm.extra.VERIFICATION_INSTALLER_UID";
PackageManager.EXTRA_VERIFICATION_PACKAGE_NAME = "android.content.pm.extra.VERIFICATION_PACKAGE_NAME";
PackageManager.EXTRA_VERIFICATION_RESULT = "android.content.pm.extra.VERIFICATION_RESULT";
PackageManager.EXTRA_VERIFICATION_VERSION_CODE = "android.content.pm.extra.VERIFICATION_VERSION_CODE";
PackageManager.ACTION_REQUEST_PERMISSION = "android.content.pm.action.REQUEST_PERMISSION";
PackageManager.EXTRA_REQUEST_PERMISSION_PERMISSION_LIST = "android.content.pm.extra.PERMISSION_LIST";
PackageManager.EXTRA_FAILURE_EXISTING_PACKAGE = "android.content.pm.extra.FAILURE_EXISTING_PACKAGE";
PackageManager.EXTRA_FAILURE_EXISTING_PERMISSION = "android.content.pm.extra.FAILURE_EXISTING_PERMISSION";
