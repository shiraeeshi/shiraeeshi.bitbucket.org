var klass = function (Parent, props) {
	var Child, F, i;
	// 1.
	// new constructor
	Child = function () {
		// first invoke parent's contructor
		// delete if needed
		// it's better to invoke parents' constructor from own constructor
		// in SomeClass' _construct it would look like the following:
		// if (SomeClass.uber && SomeClass.uber.hasOwnProperty('__construct')) {
		//     SomeClass.uber.__construct.apply(this, someArgumentsList);}
		if (Child.uber && Child.uber.hasOwnProperty("__construct")) {
			Child.uber.__construct.apply(this, arguments);
		}
		// invoke own constructor
		if (Child.prototype.hasOwnProperty("__construct")) {
			Child.prototype.__construct.apply(this, arguments);
		}
	};
	// 2.
	// inherit
	Parent = Parent || Object;
	F = function () {};
	F.prototype = Parent.prototype;
	Child.prototype = new F();
	Child.uber = Parent.prototype;
	Child.prototype.constructor = Child;
	// 3.
	// add implementation methods
	for (i in props) {
		if (props.hasOwnProperty(i)) {
			Child.prototype[i] = props[i];
		}
	}
	// return the "class"
	return Child;
};
