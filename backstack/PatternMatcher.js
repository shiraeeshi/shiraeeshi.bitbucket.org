var PatternMatcher = (function() {
	var initFunction = function PatternMatcherInitFunction(pattern, type) {
		this.mPattern = pattern;
		this.mType = type;
	};
	initFunction.prototype = {
		getPath: function() {
			return this.mPattern;
		},

		getType: function() {
			return this.mType;
		},

		match: function(str) {
			return initFunction.matchPattern(this.mPattern, str, this.mType);
		},

		toString: function() {
			var type = "? ";
			if (this.mType == initFunction.PATTERN_LITERAL) {
				type = "LITERAL: ";
			} else if (this.mType == initFunction.PATTERN_PREFIX) {
				type = "PREFIX: ";
			} else if (this.mType == initFunction.PATTERN_SIMPLE_GLOB) {
				type = "GLOB: ";
			}
			return "PatternMatcher{" + type + this.mPattern + "}";
		},

		describeContents: function() {
			return 0;
		}
	};
	initFunction.matchPattern = function(pattern, match, type) {
		if (match == null) return false;
		if (type == initFunction.PATTERN_LITERAL) {
		    return pattern == match;
		} if (type == initFunction.PATTERN_PREFIX) {
		    return match == pattern;
		} else if (type != initFunction.PATTERN_SIMPLE_GLOB) {
		    return false;
		}

		var NP = pattern.length;
		if (NP <= 0) {
		    return match.length <= 0;
		}
		var NM = match.length;
		var ip = 0, im = 0;
		var nextChar = pattern.charAt(0);
		while ((ip<NP) && (im<NM)) {
		    var c = nextChar;
		    ip++;
		    nextChar = ip < NP ? pattern.charAt(ip) : 0;
		    var escaped = (c == '\\');
		    if (escaped) {
			c = nextChar;
			ip++;
			nextChar = ip < NP ? pattern.charAt(ip) : 0;
		    }
		    if (nextChar == '*') {
			if (!escaped && c == '.') {
			    if (ip >= (NP-1)) {
				// at the end with a pattern match, so
				// all is good without checking!
				return true;
			    }
			    ip++;
			    nextChar = pattern.charAt(ip);
			    // Consume everything until the next character in the
			    // pattern is found.
			    if (nextChar == '\\') {
				ip++;
				nextChar = ip < NP ? pattern.charAt(ip) : 0;
			    }
			    do {
				if (match.charAt(im) == nextChar) {
				    break;
				}
				im++;
			    } while (im < NM);
			    if (im == NM) {
				// Whoops, the next character in the pattern didn't
				// exist in the match.
				return false;
			    }
			    ip++;
			    nextChar = ip < NP ? pattern.charAt(ip) : 0;
			    im++;
			} else {
			    // Consume only characters matching the one before '*'.
			    do {
				if (match.charAt(im) != c) {
				    break;
				}
				im++;
			    } while (im < NM);
			    ip++;
			    nextChar = ip < NP ? pattern.charAt(ip) : 0;
			}
		    } else {
			if (c != '.' && match.charAt(im) != c) return false;
			im++;
		    }
		}

		if (ip >= NP && im >= NM) {
		    // Reached the end of both strings, all is good!
		    return true;
		}

		// One last check: we may have finished the match string, but still
		// have a '.*' at the end of the pattern, which should still count
		// as a match.
		if (ip == NP-2 && pattern.charAt(ip) == '.'
		    && pattern.charAt(ip+1) == '*') {
		    return true;
		}

		return false;
	};
	initFunction.PATTERN_LITERAL = 0;
	initFunction.PATTERN_PREFIX = 1;
	initFunction.PATTERN_SIMPLE_GLOB = 2;
	return initFunction;
})();
