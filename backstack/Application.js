var Application = (function() {
	var initFunction = function ApplicationInitFunction(config) {this.config = config;};
	initFunction.isApplicationAttr = function(attr) {return com_android_internal_R_styleable.APPLICATION_ATTR_NAMES.indexOf(attr.name) >= 0;};
	return initFunction;
})();
