
var Intent = (function() {
	var initFunction = function IntentInitFunction(action_or_context_or_intent, uri_or_class_or_all, contextParam, classParam) {
		var self = this;
		if (arguments.length > 3) {
			mAction = action_or_context_or_intent;
			mData = uri_or_class_or_all;
			mComponent = new ComponentName(contextParam, classParam);
		} else {
			if (utils.isString(action_or_context_or_intent)) {
				mAction = action_or_context_or_intent;
				mData = uri_or_class_or_all;
			} else if (isIntent(action_or_context_or_intent)) {
				var intent = action_or_context_or_intent;
				mAction = intent.getAction();
				mData = intent.getData();
				mType = intent.getType();
				mPackage = intent.getPackage();
				mComponent = intent.getComponent();
				var intentCategories = intent.getCategories();
				if (intentCategories != null) {
					mCategories = newBundle(intentCategories);
				}
				if (uri_or_class_or_all != false) {
					mFlags = intent.getFlags();
					var intentExtras = intent.getExtras();
					if (intentExtras != null) {
						mExtras = intentExtras;
					}
					var intentSelector = intent.getSelector();
					if (intentSelector != null) {
						mSelector = new Intent(intentSelector);
					}
				}
			} else {
				mComponent = new ComponentName(action_or_context_or_intent, uri_or_class_or_all);
			}
		}
/*
----------------------------------------------	

    private String mAction;
    private Uri mData;
    private String mType;
    private String mPackage;
    private ComponentName mComponent;
    private int mFlags;
    private ArraySet<String> mCategories;
    private Bundle mExtras;
    private Intent mSelector;
    */

var mAction, mData, mType, mPackage, mComponent, mFlags, mCategories, mExtras, mSelector;

/*
public String getAction() {***
        return mAction;
    }
    */
self.getAction = function() {
	return mAction;
};
/*
public Uri getData() {***
        return mData;
    }
    */
self.getData = function() {
	return mData;
};
/*
public String getType() {***
        return mType;
    }
    */
self.getType = function() {
	return mType;
};

/*
public String resolveType(Context context) {***
        return resolveType(context.getContentResolver());
    }
public String resolveType(ContentResolver resolver) {***
        if (mType != null) {
            return mType;
        }
        if (mData != null) {
            if ("content".equals(mData.getScheme())) {
                return resolver.getType(mData);
            }
        }
        return null;
    }
    */
self.resolveType = function(context_or_resolver) {
	var resolver = (context_or_resolver instanceof ContentResolver) ? context_or_resolver : context_or_resolver.getContentResolver();
        if (mType != null) {
            return mType;
        }
        if (mData != null) {
            if ("content" == mData.getScheme()) {
                return resolver.getType(mData);
            }
        }
        return null;
};
/*
public String resolveTypeIfNeeded(ContentResolver resolver) {***
        if (mComponent != null) {
            return mType;
        }
        return resolveType(resolver);
    }
    */
self.resolveTypeIfNeeded = function(resolver) {
        if (mComponent != null) {
            return mType;
        }
        return self.resolveType(resolver);
};
/*
public boolean hasCategory(String category) {***
        return mCategories != null && mCategories.contains(category);
    }
    */
self.hasCategory = function(category) {
	return mCategories != null && (Object.keys(mCategories).indexOf(category) >= 0);
};
/*
public Set<String> getCategories() {***
        return mCategories;
    }
    */
self.getCategories = function() {
	return mCategories;
};
/*
public Intent getSelector() {***
        return mSelector;
    }
    */
self.getSelector = function() {
	return mSelector;
};
/*
public boolean hasExtra(String name) {***
        return mExtras != null && mExtras.containsKey(name);
    }
    */
self.hasExtra = function(name) {
	return mExtras != null && (Object.keys(mExtras) >= 0);
};


/*
@Deprecated
    public Object getExtra(String name) {
        return getExtra(name, null);
    }
@Deprecated
    public Object getExtra(String name, Object defaultValue) {
        Object result = defaultValue;
        if (mExtras != null) {
            Object result2 = mExtras.get(name);
            if (result2 != null) {
                result = result2;
            }
        }

        return result;
    }

    */

self.getExtra = function(name, defaultValue) {
	var result = defaultValue || null;
	if (mExtras == null) {return result;}
       	return mExtras[name] || result;
};

for (var i=0, len=extrasTypes.length; i<len; i++) {
	var typeName = extrasTypes[i];
	self['get' + typeName + 'Extra'] = self.getExtra;
}

/*
public Bundle getExtras() {***
        return (mExtras != null)
                ? new Bundle(mExtras)
                : null;
    }
    */
self.getExtras = function() {
	return mExtras != null ? newBundle(mExtras) : null;
};
/*
public int getFlags() {***
        return mFlags;
    }
    */
self.getFlags = function() {
	return mFlags;
};
/*
public String getPackage() {***
        return mPackage;
    }
    */
self.getPackage = function() {
	return mPackage;
};
/*
public ComponentName getComponent() {***
        return mComponent;
    }
    */
self.getComponent = function() {
	return mComponent;
};
/*
public ComponentName resolveActivity(PackageManager pm) {***
        if (mComponent != null) {
            return mComponent;
        }

        ResolveInfo info = pm.resolveActivity(
            this, PackageManager.MATCH_DEFAULT_ONLY);
        if (info != null) {
            return new ComponentName(
                    info.activityInfo.applicationInfo.packageName,
                    info.activityInfo.name);
        }

        return null;
    }
    */
self.resolveActivity = function(packageManager) {
        if (mComponent != null) {
            return mComponent;
        }

        var info = packageManager.resolveActivity(
            this, PackageManager.MATCH_DEFAULT_ONLY);
        if (info != null) {
            return new ComponentName(
                    info.activityInfo.applicationInfo.packageName,
                    info.activityInfo.name);
        }

        return null;
    }
/*
public ActivityInfo resolveActivityInfo(PackageManager pm, int flags) {***
        ActivityInfo ai = null;
        if (mComponent != null) {
            try {
                ai = pm.getActivityInfo(mComponent, flags);
            } catch (PackageManager.NameNotFoundException e) {
                // ignore
            }
        } else {
            ResolveInfo info = pm.resolveActivity(
                this, PackageManager.MATCH_DEFAULT_ONLY | flags);
            if (info != null) {
                ai = info.activityInfo;
            }
        }

        return ai;
    }
    */
self.resolveActivityInfo = function(packageManager, flags) {
        var activityInfo = null;
        if (mComponent != null) {
            try {
                activityInfo = packageManager.getActivityInfo(mComponent, flags);
            } catch (e) {
                // ignore
            }
        } else {
            var info = packageManager.resolveActivity(
                this, PackageManager.MATCH_DEFAULT_ONLY | flags);
            if (info != null) {
                activityInfo = info.activityInfo;
            }
        }

        return activityInfo;
    }
/*
public Intent setAction(String action) {***
        mAction = action != null ? action.intern() : null;
        return this;
    }
    */
self.setAction = function(action) {
	mAction = action;
	return this;
};
/*
public Intent setData(Uri data) {***
        mData = data;
        mType = null;
        return this;
    }
    */
self.setData = function(data) {
	mData = data;
	mType = null;
	return this;
};
/*
public Intent setType(String type) {***
        mData = null;
        mType = type;
        return this;
    }
    */
self.setType = function(type) {
	mData = null;
	mType = type;
	return this;
};
/*
public Intent setDataAndType(Uri data, String type) {***
        mData = data;
        mType = type;
        return this;
    }
    */
self.setDataAndType = function(data, type) {
	mData = data;
	mType = type;
	return this;
};
/*
public Intent addCategory(String category) {***
        if (mCategories == null) {
            mCategories = new ArraySet<String>();
        }
        mCategories.add(category.intern());
        return this;
    }
    */
self.addCategory = function(category) {
	if (mCategories == null) {
		mCategories = {};
	}
	mCategories[category] = null;
	return this;
};
/*
public void removeCategory(String category) {***
        if (mCategories != null) {
            mCategories.remove(category);
            if (mCategories.size() == 0) {
                mCategories = null;
            }
        }
    }
    */
self.removeCategory = function(category) {
	if (mCategories == null) {return;}
	delete mCategories[category];
	if (Object.keys(mCategories) == 0) {
		mCategories = null;
	}
};
/*
public void setSelector(Intent selector) {***
        if (selector == this) {
            throw new IllegalArgumentException(
                    "Intent being set as a selector of itself");
        }
        if (selector != null && mPackage != null) {
            throw new IllegalArgumentException(
                    "Can't set selector when package name is already set");
        }
        mSelector = selector;
    }
    */
self.setSelector = function(selector) {
        if (selector == this) {
            throw new Error(
                    "Intent being set as a selector of itself");
        }
        if (selector != null && mPackage != null) {
            throw new Error(
                    "Can't set selector when package name is already set");
        }
        mSelector = selector;
};
/*
public Intent putExtra(String name, boolean value) {***
        if (mExtras == null) {
            mExtras = new Bundle();
        }
        mExtras.putBoolean(name, value);
        return this;
    }
    */
self.putExtra = function(name, value) {
	if (mExtras == null) {
		mExtras = {};
	}
	mExtras[name] = value;
	return this;
};

/*
public Intent putExtras(Intent src) {***
        if (src.mExtras != null) {
            if (mExtras == null) {
                mExtras = new Bundle(src.mExtras);
            } else {
                mExtras.putAll(src.mExtras);
            }
        }
        return this;
    }
public Intent putExtras(Bundle extras) {***
        if (mExtras == null) {
            mExtras = new Bundle();
        }
        mExtras.putAll(extras);
        return this;
    }
    */
self.putExtras = function(donor) {
	var extras;
	if (donor instanceof Intent) {
		extras = donor != null ? donor.getExtras() : null;
	} else {
		extras = donor;
	}
	if (extras == null) {return;}
	if (mExtras == null) {mExtras = {};}
	putAll(extras, mExtras);
	return this;
};
/*
public Intent replaceExtras(Intent src) {***
        mExtras = src.mExtras != null ? new Bundle(src.mExtras) : null;
        return this;
    }
public Intent replaceExtras(Bundle extras) {***
        mExtras = extras != null ? new Bundle(extras) : null;
        return this;
    }
    */
self.replaceExtras = function(replacement) {
	var extras;
	if (replacement instanceof Intent) {
		extras = replacement != null ? replacement.getExtras() : null;
	} else {
		extras = replacement != null ? newBundle(replacement) : null; 
	}
	mExtras = extras;
	return this;
};
/*
public void removeExtra(String name) {***
        if (mExtras != null) {
            mExtras.remove(name);
            if (mExtras.size() == 0) {
                mExtras = null;
            }
        }
    }
    */
self.removeExtra = function(name) {
	if (mExtras == null) {return;}
	delete mExtras[name];
	if (mExtras.size() == 0) {
		mExtras = null;
	}
};
/*
public Intent setFlags(int flags) {***
        mFlags = flags;
        return this;
    }
    */
self.setFlags = function(flags) {
	mFlags = flags;
	return this;
};
/*
public Intent addFlags(int flags) {***
        mFlags |= flags;
        return this;
    }
    */
self.addFlags = function(flags) {
	mFlags |= flags;
	return this;
};
/*
public Intent setPackage(String packageName) {***
        if (packageName != null && mSelector != null) {
            throw new IllegalArgumentException(
                    "Can't set package name when selector is already set");
        }
        mPackage = packageName;
        return this;
    }
    */
self.setPackage = function(packageName) {
        if (packageName != null && mSelector != null) {
            throw new Error(
                    "Can't set package name when selector is already set");
        }
        mPackage = packageName;
        return this;
};
/*
public Intent setComponent(ComponentName component) {***
        mComponent = component;
        return this;
    }
    */
self.setComponent = function(component) {
	mComponent = component;
	return this;
};
/*
public Intent setClassName(Context packageContext, String className) {***
        mComponent = new ComponentName(packageContext, className);
        return this;
    }
public Intent setClassName(String packageName, String className) {***
        mComponent = new ComponentName(packageName, className);
        return this;
    }
public Intent setClass(Context packageContext, Class<?> cls) {***
        mComponent = new ComponentName(packageContext, cls);
        return this;
    }
    */
self.setClassName = function(packageContext, className) {
	mComponent = new ComponentName(packageContext, className);
	return this;
};
self.setClass = function(packageContext, className) {
	mComponent = new ComponentName(packageContext, className);
	return this;
};
	};
var extrasTypes = ['Boolean','Byte','Short','Char','Int','Long','Float','Double','String','CharSequence','Parseable','ParseableArray','ParseableArrayList','Serializable','IntegerArrayList','StringArrayList','CharSequenceArrayList','BooleanArray','ByteArray','ShortArray','CharArray','IntArray','LongArray','FloatArray','DoubleArray','StringArray','CharSequenceArray','Bundle','IBinder'];
	initFunction.prototype.clone = function() {
		return new Intent(this);
	};
	initFunction.prototype.cloneFilter = function() {
		return new Intent(this, false);
	};
	var newBundle = function(original) {
		var result = {};
		var keys = Object.keys(original);
		for (var i=0, len=keys.length; i<len; i++) {
			var key = keys[i];
			result[key] = original[key];
		}
		return result;
	};
	var putAll = function(donor, recipient) {
		var keys = Object.keys(donor);
		for (var i=0, len=keys.length; i<len; i++) {
			var key = keys[i];
			recipient[key] = donor[key];
		}
	};
	var isIntent = function(obj) {
		return (obj instanceof initFunction);
	};
    initFunction.ACTION_MAIN = "android.intent.action.MAIN";
    initFunction.ACTION_VIEW = "android.intent.action.VIEW";
    initFunction.ACTION_DEFAULT = initFunction.ACTION_VIEW;
    initFunction.ACTION_ATTACH_DATA = "android.intent.action.ATTACH_DATA";
    initFunction.ACTION_EDIT = "android.intent.action.EDIT";
    initFunction.ACTION_INSERT_OR_EDIT = "android.intent.action.INSERT_OR_EDIT";
    initFunction.ACTION_PICK = "android.intent.action.PICK";
    initFunction.ACTION_CREATE_SHORTCUT = "android.intent.action.CREATE_SHORTCUT";
    initFunction.EXTRA_SHORTCUT_INTENT = "android.intent.extra.shortcut.INTENT";
    initFunction.EXTRA_SHORTCUT_NAME = "android.intent.extra.shortcut.NAME";
    initFunction.EXTRA_SHORTCUT_ICON = "android.intent.extra.shortcut.ICON";
    initFunction.EXTRA_SHORTCUT_ICON_RESOURCE = "android.intent.extra.shortcut.ICON_RESOURCE";
    initFunction.ACTION_CHOOSER = "android.intent.action.CHOOSER";
    initFunction.ACTION_GET_CONTENT = "android.intent.action.GET_CONTENT";
    initFunction.ACTION_DIAL = "android.intent.action.DIAL";
    initFunction.ACTION_CALL = "android.intent.action.CALL";
    initFunction.ACTION_CALL_EMERGENCY = "android.intent.action.CALL_EMERGENCY";
    initFunction.ACTION_CALL_PRIVILEGED = "android.intent.action.CALL_PRIVILEGED";
    initFunction.ACTION_SENDTO = "android.intent.action.SENDTO";
    initFunction.ACTION_SEND = "android.intent.action.SEND";
    initFunction.ACTION_SEND_MULTIPLE = "android.intent.action.SEND_MULTIPLE";
    initFunction.ACTION_ANSWER = "android.intent.action.ANSWER";
    initFunction.ACTION_INSERT = "android.intent.action.INSERT";
    initFunction.ACTION_PASTE = "android.intent.action.PASTE";
    initFunction.ACTION_DELETE = "android.intent.action.DELETE";
    initFunction.ACTION_RUN = "android.intent.action.RUN";
    initFunction.ACTION_SYNC = "android.intent.action.SYNC";
    initFunction.ACTION_PICK_ACTIVITY = "android.intent.action.PICK_ACTIVITY";
    initFunction.ACTION_SEARCH = "android.intent.action.SEARCH";
    initFunction.ACTION_SYSTEM_TUTORIAL = "android.intent.action.SYSTEM_TUTORIAL";
    initFunction.ACTION_WEB_SEARCH = "android.intent.action.WEB_SEARCH";
    initFunction.ACTION_ASSIST = "android.intent.action.ASSIST";
    initFunction.ACTION_VOICE_ASSIST = "android.intent.action.VOICE_ASSIST";
    initFunction.EXTRA_ASSIST_PACKAGE = "android.intent.extra.ASSIST_PACKAGE";
    initFunction.EXTRA_ASSIST_CONTEXT = "android.intent.extra.ASSIST_CONTEXT";
    initFunction.EXTRA_ASSIST_INPUT_HINT_KEYBOARD = "android.intent.extra.ASSIST_INPUT_HINT_KEYBOARD";
    initFunction.ACTION_ALL_APPS = "android.intent.action.ALL_APPS";
    initFunction.ACTION_SET_WALLPAPER = "android.intent.action.SET_WALLPAPER";
    initFunction.ACTION_BUG_REPORT = "android.intent.action.BUG_REPORT";
    initFunction.ACTION_FACTORY_TEST = "android.intent.action.FACTORY_TEST";
    initFunction.ACTION_CALL_BUTTON = "android.intent.action.CALL_BUTTON";
    initFunction.ACTION_VOICE_COMMAND = "android.intent.action.VOICE_COMMAND";
    initFunction.ACTION_SEARCH_LONG_PRESS = "android.intent.action.SEARCH_LONG_PRESS";
    initFunction.ACTION_APP_ERROR = "android.intent.action.APP_ERROR";
    initFunction.ACTION_POWER_USAGE_SUMMARY = "android.intent.action.POWER_USAGE_SUMMARY";
    initFunction.ACTION_UPGRADE_SETUP = "android.intent.action.UPGRADE_SETUP";
    initFunction.ACTION_MANAGE_NETWORK_USAGE = "android.intent.action.MANAGE_NETWORK_USAGE";
    initFunction.ACTION_INSTALL_PACKAGE = "android.intent.action.INSTALL_PACKAGE";
    initFunction.EXTRA_INSTALLER_PACKAGE_NAME = "android.intent.extra.INSTALLER_PACKAGE_NAME";
    initFunction.EXTRA_NOT_UNKNOWN_SOURCE = "android.intent.extra.NOT_UNKNOWN_SOURCE";
    initFunction.EXTRA_ORIGINATING_URI = "android.intent.extra.ORIGINATING_URI";
    initFunction.EXTRA_REFERRER = "android.intent.extra.REFERRER";
    initFunction.EXTRA_ORIGINATING_UID = "android.intent.extra.ORIGINATING_UID";
    initFunction.EXTRA_ALLOW_REPLACE = "android.intent.extra.ALLOW_REPLACE";
    initFunction.EXTRA_RETURN_RESULT = "android.intent.extra.RETURN_RESULT";
    initFunction.EXTRA_INSTALL_RESULT = "android.intent.extra.INSTALL_RESULT";
    initFunction.ACTION_UNINSTALL_PACKAGE = "android.intent.action.UNINSTALL_PACKAGE";
    initFunction.EXTRA_UNINSTALL_ALL_USERS = "android.intent.extra.UNINSTALL_ALL_USERS";
    initFunction.METADATA_SETUP_VERSION = "android.SETUP_VERSION";
    initFunction.ACTION_SCREEN_OFF = "android.intent.action.SCREEN_OFF";
    initFunction.ACTION_SCREEN_ON = "android.intent.action.SCREEN_ON";
    initFunction.ACTION_DREAMING_STOPPED = "android.intent.action.DREAMING_STOPPED";
    initFunction.ACTION_DREAMING_STARTED = "android.intent.action.DREAMING_STARTED";
    initFunction.ACTION_USER_PRESENT = "android.intent.action.USER_PRESENT";
    initFunction.ACTION_TIME_TICK = "android.intent.action.TIME_TICK";
    initFunction.ACTION_TIME_CHANGED = "android.intent.action.TIME_SET";
    initFunction.ACTION_DATE_CHANGED = "android.intent.action.DATE_CHANGED";
    initFunction.ACTION_TIMEZONE_CHANGED = "android.intent.action.TIMEZONE_CHANGED";
    initFunction.ACTION_CLEAR_DNS_CACHE = "android.intent.action.CLEAR_DNS_CACHE";
    initFunction.ACTION_ALARM_CHANGED = "android.intent.action.ALARM_CHANGED";
    initFunction.ACTION_SYNC_STATE_CHANGED = "android.intent.action.SYNC_STATE_CHANGED";
    initFunction.ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    initFunction.ACTION_CLOSE_SYSTEM_DIALOGS = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
    initFunction.ACTION_PACKAGE_INSTALL = "android.intent.action.PACKAGE_INSTALL";
    initFunction.ACTION_PACKAGE_ADDED = "android.intent.action.PACKAGE_ADDED";
    initFunction.ACTION_PACKAGE_REPLACED = "android.intent.action.PACKAGE_REPLACED";
    initFunction.ACTION_MY_PACKAGE_REPLACED = "android.intent.action.MY_PACKAGE_REPLACED";
    initFunction.ACTION_PACKAGE_REMOVED = "android.intent.action.PACKAGE_REMOVED";
    initFunction.ACTION_PACKAGE_FULLY_REMOVED = "android.intent.action.PACKAGE_FULLY_REMOVED";
    initFunction.ACTION_PACKAGE_CHANGED = "android.intent.action.PACKAGE_CHANGED";
    initFunction.ACTION_QUERY_PACKAGE_RESTART = "android.intent.action.QUERY_PACKAGE_RESTART";
    initFunction.ACTION_PACKAGE_RESTARTED = "android.intent.action.PACKAGE_RESTARTED";
    initFunction.ACTION_PACKAGE_DATA_CLEARED = "android.intent.action.PACKAGE_DATA_CLEARED";
    initFunction.ACTION_UID_REMOVED = "android.intent.action.UID_REMOVED";
    initFunction.ACTION_PACKAGE_FIRST_LAUNCH = "android.intent.action.PACKAGE_FIRST_LAUNCH";
    initFunction.ACTION_PACKAGE_NEEDS_VERIFICATION = "android.intent.action.PACKAGE_NEEDS_VERIFICATION";
    initFunction.ACTION_PACKAGE_VERIFIED = "android.intent.action.PACKAGE_VERIFIED";
    initFunction.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE";
    initFunction.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE";
    initFunction.ACTION_WALLPAPER_CHANGED = "android.intent.action.WALLPAPER_CHANGED";
    initFunction.ACTION_CONFIGURATION_CHANGED = "android.intent.action.CONFIGURATION_CHANGED";
    initFunction.ACTION_LOCALE_CHANGED = "android.intent.action.LOCALE_CHANGED";
    initFunction.ACTION_BATTERY_CHANGED = "android.intent.action.BATTERY_CHANGED";
    initFunction.ACTION_BATTERY_LOW = "android.intent.action.BATTERY_LOW";
    initFunction.ACTION_BATTERY_OKAY = "android.intent.action.BATTERY_OKAY";
    initFunction.ACTION_POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
    initFunction.ACTION_POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";
    initFunction.ACTION_SHUTDOWN = "android.intent.action.ACTION_SHUTDOWN";
    initFunction.ACTION_REQUEST_SHUTDOWN = "android.intent.action.ACTION_REQUEST_SHUTDOWN";
    initFunction.ACTION_DEVICE_STORAGE_LOW = "android.intent.action.DEVICE_STORAGE_LOW";
    initFunction.ACTION_DEVICE_STORAGE_OK = "android.intent.action.DEVICE_STORAGE_OK";
    initFunction.ACTION_DEVICE_STORAGE_FULL = "android.intent.action.DEVICE_STORAGE_FULL";
    initFunction.ACTION_DEVICE_STORAGE_NOT_FULL = "android.intent.action.DEVICE_STORAGE_NOT_FULL";
    initFunction.ACTION_MANAGE_PACKAGE_STORAGE = "android.intent.action.MANAGE_PACKAGE_STORAGE";
    initFunction.ACTION_UMS_CONNECTED = "android.intent.action.UMS_CONNECTED";
    initFunction.ACTION_UMS_DISCONNECTED = "android.intent.action.UMS_DISCONNECTED";
    initFunction.ACTION_MEDIA_REMOVED = "android.intent.action.MEDIA_REMOVED";
    initFunction.ACTION_MEDIA_UNMOUNTED = "android.intent.action.MEDIA_UNMOUNTED";
    initFunction.ACTION_MEDIA_CHECKING = "android.intent.action.MEDIA_CHECKING";
    initFunction.ACTION_MEDIA_NOFS = "android.intent.action.MEDIA_NOFS";
    initFunction.ACTION_MEDIA_MOUNTED = "android.intent.action.MEDIA_MOUNTED";
    initFunction.ACTION_MEDIA_SHARED = "android.intent.action.MEDIA_SHARED";
    initFunction.ACTION_MEDIA_UNSHARED = "android.intent.action.MEDIA_UNSHARED";
    initFunction.ACTION_MEDIA_BAD_REMOVAL = "android.intent.action.MEDIA_BAD_REMOVAL";
    initFunction.ACTION_MEDIA_UNMOUNTABLE = "android.intent.action.MEDIA_UNMOUNTABLE";
    initFunction.ACTION_MEDIA_EJECT = "android.intent.action.MEDIA_EJECT";
    initFunction.ACTION_MEDIA_SCANNER_STARTED = "android.intent.action.MEDIA_SCANNER_STARTED";
    initFunction.ACTION_MEDIA_SCANNER_FINISHED = "android.intent.action.MEDIA_SCANNER_FINISHED";
    initFunction.ACTION_MEDIA_SCANNER_SCAN_FILE = "android.intent.action.MEDIA_SCANNER_SCAN_FILE";
    initFunction.ACTION_MEDIA_BUTTON = "android.intent.action.MEDIA_BUTTON";
    initFunction.ACTION_CAMERA_BUTTON = "android.intent.action.CAMERA_BUTTON";
    initFunction.ACTION_GTALK_SERVICE_CONNECTED = "android.intent.action.GTALK_CONNECTED";
    initFunction.ACTION_GTALK_SERVICE_DISCONNECTED = "android.intent.action.GTALK_DISCONNECTED";
    initFunction.ACTION_INPUT_METHOD_CHANGED = "android.intent.action.INPUT_METHOD_CHANGED";
    initFunction.ACTION_AIRPLANE_MODE_CHANGED = "android.intent.action.AIRPLANE_MODE";
    initFunction.ACTION_PROVIDER_CHANGED = "android.intent.action.PROVIDER_CHANGED";
    initFunction.ACTION_HEADSET_PLUG = "android.media.AudioManager.ACTION_HEADSET_PLUG";
    initFunction.ACTION_ADVANCED_SETTINGS_CHANGED = "android.intent.action.ADVANCED_SETTINGS";
    initFunction.ACTION_APPLICATION_RESTRICTIONS_CHANGED = "android.intent.action.APPLICATION_RESTRICTIONS_CHANGED";
    initFunction.ACTION_NEW_OUTGOING_CALL = "android.intent.action.NEW_OUTGOING_CALL";
    initFunction.ACTION_REBOOT = "android.intent.action.REBOOT";
    initFunction.ACTION_DOCK_EVENT = "android.intent.action.DOCK_EVENT";
    initFunction.ACTION_IDLE_MAINTENANCE_START = "android.intent.action.ACTION_IDLE_MAINTENANCE_START";
    initFunction.ACTION_IDLE_MAINTENANCE_END = "android.intent.action.ACTION_IDLE_MAINTENANCE_END";
    initFunction.ACTION_REMOTE_INTENT = "com.google.android.c2dm.intent.RECEIVE";
    initFunction.ACTION_PRE_BOOT_COMPLETED = "android.intent.action.PRE_BOOT_COMPLETED";
    initFunction.ACTION_GET_RESTRICTION_ENTRIES = "android.intent.action.GET_RESTRICTION_ENTRIES";
    initFunction.ACTION_RESTRICTIONS_CHALLENGE = "android.intent.action.RESTRICTIONS_CHALLENGE";
    initFunction.ACTION_USER_INITIALIZE = "android.intent.action.USER_INITIALIZE";
    initFunction.ACTION_USER_FOREGROUND = "android.intent.action.USER_FOREGROUND";
    initFunction.ACTION_USER_BACKGROUND = "android.intent.action.USER_BACKGROUND";
    initFunction.ACTION_USER_ADDED = "android.intent.action.USER_ADDED";
    initFunction.ACTION_USER_STARTED = "android.intent.action.USER_STARTED";
    initFunction.ACTION_USER_STARTING = "android.intent.action.USER_STARTING";
    initFunction.ACTION_USER_STOPPING = "android.intent.action.USER_STOPPING";
    initFunction.ACTION_USER_STOPPED = "android.intent.action.USER_STOPPED";
    initFunction.ACTION_USER_REMOVED = "android.intent.action.USER_REMOVED";
    initFunction.ACTION_USER_SWITCHED = "android.intent.action.USER_SWITCHED";
    initFunction.ACTION_USER_INFO_CHANGED = "android.intent.action.USER_INFO_CHANGED";
    initFunction.ACTION_MANAGED_PROFILE_ADDED = "android.intent.action.MANAGED_PROFILE_ADDED";
    initFunction.ACTION_MANAGED_PROFILE_REMOVED = "android.intent.action.MANAGED_PROFILE_REMOVED";
    initFunction.ACTION_QUICK_CLOCK = "android.intent.action.QUICK_CLOCK";
    initFunction.ACTION_SHOW_BRIGHTNESS_DIALOG = "android.intent.action.SHOW_BRIGHTNESS_DIALOG";
    initFunction.ACTION_GLOBAL_BUTTON = "android.intent.action.GLOBAL_BUTTON";
    initFunction.ACTION_OPEN_DOCUMENT = "android.intent.action.OPEN_DOCUMENT";
    initFunction.ACTION_CREATE_DOCUMENT = "android.intent.action.CREATE_DOCUMENT";
    initFunction.ACTION_MASTER_CLEAR = "android.intent.action.MASTER_CLEAR";
    initFunction.CATEGORY_DEFAULT = "android.intent.category.DEFAULT";
    initFunction.CATEGORY_BROWSABLE = "android.intent.category.BROWSABLE";
    initFunction.CATEGORY_VOICE = "android.intent.category.VOICE";
    initFunction.CATEGORY_ALTERNATIVE = "android.intent.category.ALTERNATIVE";
    initFunction.CATEGORY_SELECTED_ALTERNATIVE = "android.intent.category.SELECTED_ALTERNATIVE";
    initFunction.CATEGORY_TAB = "android.intent.category.TAB";
    initFunction.CATEGORY_LAUNCHER = "android.intent.category.LAUNCHER";
    initFunction.CATEGORY_LEANBACK_LAUNCHER = "android.intent.category.LEANBACK_LAUNCHER";
    initFunction.CATEGORY_LEANBACK_SETTINGS = "android.intent.category.LEANBACK_SETTINGS";
    initFunction.CATEGORY_INFO = "android.intent.category.INFO";
    initFunction.CATEGORY_HOME = "android.intent.category.HOME";
    initFunction.CATEGORY_PREFERENCE = "android.intent.category.PREFERENCE";
    initFunction.CATEGORY_DEVELOPMENT_PREFERENCE = "android.intent.category.DEVELOPMENT_PREFERENCE";
    initFunction.CATEGORY_EMBED = "android.intent.category.EMBED";
    initFunction.CATEGORY_APP_MARKET = "android.intent.category.APP_MARKET";
    initFunction.CATEGORY_MONKEY = "android.intent.category.MONKEY";
    initFunction.CATEGORY_TEST = "android.intent.category.TEST";
    initFunction.CATEGORY_UNIT_TEST = "android.intent.category.UNIT_TEST";
    initFunction.CATEGORY_SAMPLE_CODE = "android.intent.category.SAMPLE_CODE";
    initFunction.CATEGORY_OPENABLE = "android.intent.category.OPENABLE";
    initFunction.CATEGORY_FRAMEWORK_INSTRUMENTATION_TEST = "android.intent.category.FRAMEWORK_INSTRUMENTATION_TEST";
    initFunction.CATEGORY_CAR_DOCK = "android.intent.category.CAR_DOCK";
    initFunction.CATEGORY_DESK_DOCK = "android.intent.category.DESK_DOCK";
    initFunction.CATEGORY_LE_DESK_DOCK = "android.intent.category.LE_DESK_DOCK";
    initFunction.CATEGORY_HE_DESK_DOCK = "android.intent.category.HE_DESK_DOCK";
    initFunction.CATEGORY_CAR_MODE = "android.intent.category.CAR_MODE";
    initFunction.CATEGORY_APP_BROWSER = "android.intent.category.APP_BROWSER";
    initFunction.CATEGORY_APP_CALCULATOR = "android.intent.category.APP_CALCULATOR";
    initFunction.CATEGORY_APP_CALENDAR = "android.intent.category.APP_CALENDAR";
    initFunction.CATEGORY_APP_CONTACTS = "android.intent.category.APP_CONTACTS";
    initFunction.CATEGORY_APP_EMAIL = "android.intent.category.APP_EMAIL";
    initFunction.CATEGORY_APP_GALLERY = "android.intent.category.APP_GALLERY";
    initFunction.CATEGORY_APP_MAPS = "android.intent.category.APP_MAPS";
    initFunction.CATEGORY_APP_MESSAGING = "android.intent.category.APP_MESSAGING";
    initFunction.CATEGORY_APP_MUSIC = "android.intent.category.APP_MUSIC";
    initFunction.EXTRA_TEMPLATE = "android.intent.extra.TEMPLATE";
    initFunction.EXTRA_TEXT = "android.intent.extra.TEXT";
    initFunction.EXTRA_HTML_TEXT = "android.intent.extra.HTML_TEXT";
    initFunction.EXTRA_STREAM = "android.intent.extra.STREAM";
    initFunction.EXTRA_EMAIL       = "android.intent.extra.EMAIL";
    initFunction.EXTRA_CC       = "android.intent.extra.CC";
    initFunction.EXTRA_BCC      = "android.intent.extra.BCC";
    initFunction.EXTRA_SUBJECT  = "android.intent.extra.SUBJECT";
    initFunction.EXTRA_INTENT = "android.intent.extra.INTENT";
    initFunction.EXTRA_TITLE = "android.intent.extra.TITLE";
    initFunction.EXTRA_INITIAL_INTENTS = "android.intent.extra.INITIAL_INTENTS";
    initFunction.EXTRA_REPLACEMENT_EXTRAS = "android.intent.extra.REPLACEMENT_EXTRAS";
    initFunction.EXTRA_KEY_EVENT = "android.intent.extra.KEY_EVENT";
    initFunction.EXTRA_KEY_CONFIRM = "android.intent.extra.KEY_CONFIRM";
    initFunction.EXTRA_DONT_KILL_APP = "android.intent.extra.DONT_KILL_APP";
    initFunction.EXTRA_PHONE_NUMBER = "android.intent.extra.PHONE_NUMBER";
    initFunction.EXTRA_UID = "android.intent.extra.UID";
    initFunction.EXTRA_PACKAGES = "android.intent.extra.PACKAGES";
    initFunction.EXTRA_DATA_REMOVED = "android.intent.extra.DATA_REMOVED";
    initFunction.EXTRA_REMOVED_FOR_ALL_USERS = "android.intent.extra.REMOVED_FOR_ALL_USERS";
    initFunction.EXTRA_REPLACING = "android.intent.extra.REPLACING";
    initFunction.EXTRA_ALARM_COUNT = "android.intent.extra.ALARM_COUNT";
    initFunction.EXTRA_DOCK_STATE = "android.intent.extra.DOCK_STATE";
    initFunction.EXTRA_DOCK_STATE_UNDOCKED = 0;
    initFunction.EXTRA_DOCK_STATE_DESK = 1;
    initFunction.EXTRA_DOCK_STATE_CAR = 2;
    initFunction.EXTRA_DOCK_STATE_LE_DESK = 3;
    initFunction.EXTRA_DOCK_STATE_HE_DESK = 4;
    initFunction.METADATA_DOCK_HOME = "android.dock_home";
    initFunction.EXTRA_BUG_REPORT = "android.intent.extra.BUG_REPORT";
    initFunction.EXTRA_REMOTE_INTENT_TOKEN = "android.intent.extra.remote_intent_token";
    initFunction.EXTRA_CHANGED_COMPONENT_NAME_LIST = "android.intent.extra.changed_component_name_list";
    initFunction.EXTRA_CHANGED_PACKAGE_LIST = "android.intent.extra.changed_package_list";
    initFunction.EXTRA_CHANGED_UID_LIST = "android.intent.extra.changed_uid_list";
    initFunction.EXTRA_CLIENT_LABEL = "android.intent.extra.client_label";
    initFunction.EXTRA_CLIENT_INTENT = "android.intent.extra.client_intent";
    initFunction.EXTRA_LOCAL_ONLY = "android.intent.extra.LOCAL_ONLY";
    initFunction.EXTRA_ALLOW_MULTIPLE = "android.intent.extra.ALLOW_MULTIPLE";
    initFunction.EXTRA_USER_HANDLE = "android.intent.extra.user_handle";
    initFunction.EXTRA_USER = "android.intent.extra.USER";
    initFunction.EXTRA_RESTRICTIONS_LIST = "android.intent.extra.restrictions_list";
    initFunction.EXTRA_RESTRICTIONS_BUNDLE = "android.intent.extra.restrictions_bundle";
    initFunction.EXTRA_RESTRICTIONS_INTENT = "android.intent.extra.restrictions_intent";
    initFunction.EXTRA_MIME_TYPES = "android.intent.extra.MIME_TYPES";
    initFunction.EXTRA_SHUTDOWN_USERSPACE_ONLY = "android.intent.extra.SHUTDOWN_USERSPACE_ONLY";
    initFunction.EXTRA_TIME_PREF_24_HOUR_FORMAT = "android.intent.extra.TIME_PREF_24_HOUR_FORMAT";
    initFunction.EXTRA_REASON = "android.intent.extra.REASON";
    initFunction.FLAG_GRANT_READ_URI_PERMISSION = 0x00000001;
    initFunction.FLAG_GRANT_WRITE_URI_PERMISSION = 0x00000002;
    initFunction.FLAG_FROM_BACKGROUND = 0x00000004;
    initFunction.FLAG_DEBUG_LOG_RESOLUTION = 0x00000008;
    initFunction.FLAG_EXCLUDE_STOPPED_PACKAGES = 0x00000010;
    initFunction.FLAG_INCLUDE_STOPPED_PACKAGES = 0x00000020;
    initFunction.FLAG_GRANT_PERSISTABLE_URI_PERMISSION = 0x00000040;
    initFunction.FLAG_GRANT_PREFIX_URI_PERMISSION = 0x00000080;
    initFunction.FLAG_ACTIVITY_NO_HISTORY = 0x40000000;
    initFunction.FLAG_ACTIVITY_SINGLE_TOP = 0x20000000;
    initFunction.FLAG_ACTIVITY_NEW_TASK = 0x10000000;
    initFunction.FLAG_ACTIVITY_MULTIPLE_TASK = 0x08000000;
    initFunction.FLAG_ACTIVITY_CLEAR_TOP = 0x04000000;
    initFunction.FLAG_ACTIVITY_FORWARD_RESULT = 0x02000000;
    initFunction.FLAG_ACTIVITY_PREVIOUS_IS_TOP = 0x01000000;
    initFunction.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS = 0x00800000;
    initFunction.FLAG_ACTIVITY_BROUGHT_TO_FRONT = 0x00400000;
    initFunction.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED = 0x00200000;
    initFunction.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY = 0x00100000;
    initFunction.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET = 0x00080000;
    initFunction.FLAG_ACTIVITY_NEW_DOCUMENT = initFunction.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
    initFunction.FLAG_ACTIVITY_NO_USER_ACTION = 0x00040000;
    initFunction.FLAG_ACTIVITY_REORDER_TO_FRONT = 0X00020000;
    initFunction.FLAG_ACTIVITY_NO_ANIMATION = 0X00010000;
    initFunction.FLAG_ACTIVITY_CLEAR_TASK = 0X00008000;
    initFunction.FLAG_ACTIVITY_TASK_ON_HOME = 0X00004000;
    initFunction.FLAG_ACTIVITY_RETAIN_IN_RECENTS = 0x00002000;
    initFunction.FLAG_RECEIVER_REGISTERED_ONLY = 0x40000000;
    initFunction.FLAG_RECEIVER_REPLACE_PENDING = 0x20000000;
    initFunction.FLAG_RECEIVER_FOREGROUND = 0x10000000;
    initFunction.FLAG_RECEIVER_NO_ABORT = 0x08000000;
    initFunction.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT = 0x04000000;
    initFunction.FLAG_RECEIVER_BOOT_UPGRADE = 0x02000000;
    initFunction.IMMUTABLE_FLAGS = initFunction.FLAG_GRANT_READ_URI_PERMISSION
    initFunction.createChooser = function(target, title) {
        var intent = new Intent(initFunction.ACTION_CHOOSER);
        intent.putExtra(initFunction.EXTRA_INTENT, target);
        if (title != null) {
            intent.putExtra(initFunction.EXTRA_TITLE, title);
        }

        return intent;
    };
    initFunction.makeMainActivity = function(mainActivityName) {
        var intent = new Intent(initFunction.ACTION_MAIN);
        intent.setComponent(mainActivityName);
        intent.addCategory(initFunction.CATEGORY_LAUNCHER);
        return intent;
    };
    initFunction.makeMainSelectorActivity = function(selectorAction, selectorCategory) {
        var intent = new Intent(initFunction.ACTION_MAIN);
        intent.addCategory(initFunction.CATEGORY_LAUNCHER);
        var selector = new Intent();
        selector.setAction(selectorAction);
        selector.addCategory(selectorCategory);
        intent.setSelector(selector);
        return intent;
    };
    initFunction.makeRestartActivityTask = function(mainActivityName) {
        var intent = initFunction.makeMainActivity(mainActivityName);
        intent.addFlags(initFunction.FLAG_ACTIVITY_NEW_TASK
                | initFunction.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    };
	return initFunction;
})();
