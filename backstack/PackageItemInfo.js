var PackageItemInfo = (function() {
	var initFunction = function PackageItemInfoInitFunction(orig) {
		this.name = null; // String
		this.packageName = null; // String
		this.labelRes = 0; // int
		this.nonLocalizedLabel = null; // CharSequence
		this.icon = 0; // int
		this.banner = 0; // int
		this.logo = 0; // int
		this.metaData = null; // Bundle
		this.showUserIcon = -10000; // int UserHandle.USER_NULL
		if (orig) {
			this.name = orig.name;
			if (this.name != null) this.name = this.name.trim();
			this.packageName = orig.packageName;
			this.labelRes = orig.labelRes;
			this.nonLocalizedLabel = orig.nonLocalizedLabel;
			if (this.nonLocalizedLabel != null) this.nonLocalizedLabel = this.nonLocalizedLabel.toString().trim();
			this.icon = orig.icon;
			this.banner = orig.banner;
			this.logo = orig.logo;
			this.metaData = orig.metaData;
			this.showUserIcon = orig.showUserIcon;
		}
	};
	initFunction.prototype = {
		loadLabel: function(pm) {
			if (this.nonLocalizedLabel != null) {
			    return this.nonLocalizedLabel;
			}
			if (this.labelRes != 0) {
			    var label = pm.getText(this.packageName, this.labelRes, this.getApplicationInfo());
			    if (label != null) {
				return label.toString().trim();
			    }
			}
			if (this.name != null) {
			    return this.name;
			}
			return this.packageName;
		},
		loadIcon: function(pm) {
			return pm.loadItemIcon(this, this.getApplicationInfo());
		},

		loadBanner: function(pm) {
			if (this.banner != 0) {
			    var dr = pm.getDrawable(this.packageName, this.banner, this.getApplicationInfo());
			    if (dr != null) {
				return dr;
			    }
			}
			return this.loadDefaultBanner(pm);
		},

		loadDefaultIcon: function(pm) {
			return pm.getDefaultActivityIcon();
		},

		loadDefaultBanner: function(pm) {
			return null;
		},

		loadLogo: function(pm) {
			if (this.logo != 0) {
			    var d = pm.getDrawable(this.packageName, this.logo, this.getApplicationInfo());
			    if (d != null) {
				return d;
			    }
			}
			return this.loadDefaultLogo(pm);
		},
    
		loadDefaultLogo: function(pm) {
			return null;
		}
	};
	return initFunction;
})();
