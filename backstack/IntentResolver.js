var IteratorWrapper = function IteratorWrapperInitFunction(set, intentResolver) {
	var mCur = null;
	var mCurrentEntry = null;
	var it = set.values();
	this.next = function() {
		mCurrentEntry = it.next();
		mCur = mCurrentEntry.value;
		return mCur;
	};
	this.isDone = function() {
		return mCurrentEntry.done;
	};
	this.remove = function() {
		if (mCur) {
			intentResolver.removeFilterInternal(mCur);
		}
		set.delete(mCur);
	};
};

var IntentResolver = klass(Object, (function(){
	var getFastIntentCategories = function(intent) {
		var categories = intent.getCategories();
		if (categories == null) {
			return null;
		}
		return categories.slice();
	};
	var mResolvePrioritySorter = function(o1, o2){
		var q1 = o1.getPriority();
		var q2 = o2.getPriority();
		return (q1 > q2) ? -1 : ((q1 < q2) ? 1 : 0);
	};
	return {
	__construct: function() {
		this.mFilters = new Set(); // HashSet
		this.mTypeToFilter = {}; // ArrayMap<String, F[]>
		this.mBaseTypeToFilter = {};
		this.mWildTypeToFilter = {};
		this.mSchemeToFilter = {};
		this.mActionToFilter = {};
		this.mTypedActionToFilter = {};
	},
	addFilter: function(firstParam, secondParam, thirdParam) {
		if (arguments.length == 1) {
			var f = firstParam;
			this.mFilters.add(f);
			var numS = register_intent_filter(f, f.schemesIterator(),
				this.mSchemeToFilter, '      Scheme: ');
			var numT = register_mime_types(f, '      Type: ');
			if (numS == 0 && numT == 0) {
			    register_intent_filter(f, f.actionsIterator(),
				    mActionToFilter, "      Action: ");
			}
			if (numT != 0) {
			    register_intent_filter(f, f.actionsIterator(),
				    mTypedActionToFilter, "      TypedAction: ");
			}
		} else {
			var map = firstParam;
			var name = secondParam;
			var filter = thirdParam;

			var array = map[name];
			if (array == null) {
			    array = [];
			    map[name] = array;
			    array[0] = filter;
			} else {
			    var N = array.length;
			    var i = N;
			    while (i > 0 && array[i-1] == null) {
				i--;
			    }
			    if (i < N) {
				array[i] = filter;
			    } else {
				var newa = array.slice();
				newa[N] = filter;
				map[name] = newa;
			    }
			}
		}
	},

	filterEquals: function(f1, f2) {
		var s1 = f1.countActions();
		var s2 = f2.countActions();
		if (s1 != s2) {
		    return false;
		}
		for (var i=0; i<s1; i++) {
		    if (!f2.hasAction(f1.getAction(i))) {
			return false;
		    }
		}
		s1 = f1.countCategories();
		s2 = f2.countCategories();
		if (s1 != s2) {
		    return false;
		}
		for (var i=0; i<s1; i++) {
		    if (!f2.hasCategory(f1.getCategory(i))) {
			return false;
		    }
		}
		s1 = f1.countDataTypes();
		s2 = f2.countDataTypes();
		if (s1 != s2) {
		    return false;
		}
		for (var i=0; i<s1; i++) {
		    if (!f2.hasExactDataType(f1.getDataType(i))) {
			return false;
		    }
		}
		s1 = f1.countDataSchemes();
		s2 = f2.countDataSchemes();
		if (s1 != s2) {
		    return false;
		}
		for (var i=0; i<s1; i++) {
		    if (!f2.hasDataScheme(f1.getDataScheme(i))) {
			return false;
		    }
		}
		s1 = f1.countDataAuthorities();
		s2 = f2.countDataAuthorities();
		if (s1 != s2) {
		    return false;
		}
		for (var i=0; i<s1; i++) {
		    if (!f2.hasDataAuthority(f1.getDataAuthority(i))) {
			return false;
		    }
		}
		s1 = f1.countDataPaths();
		s2 = f2.countDataPaths();
		if (s1 != s2) {
		    return false;
		}
		for (var i=0; i<s1; i++) {
		    if (!f2.hasDataPath(f1.getDataPath(i))) {
			return false;
		    }
		}
		s1 = f1.countDataSchemeSpecificParts();
		s2 = f2.countDataSchemeSpecificParts();
		if (s1 != s2) {
		    return false;
		}
		for (var i=0; i<s1; i++) {
		    if (!f2.hasDataSchemeSpecificPart(f1.getDataSchemeSpecificPart(i))) {
			return false;
		    }
		}
		return true;
	},
	collectFilters: function(array, matching) {
		var res = null;
		if (array != null) {
		    for (var i=0; i<array.length; i++) {
			var cur = array[i];
			if (cur == null) {
			    break;
			}
			if (filterEquals(cur, matching)) {
			    if (res == null) {
				res = [];
			    }
			    res.push(cur);
			}
		    }
		}
		return res;
	},

	findFilters: function(matching) {
		if (matching.countDataSchemes() == 1) {
		    // Fast case.
		    return this.collectFilters(
				    this.mSchemeToFilter.get(matching.getDataScheme(0)), 
				    matching);
		} else if (matching.countDataTypes() != 0 && matching.countActions() == 1) {
		    // Another fast case.
		    return this.collectFilters(
				    this.mTypedActionToFilter.get(matching.getAction(0)), 
				    matching);
		} else if (matching.countDataTypes() == 0 && matching.countDataSchemes() == 0
			&& matching.countActions() == 1) {
		    // Last fast case.
		    return this.collectFilters(
				    this.mActionToFilter.get(matching.getAction(0)), 
				    matching);
		} else {
		    var res = null;
		    for (var cur of this.mFilters.values()) {
			if (this.filterEquals(cur, matching)) {
			    if (res == null) {
				res = [];
			    }
			    res.push(cur);
			}
		    }
		    return res;
		}
	},
	removeFilter: function(f) {
		this.removeFilterInternal(f);
		this.mFilters.delete(f);
	},
	removeFilterInternal: function(f) {
		var numS = this.unregister_intent_filter(f, f.schemesIterator(),
			this.mSchemeToFilter, "      Scheme: ");
		var numT = this.unregister_mime_types(f, "      Type: ");
		if (numS == 0 && numT == 0) {
		    this.unregister_intent_filter(f, f.actionsIterator(),
			    this.mActionToFilter, "      Action: ");
		}
		if (numT != 0) {
		    this.unregister_intent_filter(f, f.actionsIterator(),
			    this.mTypedActionToFilter, "      TypedAction: ");
		}
	},
	
	filterIterator: function() {
		return new IteratorWrapper(this.mFilters, this);
	},

	filterSet: function() {
		return new Set(this.mFilters);
	},

	queryIntentFromList: function(intent, resolvedType, 
		    defaultOnly, listCut) {
		var resultList = [];

		var categories = IntentResolver.getFastIntentCategories(intent);
		var scheme = intent.getScheme();
		for (var i=0, len=listCut.length; i < len; i++) {
		    this.buildResolveList(intent, categories, defaultOnly,
			    resolvedType, scheme, listCut[i], resultList);
		}
		this.sortResults(resultList);
		return resultList;
	},

	queryIntent: function(intent, resolvedType, defaultOnly) {
		var scheme = intent.getScheme();

		var finalList = [];

		var firstTypeCut = null;
		var secondTypeCut = null;
		var thirdTypeCut = null;
		var schemeCut = null;

		// If the intent includes a MIME type, then we want to collect all of
		// the filters that match that MIME type.
		if (resolvedType != null) {
		    var slashpos = resolvedType.indexOf('/');
		    if (slashpos > 0) {
			var baseType = resolvedType.substring(0, slashpos);
			if (baseType != "*") {
			    if (resolvedType.length != slashpos+2
				    || resolvedType.charAt(slashpos+1) != '*') {
				// Not a wild card, so we can just look for all filters that
				// completely match or wildcards whose base type matches.
				firstTypeCut = this.mTypeToFilter[resolvedType];
				secondTypeCut = this.mWildTypeToFilter[baseType];
			    } else {
				// We can match anything with our base type.
				firstTypeCut = this.mBaseTypeToFilter[baseType];
				secondTypeCut = this.mWildTypeToFilter[baseType];
			    }
			    // Any */* types always apply, but we only need to do this
			    // if the intent type was not already */*.
			    thirdTypeCut = this.mWildTypeToFilter["*"];
			} else if (intent.getAction() != null) {
			    // The intent specified any type ({@literal *}/*).  This
			    // can be a whole heck of a lot of things, so as a first
			    // cut let's use the action instead.
			    firstTypeCut = this.mTypedActionToFilter[intent.getAction()];
			}
		    }
		}

		// If the intent includes a data URI, then we want to collect all of
		// the filters that match its scheme (we will further refine matches
		// on the authority and path by directly matching each resulting filter).
		if (scheme != null) {
		    schemeCut = this.mSchemeToFilter[scheme];
		}

		// If the intent does not specify any data -- either a MIME type or
		// a URI -- then we will only be looking for matches against empty
		// data.
		if (resolvedType == null && scheme == null && intent.getAction() != null) {
		    firstTypeCut = this.mActionToFilter[intent.getAction()];
		}

		var categories = IntentResolver.getFastIntentCategories(intent);
		if (firstTypeCut != null) {
		    this.buildResolveList(intent, categories, defaultOnly,
			    resolvedType, scheme, firstTypeCut, finalList);
		}
		if (secondTypeCut != null) {
		    this.buildResolveList(intent, categories, defaultOnly,
			    resolvedType, scheme, secondTypeCut, finalList);
		}
		if (thirdTypeCut != null) {
		    this.buildResolveList(intent, categories, defaultOnly,
			    resolvedType, scheme, thirdTypeCut, finalList);
		}
		if (schemeCut != null) {
		    this.buildResolveList(intent, categories, defaultOnly,
			    resolvedType, scheme, schemeCut, finalList);
		}
		this.sortResults(finalList);

		return finalList;
	},

	allowFilterResult: function(filter, dest) {
		return true;
	},

	isFilterStopped: function(filter) {
		return false;
	},

	newResult: function(filter, match) {
		return filter;
	},

	sortResults(results) {
		results.sort(mResolvePrioritySorter);
	},


	register_mime_types: function(filter, prefix) {
		var i = filter.typesIterator();
		if (i == null) {
		    return 0;
		}

		var num = 0;
		while (true) {
		    var name = i.next();
		    if (i.isDone()) {
			    break;
		    }
		    num++;
		    var baseName = name;
		    var slashpos = name.indexOf('/');
		    if (slashpos > 0) {
			baseName = name.substring(0, slashpos);
		    } else {
			name = name + "/*";
		    }

		    this.addFilter(this.mTypeToFilter, name, filter);

		    if (slashpos > 0) {
			this.addFilter(this.mBaseTypeToFilter, baseName, filter);
		    } else {
			this.addFilter(this.mWildTypeToFilter, baseName, filter);
		    }
		}

		return num;
	},

	unregister_mime_types: function(filter, prefix) {
		var i = filter.typesIterator();
		if (i == null) {
		    return 0;
		}

		var num = 0;
		while (true) {
		    var name = i.next();
		    if (i.isDone()) {
			    return ;
		    }
		    num++;
		    var baseName = name;
		    var slashpos = name.indexOf('/');
		    if (slashpos > 0) {
			baseName = name.substring(0, slashpos);
		    } else {
			name = name + "/*";
		    }

		    this.remove_all_objects(this.mTypeToFilter, name, filter);

		    if (slashpos > 0) {
			this.remove_all_objects(this.mBaseTypeToFilter, baseName, filter);
		    } else {
			this.remove_all_objects(this.mWildTypeToFilter, baseName, filter);
		    }
		}
		return num;
	},

	register_intent_filter: function(filter, iterator, dest, prefix) {
		if (iterator == null) {
		    return 0;
		}

		var num = 0;
		while (true) {
		    var name = iterator.next();
		    if (iterator.isDone()) {
			    return;
		    }
		    num++;
		    this.addFilter(dest, name, filter);
		}
		return num;
	},

	unregister_intent_filter: function(filter, iterator, dest, prefix) {
		if (iterator == null) {
		    return 0;
		}

		var num = 0;
		while (true) {
		    var name = iterator.next();
		    if (iterator.isDone()) {
			    return;
		    }
		    num++;
		    this.remove_all_objects(dest, name, filter);
		}
		return num;
	},

	remove_all_objects: function(map, name, object) {
		var array = map[name];
		if (array != null) {
		    var LAST = array.length-1;
		    while (LAST >= 0 && (array[LAST] == null || array[LAST] == undefined)) {
			LAST--;
		    }
		    for (var idx=LAST; idx>=0; idx--) {
			if (array[idx] == object) {
			    var remain = LAST - idx;
			    if (remain > 0) {
				//System.arraycopy(array, idx+1, array, idx, remain);
				array.splice(idx,1);
			    }
			    array[LAST] = null;
			    LAST--;
			}
		    }
		    if (LAST < 0) {
			delete map[name];
		    } else if (LAST < (array.length/2)) {
			//System.arraycopy(array, 0, newa, 0, LAST+1);
			var newa = array.slice(0, LAST+1)
			map[name] = newa;
		    }
		}
	},
	buildResolveList: function(intent, categories, debug, defaultOnly, resolvedType, scheme, src, dest) {
		var action = intent.getAction();
		var data = intent.getData();
		var packageName = intent.getPackage();

		var excludingStopped = intent.isExcludingStopped();

		var N = src != null ? src.length : 0;
		var hasNonDefaults = false;
		var i;
		var filter;
		for (i=0; i<N && (filter=src[i]) != null; i++) {
		    var match;

		    if (excludingStopped && this.isFilterStopped(filter)) {
			continue;
		    }

		    // Is delivery being limited to filters owned by a particular package?
		    if (packageName != null && !this.isPackageForFilter(packageName, filter)) {
			continue;
		    }

		    // Do we already have this one?
		    if (!this.allowFilterResult(filter, dest)) {
			continue;
		    }

		    match = filter.match(action, resolvedType, scheme, data, categories);
		    if (match >= 0) {
			if (!defaultOnly || filter.hasCategory(Intent.CATEGORY_DEFAULT)) {
			    var oneResult = this.newResult(filter, match);
			    if (oneResult != null) {
				dest.push(oneResult);
			    }
			} else {
			    hasNonDefaults = true;
			}
		    } else {
			   // log 
		    }
		}

		if (hasNonDefaults) {
		    if (dest.size() == 0) {
			//Slog.w(TAG, "resolveIntent failed: found match, but none with CATEGORY_DEFAULT");
		    } else if (dest.size() > 1) {
			//Slog.w(TAG, "resolveIntent: multiple matches, only some with CATEGORY_DEFAULT");
		    }
		}
	    }
};
})());
