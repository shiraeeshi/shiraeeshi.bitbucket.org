var ComponentName = (function() {
	var initFunction = function ComponentNameInitFuncton(context_or_package, className) {
		this.packageName = utils.isString(context_or_package) ? context_or_package : context_or_package.getPackageName();
		this.className = className;
	};
	initFunction.prototype.toString = function() {
		return 'ComponentName0pn='+this.packageName+'0cn='+this.className;
	};
	return initFunction;
})();
