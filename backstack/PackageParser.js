var PackageParser = (function() {
	var initFunction = function PackageParserInitFunction() {};
	initFunction.generateActivityInfo = function(a, flags, state) {
		if (a == null) return null;
		if (!copyNeeded(flags, a.owner, state, a.metaData)) {
			return a.info;
		}
		// Make shallow copies so we can store the metadata safely
		var ai = new ActivityInfo(a.info);
		ai.metaData = a.metaData;
		ai.applicationInfo = generateApplicationInfo(a.owner, flags, state);
		return ai;
	};
	initFunction.generateApplicationInfo = function(p, flags, state) {
// TODO Ported to JavaScript with simplified logic. rewrite if needed
		if (p == null) return null;
		if (!copyNeeded(flags, p, state, null)
			&& ((flags&PackageManager.GET_DISABLED_UNTIL_USED_COMPONENTS) == 0
				|| state.enabled != PackageManager.COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED)) {
		    //updateApplicationInfo(p.applicationInfo, flags, state);
		    return p.applicationInfo;
		}
		// Make shallow copy so we can store the metadata/libraries safely
		var ai = new ApplicationInfo(p.applicationInfo);
		if ((flags & PackageManager.GET_META_DATA) != 0) {
		    ai.metaData = p.mAppMetaData;
		}
		if ((flags & PackageManager.GET_SHARED_LIBRARY_FILES) != 0) {
		    ai.sharedLibraryFiles = p.usesLibraryFiles;
		}
		return ai;
	};
	var copyNeeded = function(flags, pakage, state, metaData) {
		if ((flags & PackageManager.GET_META_DATA) != 0
			&& (metaData != null || pakage.mAppMetaData != null)) {
		    return true;
		}
		if ((flags & PackageManager.GET_SHARED_LIBRARY_FILES) != 0
			&& pakage.usesLibraryFiles != null) {
		    return true;
		}
		return false;
	};
	initFunction._js_parse = function(projects, configParser) {
		var projectNames = projects.getNames();
		var parsedActivities = [];
		for (var i=projectNames.length-1; i>=0; i--) {
			var project = projects.getProject(projectNames[i]);
			var parsedManifest = configParser.parseConfig(project.getManifest());
			for (var appIndex=0; appIndex<parsedManifest.config.apps.length; appIndex++) {
				var app = parsedManifest.config.apps[appIndex];
				var activities = app.config.activities;
				parsedActivities = parsedActivities.concat(activities);
			}
		}
		for (var i=parsedActivities.length-1; i>=0; i--) {
			var parsedActivity = parsedActivities[i];
			var sa = parsedActivity.config.attrs;
			var packageName = parsedActivity.app.manifest.config.attrs['package'];
			var error = validateName(packageName, true);
			if (error) {
				throw new Error(error);
			}
			var owner = new Package(packageName);
			var outError = {};
			var separateProcesses = false;
			var parseActivityArgs = new ParseComponentArgs(owner, outError, 
				com_android_internal_R_styleable.AndroidManifestActivity.name,
				com_android_internal_R_styleable.AndroidManifestActivity.label,
				com_android_internal_R_styleable.AndroidManifestActivity.icon,
				com_android_internal_R_styleable.AndroidManifestActivity.logo,
				com_android_internal_R_styleable.AndroidManifestActivity.banner,
				separateProcesses,
				com_android_internal_R_styleable.AndroidManifestActivity.process,
				com_android_internal_R_styleable.AndroidManifestActivity.description,
				com_android_internal_R_styleable.AndroidManifestActivity.enabled
				);
			parseActivityArgs.sa = sa;
			var flags = 0; // TODO simplified when ported to js
			parseActivityArgs.flags = flags; 
			var a = new initFunction.Activity(parseActivityArgs, new ActivityInfo());
			var exportedValue = sa[com_android_internal_R_styleable.AndroidManifestActivity.process];
			if (exportedValue) {
				a.info.exported = (exportedValue == 'true');
			}
			a.info.theme = sa[com_android_internal_R_styleable.AndroidManifestActivity.theme];
			a.info.uiOptions = sa[com_android_internal_R_styleable.AndroidManifestActivity.uiOptions];

			var parentName = sa[com_android_internal_R_styleable.AndroidManifestActivity.parentActivityName];
			if (parentName) {
				var parentClassName = buildClassName(a.info.packageName, parentName, outError);
				if (!outError[0]) {
					a.info.parentActivityName = parentClassName;
				} else {
					console.log('Activity ' + a.info.name + ' specified invalid parentActivityName ' + parentName);
					outError[0] = null;
				}
			}
			var str;
			str = sa[com_android_internal_R_styleable.AndroidManifestActivity.permission];
			if (!str) {
				a.info.permission = owner.applicationInfo.permission;
			} else {
				a.info.permission = str;
			}

			str = sa[com_android_internal_R_styleable.AndroidManifestActivity.taskAffinity];
			a.info.taskAffinity = buildTaskAffinityName(owner.applicationInfo.packageName, owner.applicationInfo.taskAffinity, str, outError);
			a.info.flags = 0;
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.multiprocess] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_MULTIPROCESS;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.finishOnTaskLaunch] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_FINISH_ON_TASK_LAUNCH;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.clearTaskOnLaunch] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_CLEAR_TASK_ON_LAUNCH;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.noHistory] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_NO_HISTORY;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.alwaysReturnTaskState] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_ALWAYS_RETURN_TASK_STATE;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.stateNotNeeded] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_STATE_NOT_NEEDED;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.excludeFromRecents] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_EXCLUDE_FROM_RECENTS;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.allowTaskReparenting] == 'true') {
				if ((owner.applicationInfo.flags&ApplicationInfo.FLAG_ALLOW_TASK_REPARENTING) != 0) {
					a.info.flags |= ActivityInfo.FLAG_ALLOW_TASK_REPARENTING;
				}
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.finishOnCloseSystemDialogs] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_FINISH_ON_CLOSE_SYSTEM_DIALOGS;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.showOnLockScreen] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_SHOW_ON_LOCK_SCREEN;
			}
			if (sa[com_android_internal_R_styleable.AndroidManifestActivity.immersive] == 'true') {
				a.info.flags |= ActivityInfo.FLAG_IMMERSIVE;
			}
			
			var receiver = false; // TODO: simplified when ported to js
			if (!receiver) { 
				a.info.launchMode = sa[com_android_internal_R_styleable.AndroidManifestActivity.launchMode] || ActivityInfo.LAUNCH_MULTIPLE;
				a.info.documentLaunchMode = sa[com_android_internal_R_styleable.AndroidManifestActivity.documentLaunchMode] || ActivityInfo.DOCUMENT_LAUNCH_MODE;
				a.info.maxRecents = sa[com_android_internal_R_styleable.AndroidManifestActivity.maxRecents] || 8;
				a.info.screenOrientation = sa[com_android_internal_R_styleable.AndroidManifestActivity.screenOrientation] || ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
				a.info.configChanges = sa[com_android_internal_R_styleable.AndroidManifestActivity.configChanges] || 0;
				a.info.softInputMode = sa[com_android_internal_R_styleable.AndroidManifestActivity.windowSoftInputMode] || 0;
				a.info.persistableMode = sa[com_android_internal_R_styleable.AndroidManifestActivity.persistableMode] || ActivityInfo.PERSIST_ROOT_ONLY;
				if (sa[com_android_internal_R_styleable.AndroidManifestActivity.allowEmbedded] == 'true') {
					a.info.flags |= ActivityInfo.FLAG_ALLOW_EMBEDDED;
				}
				if (sa[com_android_internal_R_styleable.AndroidManifestActivity.removeFromRecents] == 'true') {
					a.info.flags |= ActivityInfo.FLAG_REMOVE_FROM_RECENTS;
				}
				if (sa[com_android_internal_R_styleable.AndroidManifestActivity.relinquishTaskIdentity] == 'true') {
					a.info.flags |= ActivityInfo.FLAG_RELINQUISH_TASK_IDENTITY;
				}
				if (sa[com_android_internal_R_styleable.AndroidManifestActivity.resumeWhilePausing] == 'true') {
					a.info.flags |= ActivityInfo.FLAG_RESUME_WHILE_PAUSING;
				}
			} else {
				a.info.launchMode = ActivityInfo.LAUNCH_MULTIPLE;
				a.info.configChanges = 0;
			}

			if (receiver) {
				var attrSingleUser = sa[com_android_internal_R_styleable.AndroidManifestActivity.singleUser] || false;
				if (attrSingleUser == 'true') {
					a.info.flags |= ActivityInfo.FLAG_SINGLE_USER;
					if (a.info.exported && ((flags&initFunction.PARSE_IS_PRIVILEGED) == 0)) {
						console.log('Activity exported request ignored due to singleUser: ' + a.className);
						a.info.exported = false;
						setExported = false;
					}
				}
				if (sa[com_android_internal_R_styleable.AndroidManifestActivity.primaryUserOnly] == 'true') {
					a.info.flags |= ActivityInfo.FLAG_PRIMARY_USER_ONLY;
				}
			}
			if (outError[0]) {
				return null;
			}
			for (var iIndex=0, iLen=parsedActivity.config.intentFilters.length; iIndex<iLen; iIndex++) {
				var parsedIntentFilter = parsedActivity.config.intentFilters[iIndex];
				var intent = new ActivityIntentInfo(a);
				var priority = sa[com_android_internal_R_styleable.AndroidManifestIntentFilter.priority];
				if (priority) {
					priority = parseInt(priority);
					intent.setPriority(priority);
				}
				var nonLocalizedLabel = sa[com_android_internal_R_styleable.AndroidManifestIntentFilter.label];
				if (nonLocalizedLabel) {
					intent.nonLocalizedLabel = nonLocalizedLabel;
				}
				intent.icon = sa[com_android_internal_R_styleable.AndroidManifestIntentFilter.icon];
				intent.logo = sa[com_android_internal_R_styleable.AndroidManifestIntentFilter.logo];
				intent.banner = sa[com_android_internal_R_styleable.AndroidManifestIntentFilter.banner];
				var actions = parsedIntentFilter.actions;
				for (var actionIndex=0, actionsLen=actions.length; actionIndex<actionsLen; actionIndex++) {
					var action = actions[actionIndex];
					intent.addAction(action);
				}
				var categories = parsedIntentFilter.categories;
				for (var catIndex=0, catLen=categories.length; catIndex<catLen; catIndex++) {
					var cat = categories[catIndex];
					intent.addCategory(cat);
				}
				var datas = parsedIntentFilter.datas;
				for (var dataIndex=0, datasLen=datas.length; dataIndex<datasLen; dataIndex++) {
					sa = parsedIntentFilter.datas[dataIndex];
					var str = sa[com_android_internal_R_styleable.AndroidManifestData.mimeType];
					if (str) {
						try {
							intent.addDataType(str);
						} catch (e) {
							if (e instanceof IntentFilter.MalformedMimeTypeException) {
								return false;
							}
						}
					}
					
					var str = sa[com_android_internal_R_styleable.AndroidManifestData.scheme];
					if (str) {
						intent.addDataScheme(str);
					}

					str = sa[com_android_internal_R_styleable.AndroidManifestData.ssp];
					if (str) {
						intent.addDataSchemeSpecificPart(str, PatternMatcher.PATTERN_LITERAL);
					}

					str = sa[com_android_internal_R_styleable.AndroidManifestData.sspPrefix];
					if (str) {
						intent.addDataSchemeSpecificPart(str, PatternMatcher.PATTERN_PREFIX);
					}

					str = sa[com_android_internal_R_styleable.AndroidManifestData.sspPattern];
					if (str) {
						intent.addDataSchemeSpecificPart(str, PatternMatcher.PATTERN_SIMPLE_GLOB);
					}

					var host = sa[com_android_internal_R_styleable.AndroidManifestData.host];
					var port = sa[com_android_internal_R_styleable.AndroidManifestData.port];
					if (host) {
						intent.addDataAuthority(host, port);
					}

					str = sa[com_android_internal_R_styleable.AndroidManifestData.path];
					if (str) {
						intent.addDataPath(str, PatternMatcher.PATTERN_LITERAL);
					}

					str = sa[com_android_internal_R_styleable.AndroidManifestData.pathPrefix];
					if (str) {
						intent.addDataPath(str, PatternMatcher.PATTERN_PREFIX);
					}

					str = sa[com_android_internal_R_styleable.AndroidManifestData.pathPattern];
					if (str) {
						intent.addDataPath(str, PatternMatcher.PATTERN_SIMPLE_GLOB);
					}
				}
			}
			/////
			/*
			each activity = {
				config: {
					attrs: {
						'attrs of activity tag'
					},
					intentFilters: [
						{
							actions: [],
							categories: [],
							datas: [
								{
									'attrs of data tag'
								}
							]
						}
					]
				}
			};
			//////
			each PackageParser.Activity = {
				info: {
					'theme, launchMode, permission, taskAffinity, targetAffinity, flags, screenOrientation, configChanges, softInputMode, uiOptions, parentActivityName, maxRecents'
					applicationInfo: {
						'taskAffinity, className, theme, flags, etc.'
					},
				},
				owner, className, componentName,
				intents: [
						{
							extends IntentFilter (has actions, categories, dataTypes, dataSchemes, etc.),
							activity: PackageParser.Activity,
							hasDefault,
							labelRes, nonLocalizedLabel, icon, logo, banner, preferred
						}
					]

			};
			*/
			////////////
		}
	};
	var IntentInfo = klass(IntentFilter, {
		__construct: function() {
			if (IntentInfo.uber && IntentInfo.uber.hasOwnProperty('__construct')) {
				IntentInfo.uber.__construct.apply(this, []);
			}
			this.hasDefault = false;
			this.labelRes = 0;
			this.nonLocalizedLabel = null;
			this.icon = 0;
			this.logo = 0;
			this.banner = 0;
			this.preferred = 0;
		}
	});
	initFunction.IntentInfo = IntentInfo;
	var ActivityIntentInfo = klass(IntentInfo, {
		__construct: function(_activity) {
			this.activity = _activity;
		}
	});
	initFunction.ActivityIntentInfo = ActivityIntentInfo;
	var ParsePackageItemArgs = klass(Object, {
		__construct: function(_owner, _outError, _nameRes, _labelRes, _iconRes, _logoRes, _bannerRes) {
			this.owner = _owner;
			this.outError = _outError;
			this.nameRes = _nameRes;
			this.labelRes = _labelRes;
			this.iconRes = _iconRes;
			this.logoRes = _logoRes;
			this.bannerRes = _bannerRes;
			this.tag = null;
			this.sa = null;
		}
	});
	initFunction.ParsePackageItemArgs = ParsePackageItemArgs;
	var ParseComponentArgs = klass(ParsePackageItemArgs, {
		__construct: function(_owner, _outError, _nameRes, _labelRes, _iconRes, _logoRes, _bannerRes, _sepProcesses, _processRes, _descriptionRes, _enabledRes) {
			this.sepProcesses = _sepProcesses;
			this.processRes = _processRes;
			this.descriptionRes = _descriptionRes;
			this.enabledRes = _enabledRes;
		}
	});
	initFunction.ParseComponentArgs = ParseComponentArgs;
	var Package = function(packageName) {
		this.packageName = packageName;
		this.applicationInfo = {};
		this.applicationInfo.packageName = packageName;
	};
	Package.prototype.setPackageName = function(newName) {
		this.packageName = newName;
		this.applicationInfo.packageName = newName;
		for (var i=this.activities.length-1; i>=0; i--) {
			this.activities[i].setPackageName(newName);
		}
	};
	Package.prototype.hasComponentClassName = function(name) {
		for (var i=this.activities.length-1; i>=0; i--) {
			if (name == this.activities[i].className) {
				return true;
			}
		}
		return false;
	};
	initFunction.Package = Package;
	var Component = klass(Object, {
		__construct: function(firstParam, secondParam) {
			this.owner = null;
			this.intents = null;
			this.className = null;
			this.metaData = null;
			this.componentName = null;
			this.componentShortName = null;
			if (arguments.length == 2) {
				var args = firstParam;
				var outInfo = secondParam;
				this.owner = args.owner;
				this.intents = [];
				var name = args.sa[args.nameRes];
				if (!name) {
					this.className = null;
					args.outError[0] = args.tag + ' does not specify android:name';
					return;
				}
				outInfo.name = buildClassName(this.owner.applicationInfo.packageName, name, args.outError);
				if (!outInfo.name) {
					this.className = null;
					args.outError[0] = args.tag + ' does not have valid android:name';
					return;
				}
				this.className = outInfo.name;
				outInfo.packageName = this.owner.packageName;
			} else if (arguments.length == 1) {
				if (firstParam instanceof Package) {
					this.owner = firstParam;
				} else if (firstParam instanceof Component) {
					var clone = firstParam;
					this.owner = clone.owner;
					this.intents = clone.intents;
					this.className = clone.className;
					this.componentName = clone.componentName;
					this.componentShortName = clone.componentShortName;
				}
			}
		},
		getComponentName: function() {
			if (this.componentName) {
				return this.componentName;
			}
			if (this.className) {
				this.componentName = new ComponentName(this.owner.applicationInfo.packageName, this.className);
			}
			return this.componentName;
		},
		setPackageName: function(packageName) {
			this.componentName = null;
			this.componentShortName = null;
		}
	});
	initFunction.Component = Component;
	initFunction.Activity = klass(Component, {
		__construct: function(firstParam, secondParam) {
			this.info = secondParam;
			this.info.applicationInfo = firstParam.owner.applicationInfo;
		},
		setPackageName: function(packageName) {
			initFunction.Activity.uber.setPackageName.apply(this, arguments);
			this.info.packageName = packageName;
		},
		toString: function() {
			return 'PackageParser.Activity0cn=' + this.componentName;
		}
	});
	var buildClassName = function(pkg, clsSeq, outError) {
		if (!clsSeq) {
		    outError[0] = "Empty class name in package " + pkg;
		    return null;
		}
		var cls = clsSeq;
		var c = cls.charAt(0);
		if (c == '.') {
		    return (pkg + cls);
		}
		if (cls.indexOf('.') < 0) {
		    return pkg + '.' + cls;
		}
		if (c >= 'a' && c <= 'z') {
		    return cls;
		}
		outError[0] = "Bad class name " + cls + " in package " + pkg;
		return null;
	};
	var buildTaskAffinityName = function(pkg, defProc, procSeq, outError) {
		if (procSeq.length == 0) {
			return null;
		}
		if (!procSeq) {
			return defProc;
		}
		return buildCompoundName(pkg, procSeq, 'taskAffinity', outError);
	};
	var buildCompoundName = function(pkg, procSeq, type, outError) {
		var proc = procSeq.toString();
		var c = proc.charAt(0);
		if (pkg && (c == ':')) {
			if (proc.length < 2) {
				outError[0] = "Bad " + type + " name " + proc + " in package " + pkg
					+ ": must be at least two characters";
				return null;
			}
			var subName = proc.substring(1);
			var nameError = validateName(subName, false);
			if (nameError != null) {
				outError[0] = "Invalid " + type + " name " + proc + " in package "
					+ pkg + ": " + nameError;
				return null;
			}
			return (pkg + proc);
		}
		var nameError = validateName(proc, true);
		if (nameError != null && proc != "system") {
		    outError[0] = "Invalid " + type + " name " + proc + " in package "
			    + pkg + ": " + nameError;
		    return null;
		}
		return proc;
	};
	var validateName = function(name, requiresSeparator) {
		var N = name.length;
		var hasSep = false;
		var front = true;
		for (var i=0; i<N; i++) {
		    var c = name.charAt(i);
		    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
			front = false;
			continue;
		    }
		    if (!front) {
			if ((c >= '0' && c <= '9') || c == '_') {
			    continue;
			}
		    }
		    if (c == '.') {
			hasSep = true;
			front = true;
			continue;
		    }
		    return "bad character '" + c + "'";
		}
		return hasSep || !requiresSeparator
			? null : "must have at least one '.' separator";
	};
	return initFunction;
})();
