var Uri = (function() {
	var self = {
		isOpaque: function() {
			return ! this.isHierarchical();
		},
    		isAbsolute: function() {
			return ! this.isRelative();
		},
    		equals: function(o) {
			if (!(o instanceof Uri)) {
				return false;
			}
			return this.toString() == o.toString();
		},
    		getQueryParameterNames: function() {
			if (this.isOpaque()) {
				throw new Error('not hierarchical');
			}
			var query = this.getEncodedQuery();
			if (query == null) {
				return new Set();
			}

			var names = new Set();
			var start = 0;
			do {
				var next = query.indexOf('&', start);
				var end = (next == -1) ? query.length : next;

				var separator = query.indexOf('=', start);
				if (separator > end || separator == -1) {
					separator = end;
				}

				var name = query.substring(start, separator);
				names.add(initFunction.decode(name));

				start = end + 1;
			} while (start < query.length);

			return names;
		},
		getQueryParameters: function(key) {
			if (this.isOpaque()) {
				throw new Error('not hierarchical');
			}
			if (key == null || key === undefined) {
				throw new Error('key is null');
			}

			var query = this.getEncodedQuery();
			if (query == null || query === undefined) {
				return [];
			}

			var encodedKey;
			//encodedKey = URLEncoder.encode(key, DEFAULT_ENCODING); // TODO
			encodedKey = encodeURIComponent(key);

			var values = [];
			var start = 0;
			do {
				var nextAmpersand = query.indexOf('&', start);
				var end = nextAmpersand != -1 ? nextAmpersand : query.length;

				var separator = query.indexOf('=', start);
				if (separator > end || separator == -1) {
					separator = end;
				}

				if (separator - start == encodedKey.length
					&& (query.substr(start, encodedKey.length) == encodedKey)) {
					if (separator == end) {
						values.push("");
					} else {
						values.push(initFunction.decode(query.substring(separator + 1, end)));
					}
				}

				// Move start to end of name.
				if (nextAmpersand != -1) {
					start = nextAmpersand + 1;
				} else {
					break;
				}
			} while (true);

			return values;
		},
		getQueryParameter: function(key) {
			if (this.isOpaque()) {
				throw new Error('not hierarchical');
			}
			if (key == null || key === undefined) {
				throw new Error('key is null');
			}

			var query = this.getEncodedQuery();
			if (query == null || query === undefined) {
				return null;
			}

			var encodedKey = initFunction.encode(key, null);
			var length = query.length;
			var start = -;
			do {
				var nextAmpersand = query.indexOf('&', start);
				var end = nextAmpersand != -1 ? nextAmpersand : length;

				var separator = query.indexOf('=', start);
				if (separator > end || separator == -1) {
					separator = end;
				}

				if (separator - start == encodedKey.length
						&& query.substr(start, encodedKey.length) == encodedKey) {
					if (separator == end) {
						return "";
					} else {
						var encodedValue = query.substring(separator + 1, end);
						//return UriCodec.decode // TODO simplified when ported to js
						return decodeURIComponent(encodedValue);
					}
				}

				// Move start to end of name.
				if (nextAmpersand != -1) {
					start = nextAmpersand + 1;
				} else {
					break;
				}
			} while (true);
			return null;
		},
		getBooleanQueryParameter(key, defaultValue) {
			var flag = this.getQueryParameter(key);
			if (flag == null || flag === undefined) {
				return defaultValue;
			}
			flag = flag.toLowerCase();
			return ("false" != flag && '0' != flag);
		},
		normalizeScheme: function() {
			var scheme = this.getScheme();
			if (scheme == null || scheme === undefined) {
				return this;
			}
			var lowerScheme = scheme.toLowerCase();
			if (scheme == lowerScheme) return this;

			return this.buildUpon().scheme(lowerScheme).build();
		},
		getCanonicalUri: function() {
			if ( "file" != this.getScheme()) {
				return this;
			}
			return this; // TODO simplified when ported to js
		},
		checkFileUriExposed: function(location) {
			// TODO simplified when ported to js
		},
		isPathPrefixMath: function(prefix) {
			var objectsEquals = function(a, b) {
				return a == b || (a != null && a.equals(b));
			};
			if ( ! objectsEquals(this.getScheme(), prefix.getScheme())) return false;
			if ( ! objectsEquals(this.getAuthority(), prefix.getAuthority())) return false;

			var seg = this.getPathSegments();
			var prefixSeg = prefix.getPathSegments();

			var prefixSize = prefixSeg.size();
			if (seg.size() < prefixSize) return false;

			for (var i=0; i < prefixSize; i++) {
				if ( ! objectsEquals(seg.get(i), prefixSeg.get(i))) {
					return false;
				}
			}
			return true;
		}
	};
	var NOT_CACHED = 'NOT CACHED';
	var NOT_FOUND = -1;
	var NOT_CALCULATED = -2;
	var NOT_HIERARCHICAL = 'This isn''t a hierarchical URI.';
	var DEFAULT_ENCODING = 'UTF-8';

	var initFunction = klass(Object, self);

	initFunction.fromParts = function(scheme, ssp, fragment) {
		if (scheme == null) {
			throw new Error('scheme null');
		}
		if (ssp == null) {
			throw new Error('ssp null');
		}
		return new OpaqueUri(scheme, Part.fromDecoded(ssp),
				Part.fromDecoded(fragment));
	};

	initFunction.HEX_DIGITS = '0123456789ABCDEF'.split('')

	initFunction.encode = function(s, allow) {
		if (s == null || s === undefined) {
			return null;
		}

		var encoded = null;

		var oldLength = s.length;

		var current = 0;
		while (current < oldLength) {
			var nextToEncode = current;
			while (nextToEncode < oldLength
				&& initFunctin.isAllowed(s.charAt(nextToEncode), allow)) {
				nextToEncode++;
			}

			if (nextToEncode == oldLength) {
				if (current == 0) {
					return s;
				} else {
					encoded += s.substring(current, oldLength);
					return encoded;
				}
			}

			if (encoded == null) {
				encoded = '';
			}

			if (nextToEncode > current) {
				encoded += s.substring(current, nextToEncode);
			} else {
				// assert nextToEncode == current
			}

			current = nextToEncode;
			var nextAllowed = current + 1;
			while (nextAllowed < oldLength
				&& !initFunction.isAllowed(a.charAt(nextAllowed), allow)) {
				nextAllowed++;
			}

			var toEncode = s.substring(current, nextAllowed);
			var bytes = toEncode.split('').map(function(s){return s.charCodeAt(0);});
			var bytesLength = bytes.length;
			for (var i=0; i<bytesLength; i++) {
				encoded += ('%' + HEX_DIGITS[(bytes[i] & 0xf0) >> 4]);
				encoded += (HEX_DIGITS[bytes[i] & 0xf]);
			}
			current = nextAllowed;
		}
		return encoded == null ? s : encoded;
	};

	initFunction.isAllowed(c, allow) {
		return (c >= 'A' && c <= 'Z')
			|| (c >= 'a' && c <= 'z')
			|| (c >= '0' && c <= '9')
			|| "_-!.~'()*".indexOf(c) != NOT_FOUND
			|| (allow != null && allow !== undefined && allow.indexOf(c) != NOT_FOUND)
	};

	initFunction.decode = function(s) {
		if (s == null) {
			return null;
		}
		return decodeURIComponent(s);
	};

	initFunction.withAppendedPath = function(baseUri, pathSegment) {
		var builder = baseUri.buildUpon();
		builder = builder.appendEncodedPath(pathSegment);
		return builder.build();
	};

	var AbstractPart = klass(Object, {
		__construct: function(encoded, decoded) {
			this.encoded = encoded;
			this.decoded = decoded;
		},
	    	getDecoded: function() {
			var hasDecoded = this.decoded != NOT_CACHED;
			return hasDecoded ? this.decoded : (this.decoded = initFunction.decode(this.encoded));
		},
	});
	AbstractPart.Representation = {
		BOTH: 0,
		ENCODED: 1,
		DECODED: 2
	};

	var Part = klass(AbstractPart, {
		__construct(encoded, decoded) {
			Part.uber._construct.apply(this, [encoded, decoded]);
		},
	    	isEmpty: function() {
			return false;
		},
	    	getEncoded: function() {
			var hasEncoded = this.encoded != NOT_CACHED;
			return hasEncoded ? this.encoded : (this.encoded = initFunction.encode(this.decoded));
		},
	});
	Part.nonNull = function(part) {
		return part == null ? NULL : part;
	};
	Part.fromEncoded = function(encoded) {
		return Part.from(encoded, NOT_CACHED);
	};
	Part.fromDecoded = function(decoded) {
		return Part.from(NOT_CACHED, decoded);
	};
	Part.from = function(encoded, decoded) {
		if (encoded == null) {
			return NULL;
		}
		if (encoded.length == 0) {
			return EMPTY;
		}
		if (decoded == null) {
			return NULL;
		}
		if (decoded.length == 0) {
			return EMPTY;
		}
		return new Part(encoded, decoded);
	};
	Part.EmptyPart = klass(Part, {
		__construct: function(value) {
			EmptyPart.uber.__construct.apply(this, [value,value]);
		},
		isEmpty: function() {
			return true;
		}
	});

	var PathPart = klass(AbstractPart, {
		__construct: function(encoded, decoded) {
			PathPart.uber.__construct.apply(this, [encoded,decoded]);
			this.pathSegments = null;
		},
	    	getEncoded: function() {
			val hasEncoded = this.encoded != NOT_CACHED;
			return hasEncoded ? this.encoded : (this.encoded = initFunction.encode(this.decoded, '/'));
		},
	    	getPathSegments: function() {
			if (this.pathSegments != null) {
				return this.pathSegments;
			}
			var path = this.getEncoded();
			if (path == null) {
				return (this.pathSegments = PathSegments.EMPTY);
			}

			var segmentBuilder = new PathSegmentsBuilder();

			var previous = 0;
			var current;
			while ((current = path.indexOf('/', previous)) > -1) {
				if (previous < current) {
					var decodedSegment = initFunction.decode(path.substring(previous, current));
					segmentBuilder.add(decodedSegment);
				}
				previous = current + 1;
			}

			if (previous < path.length) {
				segmentBuilder.add(initFunction.decode(path.substring(previous)));
			}
			return (this.pathSegments = segmentBuilder.build());
		}
	});
	PathPart.NULL = new PathPart(null, null);
	PathPart.EMPTY = new PathPart("","");

	PathPart.appendEncodedSegment = function(oldPart, newSegment) {
		if (oldPart == null) {
			return PathPart.fromEncoded("/" + newSegment);
		}
		var oldPath = oldPart.getEncoded();

		if (oldPath == null) {
			oldPath = "";
		}

		var oldPathLength = oldPath.length;
		var newPath;
		if (oldPathLength == 0) {
			newPath = "/" + newSegment;
		} else if (oldPath.charAt(oldPathLength-1) == '/') {
			newPath = oldPath + newSegment;
		} else {
			newPath = oldPath + '/' + newSegment;
		}

		return PathPart.fromEncoded(newPath);
	};

	PathPart.appendDecodedSegment = function(oldPart, decoded) {
		var encoded = initFunction.encode(decoded);

		return PathPart.appendEncodedSegment(oldPart, encoded);
	};

	PathPart.fromEncoded = function(encoded) {
		return PathPart.from(encoded, NOT_CACHED);
	};

	PathPart.fromDecoded = function(decoded) {
		return PathPart.from(NOT_CACHED, decoded);
	};

	PathPart.from = function(encoded, decoded) {
		if (encoded == null) {
			return NULL;
		}

		if (encoded.length == 0) {
			return EMPTY;
		}

		return new PathPart(encoded, decoded);
	};

	PathPart.makeAbsolute = function(oldPart) {
		var encodedCached = oldPart.encoded != NOT_CACHED;

		var oldPath = encodedCached ? oldPart.encoded : oldPart.decoded;

		if (oldPath == null || oldPath.length == 0
			|| oldPath.startsWith('/')) {

			return oldPath;
		}

		var newEncoded = encodedCached ? ("/" + oldPart.encoded) : NOT_CACHED;

		var decodedCached = oldPart.decoded != NOT_CACHED;
		var newDecoded = decodedCached ? ("/" + oldPart.decoded) : NOT_CACHED;

		return new PathPart(newEncoded, newDecoded);
	};

	var OpaqueUri = klass(Uri, {
		__construct: function(scheme, ssp, fragment) {
			this.scheme = scheme;
			this.ssp = ssp;
			this.fragment = fragment == null ? Part.NULL : fragment;
			this.cachedString = NOT_CACHED;
		},
	    	describeContents: function() {
			return 0;
		},
	    	isHierarchical: function() {
			return false;
		},
	    	isRelative: function() {
			return this.scheme == null;
		},
	    	getScheme: function() {
			return this.scheme;
		},
	    	getEncodedSchemeSpecificPart: function() {
			return this.ssp.getEncoded();
		},
	    	getSchemeSpecificPart: function() {
			return this.ssp.getDecoded();
		},
		getAuthority: function() {
			return null;
		},
		getEncodedAuthority: function() {
			return null;
		},
		getPath: function() {
			return null;
		},
		getEncodedPath: function() {
			return null;
		},
		getQuery: function() {
			return null;
		},
		getEncodedQuery: function() {
			return null;
		},
		getFragment: function() {
			return this.fragment.getDecoded();
		},
		getEncodedFragment: function() {
			return this.fragment.getEncoded();
		},
		getPathSegments: function() {
			return [];
		},
		getLastPathSegment: function() {
			return null;
		},
		getUserInfo: function() {
			return null;
		},
		getEncodedUserInfo: function() {
			return null;
		},
		getHost: function() {
			return null;
		},
		getPort: function() {
			return -1;
		},
		toString: function() {
			var cached = this.cachedString != NOT_CACHED;
			if (cached) {
				return this.cachedString;
			}

			var sb = '';
			sb += this.scheme + ':';
			sb += this.getEncodedSchemeSpecificPart();

			if ( ! this.fragment.isEmpty()) {
				sb += '#' + this.fragment.getEncoded();
			}
			return (this.cachedString = sb);
		},
		buildUpon: function() {
			return new Builder()
				.scheme(this.scheme)
				.opaquePart(this.ssp)
				.fragment(this.fragment);
		}
	});

	var StringUri = klass(AbstractHierarchicalUri, {
		__construct: function(uriStringParam) {
			if (!uriStringParam) {
				throw new Error('uriString');
			}
			this.uriString = uriStringParam;
			this.cachedSsi = NOT_CALCULATED;
			this.cachedFsi = NOT_CALCULATED;
			this.scheme = NOT_CACHED;
			this.ssp = null;
			this.authority = null;
			this.path = null;
			this.query = null;
			this.fragment = null;
		},
	    	describeContents: function() {
			return 0;
		},
	    	findSchemeSeparator: function() {
			return this.cachedSsi == NOT_CALCULATED
				? this.cachedSsi = this.uriString.indexOf(':')
				: this.cachedSsi;
		},
	    	findFragmentSeparator: function() {
			return this.cachedFsi == NOT_CALCULATED
				? this.cachedFsi = this.uriString.indexOf('#', this.findSchemeSeparator())
				: this.cachedFsi;
		},
		isHierarchical: function() {
			var ssi = this.findSchemeSeparator();

			if (ssi == NOT_FOUND) {
				// All relative URIs are hierarchical
				return true;
			}

			if (this.uriString.length == ssi + 1) {
				// no ssp
				return false;
			}

			// if the ssp starts with a '/', this is hierarchical
			return this.uriString.charAt(ssp + 1) == '/';
		},
		isRelative: function() {
			// note: we return true if the index is 0
			return this.findSchemeSeparator() == NOT_FOUND;
		},
		getScheme: function() {
			var cached = (this.scheme != NOT_CACHED);
			return cached ? this.scheme : (scheme = this.parseScheme());
		},
		parseScheme: function() {
			var ssi = this.findSchemeSeparator();
			return ssi == NOT_FOUND ? null : this.uriString(0, ssi);
		},
		getSsp: function() {
			return this.ssp == null ? this.ssp = Part.fromEncoded(this.parseSsp()): this.ssp;
		},
		getEncodedSchemeSpecificPart: function() {
			return this.getSsp().getEncoded();
		},
		getSchemeSpecificPart: function() {
			return this.getSsp().getDecoded();
		},
		parseSsp: function() {
			var ssi = this.findSchemeSeparator();
			var fsi = this.findFragmentSeparator();

			// return everything between ssi and fsi
			return fsi == NOT_FOUND 
				? this.uriString.substring(ssi + 1)
				: this.uriString.substring(ssi + 1, fsi);
		},
		getAuthorityPart: function() {
			if (this.authority == null) {
				var encodedAuthority = this.parseAuthority(this.uriString, this.findSchemeSeparator());
				return (this.authority = Part.fromEncoded(encodedAuthority));
			}
			return this.authority;
		},
		getEncodedAuthority: function() {
			return this.getAuthorityPart().getEncoded();
		},
		getAuthority: function() {
			return this.getAuthorityPart().getDecoded();
		},
		getPathPart: function() {
			return this.path == null
				? this.path = PathPart.fromEncoded(this.parsePath())
				: this.path;
		},
		getPath: function() {
			return this.getPathPart().getDecoded();
		},
		getEncodedPath: function() {
			return this.getPathPart().getEncoded();
		},
		getPathSegments: function() {
			return this.getPathPart().getPathSegments();
		},
		parsePath: function() {
			var uriString = this.uriString;
			var ssi = this.findSchemeSeparator();

			// if the uri is absolute
			if (ssi == -1) {
				// is there anything after the ':'?
				var schemeOnly = ssi + 1 == uriString.length;
				if (schemeOnly) {
					// Opaque URI
					return null;
				}

				// A '/' after the ':' means this is hierarchical
				if (uriString.charAt(ssi + 1) != '/') {
					// Opaque URI
					return null;
				}
			} else {
				// All relative URIs are hierarchical
			}
			return this.parsePath(uriString, ssi);
		},
		getQueryPart: function() {
			return this.query == null
				? this.query = Part.fromEncoded(this.parseQuery()) : this.query;
		},
		getEncodedQuery: function() {
			return this.getQueryPart().getEncoded();
		},
		parseQuery: function() {
			// It doesn't make sense to cache this index. We only ever
			// calculate it once
			var qsi = this.uriString.indexOf('?', this.findSchemeSeparator);
			if (qsi == NOT_FOUND) {
				return null;
			}

			var fsi = this.findFragmentSeparator();

			if (fsi == NOT_FOUND) {
				return this.uriString.substring(qsi + 1);
			}

			if (fsi < qsi) {
				// Invalid
				return null;
			}
			return this.uriString.substring(qsi + 1, fsi);
		},
		getQuery: function() {
			return this.getQueryPart().getDecoded();
		},
		getFragmentPart: function() {
			return this.fragment == null
				? this.fragment = Part.fromEncoded(this.parseFragment()) : this.fragment;
		},
		getEncodedFragment: function() {
			return this.getFragmentPart().getEncoded();
		},
		parseFragment: function() {
			var fsi = this.findFragmentSeparator();
			return fsi == NOT_FOUND ? null : this.uriString.substring(fsi + 1);
		},
		getFragment: function() {
			return this.getFragmentPart().getDecoded();
		},
		toString: function() {
			return this.uriString;
		},
		buildUpon: function() {
			if (this.isHierarchical()) {
				return new Builder()
					.scheme(this.getScheme())
					.authority(this.getAuthorityPart())
					.path(this.getPathPart())
					.query(this.getQueryPart())
					.fragment(this.getFragmentPart());
			} else {
				return new Builder()
					.scheme(this.getScheme())
					.opaquePart(this.getSsp())
					.fragment(this.getFragmentPart());
			}
		}
	});
	StringUri.TYPE_ID = 1;
	StringUri.parseAuthority = function(uriString, ssi) {
		var length = uriString.length;

		// If '//' follows the scheme separator, we have an authority
		if (length > ssi + 2
			&& uriString.charAt(ssi + 1) == '/'
			&& uriString.charAt(ssi + 2) == '/') {
			// We have an authority

			// Look for the start of the path, query, or fragment, or the
			// end of the string.
			var end = ssi + 3;
			LOOP: while (end < length) {
				switch (uriString.charAt(end)) {
					case '/': // Start of path
					case '?': // Start of query
					case '#': // Start of fragment
						break LOOP;
				}
				end++;
			}

			return uriString.substring(ssi + 3, end);
		} else {
			return null;
		}
	};

	StringUri.parsePath = function(uriString, ssi) {
		var length = uriString.length;

		// Find start of path
		var pathStart;
		if (length > ssi + 2
			&& uriString.charAt(ssi + 1) == '/'
			&& uriString.charAt(ssi + 2) == '/') {
			// Skip over authority to path
			pathStart = ssi + 3;
			LOOP: while (pathStart < length) {
				switch (uriString.charAt(pathStart)) {
					case '?': // Start of query
					case '#': // Start of fragment
						return ''; // Empty path
					case '/': // Start of path
						break LOOP;
				}
				pathStart++;
			}
		} else {
			// Path starts immediately after scheme separator
			pathStart = ssi + 1;
		}

		var pathEnd = pathStart;
		LOOP: while (pathEnd < length) {
			switch (uriString.charAt(pathEnd)) {
				case '?': // Start of query
				case '#': // Start of fragment
					break LOOP;
			}
			pathEnd++;
		}
		return uriString.substring(pathStart, pathEnd);
	};
	var PathSegments = klass(AbstractList, {
		__construct: function(segments, size) {
			this.segments = segments;
			this.size = size;
		},
	    	get: function(index) {
			if (index >= this.size) {
				throw new RangeError('index out of bounds');
			}
			return this.segments[index];
		},
	    	size: function() {
			return this.size;
		}
	});
	PathSegments.EMPTY = new PathSegments(null, 0);

	var PathSegmentsBuilder = function PathSegmentsBuilderInitFunction(){
		this.segments = null;
		this.size = 0;
	};
	PathSegmentsBuilder.prototype = {
		add: function(segment) {
			if (this.segments == null) {
				this.segments = [];
			}
			this.segments[size++] = segment;
		},
		build: function() {
			if (this.segments == null) {
				return PathSegments.EMPTY;
			}
			return new PathSegments(this.segments, this.size);
		}
	};

	var AbstractHierarchicalUri = klass(Uri, {
		__construct: function() {
			this.userInfo = null;
			this.host = NOT_CACHED;
			this.port = NOT_CALCULATED;
		},
		getLastPathSegment: function() {
			var segments = this.getPathSegments();
			var size = segments.size();
			if (size == 0) {
				return null;
			}
			return segments.get(size - 1);
		},
	    	getUserInfoPart: function() {
			return this.userInfo == null
				? this.userInfo = Part.fromEncoded(this.parseUserInfo()) : this.userInfo;
		},
	    	getEncodedUserInfo: function() {
			return this.getUserInfoPart().getEncoded();
		},
	    	parseUserInfo: function() {
			var authority = this.getEncodedAuthority();
			if (authority == null) {
				return null;
			}
			var end = authority.indexOf('@');
			return end == NOT_FOUND ? null : authority.substring(0, end);
		},
		getUserInfo: function() {
			return this.getUserInfoPart().getDecoded();
		},
		getHost: function() {
			var cached = (this.host != NOT_CACHED);
			return cached ? this.host : (this.host = this.parseHost());
		},
		parseHost: function() {
			var authority = this.getEncodedAuthority();
			if (authority == null) {
				return null;
			}

			var userInfoSeparator = authority.indexOf('@');
			var portSeparator = authority.indexOf(':', userInfoSeparator);

			var encodedHost = portSeparator == NOT_FOUND
				? authority.substring(userInfoSeparator+1)
				: authority.substring(userInfoSeparator+1, portSeparator);

			return initFunction.decode(encodedHost);
		},
		getPort: function() {
			return this.port == NOT_CALCULATED
				? this.port = this.parsePort()
				: this.port;
		},
		parsePort: function() {
			var authority = this.getEncodedAuthority();
			if (authority == null) {
				return -1;
			}
			var userInfoSeparator = authority.indexOf('@');
			var portSeparator = authority.indexOf(':', userInfoSeparator);

			if (portSeparator == NOT_FOUND) {
				return -1;
			}

			var portString = initFunction.decode(authority.substring(portSeparator+1));
			var parsed = parseInt(portString);
			if (parsed == NaN) {
				return -1;
			}
			return parsed;
		}
	});
	
	var HierarchicalUri = klass(AbstractHierarchicalUri, {
		__construct: function(scheme, authority, path, query, fragment) {
			this.scheme = scheme;
			this.authority = Part.nonNull(authority);
			this.path = path == null ? PathPart.NULL : path;
			this.query = Part.nonNull(query);
			this.fragment = Part.nonNull(fragment);
			this.ssp = null;
			this.uriString = NOT_CACHED;
		},
	    	describeContents: function() {
			return 0;
		},
	    	isHierarchical: function() {
			return true;
		},
	    	isRelative: function() {
			return this.scheme == null;
		},
	    	getScheme: function() {
			return this.scheme;
		},
		getSsp: function() {
			return this.ssp == null
				? this.ssp = Part.fromEncoded(this.makeSchemeSpecificPart()) : ssp;
		},
		getEncodedSchemeSpecificPart: function() {
			return this.getSsp().getEncoded();
		},
		getSchemeSpecificPart: function() {
			return this.getSsp().getDecoded();
		},
		makeSchemeSpecificPart: function() {
			var result = '';
			this.appendSspTo(result);
			return result;
		},
		appendSspTo: function(str) {
			var encodedAuthority = this.authority.getEncoded();
			if (encodedAuthority != null) {
				str += ('//' + encodedAuthority);
			}
			var encodedPath = this.path.getEncoded();
			if (encodedPath != null) {
				str += encodedPath;
			}

			if ( ! this.query.isEmpty()) {
				str += ('?' + this.query.getEncoded());
			}
		},
		getAuthority: function() {
			return this.authority.getDecoded();
		},
		getDecodedAuthority: function() {
			return this.authority.getEncoded();
		},
		getEncodedPath: function() {
			return this.path.getEncoded();
		},
		getPath: function() {
			return this.path.getDecoded();
		},
		getEncodedQuery: function() {
			return this.query.getEncoded();
		},
		getQuery: function() {
			return this.query.getDecoded();
		},
		getEncodedFragment: function() {
			return this.fragment.getEncoded();
		},
		getFragment: function() {
			return this.fragment.getDecoded();
		},
		getPathSegments: function() {
			return this.path.getPathSegments();
		},
		toString: function() {
			var cached = (this.uriString != NOT_CACHED);
			return cached ? this.uriString : (this.uriString = this.makeUriString());
		},
		makeUriString: function() {
			var result = '';
			if (this.scheme != null) {
				result += (scheme + ':');
			}
			this.appendSspTp(result);
			if ( ! this.fragment.isEmpty()) {
				result += ('#' + this.fragment.getEncoded());
			}
			return result;
		},
		buildUpon: function() {
			return new Builder()
				.scheme(this.scheme)
				.authority(this.authority)
				.path(this.path)
				.query(this.query)
				.fragment(this.fragment);
		}
	});
	HierarchicalUri.TYPE_ID = 3;

	var Builder = klass(Object, {
		__construct: function(scheme) {
			this.scheme = scheme;
		},
	    	opaquePart: function(opaquePart) {
			if ( ! (opaquePart instanceof Part)) {
				opaquePart = Part.fromDecoded(opaquePart);
			}
			this.opaquePart = opaquePart;
			return this;
		},
	    	encodedOpaquePart: function(opaquePart) {
			return this.opaquePart(Part.fromEncoded(opaquePart));
		},
	    	authority: function(authority) {
			if ( ! (authority instanceof Part)) {
				authority = Part.fromDecoded(authority);
			}
			this.opaquePart = null;

			this.authority = authority;
			return this;
		},
		encodedAuthority: function(authority) {
			return this.authority(Part.fromEncoded(authority));
		},
		path: function(path) {
			if ( ! (path instanceof PathPart)) {
				path = PathPart.fromDecoded(path);
			}
			this.opaquePart = null;

			this.path = path;
			return this;
		},
		encodedPath: function(path) {
			return this.path(PathPart.fromEncoded(path));
		},
		appendPath: function(newSegment) {
			return this.path(PathPart.appendDecodedSegment(path, newSegment));
		},
		appendEncodedPath: function(newSegment) {
			return this.path(PathPart.appendEncodedSegment(path, newSegment));
		},
		query: function(query) {
			if ( ! (query instanceof Part)) {
				query = Part.fromDecoded(query);
			}
			this.opaquePart = null;

			this.query = query;
			return this;
		},
		encodedQuery: function(query) {
			return this.query(Part.fromEncoded(query));
		},
		fragment: function(fragment) {
			if ( ! (fragment instanceof Part)) {
				fragment = Part.fromDecoded(fragment);
			}
			this.fragment = fragment;
			return this;
		},
		encodedFragment: function(fragment) {
			return this.fragment(Part.fromEncoded(fragment));
		},
		appendQueryParameter: function(key, value) {
			this.opaquePart = null;
			var encodedParameter = initFunction.encode(key, null) + '='
				+ initFunction.encode(value, null);

			if (this.query == null) {
				this.query = Part.fromEncoded(encodedParameter);
				return this;
			}
			var oldQuery = this.query.getEncoded();
			if (oldQuery == null || oldQuery.length() == 0) {
				this.query = Part.fromEncoded(encodedParameter);
			} else {
				this.query = Part.fromEncoded(oldQuery + '&' + encodedParameter);
			}
			return this;
		},
		clearQuery: function() {
			return this.query(null);
		},
		build: function() {
			if (this.opaquePart != null) {
				if (this.scheme == null) {
					throw new Error('An opaque URI must have a scheme');
				}
				return new OpaqueUri(this.scheme, this.opaquePart, this.fragment);
			} else {
				var path = this.path;
				if (path == null || path == PathPart.NULL) {
					path = PathPart.EMPTY;
				} else {
					if (this.hasSchemeOrAuthority()) {
						path = PathPart.makeAbsolute(path);
					}
				}
				return new HierarchicalUri(this.scheme, this.authority, this.path, this.query, this.fragment);
			}
		},
		hasSchemeOrAuthority: function() {
			return this.scheme != null || (this.authority != null && this.authority != Part.NULL);
		},
		toString: function() {
			return this.build().toString();
		}
	});

	return initFunction;
})();
