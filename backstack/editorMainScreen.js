
var EditorMainScreen = (function() {
	var initFunction = function EditorMainScreenInitFunction(textareaParam) {
		var stack = [];
		var textarea = textareaParam;
		stack.push(textarea);
		var parent = textarea.parentNode;
		var self = this;
		self.show = function(element) {
			stack[stack.length-1].style.display = 'none';
			stack.push(element);
			parent.appendChild(element);
		};
		self.destroy = function() {
			if (stack.length == 1) {return;}
			var element = stack.pop();
			stack[stack.length-1].style.display = 'inline-block';
			parent.removeChild(element);
		};
		self.showTextarea = function() {
			while(stack.length > 1) {
				var element = stack.pop();
				parent.removeChild(element);
			}
			textarea.style.display = 'inline-block';
		};
		self.isShowing = function(element) {
			return element == stack[stack.length-1];
		};
	};
	return initFunction;
})();
