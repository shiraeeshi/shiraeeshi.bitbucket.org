var PackageManagerService = (function() {
	var initFunction = function PackageManagerServiceInitFunction() {
		var self = this;
		var ActivityIntentResolver = klass(IntentResolver, {
			__construct: function() {
				this.mActivities = {};
				this.mFlags = 0;
			},
			queryIntent: function(intent, resolvedType, defaultOnly_or_flags) {
				var flags, defaultOnly;
				if (typeof(defaultOnly_or_flags) == 'number') {
					flags = defaultOnly_or_flags;
					defaultOnly = ((flags & PackageManager.MATCH_DEFAULT_ONLY) != 0);
				} else {
					defaultOnly = defaultOnly_or_flags;
					flags = defaultOnly ? PackageManager.MATCH_DEFAULT_ONLY : 0;
				}
				this.mFlags = flags;
				return ActivityIntentResolver.uber.queryIntent.call(this, intent, resolvedType, defaultOnly);
			},
			queryIntentForPackage: function(intent, resolvedType, flags, packageActivities) {
				if (packageActivities == null) {
					return null;
				}
				this.mFlags = flags;
				var defaultOnly = ((flags & PackageManager.MATCH_DEFAULT_ONLY) != 0);
				var listCut = [];
				for (var i=0, len=packageActivities.length; i<len; i++) {
					var intentFilters = packageActivities[i].intents;
					if (intentFilters && intentFilters.length > 0) {
						var array = [].concat(intentFilters);
						listCut.push(push);
					}
				}
				return ActivityIntentResolver.uber.queryIntentFromList.call(this, intent, resolvedType, defaultOnly, listCut);
			},
			addActivity: function(a, type) {
				this.mActivities[a.getComponentName()] = a;
				for (var i=0, len=a.intents.length; i<len; i++) {
					var intent = a.intents[i];
					ActivityIntentResolver.uber.addFilter.call(this, intent);
				}
			},
			removeActivity: function(a, type) {
				delete this.mActivities[a.getComponentName()];
				for (var i=0, len=a.intents.length; i<len; i++) {
					var intent = a.intents[i];
					ActivityIntentResolver.uber.removeFilter.call(this, intent);
				}
			},
			allowFilterResult: function(filter, dest) {
				var filterAi = filter.activity.info;
				for (var i=dest.length-1; i>=0; i--) {
					var destAi = dest[i].activityInfo;
					if (destAi.name == filterAi.name && destAi.packageName == filterAi.packageName) {
						return false;
					}
				}
				return true;
			},
			newArray: function(size) {
				return new Array(size);
			},
			isFilterStopped: function(filter) {
				var pakage = filter.activity.owner;
				if (pakage == null) {
					return false;
				}
				var packageSetting = pakage.mExtras;
				if (packageSetting == null) {
					return false;
				}
				return packageSetting.getStopped();
			},
			isPackageForFilter: function(packageName, info) {
				return packageName == info.activity.owner.packageName;
			},
			newResult: function(info, match) {
				var activity = info.activity;
				var packageSetting = activity.owner.mExtras;
				if (packageSetting == null) {
					return null;
				}
				var ai = PackageParser.generateActivityInfo(activity, this.mFlags);
				if (ai == null) {
					return null;
				}
				var resolveInfo = new ResolveInfo();
				resolveInfo.activityInfo = ai;
				if ((this.mFlags & PackageManager.GET_RESOLVED_FILTER) != 0) {
					resolveInfo.filter = info;
				}
				resolveInfo.preferredOrder = activity.owner.mPreferredOrder;
				resolveInfo.match = match;
				resolveInfo.isDefault = info.hasDefault;
				resolveInfo.labelRes = info.labelRes;
				resolveInfo.nonLocalizedLabel = info.nonLocalizedLabel;
				return resolveInfo;
			},
			sortResults: function(results) {}
		});
		var mActivities = new ActivityIntentResolver();
		var mAndroidApplication; // ApplicationInfo
		var mSimplifiedJSLogicIntentResolver; // TODO implement its class
		var chooseBestActivity = function(intent, resolvedType, flags, query) {
// TODO ported to JavaScript with simplified logic. rewrite if needed. 
			if (!query || query.length == 0) {return null;}
			return query[0];
		};
		var getMatchingCrossProfileIntentFilters = function(intent, resolvedType) {
// TODO ported to JavaScript with simplified logic. rewrite if needed. 
			return mSimplifiedJSLogicIntentResolver.queryIntent(intent, resolvedType, false);
		};

		var queryCrossProfileIntents = function(matchingFilters, intent, resolvedType, flags) {
// TODO ported to JavaScript with simplified logic. rewrite if needed. 
			if (matchingFilters == null) {
				return null;
			}
			for (var i=0, len=matchingFilters.len; i<len; i++) {
				var filter = matchingFilters[i];
				var resolveInfo = checkTargetCanHandle(filter, intent, resolvedType, flags);
				if (resolveInfo != null) {
					return resolveInfo;
				}
			}
			return null;
		};
		var querySkipCurrentProfileIntents = function(matchingFilters, intent, resolvedType, flags) {
			if (matchingFilters == null) {
				return null;
			}
			for (var i=0, len=matchingFilters.len; i<len; i++) {
				var filter = matchingFilters[i];
				if ((filter.getFlags() & PackageManager.SKIP_CURRENT_PROFILE) != 0) {
					var resolveInfo = checkTargetCanHandle(filter, intent, resolvedType, flags);
					if (resolveInfo != null) {
						return resolveInfo;
					}
				}
			}
			return null;
		};
		var checkTargetCanHandle = function(filter, intent, resolvedType, flags) {
			var resultTarget = mActivities.queryIntent(intent, resolvedType, flags);
			if (resultTarget != null && resultTarget.length != 0) {
				return createForwardingResolveInfo(filter);
			}
			return null;
		};
		var createForwardingResolveInfo = function(filter) {
			var forwardingResolveInfo = new ResolveInfo();
			var className;
			//if (targetUserId == UserHandle.USER_OWNER) {
			//    className = FORWARD_INTENT_TO_USER_OWNER;
			//} else {
			//    className = FORWARD_INTENT_TO_MANAGED_PROFILE;
			//}
			className = initFunction.FORWARD_INTENT_TO_USER_OWNER;
			var forwardingActivityComponentName = new ComponentName(
				mAndroidApplication.packageName, className);
			var forwardingActivityInfo = self.getActivityInfo(forwardingActivityComponentName, 0);
			//if (targetUserId == UserHandle.USER_OWNER) {
			    forwardingActivityInfo.showUserIcon = -2; // UserHandle.USER_OWNER;
			    forwardingResolveInfo.noResourceId = true;
			//}
			forwardingResolveInfo.activityInfo = forwardingActivityInfo;
			forwardingResolveInfo.priority = 0;
			forwardingResolveInfo.preferredOrder = 0;
			forwardingResolveInfo.match = 0;
			forwardingResolveInfo.isDefault = true;
			forwardingResolveInfo.filter = filter;
			//forwardingResolveInfo.targetUserId = targetUserId;
			return forwardingResolveInfo;
		};

		self.resolveIntent = function(intent, resolvedType, flags) {
			var query = self.queryIntentActivities(intent, resolvedType, flags);
			return chooseBestActivity(intent, resolvedType, flags, query);
		};
		self.queryIntentActivities = function(intent, resolvedType, flags) {
			var comp = intent.getComponent();
			if (comp == null) {
				if (intent.getSelector() != null) {
					intent = intent.getSelector();
					comp = intent.getComponent();
				}
			}
			if (comp != null) {
				var list = [];
				var ai = self.getActivityInfo(comp, flags);
				if (ai != null) {
					var ri = new ResolveInfo(); 
					ri.activityInfo = ai;
					list.push(ri);
				}
				return list;
			}
			var pkgName = intent.getPackage();
			if (pkgName == null) {
				var matchingFilters = getMatchingCrossProfileIntentFilters(intent, resolvedType);
				var resolveInfo = querySkipCurrentProfileIntents(matchingFilters, intent, resolvedType, flags);
				if (resolveInfo != null) {
					return [resolveInfo];
				}
				resolveInfo = queryCrossProfileIntents(matchingFilters, intent, resolvedType, flags);
				var result = mActivities.queryIntent(intent, resolvedType, flags);
				if (resolveInfo != null) {
					result.push(resolveInfo);
					result.sort(mResolvePrioritySorter);
				}
				return result;
			}
			var pkg = mPackages[pkgName];
			if (pkg != null) {
				return mActivities.queryIntentForPackage(intent, resolvedType, flags, pkg.activities);
			}
			return [];
		};
		self.getActivityInfo = function(component, flags) {// TODO reimplement
			var a = mActivities.mActivities.get(component);

			if (a != null && mSettings.isEnabledLPr(a.info, flags, userId)) {
				var ps = mSettings.mPackages.get(component.getPackageName());
				if (ps == null) return null;
				return PackageParser.generateActivityInfo(a, flags, ps.readUserState(userId));
			}
			if (mResolveComponentName.equals(component)) {
				return PackageParser.generateActivityInfo(mResolveActivity, flags,
					new PackageUserState());
			}
			return null;
		};
		self._js_run = function(projects, configParser) {
			var activities = PackageParser._js_parse(projects, configParser);
			for (var i=0, len=activities.length; i<len; i++) {
				mActivities.addActivity(activities[i]);
			}
		};
	};
	var mResolvePrioritySorter = function(r1, r2) {
		return 0; // ported to JavaScript with simplified logic
		var v1 = r1.priority;
		var v2 = r2.priority;
		//System.out.println("Comparing: q1=" + q1 + " q2=" + q2);
		if (v1 != v2) {
			return (v1 > v2) ? -1 : 1;
		}
		v1 = r1.preferredOrder;
		v2 = r2.preferredOrder;
		if (v1 != v2) {
			return (v1 > v2) ? -1 : 1;
		}
		if (r1.isDefault != r2.isDefault) {
			return r1.isDefault ? -1 : 1;
		}
		v1 = r1.match;
		v2 = r2.match;
		//System.out.println("Comparing: m1=" + m1 + " m2=" + m2);
		if (v1 != v2) {
			return (v1 > v2) ? -1 : 1;
		}
		if (r1.system != r2.system) {
			return r1.system ? -1 : 1;
		}
		return 0;
	};
	return initFunction;
})();
