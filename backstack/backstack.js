
var BackStack = (function() {
	var initFunction = function BackStackInitFunction() {
		var stack = [];
		this.push = function(activity) {
			stack.push(activity);
		};
		this.pop = function() {
			if (stack.length == 0) {
				throw new RangeError('cannot pop: stack is empty');
			}
			return stack.pop();
		};
	};
	return initFunction;
})();

