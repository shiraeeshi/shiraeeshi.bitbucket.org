var ActivityInfo = (function() {
	var initFunction = function ActivityInfoInitFunction(orig) {
		this.theme = 0; // int
		this.launchMode = 0; // int
		this.documentLaunchMode = 0; // int
		this.persistableMode = 0; // int
		this.maxRecents = 0; // int
		this.permission = null; // String
		this.taskAffinity = null; // String
		this.targetActivity = null; // String
		this.flags = 0; // int
		this.configChanges = 0; // int
		this.softInputMode = 0; // int
		this.uiOptions = 0; // int
		this.parentActivityName = null; // String
		this.screenOrientation = initFunction.SCREEN_ORIENTATION_UNSPECIFIED; // int
		if (orig) {
			this.theme = orig.theme;
			this.launchMode = orig.launchMode;
			this.documentLaunchMode = orig.documentLaunchMode;
			this.persistableMode = orig.persistableMode;
			this.maxRecents = orig.maxRecents;
			this.permission = orig.permission;
			this.taskAffinity = orig.taskAffinity;
			this.targetActivity = orig.targetActivity;
			this.flags = orig.flags;
			this.configChanges = orig.configChanges;
			this.softInputMode = orig.softInputMode;
			this.uiOptions = orig.uiOptions;
			this.parentActivityName = orig.parentActivityName;
			this.screenOrientation = orig.screenOrientation;
		}

		this.getRealConfigChanged = function() {
			return configChanges;
		};
		var supper = new ComponentInfo(orig);
		this.prototype = supper;
	};
	initFunction.LAUNCH_MULTIPLE = 0;
	initFunction.LAUNCH_SINGLE_TOP = 1;
	initFunction.LAUNCH_SINGLE_TASK = 2;
	initFunction.LAUNCH_SINGLE_INSTANCE = 3;
	initFunction.DOCUMENT_LAUNCH_NONE = 0;
	initFunction.DOCUMENT_LAUNCH_INTO_EXISTING = 1;
	initFunction.DOCUMENT_LAUNCH_ALWAYS = 2;
	initFunction.DOCUMENT_LAUNCH_NEVER = 3;
	initFunction.PERSIST_ROOT_ONLY = 0;
	initFunction.PERSIST_NEVER = 1;
	initFunction.PERSIST_ACROSS_REBOOTS = 2;
	initFunction.FLAG_MULTIPROCESS = 0x0001;
	initFunction.FLAG_FINISH_ON_TASK_LAUNCH = 0x0002;
	initFunction.FLAG_CLEAR_TASK_ON_LAUNCH = 0x0004;
	initFunction.FLAG_ALWAYS_RETAIN_TASK_STATE = 0x0008;
	initFunction.FLAG_STATE_NOT_NEEDED = 0x0010;
	initFunction.FLAG_EXCLUDE_FROM_RECENTS = 0x0020;
	initFunction.FLAG_ALLOW_TASK_REPARENTING = 0x0040;
	initFunction.FLAG_NO_HISTORY = 0x0080;
	initFunction.FLAG_FINISH_ON_CLOSE_SYSTEM_DIALOGS = 0x0100;
	initFunction.FLAG_HARDWARE_ACCELERATED = 0x0200;
	initFunction.FLAG_SHOW_ON_LOCK_SCREEN = 0x0400;
	initFunction.FLAG_IMMERSIVE = 0x0800;
	initFunction.FLAG_RELINQUISH_TASK_IDENTITY = 0x1000;
	initFunction.FLAG_AUTO_REMOVE_FROM_RECENTS = 0x2000;
	initFunction.FLAG_RESUME_WHILE_PAUSING = 0x4000;
	initFunction.FLAG_PRIMARY_USER_ONLY = 0x20000000;
	initFunction.FLAG_SINGLE_USER = 0x40000000;
	initFunction.FLAG_ALLOW_EMBEDDED = 0x80000000;
	initFunction.SCREEN_ORIENTATION_UNSPECIFIED = -1;
	initFunction.SCREEN_ORIENTATION_LANDSCAPE = 0;
	initFunction.SCREEN_ORIENTATION_PORTRAIT = 1;
	initFunction.SCREEN_ORIENTATION_USER = 2;
	initFunction.SCREEN_ORIENTATION_BEHIND = 3;
	initFunction.SCREEN_ORIENTATION_SENSOR = 4;
	initFunction.SCREEN_ORIENTATION_NOSENSOR = 5;
	initFunction.SCREEN_ORIENTATION_SENSOR_LANDSCAPE = 6;
	initFunction.SCREEN_ORIENTATION_SENSOR_PORTRAIT = 7;
	initFunction.SCREEN_ORIENTATION_REVERSE_LANDSCAPE = 8;
	initFunction.SCREEN_ORIENTATION_REVERSE_PORTRAIT = 9;
	initFunction.SCREEN_ORIENTATION_FULL_SENSOR = 10;
	initFunction.SCREEN_ORIENTATION_USER_LANDSCAPE = 11;
	initFunction.SCREEN_ORIENTATION_USER_PORTRAIT = 12;
	initFunction.SCREEN_ORIENTATION_FULL_USER = 13;
	initFunction.SCREEN_ORIENTATION_LOCKED = 14;
	initFunction.UIOPTION_SPLIT_ACTION_BAR_WHEN_NARROW = 1;
	initFunction.CONFIG_MCC = 0x0001;
	initFunction.CONFIG_MNC = 0x0002;
	initFunction.CONFIG_LOCALE = 0x0004;
	initFunction.CONFIG_TOUCHSCREEN = 0x0008;
	initFunction.CONFIG_KEYBOARD = 0x0010;
	initFunction.CONFIG_KEYBOARD_HIDDEN = 0x0020;
	initFunction.CONFIG_NAVIGATION = 0x0040;
	initFunction.CONFIG_ORIENTATION = 0x0080;
	initFunction.CONFIG_SCREEN_LAYOUT = 0x0100;
	initFunction.CONFIG_UI_MODE = 0x0200;
	initFunction.CONFIG_SCREEN_SIZE = 0x0400;
	initFunction.CONFIG_SMALLEST_SCREEN_SIZE = 0x0800;
	initFunction.CONFIG_DENSITY = 0x1000;
	initFunction.CONFIG_LAYOUT_DIRECTION = 0x2000;
	initFunction.CONFIG_FONT_SCALE = 0x40000000;
	initFunction.CONFIG_NATIVE_BITS = [
		Configuration.NATIVE_CONFIG_MNC,                    // MNC
		Configuration.NATIVE_CONFIG_MCC,                    // MCC
		Configuration.NATIVE_CONFIG_LOCALE,                 // LOCALE
		Configuration.NATIVE_CONFIG_TOUCHSCREEN,            // TOUCH SCREEN
		Configuration.NATIVE_CONFIG_KEYBOARD,               // KEYBOARD
		Configuration.NATIVE_CONFIG_KEYBOARD_HIDDEN,        // KEYBOARD HIDDEN
		Configuration.NATIVE_CONFIG_NAVIGATION,             // NAVIGATION
		Configuration.NATIVE_CONFIG_ORIENTATION,            // ORIENTATION
		Configuration.NATIVE_CONFIG_SCREEN_LAYOUT,          // SCREEN LAYOUT
		Configuration.NATIVE_CONFIG_UI_MODE,                // UI MODE
		Configuration.NATIVE_CONFIG_SCREEN_SIZE,            // SCREEN SIZE
		Configuration.NATIVE_CONFIG_SMALLEST_SCREEN_SIZE,   // SMALLEST SCREEN SIZE
		Configuration.NATIVE_CONFIG_DENSITY,                // DENSITY
		Configuration.NATIVE_CONFIG_LAYOUTDIR,              // LAYOUT DIRECTION
	    ];
	initFunction.activityInfoConfigToNative = function(input) {
		var output = 0;
		for (var i=0; i<initFunction.CONFIG_NATIVE_BITS.length; i++) {
		    if ((input&(1<<i)) != 0) {
			output |= initFunction.CONFIG_NATIVE_BITS[i];
		    }
		}
		return output;
	};

	return initFunction;
})();
