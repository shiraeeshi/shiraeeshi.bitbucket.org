var ApplicationInfo = (function() {
	var initFunction = function ApplicationInfoInitFunction() {
		this.taskAffinity = null; // String
		this.permission = null; // String
		this.processName = null; // String
		this.className = null; // String
		this.descriptionRes = 0;  // int
		this.theme = 0; // int
		this.manageSpaceActivityName = null;     // String
		this.backupAgentName = null; // String
		this.uiOptions = 0; // int
		this.flags = 0; // int
		this.requiresSmallestWidthDp = 0; // int
		this.compatibleWidthLimitDp = 0; // int
		this.largestWidthLimitDp = 0; // int
		this.scanSourceDir = null; // String
		this.scanPublicSourceDir = null; // String
		this.sourceDir = null; // String
		this.publicSourceDir = null; // String
		this.splitSourceDirs = null; // String[]
		this.splitPublicSourceDirs = null; // String[]
		this.resourceDirs = null; // String[]
		this.seinfo = null; // String
		this.sharedLibraryFiles = null; // String[]
		this.dataDir = null; // String
		this.nativeLibraryDir = null; // String
		this.secondaryNativeLibraryDir = null; // String
		this.nativeLibraryRootDir = null; // String
		this.nativeLibraryRootRequiresIsa = null; // boolean
		this.primaryCpuAbi = null; // String
		this.secondaryCpuAbi = null; // String
		this.uid = 0; // int
		this.targetSdkVersion = 0; // int
		this.versionCode = 0; // int
		this.enabled = true; // boolean
		this.enabledSetting = PackageManager.COMPONENT_ENABLED_STATE_DEFAULT; // int
		this.installLocation = PackageInfo.INSTALL_LOCATION_UNSPECIFIED; // int
		if (orig) {
			this.taskAffinity = orig.taskAffinity;
			this.permission = orig.permission;
			this.processName = orig.processName;
			this.className = orig.className;
			this.theme = orig.theme;
			this.flags = orig.flags;
			this.requiresSmallestWidthDp = orig.requiresSmallestWidthDp;
			this.compatibleWidthLimitDp = orig.compatibleWidthLimitDp;
			this.largestWidthLimitDp = orig.largestWidthLimitDp;
			this.scanSourceDir = orig.scanSourceDir;
			this.scanPublicSourceDir = orig.scanPublicSourceDir;
			this.sourceDir = orig.sourceDir;
			this.publicSourceDir = orig.publicSourceDir;
			this.splitSourceDirs = orig.splitSourceDirs;
			this.splitPublicSourceDirs = orig.splitPublicSourceDirs;
			this.nativeLibraryDir = orig.nativeLibraryDir;
			this.secondaryNativeLibraryDir = orig.secondaryNativeLibraryDir;
			this.nativeLibraryRootDir = orig.nativeLibraryRootDir;
			this.nativeLibraryRootRequiresIsa = orig.nativeLibraryRootRequiresIsa;
			this.primaryCpuAbi = orig.primaryCpuAbi;
			this.secondaryCpuAbi = orig.secondaryCpuAbi;
			this.resourceDirs = orig.resourceDirs;
			this.seinfo = orig.seinfo;
			this.sharedLibraryFiles = orig.sharedLibraryFiles;
			this.dataDir = orig.dataDir;
			this.uid = orig.uid;
			this.targetSdkVersion = orig.targetSdkVersion;
			this.versionCode = orig.versionCode;
			this.enabled = orig.enabled;
			this.enabledSetting = orig.enabledSetting;
			this.installLocation = orig.installLocation;
			this.manageSpaceActivityName = orig.manageSpaceActivityName;
			this.descriptionRes = orig.descriptionRes;
			this.uiOptions = orig.uiOptions;
			this.backupAgentName = orig.backupAgentName;
		}
		this.hasRtlSupport = function() {
			return (this.flags & initFunction.FLAG_SUPPORTS_RTL) == initFunction.FLAG_SUPPORTS_RTL;
		};
		this.loadDescription = function(pm) {
			if (this.descriptionRes != 0) {
			    var label = pm.getText(this.packageName, this.descriptionRes, this);
			    if (label != null) {
				return label;
			    }
			}
			return null;
		};
		this.disableCompatibilityMode = function() {
			this.flags |= (initFunction.FLAG_SUPPORTS_LARGE_SCREENS | initFunction.FLAG_SUPPORTS_NORMAL_SCREENS |
				initFunction.FLAG_SUPPORTS_SMALL_SCREENS | initFunction.FLAG_RESIZEABLE_FOR_SCREENS |
				initFunction.FLAG_SUPPORTS_SCREEN_DENSITIES | initFunction.FLAG_SUPPORTS_XLARGE_SCREENS);
		};
		this.loadDefaultIcon = function(pm) {
			if ((this.flags & initFunction.FLAG_EXTERNAL_STORAGE) != 0
				&& isPackageUnavailable(pm)) {
			    //return Resources.getSystem().getDrawable(com.android.internal.R.drawable.sym_app_on_sd_unavailable_icon);
			    return null;
			}
			return pm.getDefaultActivityIcon();
		};
		var isPackageUnavailable = function(pm) {
			try {
			    return pm.getPackageInfo(this.packageName, 0) == null;
			} catch (ex) {
			    return true;
			}
		};
		this.getApplicationInfo = function() {return this;};
		this.setCodePath = function(codePath) { this.scanSourceDir = codePath; }
		this.setBaseCodePath = function(baseCodePath) { this.sourceDir = baseCodePath; }
		this.setSplitCodePaths = function(splitCodePaths) { this.splitSourceDirs = splitCodePaths; }
		this.setResourcePath = function(resourcePath) { this.scanPublicSourceDir = resourcePath; }
		this.setBaseResourcePath = function(baseResourcePath) { this.publicSourceDir = baseResourcePath; }
		this.setSplitResourcePaths = function(splitResourcePaths) { this.splitPublicSourceDirs = splitResourcePaths; }

		this.getCodePath = function() { return this.scanSourceDir; }
		this.getBaseCodePath = function() { return this.sourceDir; }
		this.getSplitCodePaths = function() { return this.splitSourceDirs; }
		this.getResourcePath = function() { return this.scanPublicSourceDir; }
		this.getBaseResourcePath = function() { return this.publicSourceDir; }
		this.getSplitResourcePaths = function() { return this.splitSourceDirs; }
		var supper = new PackageItemInfo(orig);
		this.prototype = supper;
	};
	initFunction.FLAG_SYSTEM = 1<<0;
	initFunction.FLAG_DEBUGGABLE = 1<<1;
	initFunction.FLAG_HAS_CODE = 1<<2;
	initFunction.FLAG_PERSISTENT = 1<<3;
	initFunction.FLAG_FACTORY_TEST = 1<<4;
	initFunction.FLAG_ALLOW_TASK_REPARENTING = 1<<5;
	initFunction.FLAG_ALLOW_CLEAR_USER_DATA = 1<<6;
	initFunction.FLAG_UPDATED_SYSTEM_APP = 1<<7;
	initFunction.FLAG_TEST_ONLY = 1<<8;
	initFunction.FLAG_SUPPORTS_SMALL_SCREENS = 1<<9;
	initFunction.FLAG_SUPPORTS_NORMAL_SCREENS = 1<<10; 
	initFunction.FLAG_SUPPORTS_LARGE_SCREENS = 1<<11;
	initFunction.FLAG_RESIZEABLE_FOR_SCREENS = 1<<12;
	initFunction.FLAG_SUPPORTS_SCREEN_DENSITIES = 1<<13;
	initFunction.FLAG_VM_SAFE_MODE = 1<<14;
	initFunction.FLAG_ALLOW_BACKUP = 1<<15;
	initFunction.FLAG_KILL_AFTER_RESTORE = 1<<16;
	initFunction.FLAG_RESTORE_ANY_VERSION = 1<<17;
	initFunction.FLAG_EXTERNAL_STORAGE = 1<<18;
	initFunction.FLAG_SUPPORTS_XLARGE_SCREENS = 1<<19;
	initFunction.FLAG_LARGE_HEAP = 1<<20;
	initFunction.FLAG_STOPPED = 1<<21;
	initFunction.FLAG_SUPPORTS_RTL = 1<<22;
	initFunction.FLAG_INSTALLED = 1<<23;
	initFunction.FLAG_IS_DATA_ONLY = 1<<24;
	initFunction.FLAG_IS_GAME = 1<<25;
	initFunction.FLAG_FULL_BACKUP_ONLY = 1<<26;
	initFunction.FLAG_HIDDEN = 1<<27;
	initFunction.FLAG_CANT_SAVE_STATE = 1<<28;
	initFunction.FLAG_FORWARD_LOCK = 1<<29;
	initFunction.FLAG_PRIVILEGED = 1<<30;
	initFunction.FLAG_MULTIARCH  = 1 << 31;
	return initFunction;
})();
