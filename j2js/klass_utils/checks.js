
var memberIsVisibleFor = function(classInitFunction, member, invoker) {
	if (member.isPublic()) return true;
	if (areTheSame(invoker, classInitFunction)) return true;
	if (areNested(invoker, classInitFunction)) return true;// TODO static nested class' instance cannot see instance members of outer class. if invoker is non-static nested class of Middle, and Middle is static nested class of Outer, then invoker's instance cannot see Outers instance members (cannot see Outer.this, but can see Middle.this)
	if (areInTheSamePackage(invoker, classInitFunction)) {
		return member.isProtected() || member.isPackageVisible();
	}
	if (isSubclass(invoker, classInitFunction)) {
		return member.isProtected();
	}
	return false;
};
var areTheSame = function(invoker, provider) {
	return invoker == provider;
};
var areNested = function(invoker, provider) {
	if ( ! invoker) return false;
	if ( ! areInTheSamePackage(invoker, provider)) return false;
	
	var invokerClass = invoker.prototype.__clazz;
	var providerClass = provider.prototype.__clazz;

	return invokerClass.isNestedIn(providerClass) ||
		providerClass.isNestedIn(invokerClass);
};
var isSubclass = function(invoker, provider) {
	return invoker && provider.prototype.isPrototypeOf(invoker.prototype);
};
var areInTheSamePackage = function(invoker, provider) {
	return invoker && provider.prototype.__clazz.package == invoker.prototype.__clazz.package;
};

module.exports = {};
module.exports.memberIsVisibleFor = memberIsVisibleFor;
module.exports.areTheSame = areTheSame;
module.exports.areNested = areNested;
module.exports.areInTheSamePackage = areInTheSamePackage;
module.exports.isSubclass = isSubclass;
