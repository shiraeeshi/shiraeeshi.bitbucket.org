var window = global;

var withInstanceContext = require('./context.js').withInstanceContext;

var callback = function(instance, callbackFunc) { // must be global
	var invoker = window['globalJ2JS'].invoker;
	var className = invoker.prototype.__clazz.name; // TODO dollar signs in nested classes
	var staticContext = window[className];
	return function() {
		var args = arguments;
		withInstanceContext(instance, invoker.prototype.__clazz, staticContext, invoker, function() {
			callbackFunc.apply(instance, args);
		});
	};
};

module.exports = callback;
