central thread manager that creates threads and directly communicates with them. 
all threads instead of mutating objects in their own sandboxes send a message to the manager "mutate that object for me".
reading object properties is done through messages too.
how to uniquely identify each object?
the manager becomes a bottleneck.
use transferable objects as locks.
