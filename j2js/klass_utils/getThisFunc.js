var window = global;

var getUberFunc = require('./getUberFunc.js');
var Visibility = require('../Visibility.js');

var checks = require('./checks');

var areTheSame = checks.areTheSame;
var areInTheSamePackage = checks.areInTheSamePackage;
var isSubclass = checks.isSubclass;

var getThisFunc = function(instance, invoker, provider) {
	return function() {
		var nesterInstance = globalJ2JS.nesterInstance;
		if (nesterInstance) {
			delete globalJ2JS.nesterInstance;
		}
		window['uber'] = getUberFunc(instance, provider, nesterInstance);
		window['This'] = function() {
			var constr = findConstr(provider, provider, arguments);
			runConstr(constr, instance, arguments);
		};
		var constr = findConstr(invoker, provider, arguments);
		runConstr(constr, instance, arguments);
		delete window['uber'];
		delete window['This'];
	};
};

module.exports = getThisFunc;

var runConstr = function(constr, instance, args) {
	var calleeName = globalJ2JS.calleeNames.constructor.str;
	var oldCalleeName = globalJ2JS.calleeName;
	if (oldCalleeName == globalJ2JS.calleeNames.constructor) {
		constr.apply(instance, args);
		return;
	}
	globalJ2JS.calleeName = globalJ2JS.calleeNames.constructor;
	instance.__j2js.anons[calleeName] = {};
	var uninject = function() {
		globalJ2JS.calleeName = oldCalleeName;
		delete instance.__j2js.anons[calleeName]; // safe to delete (will not run second time)
	};
	try {
		constr.apply(instance, args);
	} catch(e) {
		uninject();
		throw e;
	}
	uninject();
};

var findConstr = function(invoker, provider, args) {
	var constructorsByVisibility = provider.prototype.__clazz.members.instance.constructors;
	var constructors = [];
	var addConstructors = function(visibility, array) {
		Array.prototype.splice.apply(array, [array.length, 0].concat(constructorsByVisibility[visibility]));
	};
	if (areTheSame(invoker, provider)) {
		// add all constructors
		addConstructors(Visibility.PUBLIC, constructors);
		addConstructors(Visibility.PROTECTED, constructors);
		addConstructors(Visibility.PRIVATE, constructors);
		addConstructors(Visibility.DEFAULT, constructors);
	} else {
		if (areInTheSamePackage(invoker, provider)) {
			// add protected constructors
			// add package-visible constructors
			addConstructors(Visibility.PROTECTED, constructors);
			addConstructors(Visibility.DEFAULT, constructors);
		}
		else if (isSubclass(invoker, provider)) {
			// add protected constructors
			addConstructors(Visibility.PROTECTED, constructors);
		}
		// add public constructors
		addConstructors(Visibility.PUBLIC, constructors);
	}
	var suitableConstructors = [];
	for (var i=0; i<constructors.length; i++) {
		var constr = constructors[i];
		if (constr.isSuitableFor(args)) {
			suitableConstructors.push(constr.body);
		}
	}
	if (suitableConstructors.length == 0) {
		var argsString = Array.prototype.join.call(args);
		throw new Error('cannot find suitable constructor for args ['+argsString+']');
	}
	if (suitableConstructors.length > 1) {
		var argsString = Array.prototype.join.call(args);
		throw new Error('constructors are ambiguous for args ['+argsString+']');
	}
	var constructor = suitableConstructors[0];
	return constructor;
};
