var Visibility = require('../Visibility.js');
var Class = require('../expr/Class.js');
var Method = require('../expr/Method.js');
var klass = require('../klass.js');
var Interface = require('../expr/Interface.js');
var methodUtils = require('./methodUtils.js');

var new_ = function(parent) {
	// parent is not a string, because it was injected by import
	// it can be string only if it's a fully qualified name
	// TODO qualify if needed
	if (typeof(parent) == 'string') {
		parent = Class.forName(parent);
	} else if (globalJ2JS.mainParent.prototype.isPrototypeOf(parent.prototype)) {
		parent = parent.prototype.__clazz;
	}

	return function() {
		var constructorArgs = Array.prototype.slice.call(arguments);
		return function() {
			var body = Array.prototype.slice.call(arguments);

			var invoker = globalJ2JS.invoker;
			var invokerClass = invoker.prototype.__clazz;
			var invokerInstance = globalJ2JS.invokerInstance;
			var calleeName = globalJ2JS.calleeName;
			if (calleeName.str) {
				calleeName = calleeName.str
			}
			var parentName = parent.name;

			var invokerAnons;
			if (invokerInstance) {
				invokerAnons = invokerInstance.__j2js.anons;
			} else {
				invokerAnons = invoker.__j2js.anons;
			}
			if ( ! invokerAnons[calleeName]) invokerAnons[calleeName] = {};
			var calleeAnons = invokerAnons[calleeName];
			if ( ! calleeAnons[parentName]) calleeAnons[parentName] = 1;
			var numberToDistinguishAnons = calleeAnons[parentName];

			// needed for use as a key in nesters map of inner instances (inner of inner) if present
			var name = invokerClass.name + '$' + calleeName + '$' + parentName + '$' + numberToDistinguishAnons;
			var superClass = undefined;
			var interfaces = undefined;

			if (parent instanceof Interface) {
				interfaces = [parent.fullName];
			} else {
				superClass = parent.fullName;
			}
			if (constructorArgs.length) {
				var typeInfo = methodUtils.mapToTypeInfo(constructorArgs);
				var defaultConstr = new Method(Visibility.PUBLIC, false, false, false, false, undefined, '', 
						typeInfo, function() {uber.apply(null, constructorArgs);}, true)
				defaultConstr.length = typeInfo.length;
				body.push(defaultConstr);
			}

			var isStatic = !invokerInstance;
			var isFinal = true;
			var isAbstract = false;

			var clz = new Class(Visibility.PRIVATE, isStatic, isFinal, isAbstract, name, body, superClass, interfaces);
			clz.package = invokerClass.package;
			clz.outerClass = invokerClass;
			clz.setImportsArray(invokerClass.importsArray);

			if (parent.inClasspath.__j2js.isTemporary) {
				parent.initStatic(); // TODO eliminate unnecessary init (no need to init class when subclass init?)
			}
			var initFunc = klass(clz);
			if ( ! isStatic) {
				globalJ2JS.nesterInstance = invokerInstance;
			}
			var instance = initFunc.apply(null, constructorArgs);
			calleeAnons[parentName]++;
			return instance;
		};
	};
};

module.exports.new_ = new_;
