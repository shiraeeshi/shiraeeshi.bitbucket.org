var window = global;

var klass = require('../klass.js');
var memberIsVisibleFor = require('./checks.js').memberIsVisibleFor;
var fieldUtils = require('./fieldUtils.js');
var copyDefaultValues = fieldUtils.copyDefaultValues;
var copyValues = fieldUtils.copyValues;
var methodUtils = require('./methodUtils.js');
//var withStaticContext = require('./context.js').withStaticContext;
var withImportedContext = require('./context.js').withImportedContext;
var Visibility = require('../Visibility.js');

var instance = {};

module.exports = instance;

instance.build = function(sourceFiles) {
	var sourcesByPackage = mapSourceFilesByPackage(sourceFiles);
	var packages = {};
	for (var packageString in sourcesByPackage) {
		if ( ! sourcesByPackage.hasOwnProperty(packageString)) continue;
		var packageSources = sourcesByPackage[packageString];
		packages[packageString] = new Package(packageString, packageSources);
	}
	var globalNamesTree = {};
	
	var foreachPackage = function(act) {
		for (var packageString in packages) {
			if ( ! packages.hasOwnProperty(packageString)) continue;
			var package = packages[packageString];
			act(package);
		}
	};
	
	foreachPackage(function(package) {
		var packageNamesTree = package.namesTree();
		mergeNames(globalNamesTree, packageNamesTree);
	});
	globalJ2JS.namesTree = globalNamesTree;

	var foreachSource = function(act) {
		foreachPackage(function(package) {
			for (var i=0, len=package.sources.length; i<len; i++) {
				var source = package.sources[i];
				act(source);
			}
		});
	};

	// determine fully qualified names
	foreachSource(qualifyNamesInSourceFile);

	// build initial js for each class in each package
	foreachSource(buildFromVirtualSource);
};

var qualifyNamesInSourceFile = function(sourceFile) {
	for (var i=0, len=sourceFile.classes.length; i<len; i++) {
		var _class = sourceFile.classes[i];
		qualifyNames(_class);
	}
};

var qualifyNames = function(_class) {
	var members = _class.members;
	var qualified = createQualifier(_class.importsArray.imports, _class.package);
	
	var foreachMethod = function(methods, act) {
		for (var vis in methods) {
			if ( ! methods.hasOwnProperty(vis)) continue;
			for (var methodName in methods[vis]) {
				if ( ! methods[vis].hasOwnProperty(methodName)) continue;
				var methodsWithName = methods[vis][methodName];
				for (var i=0, len=methodsWithName.length; i<len; i++) {
					var method = methodsWithName[i];
					act(method);
				}
			}
		}
	};

	var qualifyMethod = function(method) {
		if (method.returnType) {
			method.returnType = qualified(method.returnType);
		}
		// TODO method.paramTypeInfo
	};
	
	foreachMethod(members.static.methods, qualifyMethod);
	foreachMethod(members.instance.methods, qualifyMethod);

	// TODO constructor

	var foreachField = function(fields, act) {
		for (var i=0, len=fields.length; i<len; i++) {
			var field = fields[i];
			act(field);
		}
	};
	
	var qualifyField = function(field) {
		field.type = qualified(field.type);
	};

	foreachField(members.static.fields, qualifyField);
	foreachField(members.instance.fields, qualifyField);

	if (_class.superClassStr) {
		_class.superClassStr = qualified(_class.superClassStr);
	}

	var foreachNested = function(classes, act) {
		for (var vis in classes) {
			if ( ! classes.hasOwnProperty(vis)) continue;
			for (var nestedName in classes[vis]) {
				var nested = classes[vis][nestedName];
				act(nested);
			}
		}
	};

	if (members.static.classes) {
		foreachNested(members.static.classes, qualifyNames);
	}
	if (members.instance.classes) {
		foreachNested(members.instance.classes, qualifyNames);
	}
};

var isFullyQualified = function(name) {
	var splitted = name.split(/\./);
	if (splitted.length == 1) return false;
	var namesTree = globalJ2JS.namesTree;
	var node = namesTree;
	for (var i=0, len=splitted.length; i<len; i++) {
		var name = splitted[i];
		if ( ! node[name]) return false;
		node = node[name];
	}
	return node.__j2js__parsedClass !== undefined;
};

var Import = require('../expr/Import.js');
var GenericName = require('../expr/GenericName.js');

var createQualifier = function(importsOriginal, package) {
	var Class = require('../expr/Class.js');
	var imports = importsOriginal.slice();
	imports.push(new Import(package + '.*'));

	return function(fullName) {
		if (fullName instanceof GenericName) return fullName;
		if (Class.isBuiltin(fullName) || isFullyQualified(fullName)) return fullName;
		var name = fullName.split(/\./)[0];
		for (var i=imports.length-1; i>=0; i--) {
			var _import = imports[i];
			if (_import.isStatic) continue;
			if (importsName(_import, name)) return prefixFromImport(_import, name) + fullName;
		}
		throw new Error('cannot find fully qualified name for name "' + fullName + '".');
	};
}

var importsName = function(_import, name) {
	var splitted = _import.importString.split(/\./);
	if (splitted[splitted.length-1] == name) {
		var lastNode = lastNodeFromNamesTree(splitted);
		var isClassName = lastNode.__j2js__parsedClass != undefined;
		return isClassName;
	}
	if (splitted[splitted.length-1] == '*') {
		var lastParentNode = lastNodeFromNamesTree(splitted.slice(0, -1));
		if ( ! lastParentNode[name]) return false;
		var lastNode = lastParentNode[name];
		var isClassName = lastNode.__j2js__parsedClass != undefined;
		return isClassName;
	}
	return false;
};

var lastNodeFromNamesTree = function(namesChain) {
	var node = globalJ2JS.namesTree;
	for (var i=0, len=namesChain.length; i<len; i++) {
		var name = namesChain[i];
		node = node[name];
	}
	return node;
};

var prefixFromImport = function(_import, name) {
	var importString = _import.importString;
	if (importString.indexOf('*') == importString.length - 1) 
		return importString.substring(0, importString.length-1);
	if (importString.indexOf(name) == importString.length - name.length) 
		return importString.substring(0, importString.length - name.length);
	throw new Error('cannot prefix: import string "' + _import.importString + 
				'" doesnt end with "' + name + '" or "*"');
};

var Package = (function() {
	var initFunction = function PackageInitFunction(packageString, sources) {
		this.packageString = packageString;
		this.sources = sources;
	};

	initFunction.prototype.namesTree = function() {
		var splitted = this.packageString.split(/\./);
		var tree = {};
		var node = tree;
		for (var i=0, len=splitted.length; i<len; i++) {
			var folderName = splitted[i];
			var newNode = {};
			node[folderName] = newNode;
			node = newNode;
		}
		for (var i=0, len=this.sources.length; i<len; i++) {
			var source = this.sources[i];
			var classNamesTree = source.classNamesTree();
			for (var rootClassName in classNamesTree) {
				if ( ! classNamesTree.hasOwnProperty(rootClassName)) continue;
				node[rootClassName] = classNamesTree[rootClassName];
			}
		}
		return tree;
	};
	return initFunction;
})();

var mergeNames = function(recipient, donor) {
	for (var name in donor) {
		if ( ! donor.hasOwnProperty(name)) continue;
		if ( ! recipient[name]) {
			recipient[name] = donor[name];
			continue;
		}
		if (recipient[name].__j2js__parsedClass ||
			donor[name].__j2js__parsedClass) throw new Error('Classname conflict');
		mergeNames(recipient[name], donor[name]);
	}
};

var mapSourceFilesByPackage = function(sourceFiles) {
	var byPackage = {};
	for (var i=0, len=sourceFiles.length; i<len; i++) {
		var source = sourceFiles[i];
		var packageString = source.packageString;
		if ( ! byPackage[packageString]) byPackage[packageString] = [];
		byPackage[packageString].push(source);
	}
	return byPackage;
};

var buildFromVirtualSource = function(source) {

	for (var i=source.classes.length-1; i>=0; i--) {
		var eachClass = source.classes[i];
		var enclosingPackage = source.last;
		if ( ! enclosingPackage[eachClass.name]) {
			enclosingPackage[eachClass.name] = buildInitialJS(eachClass);
		}
	}
};

var VirtualSource = require('../expr/Package.js');

var buildInitialJS = function(parsedClass) {
	if (parsedClass === globalJ2JS.mainParent.prototype.__clazz) return;
	// if (parsedClass.linkToSuperclass() instanceof Class) { // initial js haven't been built for superclass TODO
	if (parsedClass.superClassStr && parsedClass.superClass.inClasspath == undefined) {
		var superPackage = parsedClass.superClass.package;
		var superName = parsedClass.superClass.name;
		VirtualSource.last(superPackage)[superName] = buildInitialJS(parsedClass.superClass);
	}
	var tempClass = buildTempClass(parsedClass);

	defineTempFields(parsedClass, tempClass);
	defineTempMethods(parsedClass, tempClass);
	handleNestedClasses(parsedClass, tempClass);
	return tempClass;
};

instance.buildInitialJS = buildInitialJS;

var buildTempClass = function(parsedClass) {

	var tempClass = function() {
		parsedClass.initStatic();
		var realClass = parsedClass.inClasspath;
		var realClassInstance = realClass.apply(null, arguments);
		return realClassInstance;
	};

	var F = function() {};
	F.prototype = parsedClass.superClass.inClasspath.prototype;
	tempClass.prototype = new F();
	tempClass.prototype.__clazz = parsedClass;

	Object.defineProperty(tempClass, '__j2js', {value: {}});
	tempClass.__j2js.isTemporary = true;

	return tempClass;
};

var defineTempFields = function(parsedClass, tempClass) {

	var members = parsedClass.members;

	for (var i=0, len=members.static.fields.length; i<len; i++) {
		(function(field) {
			//var field = members.static.fields[i];
			Object.defineProperty(tempClass, field.name, {
				get: function() {
					parsedClass.initStatic();
					var realClass = parsedClass.inClasspath;
					var invoker = window['globalJ2JS'].invoker;
					if (!memberIsVisibleFor(realClass,field,invoker)) {
						throw new Error('field ' + field.name + ' of class ' + parsedClass.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
					}
					return realClass[field.name];
				},
				set: function(val) {
					parsedClass.initStatic();
					var realClass = parsedClass.inClasspath;
					var invoker = window['globalJ2JS'].invoker;
					if (!memberIsVisibleFor(realClass,field,invoker)) {
						throw new Error('field ' + field.name + ' of class ' + parsedClass.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
					}
					realClass[field.name] = val;
				}
			});
		})(members.static.fields[i]);
	}
};

var defineTempMethods = function(parsedClass, tempClass) {

	var members = parsedClass.members;

	for (var visibilityName in members.static.methods) {
		if (!members.static.methods.hasOwnProperty(visibilityName)) continue;
		var visibilityMethods = members.static.methods[visibilityName];
		for (var methodName in visibilityMethods) {
			if (!visibilityMethods.hasOwnProperty(methodName)) continue;
			(function(methodName) {
				tempClass[methodName] = function() {
					parsedClass.initStatic();
					var realClass = parsedClass.inClasspath;
					return realClass[methodName].apply(null, arguments);
				};
			})(methodName);
		}
	}
}

var handleNestedClasses = function(parsedClass, tempClass) {
	var members = parsedClass.members;

	var buildInitialNested = function(nested) {
		nested.package = parsedClass.package;
		nested.importsArray = parsedClass.importsArray;

		var nestedTempClass = buildInitialJS(nested);

		if (tempClass.hasOwnProperty(nestedTempClass.name)) {
			throw new Error('The name "'+nestedTempClass.name+'" in class "'+tempClass.name+'" is ambiguous. (This error is j2js-specific, java can handle such ambiguous names)');
		}
		tempClass[nested.name] = nestedTempClass;
	};

	for (var vis in members.static.classes) {
		if ( ! members.static.classes.hasOwnProperty(vis)) continue;
		for (var nestedName in members.static.classes[vis]) {
			if ( ! members.static.classes[vis].hasOwnProperty(nestedName)) continue;
			var nested = members.static.classes[vis][nestedName];

			// TODO temporarily inject outer class' properties into global scope
			// (via Object.defineProperty(window,...)), nested class must see
			// private (static) members via direct call (through window) and via 
			// Outer class (Outer.somePrivateField). It must work for any depth of nesting.

			buildInitialNested(nested);
		}
	}

	for (var vis in members.instance.classes) {
		if ( ! members.instance.classes.hasOwnProperty(vis)) continue;
		for (var nestedName in members.instance.classes[vis]) {
			if ( ! members.instance.classes[vis].hasOwnProperty(nestedName)) continue;
			var nested = members.instance.classes[vis][nestedName];

			// TODO define a field that references to the instance of outer class.
			// initialize it somehow. temporarily inject "This" property into
			// outer class's function: inner can call "Outer.this".

			buildInitialNested(nested);
		}
	}
};

var buildJS = function(parsedClass) {// TODO
	var jsClass = (function() {
		var members = parsedClass.members;
		var staticContext = {};
		copyDefaultValues(members.static.fields, staticContext);
		var tempInvoker = createTempInvoker(parsedClass);
		putMethods(members, staticContext, parsedClass, tempInvoker);
		putNestedClasses(parsedClass, staticContext);
		var initFunction;
		withContext(parsedClass.name, staticContext, function() {
			withTempInvoker(tempInvoker, function() {
				withImportedContext(parsedClass, function() {
					copyValues(members.static.fields, staticContext);
					if (members.static.initBlock) {
						runStaticInitBlock(members.static.initBlock);
					}
				});
			});
			initFunction = klass(parsedClass);
		});
		copyStaticMembers(parsedClass, staticContext, initFunction, members.static);
		Object.defineProperty(initFunction, '__j2js', {value: {
			anons: {} // needed to give distinct names to anonymous classes
		}});
		return initFunction;
	})();
	return jsClass;
};

instance.buildJS = buildJS;

var runStaticInitBlock = function(initBlock) {
	var calleeName = globalJ2JS.calleeNames.staticInitBlock;
	var oldCalleeName = globalJ2JS.calleeName;
	if (oldCalleeName == globalJ2JS.calleeNames.staticInitBlock) {
		initBlock.call(null);
		return;
	}
	var uninject = function() {
		globalJ2JS.calleeName = oldCalleeName;
		delete globalJ2JS.invoker.__j2js.anons[calleeName]; // safe to delete (will not run second time)
	};
	globalJ2JS.calleeName = globalJ2JS.calleeNames.staticInitBlock;
	globalJ2JS.invoker.__j2js.anons[calleeName] = {};
	try {
		initBlock.call(null);
	} catch (e) {
		uninject();
		throw e;
	}
	uninject();
};

var putNestedClasses = function(parsedClass, staticContext) {
	var members = parsedClass.members;

	var foreachNested = function(classes, act) {
		for (var vis in classes) {
			if ( ! classes.hasOwnProperty(vis)) continue;
			for (var nestedName in classes[vis]) {
				var nested = classes[vis][nestedName];
				act(nestedName, nested);
			}
		}
	};

	var setNested = function(nestedName, nested) {
		staticContext[nestedName] = nested.inClasspath;
	};

	if (members.instance.classes) {
		foreachNested(members.instance.classes, setNested);
	}
	if (members.static.classes) {
		foreachNested(members.static.classes, setNested);
	}
};

var createTempInvoker = function(parsedClass) {
	var result = function() {};
	var F = function () {};
	F.prototype = parsedClass.superClass.inClasspath.prototype;
	result.prototype = new F();
	result.prototype.__clazz = parsedClass;
	Object.defineProperty(result, '__j2js', {value: {
		anons: {} // needed to give distinct names to anonymous classes
	}});
	return result;
};
var withContext = function(name, context, act) {// TODO find better solution
	var oldBoundValue = window[name];
	window[name] = context;
	var uninject = function() {
		window[name] = oldBoundValue;
	};
	try {
		act();
	} catch (e) {
		uninject();
		throw e;
	}
	uninject();
};
var withTempInvoker = function(tempInvoker, act) {
	var oldInvoker = globalJ2JS.invoker;
	var oldInvokerInstance = globalJ2JS.invokerInstance;
	globalJ2JS.invoker = tempInvoker;
	globalJ2JS.invokerInstance = undefined;
	var uninject = function() {
		globalJ2JS.invoker = oldInvoker;
		globalJ2JS.invokerInstance = oldInvokerInstance;
	};
	try {
		act();
	} catch (e) {
		uninject();
		throw e;
	}
	uninject();
};
var copyStaticMembers = function(parsedClass, staticContext, initFunc, staticMembers) {
	for (var i=0, len=staticMembers.fields.length; i<len; i++) {
		(function(field) {
			//var field = staticMembers.fields[i];
			Object.defineProperty(initFunc, field.name, {
				get: function() {
					var invoker = window['globalJ2JS'].invoker;
					if (!memberIsVisibleFor(initFunc,field,invoker)) {
						throw new Error('field ' + field.name + ' of class ' + initFunc.prototype.__clazz.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
					}
					return staticContext[field.name];
				},
				set: function(val) {
					var invoker = window['globalJ2JS'].invoker;
					if (!memberIsVisibleFor(initFunc,field,invoker)) {
						throw new Error('field ' + field.name + ' of class ' + initFunc.prototype.__clazz.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
					}
					staticContext[field.name] = val;
				}
			});
		})(staticMembers.fields[i]);
	}
//	for (var visibilityName in staticMembers.methods) {
//		if (!staticMembers.methods.hasOwnProperty(visibilityName)) continue;
//		var visibilityMethods = staticMembers.methods[visibilityName];
//		for (var methodName in visibilityMethods) {
//			if (!visibilityMethods.hasOwnProperty(methodName)) continue;
//			(function(met) {
//				initFunc[met.name] = function() {
//					var invoker = window['globalJ2JS'].invoker;
//					if (!memberIsVisibleFor(initFunc,met,invoker)) {
//						throw new Error('method ' + met.name + ' of class ' + initFunc.prototype.__clazz.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
//					}
//					var args = arguments;
//					return withStaticContext(parsedClass, staticContext, initFunc, function() {
//						var result = met.body.apply(null, args);
//						// TODO hide invisible members of result if needed
//						return result;
//					});
//				};
//			})(visibilityMethods[methodName]);
//		}
//	}
};
var putMethods = function(members, staticContext, parsedClass, tempInvoker) {
	var methodUtils = require('./methodUtils.js');

	var copyMethods = methodUtils.copyMethods;
	var insertIntoAllMethods = methodUtils.insertIntoAllMethods;
	var findSuitable = methodUtils.findSuitable;

	var allStaticMethods = {};
	(function() {
		var tmp = {};
		copyMethods(members.static.methods[Visibility.PUBLIC], tmp);
		copyMethods(members.static.methods[Visibility.PROTECTED], tmp);
		copyMethods(members.static.methods[Visibility.DEFAULT], tmp);
		copyMethods(members.static.methods[Visibility.PRIVATE], tmp);
		for (var name in tmp) {
			if ( ! tmp.hasOwnProperty(name)) continue;
			if ( ! allStaticMethods[name]) allStaticMethods[name] = {};
			insertIntoAllMethods(tmp, name, allStaticMethods);
			if (tmp[name]['*']) {
				insertIntoAllMethods(tmp, name, allStaticMethods, true);
			}
		}
	})();
	for (methodName in allStaticMethods) {
		if (!allStaticMethods.hasOwnProperty(methodName)) continue;
		staticContext[methodName] = (function(methodName, methodsByLength) {
			return function() {
				var args = arguments;
				var invoker = window['globalJ2JS'].invoker;
				var met = findSuitable(methodsByLength, args);
				if ( ! met) {
					var argsStr = Array.prototype.join.apply(args, [',']);
					throw new Error('cannot find suitable method "' + methodName + '" for args "' + argsStr + '"');
				}
				if (!memberIsVisibleFor(tempInvoker, met, invoker)) {
					throw new Error('method ' + met.name + ' of class ' + tempInvoker.prototype.__clazz.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
				}
//				var result = withStaticContext(parsedClass, staticContext, tempInvoker, function() {
//					return met.body.apply(undefined, args);
//				});
				var result = methodUtils.invokeStaticMethod(met, args, parsedClass, staticContext, tempInvoker);
				return result;
			};
		})(methodName, allStaticMethods[methodName]);
	}
//	var copyAllPropertiesBodies = require('./copiers.js').copyAllPropertiesBodies;
//	copyAllPropertiesBodies(members.static.methods[Visibility.PUBLIC], staticContext);
//	copyAllPropertiesBodies(members.static.methods[Visibility.PROTECTED], staticContext);
//	copyAllPropertiesBodies(members.static.methods[Visibility.PRIVATE], staticContext);
//	copyAllPropertiesBodies(members.static.methods[Visibility.DEFAULT], staticContext);
};
