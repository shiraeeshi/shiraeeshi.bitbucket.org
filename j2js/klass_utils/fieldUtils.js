
var copyDefaultValues = function(donor, recipient) {
	for (var i=0, len=donor.length; i<len; i++) {
		var field = donor[i];
		recipient[field.name] = field.defaultValue();
	}
};
var copyValues = function(donor, recipient, instance) {
	for (var i=0, len=donor.length; i<len; i++) {
		var field = donor[i];
		var value = field.value;
		if (value instanceof Function) {
			var initializer = value;
			value = runFieldInitializer(field.name, instance, initializer);
		}
		recipient[field.name] = value;
	}
};

var context = require('./context.js');
var withInstanceContext = context.withInstanceContext;
var withStaticContext = context.withStaticContext;

var runFieldInitializer = function(fieldName, instance, initializer) {
	var calleeNameObj = globalJ2JS.calleeNames.createFieldInitializerName(fieldName, isStatic);
	var calleeName = calleeNameObj.str;
	var invokerAnons;
	if (instance) {
		invokerAnons = instance.__j2js.anons;
	} else {
		invokerAnons = globalJ2JS.invoker.__j2js.anons;
	}
	var oldCalleeAnons = invokerAnons[calleeName];
	var oldCalleeName = globalJ2JS.calleeName;
	var uninject = function() {
		globalJ2JS.calleeName = oldCalleeName;
		invokerAnons[calleeName] = oldCalleeAnons;
	};
	var isStatic = !instance;
	globalJ2JS.calleeName = calleeNameObj;
	invokerAnons[calleeName] = {};
	//var invoker = globalJ2JS.invoker;
	//var parsedClass = invoker.prototype.__clazz;
	//var context = global[parsedClass.name];
	var result;
	try {
		if (isStatic) {
			//withStaticContext(parsedClass, context, invoker, function() {
				result = initializer.call(instance);
			//});
		} else {
			//withInstanceContext(instance, parsedClass, context, invoker, function() {
				result = initializer.call(instance);
			//});
		}
	} catch (e) {
		uninject();
		throw e;
	}
	uninject();
	return result;
};

module.exports = {};
module.exports.copyDefaultValues = copyDefaultValues;
module.exports.copyValues = copyValues;
