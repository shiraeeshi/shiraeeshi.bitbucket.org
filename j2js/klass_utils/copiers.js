var copyAllProperties = function(donor, recipient) {
	for (i in donor) {
		if (!donor.hasOwnProperty(i)) continue;
		recipient[i] = donor[i];
	}
};
//var copyAllPropertiesBodies = function(donor, recipient) {
//	for (i in donor) {
//		if (!donor.hasOwnProperty(i)) continue;
//		recipient[i] = donor[i].body;
//	}
//};

module.exports = {};
module.exports.copyAllProperties = copyAllProperties;
//module.exports.copyAllPropertiesBodies = copyAllPropertiesBodies;
