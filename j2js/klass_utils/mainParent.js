var window = global;

var memberIsVisibleFor = require('./checks.js').memberIsVisibleFor;
var copyAllProperties = require('./copiers.js').copyAllProperties;
var Class = require('../expr/Class.js');
var Interface = require('../expr/Interface.js');

var mainParent = function() {
	var instance = Object.create(mainParent.prototype);
	return instance;
};
mainParent.prototype = {
	new_ : function() {
		if (arguments.length == 0) throw new Error("new_ expects inner class and arguments to call it's constructor with.");
		var innerClass = arguments[0];
		var args = Array.prototype.slice.call(arguments, 1);
		var invoker = window['globalJ2JS'].invoker;
		var proto = Object.getPrototypeOf(this);
		var vis = innerClass.prototype.__clazz.visibility;
		if ( ! memberIsVisibleFor(proto.constructor, proto.__clazz.members.instance.classes[vis][innerClass.prototype.__clazz.name], invoker)) {
			throw new Error('inner class "' + innerClass.prototype.__clazz.name + '" is not visible for invoker ' + (invoker ? invoker.prototype.__clazz.name : 'undefined'));
		}
		globalJ2JS.nesterInstance = this;
		var newInstance = innerClass.apply(null, args);
		return newInstance;
	},
	instanceOf: function(kind_of_thing) {
		if (typeof(kind_of_thing) == 'string') {
			kind_of_thing = Class.forName(kind_of_thing);
		}
		if (kind_of_thing instanceof Interface) {
			var initFunc = Object.getPrototypeOf(this);
			return initFunc.interfaces.hasOwnProperty(kind_of_thing.fullName);
		} else if (kind_of_thing instanceof Class) {
			return this instanceof kind_of_thing;
		} else {
			throw new Error('illegal argument (must be either interface or class)');
		}
	}
};
mainParent.prototype.constructor = mainParent;

var parsedMainParent = {
	package: '',
	interfaces: [],
	getPackageVisibleFields: function() {
		return [];
	},
	inClasspath: mainParent
};

mainParent.prototype.__clazz = parsedMainParent;
mainParent.__j2js = {};
module.exports = mainParent;
