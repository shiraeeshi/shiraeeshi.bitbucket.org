var copyAllProperties = require('./copiers.js').copyAllProperties;

var injectNesters = function(newInstance, nesterInstance) {
	var nesters = {};
	var nesterClz = Object.getPrototypeOf(nesterInstance).__clazz;
	nesters[nesterClz.fullName] = nesterInstance;
	if (nesterInstance.__j2js.nesters) {
		copyAllProperties(nesterInstance.__j2js.nesters, nesters);
	}
	newInstance.__j2js.nesters = nesters;
};

module.exports = injectNesters;
