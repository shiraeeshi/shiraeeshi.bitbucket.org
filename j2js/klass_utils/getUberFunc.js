var window = global;

var fieldUtils = require('./fieldUtils.js');
var injectNesters = require('./injectNesters.js');
var copyDefaultValues = fieldUtils.copyDefaultValues;
var copyValues = fieldUtils.copyValues;
//var withImportedContext = require('./context.js').withImportedContext;
var withInstanceContext = require('./context.js').withInstanceContext;

// TODO make uberFunc and ThisFunc global, place "instance, invoker, provider, constructorFunc" objects
// into other global holder
var getUberFunc = function(instance, constructorFunc, nesterInstance) {
	return function() {
		var uberInstance;
		var clz = constructorFunc.prototype.__clazz;
		if (constructorFunc.uber && constructorFunc.uber.hasOwnProperty('constructor')) {
			window['globalJ2JS'].invoker = constructorFunc;
			withInvoker(constructorFunc, function() {
				uberInstance = constructorFunc.uber.constructor.apply(null, arguments);
				definePackageFieldsToInherit(clz.package, uberInstance);
			});
		}
		if (nesterInstance) {
			injectNesters(instance, nesterInstance);
		}
		instance.__j2js.uber = uberInstance;
		instance.__j2js.uberCalled = true;
		window['uber'] = uberInstance;
		withInstanceContext(instance, clz, constructorFunc, constructorFunc, function() {
			copyDefaultValues(clz.members.instance.fields, instance.__j2js.fields);
			copyValues(clz.members.instance.fields, instance.__j2js.fields, instance);
			if (clz.members.instance.initBlock) {
				runInitBlock(clz.members.instance.initBlock, instance);
			}
		});

	};
};

var withInvoker = function(invoker, act) {
	var oldInvoker = window['globalJ2JS'].invoker;
	window['globalJ2JS'].invoker = invoker;
	var uninject = function() {
		window['globalJ2JS'].invoker = oldInvoker;
	};
	try {
		act();
	} catch (e) {
		uninject();
		throw e;
	}
	uninject();
};

var runInitBlock = function(initBlock, instance) {
	var calleeName = globalJ2JS.calleeNames.initBlock.str;
	var oldCalleeName = globalJ2JS.calleeName;
	if (oldCalleeName == globalJ2JS.calleeNames.initBlock) {
		initBlock.call(instance);
		return;
	}
	var uninject = function() {
		globalJ2JS.calleeName = oldCalleeName;
		delete instance.__j2js.anons[calleeName]; // safe to delete (will not run second time)
	};
	globalJ2JS.calleeName = globalJ2JS.calleeNames.initBlock;
	instance.__j2js.anons[calleeName] = {};
	try {
		initBlock.call(instance);
	} catch (e) {
		uninject();
		throw e;
	}
	uninject();
};

var definePackageFieldsToInherit = function(package, instance) {
	if (!instance.constructor.uber) return;
	var uberClass = instance.constructor.uber.prototype.__clazz;
	if (uberClass.package !== package) { // TODO if (uberClass.package === package && instance.prototype.__clazz.package !== package)
		var fieldsToDefine = uberClass.getPackageVisibleFields();
		for (var i=fieldsToDefine.length-1; i>=0; i--) {
			var field = fieldsToDefine[i];
			Object.defineProperty(instance, field.name, {
				get: function() {return this.__j2js.uber[field.name];},
				set: function(value) {this.__j2js.uber[field.name] = value;}
			});
		}
	}
};

module.exports = getUberFunc;
