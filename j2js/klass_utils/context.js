var window = global;

var withInstanceContext = function(instance, parsedClass, context, invoker, act) {
	var oldUber = window['uber'];
	window['uber'] = instance.__j2js.uber;
	var oldInvokerInstance = globalJ2JS.invokerInstance;
	globalJ2JS.invokerInstance = instance;
	var uninjectContext = function() {
		window['uber'] = oldUber;
		globalJ2JS.invokerInstance = oldInvokerInstance;
	};
	try {
		var result = doWithStaticContext(parsedClass, context, invoker, act);
	} catch (e) {
		uninjectContext();
		throw e;
	}
	uninjectContext();
	return result;
};
var withStaticContext = function(parsedClass, context, invoker, act) {
	var oldInvokerInstance = globalJ2JS.invokerInstance;
	globalJ2JS.invokerInstance = undefined;
	var uninjectContext = function() {
		globalJ2JS.invokerInstance = oldInvokerInstance;
	};
	try {
		var result = doWithStaticContext(parsedClass, context, invoker, act);
	} catch (e) {
		uninjectContext();
		throw e;
	}
	uninjectContext();
	return result;
};

var doWithStaticContext = function(parsedClass, context, invoker, act) {
	var className = parsedClass.name; // TODO dollar signs in nested classes
	var oldClassnameValue = window[className];
	window[className] = context;
	var oldInvoker = window['globalJ2JS'].invoker;
	window['globalJ2JS'].invoker = invoker;
	var uninjectContext = function() {
		window['globalJ2JS'].invoker = oldInvoker;
		window[className] = oldClassnameValue;
	};
	try {
		var result = withImportedContext(parsedClass, act);
	} catch (e) {
		uninjectContext();
		throw e;
	}
	uninjectContext();
	return result;
};

var withImportedContext = function(parsedClass, act) {
	var oldValuesForImported = injectImportedContext(parsedClass);
	var uninjectContext = function() {
		uninjectImportedContext(oldValuesForImported);
	};
	try {
		var result = act();
	} catch (e) {
		uninjectContext();
		throw e;
	}
	uninjectContext();
	return result;
};

var Import = require('../expr/Import.js');
var copyAllProperties = require('../klass_utils/copiers.js').copyAllProperties;

var injectImportedContext = function(parsedClass) {
	var imports = parsedClass.importsArray.imports.slice();
	//imports.push(new Import(parsedClass.package + '.*'));
	var oldValuesHolder = {};
	for (var i=0, len=imports.length; i<len; i++) {
		var _import = imports[i];
		var oldValues = injectImport(_import, parsedClass);
		copyAllProperties(oldValues, oldValuesHolder);
	}
	var oldValues = injectOwnPackageImport(parsedClass);
	copyAllProperties(oldValues, oldValuesHolder);
	return oldValuesHolder;
};

var Package = require('../expr/Package.js');

var injectOwnPackageImport = function(parsedClass) {
	var oldValuesHolder = {};
	var enclosingPackage = Package.last(parsedClass.package);
	var packageClassNames = Object.getOwnPropertyNames(enclosingPackage);
	removeOwnNameOrNesters(parsedClass, packageClassNames);
	for (var i=packageClassNames.length-1; i >= 0; i--) {
		var importedClassName = packageClassNames[i];
		oldValuesHolder[importedClassName] = window[importedClassName];
		window[importedClassName] = enclosingPackage[importedClassName];
	}
	return oldValuesHolder;
};

var removeOwnNameOrNesters = function(parsedClass, packageClassNames) {
	if (parsedClass.outerClass) {
		removeOwnNameOrNesters(parsedClass.outerClass, packageClassNames);
		return;
	}
	var ind = packageClassNames.indexOf(parsedClass.name);
	packageClassNames.splice(ind, 1);
};

var injectImport = function(_import, parsedClass) {
	var oldValuesHolder = {};
	var enclosingPackage = Package.last(_import.packageArray);
	var className = _import.className;
	if (className == '*') {
		var packageClassNames = Object.getOwnPropertyNames(enclosingPackage);
		for (var i=packageClassNames.length-1; i >= 0; i--) {
			var importedClassName = packageClassNames[i];
			oldValuesHolder[importedClassName] = window[importedClassName];
			window[importedClassName] = enclosingPackage[importedClassName];
		}
	} else if (_import.isStatic){
		var funcName = _import.funcName;
		oldValuesHolder[funcName] = window[funcName];
		window[funcName] = enclosingPackage[className][funcName];
	} else {
		oldValuesHolder[className] = window[className];
		window[className] = enclosingPackage[className];
	}
	return oldValuesHolder;
};
var uninjectImportedContext = function(oldValues) {
	for (var i in oldValues) {
		if (!oldValues.hasOwnProperty(i)) continue;
		window[i] = oldValues[i];
	}
};

module.exports = {};
module.exports.withInstanceContext = withInstanceContext;
module.exports.withStaticContext = withStaticContext;
module.exports.injectImport = injectImport;
module.exports.withImportedContext = withImportedContext;
