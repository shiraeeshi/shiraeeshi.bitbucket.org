
var context = require('./context.js');
var withInstanceContext = context.withInstanceContext;
var withStaticContext = context.withStaticContext;

var invokeStaticMethod = function(method, args, parsedClass, context, invoker) {
	return invokeMethod(method, args, undefined, parsedClass, context, invoker);
};
var invokeMethod = function(method, args, instance, parsedClass, context, invoker) {
	var calleeName = method.name;
	var oldCallee = globalJ2JS.calleeName;
	var invokerAnons;
	if (instance) {
		invokerAnons = instance.__j2js.anons;
	} else {
		invokerAnons = invoker.__j2js.anons;
	}
	var oldCalleeAnons = invokerAnons[calleeName];
	var uninject = function() {
		globalJ2JS.calleeName = oldCallee;
		invokerAnons[calleeName] = oldCalleeAnons;
	};
	globalJ2JS.calleeName = calleeName;
	invokerAnons[calleeName] = {};
	var result;
	try {
		if (instance) {
			result = withInstanceContext(instance, parsedClass, context, invoker, function() {
				return method.body.apply(instance, args);
			});
		} else {
			result = withStaticContext(parsedClass, context, invoker, function() {
				return method.body.apply(undefined, args);
			});
		}
	} catch (e) {
		uninject();
		throw e;
	}
	uninject();
	return result;
};

var copyMethods = function(mets, recipient) {
	for (var name in mets) {
		if ( ! mets.hasOwnProperty(name)) continue;
		if ( ! recipient[name]) recipient[name] = {}; 
		var metsWithName = mets[name];
		for (var i=0, len=metsWithName.length; i<len; i++) {
			var met = metsWithName[i];
			if (met.isVarargs) {
				if ( ! recipient[name]['*']) recipient[name]['*'] = {};// method with signature (int some, String another, double ... yetAnother) will reside in recipient[name]['*'][3], the last 3 is declared params count
				if ( ! recipient[name]['*'][met.length]) recipient[name]['*'][met.length] = [];
				recipient[name]['*'][met.length].push(met);
			} else {
				if ( ! recipient[name][met.length]) recipient[name][met.length] = [];
				var metsWithNameAndLen = recipient[name][met.length];
//				var overridden = false;
//				for (var i=0, len=metsWithNameAndLen.length; i<len; i++) {
//					var prev = metsWithNameAndLen[i];
//					if (met.overrides(prev)) {
//						metsWithNameAndLen[i] = met;
//						overridden = true;
//						break;
//					}
//				}
//				if ( ! overridden) {
//					metsWithNameAndLen.push(met);
//				}
				metsWithNameAndLen.push(met);
			}
		}
	}
};
var insertIntoAllMethods = function(tmp, name, allMethods, isVarargs) {
	var tmpName = tmp[name];
	var allMethodsName = allMethods[name];
	if (isVarargs) {
		tmpName = tmp[name]['*'];
		allMethodsName = allMethods[name]['*'];
	}
	for (var len in tmpName) {
		if (len == '*') continue;
		if ( ! allMethodsName[len]) allMethodsName[len] = [];
		var arr = tmpName[len];
		var allMethodsArr = allMethodsName[len];
		// remove overridden methods from allMethods
		for (var ind = arr.length-1; ind >= 0; ind--) {
			var met = arr[ind];
			for (var a = allMethodsArr.length-1; a >= 0; a--) {
				var each = allMethodsArr[a];
				if (met.overrides(each)) {
					allMethodsArr.splice(a, 1);
					break;
				}
			}
		}
		// add methods from tmp to allMethods
//		for (var ind = arr.length-1; ind >= 0; ind--) {
//			var met = arr[ind];
//			allMethodsArr.push(met);
//		}
		allMethodsName[len] = allMethodsArr.concat(arr);
	}
};

var findSuitable = function(methodsByLength, args, genericInfo, methodGenericValues) {
	var arr = [];
	if (methodsByLength[args.length]) {
		arr = methodsByLength[args.length];
	}
	if (methodsByLength['*']) {
		for (var len in methodsByLength['*']) {
			if ( ! methodsByLength['*'].hasOwnProperty(len)) continue;
			if (len > args.length) continue;
			arr = arr.concat(methodsByLength['*'][len]);
		}
	}
	for (var i=0, len=arr.length; i<len; i++) {
		var met = arr[i];
		if (met.isSuitableFor(args, genericInfo, methodGenericValues)) return met;
	}
};

var mapToTypeInfo = function(args) {
	var Class = require('../expr/Class.js');
	var typeInfo = [];
	for (var i=0, len=args.length; i<len; i++) {
		typeInfo[i] = Class.typeNameOf(args[i]);
	}
	return typeInfo;
};

module.exports.copyMethods = copyMethods;
module.exports.insertIntoAllMethods = insertIntoAllMethods;
module.exports.findSuitable = findSuitable;
module.exports.invokeMethod = invokeMethod;
module.exports.invokeStaticMethod = invokeStaticMethod;
module.exports.mapToTypeInfo = mapToTypeInfo;
