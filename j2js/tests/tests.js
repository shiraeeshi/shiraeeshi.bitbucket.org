var window = global;

var testStaticFieldVisibility = require('./testStaticFieldVisibility.js');
var testStaticMethodVisibility = require('./testStaticMethodVisibility.js');
var testStaticMethods = require('./testStaticMethods.js');
var testStaticCounter = require('./testStaticCounter.js');
var testOuterThis = require('./testOuterThis.js');
var testParentNotCallingUber = require('./testParentNotCallingUber.js');
var testNewChild = require('./testNewChild.js');
var testDefaultConstructorInjection = require('./testDefaultConstructorInjection.js');
var testOverride = require('./testOverride.js');
var testNestedInitOrder = require('./testNestedInitOrder.js');
var testAnonSimple = require('./testAnonSimple.js');

var tests = [
	testStaticFieldVisibility
	, testStaticMethodVisibility
	, testStaticMethods
	, testStaticCounter
	, testOuterThis
	, testParentNotCallingUber
	, testNewChild
	, testDefaultConstructorInjection
	, testOverride
	, testNestedInitOrder
	, testAnonSimple
	, require('./testAnonSimpleOverride.js')
	, require('./testAnonField.js')
	, require('./testAnonOuterThis.js')
	, require('./testAnonDoubleInner.js')
	, require('./testGenericClassSimple.js')
];
var run = function() {
	require('../initGlobal.js');
	console.log('tests: ');
	console.log(tests);
	for (var i=0, len=tests.length; i<len; i++) {
		console.log('--------------------------------------------');
		console.log(tests[i].name + ':');
		tests[i].run();
		window['globalJ2JS'].classpath = {};
	}
};

run();

