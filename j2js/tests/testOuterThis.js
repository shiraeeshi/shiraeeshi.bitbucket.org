var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;

	return [
		package('com.example')(

		public().clazz('Frog')(

			private('int', 'legsCount', 4),

			public('Frog', [], function() {
				uber();
			}),

			public().clazz('Inner')(
				public('Inner', [], function() {
					uber();
				}),
				public('int', 'getPrivateOuterValue', [], function() {
					return Frog.This.legsCount;
				})
			)
		)
		)
	];

};

var testOuterThis = {name: 'Inner class sees outer this'};
module.exports = testOuterThis;

testOuterThis.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');

//	for (var i=packages.length-1; i>=0; i--) {
//		var eachPackage = packages[i];
//		klassBuilder.buildPackage(eachPackage);
//	}

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.Frog'));
	
	var frog = new Frog();

	console.assert(frog instanceof globalJ2JS.mainParent, 'frog is not instanceof mainParent');
	console.assert(frog instanceof globalJ2JS.classpath.com.example.Frog, 'frog is not instanceof globalJ2JS.classpath.com.example.Frog');
	console.assert(frog instanceof Frog, 'frog is not instanceof Frog');
	console.assert(globalJ2JS.mainParent.prototype.isPrototypeOf(Object.getPrototypeOf(frog)), 'mainParent is not proto of frog');

	var inn = frog.new_(Frog.Inner);
	console.assert(inn != null && inn != undefined, 'inn is initialized');
	console.assert(inn instanceof globalJ2JS.classpath.com.example.Frog.Inner, 'inn is not instanceof globalJ2JS.classpath.com.example.Inner');
	console.assert(inn instanceof Frog.Inner, 'inn is not instanceof Inner');
	console.assert(inn.getPrivateOuterValue() == 4);
	console.log('checked');
};

