var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var protected = keywords.protected;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;
	var _import = keywords._import;

	return [
		package('com.example.pluser')(

		clazz('Pluser')(
			protected('int', 'a', 2),
			public('int', 'plus', ['int'], function(num) {return this.a + num;})
		)

		),
		package('com.example')(
		_import('com.example.pluser.*'),
		public().clazz('Frog')(
			private('int', 'a'),
			public().Void('setA', ['int'], function(a) {this.a = a;}),

			public('int', 'mult', ['int', 'int'], function(a, b) {
				this.setA(b);
				return this.mult(a);
			}),
			private('int', 'mult', ['int'], function(a) {
				var pluser = new_(Pluser)()(
					function() { // init block
						this.a = Frog.This.a;
					}
				);
				var result = 0;
				for (var i=a; i>0; i--) {
					result = pluser.plus(result);
				}
				return result;
			})
		)
		)
	];

};

var testAnonSimple = {name: 'Test anonymous inner sees outer this'};
module.exports = testAnonSimple;

testAnonSimple.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.Frog'));
	
	var frog = new Frog();
	console.assert(frog.mult(5, 7) == 35, 'something is wrong with mult');
	console.assert(frog.mult(5, 7) == 35, 'something is wrong with mult: error when invoked twice ');
	console.log('checked');
};

