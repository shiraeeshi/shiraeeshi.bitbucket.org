var window = global;

var buildPackages = function() {

	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var protected = keywords.protected;
	var private = keywords.private;
	var static = keywords.static;
	var final = keywords.final;
	var clazz = keywords.clazz;
	var _import = keywords._import;
	var package = keywords.package;

	var counter = 1;

	return [

		package("com.example")(

		public().clazz('Pantene')(
			public().static().final('int', 'PUBLIC_CONSTANT', counter++),
			protected().static().final('int', 'PROTECTED_CONSTANT', counter++),
			static().final('int', 'PACKAGE_VISIBLE_CONSTANT', counter++),
			private().static().final('int', 'PRIVATE_CONSTANT', counter++),

			public().static('int', 'publicStaticField', counter++),
			protected().static('int', 'protectedStaticField', counter++),
			static('int', 'packageVisibleStaticField', counter++),
			private().static('int', 'privateStaticField', counter++),

			public().static('int', 'publicStaticField2', function() {return Pantene.PUBLIC_CONSTANT;}),
			protected().static('int', 'protectedStaticField2', function() {return Pantene.PROTECTED_CONSTANT;}),
			static('int', 'packageVisibleStaticField2', function() {return Pantene.PACKAGE_VISIBLE_CONSTANT;}),
			private().static('int', 'privateStaticField2', function() {return Pantene.PRIVATE_CONSTANT;}),
			public().static('int', 'runFunc', ['func'], function(f) {return f();})
		),

		public().clazz('ClassInTheSamePackage')(
			public().static('int', 'runFunc', ['func'], function(f) {return f();})
		),

		public().clazz('SubclassInTheSamePackage').extends('Pantene')(
			public().static('int', 'runFunc', ['func'], function(f) {return f();})
		)

		),

		package('com.example.another')(
		_import('com.example.Pantene'),

		public().clazz('SubclassInAnotherPackage').extends('Pantene')(
			public().static('int', 'runFunc', ['func'], function(f) {return f();})
		),

		public().clazz('ClassInAnotherPackage')(
			public().static('int', 'runFunc', ['func'], function(f) {return f();})
		)

		)

	];
};

var testStaticFieldVisibility = {name: 'Field Visibility'};

module.exports = testStaticFieldVisibility;

testStaticFieldVisibility.run = function() {
	var packages = buildPackages();
	var helpers = require('./helpers.js');
	var fieldInvis = helpers.fieldInvis;
	var assertError = helpers.assertError;
	var klassBuilder = require('../klass_utils/klassbuilder.js');

//	for (var i=packages.length-1; i>=0; i--) {
//		var eachPackage = packages[i];
//		klassBuilder.buildPackage(eachPackage);
//	}

	klassBuilder.build(packages);

	var com = globalJ2JS.classpath.com;

	console.log('global:');

	console.assert(com.example.Pantene.PUBLIC_CONSTANT == 1);
	console.log('Pantene.PUBLIC_CONSTANT checked');
	console.assert(com.example.Pantene.publicStaticField == 5);
	console.log('Pantene.publicStaticField checked');
	console.assert(com.example.Pantene.publicStaticField2 == 1);
	console.log('Pantene.publicStaticField2 checked');
	assertError(function() {return com.example.Pantene.PROTECTED_CONSTANT;}, fieldInvis('PROTECTED_CONSTANT', 'Pantene', 'undefined'));
	console.log('Pantene.PROTECTED_CONSTANT checked');
	assertError(function() {return com.example.Pantene.protectedStaticField;}, fieldInvis('protectedStaticField', 'Pantene', 'undefined'));
	assertError(function() {return com.example.Pantene.protectedStaticField2;}, fieldInvis('protectedStaticField2', 'Pantene', 'undefined'));
	assertError(function() {return com.example.Pantene.PACKAGE_VISIBLE_CONSTANT;}, fieldInvis('PACKAGE_VISIBLE_CONSTANT', 'Pantene', 'undefined'));
	console.log('Pantene.PACKAGE_VISIBLE_CONSTANT checked');
	assertError(function() {return com.example.Pantene.packageVisibleStaticField;}, fieldInvis('packageVisibleStaticField', 'Pantene', 'undefined'));
	assertError(function() {return com.example.Pantene.packageVisibleStaticField2;}, fieldInvis('packageVisibleStaticField2', 'Pantene', 'undefined'));
	assertError(function() {return com.example.Pantene.PRIVATE_CONSTANT;}, fieldInvis('PRIVATE_CONSTANT', 'Pantene', 'undefined'));
	console.log('Pantene.PRIVATE_CONSTANT checked');
	assertError(function() {return com.example.Pantene.privateStaticField;}, fieldInvis('privateStaticField', 'Pantene', 'undefined'));
	assertError(function() {return com.example.Pantene.privateStaticField2;}, fieldInvis('privateStaticField2', 'Pantene', 'undefined'));

	console.log('class itself:');

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.PUBLIC_CONSTANT;}) == 1)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.publicStaticField;}) == 5)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.publicStaticField2;}) == 1)

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.PROTECTED_CONSTANT;}) == 2)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.protectedStaticField;}) == 6)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.protectedStaticField2;}) == 2)

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.PACKAGE_VISIBLE_CONSTANT;}) == 3)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.packageVisibleStaticField;}) == 7)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.packageVisibleStaticField2;}) == 3)

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.PRIVATE_CONSTANT;}) == 4)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.privateStaticField;}) == 8)
	console.assert(com.example.Pantene.runFunc(function() {return Pantene.privateStaticField2;}) == 4)

	console.log('Another class in the same package:');

	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.PUBLIC_CONSTANT;}) == 1);
	console.log('Pantene.PUBLIC_CONSTANT checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.publicStaticField;}) == 5);
	console.log('Pantene.publicStaticField checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.publicStaticField2;}) == 1);
	console.log('Pantene.publicStaticField2 checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.PROTECTED_CONSTANT;}) == 2);
	console.log('Pantene.PROTECTED_CONSTANT checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.protectedStaticField;}) == 6);
	console.log('Pantene.protectedStaticField checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.protectedStaticField2;}) == 2);
	console.log('Pantene.protectedStaticField2 checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.PACKAGE_VISIBLE_CONSTANT;}) == 3);
	console.log('Pantene.PACKAGE_VISIBLE_CONSTANT checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.packageVisibleStaticField;}) == 7);
	console.log('Pantene.packageVisibleStaticField checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.packageVisibleStaticField2;}) == 3);
	console.log('Pantene.packageVisibleStaticField2 checked');
	assertError(function() {com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.PRIVATE_CONSTANT;})}, fieldInvis('PRIVATE_CONSTANT', 'Pantene', 'ClassInTheSamePackage'));
	assertError(function() {com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.privateStaticField;})}, fieldInvis('privateStaticField', 'Pantene', 'ClassInTheSamePackage'));
	assertError(function() {com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.privateStaticField2;})}, fieldInvis('privateStaticField2', 'Pantene', 'ClassInTheSamePackage'));


	console.log('Subclass in the same package:');

	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.PUBLIC_CONSTANT;}) == 1);
	console.log('Pantene.PUBLIC_CONSTANT checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.publicStaticField;}) == 5);
	console.log('Pantene.publicStaticField checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.publicStaticField2;}) == 1);
	console.log('Pantene.publicStaticField2 checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.PROTECTED_CONSTANT;}) == 2);
	console.log('Pantene.PROTECTED_CONSTANT checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.protectedStaticField;}) == 6);
	console.log('Pantene.protectedStaticField checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.protectedStaticField2;}) == 2);
	console.log('Pantene.protectedStaticField2 checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.PACKAGE_VISIBLE_CONSTANT;}) == 3);
	console.log('Pantene.PACKAGE_VISIBLE_CONSTANT checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.packageVisibleStaticField;}) == 7);
	console.log('Pantene.packageVisibleStaticField checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.packageVisibleStaticField2;}) == 3);
	console.log('Pantene.packageVisibleStaticField2 checked');
	assertError(function() {com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.PRIVATE_CONSTANT;})}, fieldInvis('PRIVATE_CONSTANT', 'Pantene', 'SubclassInTheSamePackage'));
	assertError(function() {com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.privateStaticField;})}, fieldInvis('privateStaticField', 'Pantene', 'SubclassInTheSamePackage'));
	assertError(function() {com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.privateStaticField2;})}, fieldInvis('privateStaticField2', 'Pantene', 'SubclassInTheSamePackage'));

	console.log('Class in another package:');

	console.assert(com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.PUBLIC_CONSTANT;}) == 1);
	console.log('Pantene.PUBLIC_CONSTANT checked');
	console.assert(com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.publicStaticField;}) == 5);
	console.log('Pantene.publicStaticField checked');
	console.assert(com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.publicStaticField2;}) == 1);
	console.log('Pantene.publicStaticField2 checked');
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.PROTECTED_CONSTANT;})}, fieldInvis('PROTECTED_CONSTANT', 'Pantene', 'ClassInAnotherPackage'));
	console.log('Pantene.PROTECTED_CONSTANT checked');
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.protectedStaticField;})}, fieldInvis('protectedStaticField', 'Pantene', 'ClassInAnotherPackage'));
	console.log('Pantene.protectedStaticField checked');
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.protectedStaticField2;})}, fieldInvis('protectedStaticField2', 'Pantene', 'ClassInAnotherPackage'));
	console.log('Pantene.protectedStaticField2 checked');
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.PACKAGE_VISIBLE_CONSTANT;})}, fieldInvis('PACKAGE_VISIBLE_CONSTANT', 'Pantene', 'ClassInAnotherPackage'));
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.packageVisibleStaticField;})}, fieldInvis('packageVisibleStaticField', 'Pantene', 'ClassInAnotherPackage'));
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.packageVisibleStaticField2;})}, fieldInvis('packageVisibleStaticField2', 'Pantene', 'ClassInAnotherPackage'));
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.PRIVATE_CONSTANT;})}, fieldInvis('PRIVATE_CONSTANT', 'Pantene', 'ClassInAnotherPackage'));
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.privateStaticField;})}, fieldInvis('privateStaticField', 'Pantene', 'ClassInAnotherPackage'));
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.privateStaticField2;})}, fieldInvis('privateStaticField2', 'Pantene', 'ClassInAnotherPackage'));

	console.log('Subclass in another package:');

	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.PUBLIC_CONSTANT;}) == 1);
	console.log('Pantene.PUBLIC_CONSTANT checked');
	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.publicStaticField;}) == 5);
	console.log('Pantene.publicStaticField checked');
	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.publicStaticField2;}) == 1);
	console.log('Pantene.publicStaticField2 checked');
	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.PROTECTED_CONSTANT;}) == 2);
	console.log('Pantene.PROTECTED_CONSTANT checked');
	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.protectedStaticField;}) == 6);
	console.log('Pantene.protectedStaticField checked');
	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.protectedStaticField2;}) == 2);
	console.log('Pantene.protectedStaticField2 checked');
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.PACKAGE_VISIBLE_CONSTANT;})}, fieldInvis('PACKAGE_VISIBLE_CONSTANT', 'Pantene', 'SubclassInAnotherPackage'));
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.packageVisibleStaticField;})}, fieldInvis('packageVisibleStaticField', 'Pantene', 'SubclassInAnotherPackage'));
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.packageVisibleStaticField2;})}, fieldInvis('packageVisibleStaticField2', 'Pantene', 'SubclassInAnotherPackage'));
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.PRIVATE_CONSTANT;})}, fieldInvis('PRIVATE_CONSTANT', 'Pantene', 'SubclassInAnotherPackage'));
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.privateStaticField;})}, fieldInvis('privateStaticField', 'Pantene', 'SubclassInAnotherPackage'));
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.privateStaticField2;})}, fieldInvis('privateStaticField2', 'Pantene', 'SubclassInAnotherPackage'));

};

