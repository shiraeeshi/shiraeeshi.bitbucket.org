var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;
	var _import = keywords._import;

	return [
		package('com.example')(

		public().clazz('Outer1')(
		
			public().static('int', 'a', 7),	
			static(function() {
				console.log('outer static init block. Outer1.a=' + Outer1.a);
			}),
			static().clazz('Inner')(
				static(function() {
					console.log('inner static init block. Outer1.a=' + Outer1.a);
				}),
				static('int', 'ia', function() {return Outer1.a;})
			)
		),
		public().clazz('Outer2')(
		
			public().static('int', 'a', function() {return Outer2.Inner.ia}),	
			static(function() {
				console.log('outer static init block. Outer2.a=' + Outer2.a);
			}),
			static().clazz('Inner')(
				static(function() {
					console.log('inner static init block. Outer2.a=' + Outer2.a);
				}),
				static('int', 'ia', function() {return Outer2.a;})
			)
		)
		)
	];

};

var testNestedInitOrder = {name: 'Initialization order when nested'};
module.exports = testNestedInitOrder;

testNestedInitOrder.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.Outer1'));
	client(_import('com.example.Outer2'));
	
	new Outer1();
	new Outer2();

	//console.assert(preFrog.someMethod(4) == 8);
	//console.assert(frog.someMethod(4, 5) == 9);
	//console.assert(frog.someMethod(4) == 42);

	console.log('Outer1.Inner.ia = ' + Outer1.Inner.ia);
	console.log('Outer2.Inner.ia = ' + Outer2.Inner.ia);

	console.assert(Outer1.a == 7);
	console.assert(Outer2.a == 0);

	console.assert(Outer1.Inner.ia == 7);
	console.assert(Outer2.Inner.ia == 0);

	console.log('checked');
};

