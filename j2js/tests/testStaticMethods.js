var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;

	return [
		package('com.example')(

		public().clazz('Pantene')(
			public().static('int', 'a', function() {return Pantene.initialA();}),
			public().static('int', 'initialA', [], function(){return 43;}),
			public().static('int', 'b', function() {return Pantene.initialB();}),
			public().static('int', 'initialB', [], function(){return Pantene.privateInitialB();}),
			private().static('int', 'privateInitialB', [], function(){return 52;})
		)

		)
	];

};

var testStaticMethods = {name: 'Field Initialization By Invoking Methods'};
module.exports = testStaticMethods;

testStaticMethods.run = function() {
	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');

//	for (var i=packages.length-1; i>=0; i--) {
//		var eachPackage = packages[i];
//		klassBuilder.buildPackage(eachPackage);
//	}
	klassBuilder.build(packages);
	var methodInvis = helpers.methodInvis;
	var assertError = helpers.assertError;

	var com = globalJ2JS.classpath.com;

	console.assert(com.example.Pantene.a == 43);
	console.log('Pantene.a checked');
	console.assert(com.example.Pantene.initialA() == 43);
	console.log('Pantene.initialA() checked');

	console.assert(com.example.Pantene.b == 52);
	console.log('Pantene.b checked');
	console.assert(com.example.Pantene.initialB() == 52);
	console.log('Pantene.initialB() checked');
	assertError(function() {com.example.Pantene.privateInitialB()}, methodInvis('privateInitialB', 'Pantene', 'undefined'));
	console.log('Pantene.privateInitialB() checked');
};

