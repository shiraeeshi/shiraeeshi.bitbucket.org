var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;

	return [
		package('com.example')(

		public().clazz('PreFrog')(),

		public().clazz('Frog')(['A','B']).extends('PreFrog')(

			public(B, 'someMethod', [A, B], function(a, b) {return a + b;})

		)
		)
	];

};

var testGenericClassSimple = {name: "generic class simple"};
module.exports = testGenericClassSimple;

testGenericClassSimple.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');
	var assertError = helpers.assertError;

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.Frog'));

	var stringFrog = new Frog(['String', 'String'])();
	console.assert(stringFrog instanceof Frog, 'stringFrog is not instanceof Frog?');
	console.assert(stringFrog.someMethod('Hello, ', 'World!') == 'Hello, World!', 'something wrong with generic method "someMethod"');
	
	var intFrog = new Frog(['int', 'int'])();
	console.assert(intFrog instanceof Frog, 'intFrog is not instanceof Frog?');
	console.assert(intFrog.someMethod(5, 3) == 8, 'something wrong with generic method "someMethod"');

	var intStringFrog = new Frog(['int', 'String'])();
	console.assert(intStringFrog instanceof Frog, 'intStringFrog is not instanceof Frog?');
	console.assert(intStringFrog.someMethod(34, 'World!') == '34World!', 'something wrong with generic method "someMethod"');

	assertError(function() {
		stringFrog.someMethod(45, 'bdfg');
	}, 'cannot find suitable method "someMethod" for args "45,bdfg"');

	assertError(function() {
		intFrog.someMethod(45, 'bdfg');
	}, 'cannot find suitable method "someMethod" for args "45,bdfg"');

	assertError(function() {
		intStringFrog.someMethod('45', 'bdfg');
	}, 'cannot find suitable method "someMethod" for args "45,bdfg"');

	console.log('checked');
};

