var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;

	return [
		package('com.example')(

		public().clazz('PreFrog')(),

		public().clazz('Frog').extends('PreFrog')(

			public().static('B', ['A', 'B'], 'someMethod', [A, B], function(a, b) {return a + b;})

		)
		)
	];

};

var testGenericMethodSimple = {name: "generic method simple"};
module.exports = testGenericMethodSimple;

testGenericMethodSimple.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');
	var assertError = helpers.assertError;

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.Frog'));

	console.assert(Frog.someMethod(['String', 'String'])('cor', 'rect') == 'correct')
	console.assert(Frog.someMethod(['int', 'String'])(5, 'hundred') == '5hundred')
	console.assert(Frog.someMethod(['int', 'int'])(5, 6) == 11)

	assertError(function() {
		Frog.someMethod(['String', 'String'])(45, 'bdfg');
	}, '-----------');

	assertError(function() {
		Frog.someMethod(['int', 'String'])('bdfg', 'sdfv');
	}, '-----------');

	assertError(function() {
		StringFrog.someMethod(['int', 'int'])('45', 'bdfg');
	}, '-----------');

	console.log('checked');
};

