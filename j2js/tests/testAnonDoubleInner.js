var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var protected = keywords.protected;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var interface = keywords.interface;
	var package = keywords.package;
	var _import = keywords._import;

	return [
		package('com.example.pluser')(

		clazz('Pluser')(
			protected('int', 'a', 2),
			public('int', 'plus', ['int'], function(num) {return this.a + num;})
		),

		interface('Incrementer')(
			public('int', 'incr', ['int'])
		)

		),
		package('com.example')(
		_import('com.example.pluser.*'),
		public().clazz('Frog')(
			private('Pluser', 'pluser', function() {return new_(Pluser)()(
				public('int', 'plus', ['int'], function(num) {
					var incrementer = new_(Incrementer)()(
						public('int', 'incr', ['int'], function(num) {return num+1;})
					);
					
					var result = num;
					for (var i=this.a; i>0; i--) {
						result = incrementer.incr(result);
					}
					return result;
				})
			);}),

			public('int', 'mult', ['int', 'int'], function(a, b) {
				//this.pluser.setA(b);
				this.pluser.a = b;
				var result = 0;
				for (var i=a; i>0; i--) {
					result = this.pluser.plus(result);
				}
				return result;
			})
		)
		)
	];

};

var testAnonDoubleInner = {name: 'Anonymous inner nested in another inner'};
module.exports = testAnonDoubleInner;

testAnonDoubleInner.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.Frog'));
	
	var frog = new Frog();
	console.assert(frog.mult(5, 7) == 35, 'something is wrong with mult');
	console.assert(frog.mult(5, 7) == 35, 'something is wrong with mult: error when invoked twice ');
	console.log('checked');
};

