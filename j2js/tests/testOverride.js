var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;
	var _import = keywords._import;

	return [
		package('com.example')(

		public().clazz('PreFrog')(

			private('int', 'legsCount', 4),
			public('int', 'someMethod', ['int'], function(n) {
				return this.legsCount + n;
			})
		)
		),
		package('com.example.another')(
		_import('com.example.*'),
		public().clazz('Frog').extends('PreFrog')(
			public('int', 'someMethod', ['int'], function(n) {
				return 42;
			}),
			public('int', 'someMethod', ['int', 'int'], function(n, m) {
				return n + m;
			})
		)
		),
		package('com.example.stat')(
		public().clazz('SomeClass')(
			public().static('int', 'someMethod', [], function(){return 42;}),
			public().static('int', 'someMethod', ['int'], function(n){return n;}),
			public().static('int', 'someMethod', ['int', 'int'], function(n, m){return n + m;})
		)
		)
	];

};

var testOverride = {name: 'Method overriding and overloading'};
module.exports = testOverride;

testOverride.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.PreFrog'));
	client(_import('com.example.another.Frog'));
	
	var preFrog = new PreFrog();
	var frog = new Frog();

	console.assert(preFrog.someMethod(4) == 8);
	console.assert(frog.someMethod(4, 5) == 9);
	console.assert(frog.someMethod(4) == 42);

	client(_import('com.example.stat.SomeClass'));
	console.assert(SomeClass.someMethod() == 42);
	console.assert(SomeClass.someMethod(7) == 7);
	console.assert(SomeClass.someMethod(2, 4) == 6);

	console.log('checked');
};

