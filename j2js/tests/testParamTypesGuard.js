var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;
	var _import = keywords._import;

	return [
		package('com.some.package')(

		public().clazz('Snake')(

			private().String('name'),

			public().Snake(function(name) {
				uber();
				this.name = name;
			}),

			public().String('toString', function() {
				return this.name + ' snake';
			})

		)

		),

		package('com.example')(
		_import('com.some.package.Snake'),

		public().clazz('Frog').extends(PreFrog)(

			public().Void('someMethod', function(paramOne, paramTwo) { })

			public().Void('someMethod', #('int', 'paramOne'), #('String', 'paramTwo'), function() { })

			public().Void('someMethod', @('FormParam',{name: 'address'}).int('paramOne'), String('paramTwo'), Snake('snake'), function() {
				console.log(paramOne + paramTwo + snake)
			})

			public().Void('someMethod', [annotated(['FormParam', {name: 'address'}],'int'), 'String', 'Snake'], function(paramOne, paramTwo, snake) { })

		),

		public().clazz('PreFrog')()

		)
	];

};

var testParamTypesGuard = {name: "method parameter types"};
module.exports = testParamTypesGuard;

testParamTypesGuard.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');
	var assertError = helpers.assertError;

	for (var i=packages.length-1; i>=0; i--) {
		var eachPackage = packages[i];
		klassBuilder.buildPackage(eachPackage);
	}

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.PreFrog'));
	client(_import('com.example.Frog'));

	console.assert(Object.keys(PreFrog.prototype.__clazz.members.instance.constructors) != 0);
	console.assert(Object.keys(Frog.prototype.__clazz.members.instance.constructors) != 0);
	
	new Frog();

	console.log('checked');
};

