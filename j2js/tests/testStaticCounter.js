var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;

	return [
		package('com.example')(

		public().clazz('Frog')(

			public().static('int', 'frogCount', 0),

			public('Frog', [], function() {
				uber();
				Frog.frogCount++;
			}),

			public().static('int', 'treeFrogs', [], function(){
				new Frog();
				new Frog();
				new Frog();
				return Frog.frogCount;
			})
		)
		)
	];

};

var testStaticCounter = {name: 'Static Counter Increments In Constructor'};
module.exports = testStaticCounter;

testStaticCounter.run = function() {
	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');

//	for (var i=packages.length-1; i>=0; i--) {
//		var eachPackage = packages[i];
//		klassBuilder.buildPackage(eachPackage);
//	}

	klassBuilder.build(packages);

	var Frog = globalJ2JS.classpath.com.example.Frog;

	console.assert(Frog.treeFrogs() == 3);
	console.assert(Frog.frogCount == 3);
	console.log('checked');
};

