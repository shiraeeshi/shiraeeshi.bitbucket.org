var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var protected = keywords.protected;
	var private = keywords.private;
	var static = keywords.static;
	var final = keywords.final;
	var clazz = keywords.clazz;
	var _import = keywords._import;
	var package = keywords.package;

	return [

		package("com.example")(

		public().clazz('Pantene')(

			public().static('int', 'publicStatic', [], function() {return 75;}),
			protected().static('int', 'protectedStatic', [], function() {return 76;}),
			static('int', 'packageVisibleStatic', [], function() {return 77;}),
			private().static('int', 'privateStatic', [], function() {return 78;}),

			public().static('int', 'runFunc', ['func'], function(f){return f();})
		),

		public().clazz('ClassInTheSamePackage')(
			public().static('int', 'runFunc', ['func'], function(f){return f();})
		),

		public().clazz('SubclassInTheSamePackage').extends('Pantene')(
			public().static('int', 'runFunc', ['func'], function(f){return f();})
		)

		),

		package('com.example.another')(
		_import('com.example.Pantene'),

		public().clazz('SubclassInAnotherPackage').extends('Pantene')(
			public().static('int', 'runFunc', ['func'], function(f){return f();})
		),

		public().clazz('ClassInAnotherPackage')(
			public().static('int', 'runFunc', ['func'], function(f){return f();})
		)

		)

	];
};

var testStaticMethodVisibility = {name: 'Method Visibility'};

module.exports = testStaticMethodVisibility;

testStaticMethodVisibility.run = function() {

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');
	var methodInvis = helpers.methodInvis;
	var assertError = helpers.assertError;

	var packages = buildPackages();

//	for (var i=packages.length-1; i>=0; i--) {
//		var eachPackage = packages[i];
//		klassBuilder.buildPackage(eachPackage);
//	}

	klassBuilder.build(packages);

	var com = globalJ2JS.classpath.com;

	console.log('global:');

	console.assert(com.example.Pantene.publicStatic() == 75);
	console.log('Pantene.publicStatic() checked');
	assertError(function() {return com.example.Pantene.protectedStatic();}, methodInvis('protectedStatic', 'Pantene', 'undefined'));
	console.log('Pantene.protectedStatic() checked');
	assertError(function() {return com.example.Pantene.packageVisibleStatic();}, methodInvis('packageVisibleStatic', 'Pantene', 'undefined'));
	console.log('Pantene.packageVisibleStatic() checked');
	assertError(function() {return com.example.Pantene.privateStatic();}, methodInvis('privateStatic', 'Pantene', 'undefined'));
	console.log('Pantene.privateStatic() checked');

	console.log('class itself:');

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.publicStatic();}) == 75);

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.protectedStatic();}) == 76);

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.packageVisibleStatic();}) == 77);

	console.assert(com.example.Pantene.runFunc(function() {return Pantene.privateStatic();}) == 78);

	console.log('Another class in the same package:');

	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.publicStatic();}) == 75);
	console.log('Pantene.publicStatic() checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.protectedStatic();}) == 76);
	console.log('Pantene.protectedStatic() checked');
	console.assert(com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.packageVisibleStatic();}) == 77);
	console.log('Pantene.packageVisibleStatic() checked');
	assertError(function() {com.example.ClassInTheSamePackage.runFunc(function() {return Pantene.privateStatic();})}, methodInvis('privateStatic', 'Pantene', 'ClassInTheSamePackage'));
	console.log('Pantene.privateStatic() checked');

	console.log('Subclass in the same package:');

	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.publicStatic();}) == 75);
	console.log('Pantene.publicStatic() checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.protectedStatic();}) == 76);
	console.log('Pantene.protectedStatic() checked');
	console.assert(com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.packageVisibleStatic();}) == 77);
	console.log('Pantene.packageVisibleStatic() checked');
	assertError(function() {com.example.SubclassInTheSamePackage.runFunc(function() {return Pantene.privateStatic();})}, methodInvis('privateStatic', 'Pantene', 'SubclassInTheSamePackage'));
	console.log('Pantene.privateStatic() checked');

	console.log('Class in another package:');

	console.assert(com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.publicStatic();}) == 75);
	console.log('Pantene.publicStatic() checked');
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.protectedStatic();})}, methodInvis('protectedStatic', 'Pantene', 'ClassInAnotherPackage'));
	console.log('Pantene.protectedStatic() checked');
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.packageVisibleStatic();})}, methodInvis('packageVisibleStatic', 'Pantene', 'ClassInAnotherPackage'));
	console.log('Pantene.packageVisibleStatic() checked');
	assertError(function() {com.example.another.ClassInAnotherPackage.runFunc(function() {return Pantene.privateStatic();})}, methodInvis('privateStatic', 'Pantene', 'ClassInAnotherPackage'));
	console.log('Pantene.privateStatic() checked');

	console.log('Subclass in another package:');

	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.publicStatic();}) == 75);
	console.log('Pantene.publicStatic() checked');
	console.assert(com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.protectedStatic();}) == 76);
	console.log('Pantene.protectedStatic() checked');
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.packageVisibleStatic();})}, methodInvis('packageVisibleStatic', 'Pantene', 'SubclassInAnotherPackage'));
	console.log('Pantene.packageVisibleStatic() checked');
	assertError(function() {com.example.another.SubclassInAnotherPackage.runFunc(function() {return Pantene.privateStatic();})}, methodInvis('privateStatic', 'Pantene', 'SubclassInAnotherPackage'));
	console.log('Pantene.privateStatic() checked');

};

