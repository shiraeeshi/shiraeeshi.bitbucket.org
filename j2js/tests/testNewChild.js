var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;

	return [
		package('com.example')(

		public().clazz('PreFrog')(),

		public().clazz('Frog').extends('PreFrog')()
		)
	];

};

var testNewChild = {name: "Subclass initialization works"};
module.exports = testNewChild;

testNewChild.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');
	var assertError = helpers.assertError;

//	for (var i=packages.length-1; i>=0; i--) {
//		var eachPackage = packages[i];
//		klassBuilder.buildPackage(eachPackage);
//	}

	klassBuilder.build(packages);

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.PreFrog'));
	client(_import('com.example.Frog'));
	
	var frog = new Frog();
	
	console.assert(frog instanceof PreFrog);
	console.assert(frog instanceof Frog);

	console.log('checked');
};

