
var assertError = function(f, msg) {
	try {
		f();
		throw new ErrorNotThrownError('expected error, but nothing was thrown: ' + msg);
	}
	catch (e) {
		if (e instanceof ErrorNotThrownError) {
			throw new Error(e.message);
		}
		console.assert(e.message == msg, 'msg: "' + msg + '", e.message: "' + e.message + '"');
	}
};
var fieldInvis = function(fieldName, className, invokerName) {
	return 'field ' + fieldName + ' of class ' + className + 
		' is invisible for invoker ' + invokerName;
};
var methodInvis = function(methodName, className, invokerName) {
	return 'method ' + methodName + ' of class ' + className + 
		' is invisible for invoker ' + invokerName;
};

var ErrorNotThrownError = function(msg) {
	this.message = msg;
};

var helpers = {};
helpers.assertError = assertError;
helpers.fieldInvis = fieldInvis;
helpers.methodInvis = methodInvis;

module.exports = helpers;
