
var Visibility = function VisibilityInitFunction(tostr){
	this.toString = function() {return tostr;}
};
var Visibilities = {
	'PUBLIC': new Visibility('public'),
	'PROTECTED': new Visibility('protected'),
	'PRIVATE': new Visibility('private'),
	'DEFAULT': new Visibility('default')
};

module.exports = {};
module.exports = Visibilities;
