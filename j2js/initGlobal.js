var mainParent = require('./klass_utils/mainParent.js');
var anon = require('./klass_utils/anon.js');

var globalJ2JS = {
	classpath: {},
	mainParent: mainParent,
	calleeNames: {
		constructor: {
			isConstructor: true,
			str: '__j2js__constr'
		},
		initBlock: {
			isStatic: false, 
			isInitBlock: true,
			str: '__j2js__init'
		},
		staticInitBlock: {
			isStatic: true, 
			isInitBlock: true,
			str: '__j2js__st_init'
		},
		createFieldInitializerName: function(fieldName, isStatic) {
			return {
				isStatic: isStatic,
				isField: true,
				fieldName: fieldName,
				str: '__j2js__' + (isStatic ? 's_' : '') + 'f_' + fieldName
			};
		}
	}
};

global.globalJ2JS = globalJ2JS;

global.new_ = anon.new_;
