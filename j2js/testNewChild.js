var window = global;

var buildPackages = function() {
	var keywords = require('../expr/keywords.js');

	var public = keywords.public;
	var private = keywords.private;
	var static = keywords.static;
	var clazz = keywords.clazz;
	var package = keywords.package;

	return [
		package('com.example')(

		public().clazz('PreFrog')(
			public('PreFrog', function() {
				// doesn't call uber
				uber();
			})
		),

		public().clazz('Frog').extends('PreFrog')(

			private('int', 'legsCount', '=', 4),

			public('Frog', function() {
				uber();
			})

		)
		)
	];

};

var testParentNotCallingUber = {name: "parent constructor doesn't call uber"};
module.exports = testParentNotCallingUber;

testParentNotCallingUber.run = function() {

	var packages = buildPackages();

	var klassBuilder = require('../klass_utils/klassbuilder.js');
	var helpers = require('./helpers.js');
	var assertError = helpers.assertError;

	for (var i=packages.length-1; i>=0; i--) {
		var eachPackage = packages[i];
		klassBuilder.buildPackage(eachPackage);
	}

	var client = require('../expr/clientImport.js').client;
	var _import = require('../expr/keywords.js')._import;

	client(_import('com.example.Frog'));
	
	assertError(function() {
		new Frog();
	}, 'constructor must call either uber or this.');

	console.log('checked');
};

