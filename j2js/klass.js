var window = global;

var getThisFunc = require('./klass_utils/getThisFunc.js');
var memberIsVisibleFor = require('./klass_utils/checks.js').memberIsVisibleFor;
var copyAllProperties = require('./klass_utils/copiers.js').copyAllProperties;
var Visibility = require('./Visibility.js');
var methodUtils = require('./klass_utils/methodUtils.js');

var klass = function (parsedClass) {
	var Parent = parsedClass.superClass.inClasspath;
	var Child, F, i;
	var publicMethods = parsedClass.members.instance.methods[Visibility.PUBLIC],
	    protectedMethods = parsedClass.members.instance.methods[Visibility.PROTECTED],
	    privateMethods = parsedClass.members.instance.methods[Visibility.PRIVATE],
	    packageMethods = parsedClass.members.instance.methods[Visibility.DEFAULT];
	var fields = parsedClass.members.instance.fields;
	var constrs = parsedClass.members.instance.constructors;
	// 1.
	// new constructor
	Child = function () {
		var invoker = globalJ2JS.invoker;
		var instance = Object.create(new ChildProto(), {
			'__j2js': {
				value: {
					uber: undefined, // will be defined in uberFunc
					fields: undefined,
					anons: {} // needed to give distinct names to anonymous classes
				}
			}
		});

		instance.__j2js.fields = Object.create(fieldsProto, {
			'__j2js': {
				value: {instance: instance}
			}
		});

		var thisFunc = getThisFunc(instance, globalJ2JS.invoker, Child);
//		thisFunc.apply(null, arguments);
//
//		if ( ! instance.__j2js.uberCalled) {
//			throw new Error('constructor must call either uber or this.');
//		}
//		delete instance.__j2js.uberCalled;
//		
//		return instance;

		var innerConstructorFunc = function() {

			thisFunc.apply(null, arguments);

			handleUberCalled(instance);
			return instance;
		};
		extend(innerConstructorFunc, Child); // TODO consider generic after extends keyword (and also instanceof)
		// innerConstructorFunc.__j2js = {generic: Child};

		if (parsedClass.isGeneric) {
			instance.__j2js.genericInfo = parsedClass.buildGenericInfo(arguments[0]);
			return innerConstructorFunc;
		} else {
			return innerConstructorFunc.apply(null, arguments);
		}
	};

	var extend = function(A, B) {
		var Middle = function() {};
		Middle.prototype = B.prototype;
		A.prototype = new Middle();
	};

	var handleUberCalled = function(instance) {
		if ( ! instance.__j2js.uberCalled) {
			throw new Error('constructor must call either uber or this.');
		}
		delete instance.__j2js.uberCalled;
	};

	Object.defineProperty(Child, 'This', {// 'This' reference for inner classes
		get: function() {
			var invokerInstance = globalJ2JS.invokerInstance;
			var nester = invokerInstance.__j2js.nesters[parsedClass.fullName];
			if (nester === undefined) {
				var invoker = globalJ2JS.invoker;
				throw new Error(
					"no nester of type \"" + parsedClass.package + '.' + parsedClass.name + "\""+
					" avaiable for type \"" + (invoker === undefined?'undefined':invoker.prototype.__clazz.name)
					 + "\"");
			}
			return nester;
		}
	});

	// 2.
	// inherit
	var mainParent = window['globalJ2JS'].mainParent;
	var MiddleMain = function() {};
	MiddleMain.prototype = mainParent.prototype;

	var hierarchy = [];

	if ( ! Parent) {
		Parent = function() {}
		Parent.prototype = new MiddleMain();
	}
	else {
		var proto = Parent.prototype;
		while (true) {
			if (mainParent.prototype == proto) break;
			hierarchy.push(proto.constructor);
			proto = Object.getPrototypeOf(proto);
		}
		if ( ! mainParent.prototype.isPrototypeOf(Parent.prototype)) {
			proto.prototype = new MiddleMain();
			proto.prototype.uber = mainParent;
		}
	}
	F = function () {};
	F.prototype = Parent.prototype;
	Child.prototype = new F();

	var G = function() {};
	G.prototype = Child.prototype;
	var ChildProto = function() {};
	ChildProto.prototype = new G();

	var interfaces = {};
	// collect interfaces
	for (var i = hierarchy.length-1; i >= 0; i--) {
		var parent = hierarchy[i];
		var parentInterfaces = parent.prototype.__clazz.interfaces;
		for (var j=parentInterfaces.length-1; j >= 0; j--) {
			var pint = parentInterfaces[j];
			interfaces[pint.fullName] = undefined;
		}
	}
	for (var j=parsedClass.interfaces.length-1; j >= 0; j--) {
		var pint = parsedClass.interfaces[j];
		interfaces[pint.fullName] = undefined;
	}
	Child.interfaces = interfaces;

	var allFields = {};
	var allMethods = {};
	var allStaticMethods = {};

	var copyMethods = methodUtils.copyMethods;
	var insertIntoAllMethods = methodUtils.insertIntoAllMethods;
	var findSuitable = methodUtils.findSuitable;

	var fromClassToAllMethods = function(clazz) {
		var tmp = {};

		// TODO copy all methods into tmp
		// copy from tmp to allMethods
		// consider method overloading/overriding

		var tmp = {};

		copyMethods(clazz.members.instance.methods[Visibility.PUBLIC], tmp);
		copyMethods(clazz.members.instance.methods[Visibility.PROTECTED], tmp);
		var isFromSamePackage = clazz.package == parsedClass.package;
		if (isFromSamePackage) {
			copyMethods(clazz.members.instance.methods[Visibility.DEFAULT], tmp);
		}
		if (clazz == parsedClass) {
			copyMethods(clazz.members.instance.methods[Visibility.PRIVATE], tmp);
		}

		for (var name in tmp) {
			if ( ! tmp.hasOwnProperty(name)) continue;
			if ( ! allMethods[name]) allMethods[name] = {};
			insertIntoAllMethods(tmp, name, allMethods);
			if (tmp[name]['*']) {
				insertIntoAllMethods(tmp, name, allMethods, true);
			}
		}
	};
	for (var i = hierarchy.length-1; i >= 0; i--) {
		var parent = hierarchy[i];
		fromClassToAllMethods(parent.prototype.__clazz);

		// old version:

//		copyAllProperties(parent.prototype.__clazz.members.instance.methods[Visibility.PUBLIC], allMethods);
//		copyAllProperties(parent.prototype.__clazz.members.instance.methods[Visibility.PROTECTED], allMethods);
//		var isFromSamePackage = parent.prototype.__clazz.package == parsedClass.package;
//		if (isFromSamePackage) {
//			copyAllProperties(parent.prototype.__clazz.members.instance.methods[Visibility.DEFAULT], allMethods);
//		}
	}
	fromClassToAllMethods(parsedClass);

	// static methods
	(function() {
		var tmp = {};
		copyMethods(parsedClass.members.static.methods[Visibility.PUBLIC], tmp);
		copyMethods(parsedClass.members.static.methods[Visibility.PROTECTED], tmp);
		copyMethods(parsedClass.members.static.methods[Visibility.DEFAULT], tmp);
		copyMethods(parsedClass.members.static.methods[Visibility.PRIVATE], tmp);
		for (var name in tmp) {
			if ( ! tmp.hasOwnProperty(name)) continue;
			if ( ! allStaticMethods[name]) allStaticMethods[name] = {};
			insertIntoAllMethods(tmp, name, allStaticMethods);
			if (tmp[name]['*']) {
				insertIntoAllMethods(tmp, name, allStaticMethods, true);
			}
		}
	})();

	// copy fields
	for (var i = hierarchy.length-1; i >= 0; i--) {
		var parent = hierarchy[i];
		var isFromSamePackage = parent.prototype.__clazz.package == parsedClass.package;
		var parentFields = parent.prototype.__clazz.members.instance.fields;
		for (var j=parentFields.length-1; j>=0; j--) {
			var field = parentFields[j];
			if (field.isPrivate()) continue;
			if (field.isPackageVisible() && ! isFromSamePackage) continue;
			allFields[field.name] = field;
		}
	}
	var ownFieldNames = {};
	for (var i=0, len=fields.length; i<len; i++) {
		var field = fields[i];
		allFields[field.name] = field;
		ownFieldNames[field.name] = undefined; // 'hasOwnProperty' will return 'true'
	}
	var fieldsProto = {};
	for (var fieldName in allFields) {
		if ( ! allFields.hasOwnProperty(fieldName)) continue;
		var field = allFields[fieldName];
		if ( ! ownFieldNames.hasOwnProperty(fieldName)) { // TODO rewrite (get rid of fieldValueInitialized, and maybe of fieldsProto too)
			Object.defineProperty(fieldsProto, fieldName, (function() { 
					var fieldValue;
					var fieldValueInitialized;
					return {
						get: function() {
							var invoker = window['globalJ2JS'].invoker;
							if (!memberIsVisibleFor(Child,field,invoker)) {
								throw new Error('field ' + field.name + ' of class ' + parsedClass.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
							}
							if ( ! fieldValueInitialized) {
								window['globalJ2JS'].invoker = Child;
								fieldValue = this.__j2js.instance.__j2js.uber[fieldName]
								window['globalJ2JS'].invoker = invoker;
								fieldValueInitialized = true;
							}
							return fieldValue; 
						},
						set: function(val) {
							var invoker = window['globalJ2JS'].invoker;
							if (!memberIsVisibleFor(Child,field,invoker)) {
								throw new Error('field ' + field.name + ' of class ' + parsedClass.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
							}
							fieldValue = val;
							fieldValueInitialized = true;
						}
					};
				})()
			);
		}
		Object.defineProperty(ChildProto.prototype, fieldName, {
			get: function() {
				var invoker = window['globalJ2JS'].invoker;
				if (!memberIsVisibleFor(Child,field,invoker)) {
					throw new Error('field ' + field.name + ' of class ' + parsedClass.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
				}
				return this.__j2js.fields[fieldName];
			},
			set: function(val) {
				var invoker = window['globalJ2JS'].invoker;
				if (!memberIsVisibleFor(Child,field,invoker)) {
					throw new Error('field ' + field.name + ' of class ' + parsedClass.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
				}
				this.__j2js.fields[fieldName] = val;
			}
		});
	}
//	copyAllProperties(parsedClass.members.instance.methods[Visibility.PUBLIC], allMethods);
//	copyAllProperties(parsedClass.members.instance.methods[Visibility.PROTECTED], allMethods);
//	copyAllProperties(parsedClass.members.instance.methods[Visibility.PRIVATE], allMethods);
//	copyAllProperties(parsedClass.members.instance.methods[Visibility.DEFAULT], allMethods);

	Child.uber = Parent.prototype;

	Child.prototype.constructor = Child;
	ChildProto.prototype.constructor = Child;
	Child.prototype.__clazz = parsedClass;

	for (methodName in allMethods) {
		if (!allMethods.hasOwnProperty(methodName)) continue;
		ChildProto.prototype[methodName] = (function me(methodName, methodsByLength, methodGenericValues) {
			return function() {
				var self = this;
				var args = arguments;
				var invoker = window['globalJ2JS'].invoker;
				var genericInfo = self.__j2js.genericInfo;
				if (args.length == 1 && args[0] instanceof Array) { // single array argument has special meaning
					if (methodGenericValues) throw new Error('wrong syntax: specifying generic values twice (single array argument has special meaning).');
					var methodGV = args[0];
					return me.call(self, methodName, methodsByLength, methodGV);
				}
				var met = findSuitable(methodsByLength, args, genericInfo, methodGenericValues);
				if ( ! met) {
					var argsStr = Array.prototype.join.apply(args, [',']);
					throw new Error('cannot find suitable method "' + methodName + '" for args "' + argsStr + '"');
				}
				if (!memberIsVisibleFor(Child, met, invoker)) {
					throw new Error('method ' + met.name + ' of class ' + Child.prototype.__clazz.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
				}
				var result = methodUtils.invokeMethod(met, args, self, parsedClass, Child, Child);
				return result;
			};
		})(methodName, allMethods[methodName]);
	}

	for (methodName in allStaticMethods) {
		if (!allStaticMethods.hasOwnProperty(methodName)) continue;
		Child[methodName] = (function(methodName, methodsByLength) {
			return function() {
				var args = arguments;
				var invoker = window['globalJ2JS'].invoker;
				var met = findSuitable(methodsByLength, args);
				if ( ! met) {
					var argsStr = Array.prototype.join.apply(args, [',']);
					throw new Error('cannot find suitable method "' + methodName + '" for args "' + argsStr + '"');
				}
				if (!memberIsVisibleFor(Child, met, invoker)) {
					throw new Error('method ' + met.name + ' of class ' + Child.prototype.__clazz.name + ' is invisible for invoker ' + (invoker === undefined?'undefined':invoker.prototype.__clazz.name));
				}
				var result = methodUtils.invokeStaticMethod(met, args, parsedClass, Child, Child);
				return result;
			};
		})(methodName, allStaticMethods[methodName]);
	}
	// return the "class"
	return Child;
};

module.exports = klass;
