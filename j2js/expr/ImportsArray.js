var Import = require('./Import.js');

var ImportsArray = (function() {
	var initFunction = function ImportsArrayInitFunction(imports) {
		for (var i=0, len=imports.length; i<len; i++) {
			if ( !(imports[i] instanceof Import)) {
				throw new Error('bad inputs array element.');
			}
		}
		this.imports = imports;
	};
	initFunction.prototype.containsImport = function(importString) {
		for (var i=0, len=this.imports.length; i<len; i++) {
			if (this.imports[i].importString == importString) return true;
		}
		return false;
	};
	return initFunction;
})();

module.exports = ImportsArray;
