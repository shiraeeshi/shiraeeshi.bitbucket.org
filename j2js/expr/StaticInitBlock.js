
var StaticInitBlock = (function() {
	var initFunction = function StaticInitBlockInitFunction(body) {
		this.body = body;
	};
	return initFunction;
})();

module.exports = StaticInitBlock;
