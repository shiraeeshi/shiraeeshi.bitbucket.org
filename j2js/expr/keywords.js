var ExpressionBuilder = require('./ExpressionBuilder.js');

var window = global;

var public = function() {
	var expr = new ExpressionBuilder();
	return expr.public.apply(expr, arguments);
};

var protected = function() {
	var expr = new ExpressionBuilder();
	return expr.protected.apply(expr, arguments);
};

var private = function() {
	var expr = new ExpressionBuilder();
	return expr.private.apply(expr, arguments);
};

var package_visible = function() {
	var expr = new ExpressionBuilder();
	return expr.package_visible.apply(expr, arguments);
};

var final = function() {
	var expr = new ExpressionBuilder();
	return expr.final.apply(expr, arguments);
};

var Void = function() {
	var expr = new ExpressionBuilder();
	return expr.Void.apply(expr, arguments);
};

var abstract = function() {
	var expr = new ExpressionBuilder();
	return expr.abstract.apply(expr, arguments);
};

var StaticInitBlock = require('./StaticInitBlock.js');

var static = function() {
	if (arguments.length == 1 && arguments[0] instanceof Function) {
		return new StaticInitBlock(arguments[0]);
	}
	var expr = new ExpressionBuilder();
	return expr.static.apply(expr, arguments);
};

var Visibility = require('../Visibility.js');
var ClassExprBuilder = require('./ClassExprBuilder.js');
var InterfaceExprBuilder = require('./InterfaceExprBuilder.js');

var clazz = function(className) {
	return new ClassExprBuilder(Visibility.DEFAULT, false, false, false, className);
};

var interface = function(name) {
	return new InterfaceExprBuilder(Visibility.DEFAULT, false, name);
};

var SourceFile = require('./Package.js');

var package = function(packageString) {
	if (packageString.length == 0) throw new Error('empty package name');
	return function() {
		var classesAndImports = Array.prototype.slice.call(arguments);
		return new SourceFile(packageString, classesAndImports);
	};
};


var Import = require('./Import.js');

var importFunc = function() {
	if (arguments.length == 1 && typeof arguments[0] == 'string') {
		var importString = arguments[0];
		var imp = new Import(importString);
		return imp;
	}
	if (arguments.length == 0) {
		return {
			static: function(importString) {
				var imp = new Import(importString, true);
				return imp;
			}
		};
	}
	throw new Error('unknown import format');
};

module.exports = {};
module.exports.public = public;
module.exports.protected = protected;
module.exports.private = private;
module.exports.package_visible = package_visible;
module.exports.Void = Void;
module.exports.static = static;
module.exports._import = importFunc;
module.exports.final = final;
module.exports.abstract = abstract;
module.exports.clazz = clazz;
module.exports.interface = interface;
module.exports.package = package;

