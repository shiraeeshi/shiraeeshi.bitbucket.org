var Class = require('./Class.js');

var Interface = (function() {
	var initFunction = function InterfaceInitFunction(visibility, isStatic, name, body, superClass) {
		var isFinal = false;
		var isAbstract = false;
		var implementedInterfaces = [];
		var instance = new Class(visibility, isStatic, isFinal, isAbstract, name, body, superClass, implementedInterfaces);
		validateBody(instance);
		return instance;
	};

	var validateBody = function(instance) {}; // TODO check that interface-specific rules aren't broken (interface may not declare static methods, only constants, interface can only extend interface and other rules like that)

	var F = function() {};
	F.prototype = Class.prototype;
	initFunction.prototype = new F();

	return initFunction;
})();

module.exports = Interface;
