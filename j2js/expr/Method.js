var Visibility = require('../Visibility.js');
var Class = require('./Class.js');
var GenericName = require('./GenericName.js');
var copyAllProperties = require('../klass_utils/copiers.js').copyAllProperties;

var Method = (function() {
	var initFunction = function MethodInitFunction(visibility, isStatic, isFinal, isAbstract, 
			isVoid, returnType, name, paramTypeInfo, body, isConstructor, genericNames) {
		this.visibility = visibility;
		this.isStatic = isStatic;
		this.isFinal = isFinal;
		this.isAbstract = isAbstract;
		this.isVoid = isVoid;
		this.returnType = returnType;
		this.name = name;
		this.paramTypeInfo = paramTypeInfo;
		this.genericNames = genericNames;
		this.hasGenericParams = hasGenericParams(paramTypeInfo);
		this.isVarargs = isVarargs(paramTypeInfo);
		if (this.isVarargs) {
			removeThreeDots(this);
		}
		this.body = body;
		this.isConstructor = isConstructor;

		this.length = paramTypeInfo.length;
		/*
		if (isAbstract) {
			if (paramTypeInfo === undefined) throw new Error("abstract method's param types info needs to be specified");
			this.length = paramTypeInfo.length;
		} else {
			this.length = body.length;
		}
		*/
	};
	var removeThreeDots = function(instance) {
		var arr = instance.paramTypeInfo;
		var len = arr.length;
		var lastParam = arr[len-1];
		instance.paramTypeInfo[len-1] = lastParam.substr(0, lastParam.indexOf('...')).trim();
	};
	var isVarargs = function(paramTypeInfo) {
		if ( ! paramTypeInfo) return false;
		if (paramTypeInfo.length == 0) return false;
		var lastParamInfo = paramTypeInfo[paramTypeInfo.length-1];
		if (typeof(lastParamInfo) == 'string') {
			lastParamInfo = lastParamInfo.trim();
			return lastParamInfo.indexOf('...') == lastParamInfo.length - 3;
		} else {
			if (lastParamInfo.isGeneric) return false; // TODO temporary workaround
			throw new Error('not implemented');
		}
	};
	var hasGenericParams = function(paramTypeInfo) {
		for (var i=paramTypeInfo.length-1; i>=0; i--) {
			if (paramTypeInfo[i].isGeneric) return true;
		}
		return false;
	};
	initFunction.prototype.isPublic = function() {
		return this.visibility == Visibility.PUBLIC;
	};
	initFunction.prototype.isProtected = function() {
		return this.visibility == Visibility.PROTECTED;
	};
	initFunction.prototype.isPrivate = function() {
		return this.visibility == Visibility.PRIVATE;
	};
	initFunction.prototype.isPackageVisible = function() {
		return this.visibility == Visibility.DEFAULT;
	};
	initFunction.prototype.overrides = function(met) {
		if (this.name !== met.name) return false;
		var selfParams = this.paramTypeInfo;
		var otherParams = met.paramTypeInfo;
		if (selfParams.length != otherParams.length) return false;
		for (var i=0, len=selfParams.length; i<len; i++) {
			if (selfParams[i] !== otherParams[i]) return false;
		}
		if (this.visibility != met.visibility) throw new Error('cannot override with another visibility');
		if (this.returnType != met.returnType) throw new Error('cannot override with another returnType');
		if (met.isFinal) throw new Error('cannot override final method');
		if (this.isStatic || met.isStatic) throw new Error('one of two methods is static');
		return true;
	};
	initFunction.prototype.isSuitableFor = function(args, genericInfo, methodGenericValues) {
		if (args.length < this.paramTypeInfo.length) return false;
		if (args.length > this.paramTypeInfo.length && ! this.isVarargs) return false;
		if (this.genericNames && !methodGenericValues ||
				methodGenericValues && !this.genericNames) return false;

		if (this.genericNames) {
			if (this.genericNames.length != methodGenericValues.length) return false;
			var methodGenericInfo = createMethodGenericInfo(this.genericNames, methodGenericValues);
			genericInfo = combine(genericInfo, methodGenericInfo);
		}
		
		var paramTypeInfo = this.paramTypeInfo;
		if (this.hasGenericParams) {
			paramTypeInfo = paramTypeInfo.slice();
			for (var i=paramTypeInfo.length-1; i>=0; i--) {
				var paramType = paramTypeInfo[i];
				if (paramType.isGeneric) {
					paramTypeInfo[i] = genericInfo[paramType.name];
				}
			}
		}

		var isInstanceOf = function(value, paramType) {
			if (Class.isBuiltin(paramType)) {
				if ( ! Class.isInstanceOfBuiltin(value, paramType)) return false;
			} else {
				var paramClass = Class.forName(paramType).inClasspath;
				if ( ! value instanceof paramClass) return false;
			}
			return true;
		};
		for (var i=0, len=paramTypeInfo.length; i<len; i++) {
			var value = args[i];
			var paramType = paramTypeInfo[i];
			if ( ! isInstanceOf(value, paramType)) return false;
		}
		if (args.length > paramTypeInfo.length) {
			var paramType = paramTypeInfo[paramTypeInfo.length-1];
			for (var i=paramTypeInfo.length, len=args.length; i<len; i++) {
				var value = args[i];
				if ( ! isInstanceOf(value, paramType)) return false;
			}
		}
		return true;
	};
	var createMethodGenericInfo = function(names, values) {
		var result = {};
		for (var i=0, len=names.length; i<len; i++) {
			var name = names[i];
			var value = values[i];
			result[name] = value;
		}
		return result;
	};
	var combine = function(first, second) {
		var obj = {};
		copyAllProperties(first, obj);
		copyAllProperties(second, obj);
		return obj;
	};
	return initFunction;
})();

module.exports = Method;
