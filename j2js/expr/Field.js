var Visibility = require('../Visibility.js');

var Field = (function() {
	var initFunction = function FieldInitFunction(visibility, isStatic, isFinal, type, name, value) {
		this.visibility = visibility;
		this.isStatic = isStatic;
		this.isFinal = isFinal;
		this.type = type;
		this.name = name;
		if (value === undefined) {
			value = defaultValue.call(this);
		}
		this.value = value;
	};
	var defaultValue = function() {
		var type = this.type;
		if (type == 'int' || 
			type == 'char' || 
			type == 'byte' ||
			type == 'short' ||
			type == 'long') return 0;
		if (type == 'double' || 
			type == 'float') return 0.0;
		if (type == 'boolean') return false;
		return null;
	};
	initFunction.prototype.defaultValue = defaultValue;
	initFunction.prototype.isPublic = function() {
		return this.visibility == Visibility.PUBLIC;
	};
	initFunction.prototype.isProtected = function() {
		return this.visibility == Visibility.PROTECTED;
	};
	initFunction.prototype.isPrivate = function() {
		return this.visibility == Visibility.PRIVATE;
	};
	initFunction.prototype.isPackageVisible = function() {
		return this.visibility == Visibility.DEFAULT;
	};
	return initFunction;
})();

module.exports = Field;
