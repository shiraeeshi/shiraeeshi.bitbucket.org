
var injectImport = require('../klass_utils/context.js').injectImport;

var client = function(_import) {
	injectImport(_import);
};

module.exports.client = client;
