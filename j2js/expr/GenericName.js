var GenericName = (function() {
	var initFunction = function GenericNameInitFunction(name) {
//		if ( ! (name instanceof String) || name.length == 0) {
//			throw new Error("illegal GenericName argument: " + name);
//		}
		this.name = name;
	};
	initFunction.prototype.isGeneric = true;
	return initFunction;
})();

module.exports = GenericName;
