var Field = require('./Field.js');
var Method = require('./Method.js');
var ClassExprBuilder = require('./ClassExprBuilder.js');
var InterfaceExprBuilder = require('./InterfaceExprBuilder.js');
var Visibility = require('../Visibility.js');
var GenericName = require('./GenericName.js');

var ExpressionBuilder = (function() {
	var initFunction = function ExpressionBuilderInitFunction() {
		var isPublic = false, isProtected = false, isPrivate = false, isStatic = false, isFinal = false, isAbstract = false,
    			isField = false, isMethod = false, 
    			isVoid = false;
		var fieldName, methodName;
		var fieldType, fieldValue;
		var methodGenericNames, methodParamTypeInfo, methodReturnType, methodBody;

		var instance = {};
		var returnExprOrSelf = function(type, name) {

			if (arguments[2] instanceof Array) {

	//if (arguments.length == 3 && arguments[2] instanceof Function ||
	//	 arguments.length == 4 && arguments[3] instanceof Function && arguments[2] instanceof Array ||
	//	 arguments.length == 2 && isAbstract) {
				isMethod = true;
				setMethodReturnType(type);
				setMethodName(name);
				if (arguments.length == 4) {
					setMethodParamTypeInfo(arguments[2]);
					setMethodBody(arguments[3]);
				} else if (arguments.length == 3) {
					setMethodParamTypeInfo(arguments[2]);
				}
				return method();
			}
			else if (name instanceof Array) {
				isMethod = true;
				methodGenericNames = name;
				name = arguments[2];
				setMethodReturnType(type);
				setMethodName(name);
				setMethodGenericNames(methodGenericNames);
				if (arguments.length == 5) {
					setMethodParamTypeInfo(arguments[3]);
					setMethodBody(arguments[4]);
				} else if (arguments.length == 4) {
					setMethodParamTypeInfo(arguments[3]);
				}
				return method();
			}
			else if (arguments.length > 1) {
	//else if (arguments.length == 4 || arguments[2] == '=' ||
	//	  arguments.length == 2){
				isField = true;
				setFieldType(type);
				setFieldName(name);
				setFieldValue(arguments[2]);
				return field();
			}
		       	else {
				return this;
			}
		};
		var field = function() {
			return new Field(visibility(), isStatic, isFinal, fieldType, fieldName, fieldValue);
		};
		var method = function(isConstructor) {
			objectifyGenericStrings();
			return new Method(visibility(), isStatic, isFinal, isAbstract, isVoid, methodReturnType, methodName, methodParamTypeInfo, methodBody, isConstructor, methodGenericNames);
		};
		var objectifyGenericStrings = function() {
			if ( ! methodGenericNames) {
				return;
			}
			if (methodReturnType && methodGenericNames.indexOf(methodReturnType) >= 0) {
				methodReturnType = new GenericName(methodReturnType);
			}
			for (var i=0, len=methodParamTypeInfo.length; i<len; i++) {
				var current = methodParamTypeInfo[i];
				if (current instanceof GenericName) return;
			}
			for (var i=0, len=methodParamTypeInfo.length; i<len; i++) {
				var current = methodParamTypeInfo[i];
				if (methodGenericNames.indexOf(current) >= 0) {
					methodParamTypeInfo[i] = new GenericName(current);
				}
			}
		};
		var visibility = function() {
			return initFunction.visibility(isPublic, isProtected, isPrivate);
		};
		var returnConstructorOrExprOrSelf = function() {
			if (arguments.length == 2 && arguments[1] instanceof Function) {
				isMethod = true;
				setMethodName(arguments[0]);
				setMethodBody(arguments[1]);
				var isConstructor = true;
				return method(isConstructor);
			}
			if (arguments.length == 3 && arguments[1] instanceof Array && arguments[2] instanceof Function) {
				isMethod = true;
				setMethodName(arguments[0]);
				setMethodParamTypeInfo(arguments[1]);
				setMethodBody(arguments[2]);
				var isConstructor = true;
				return method(isConstructor);
			}
			return returnExprOrSelf.apply(instance, arguments);
		};
		instance.public = function() {
			isPublic = true;
			return returnConstructorOrExprOrSelf.apply(instance, arguments);
		};
		instance.protected = function() {
			isProtected = true;
			return returnConstructorOrExprOrSelf.apply(instance, arguments);
		};
		instance.private = function() {
			isPrivate = true;
			return returnConstructorOrExprOrSelf.apply(instance, arguments);
		};
		instance.package_visible = function() {
			return returnConstructorOrExprOrSelf.apply(instance, arguments);
		};
		instance.static = function() {
			isStatic = true;
			return returnExprOrSelf.apply(instance, arguments);
		};
		instance.final = function() {
			isFinal = true;
			return returnExprOrSelf.apply(instance, arguments);
		};
		instance.abstract = function(type, name, paramTypeInfo) {
			isAbstract = true;
			if (arguments.length > 1) {
				isMethod = true;
				setMethodReturnType(type);
				setMethodName(name);
				setMethodParamTypeInfo(paramTypeInfo);
				return method();
			}
			return instance;
		};
		instance.Void = function(methodName) {
			isMethod = true;
			isVoid = true;
			if (methodName instanceof Array) {
				var genericNames = methodName;
				setMethodGenericNames(genericNames);
				methodName = arguments[1];
				setMethodName(methodName);
				setMethodParamTypeInfo(arguments[2]);
				setMethodBody(arguments[3]);
				return method();
			}
			setMethodName(methodName);
			setMethodParamTypeInfo(arguments[1]);
			setMethodBody(arguments[2]);
			return method();
		};
		instance.clazz = function(className) {
			return new ClassExprBuilder(visibility(), isStatic, isFinal, isAbstract, className);
		};
		instance.interface = function(name) {
			return new InterfaceExprBuilder(visibility(), isStatic, name);
		};
		var setMethodName = function(method_name) {
			if (!isMethod) throw new Error('set method name invoked on expression that is not method expression');
			methodName = method_name;
		};
		var setMethodParamTypeInfo = function(param_type_info_array) {
			methodParamTypeInfo = param_type_info_array;
		};
		var setMethodGenericNames = function(generic_info_array) {
			methodGenericNames = generic_info_array;
		};
		var setMethodReturnType = function(return_type) {
			methodReturnType = return_type;
		}
		var setMethodBody = function(method_body) {
			methodBody = method_body;
		};
		var setFieldType = function(type) {
			fieldType = type;
		};
		var setFieldName = function(name) {
			fieldName = name;
		};
		var setFieldValue = function(value) {
			fieldValue = value;
		};
		return instance;
	};
	initFunction.visibility = function(isPublic, isProtected, isPrivate) {
		if (isPublic && !isProtected && !isPrivate) {
			return Visibility.PUBLIC;
		} else if (isProtected && !isPublic && !isPrivate) {
			return Visibility.PROTECTED;
		} else if (isPrivate && !isPublic && !isProtected) {
			return Visibility.PRIVATE;
		} else if (!isPrivate && !isPublic && !isProtected) {
			return Visibility.DEFAULT;
		} else {
			throw new Error('cannot determine visibility for params: isPublic: ' + isPublic + ', isProtected: ' + isProtected + ', isPrivate: ' + isPrivate);
		}
	};
	return initFunction;
})();

module.exports = ExpressionBuilder;
