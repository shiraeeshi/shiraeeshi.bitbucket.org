var ImportsArray = require('./ImportsArray.js');

var window = global;

var SourceFile = (function() {
	var initFunction = function SourceFileInitFunction(packageString, classesAndImports) {
		var Class = require('./Class');
		this.packageString = packageString;
		var imports = [];
		var classes = [];
		var firstClassesIndex = -1;
		for (var i=0, len=classesAndImports.length; i<len; i++) {
			if (classesAndImports[i] instanceof Class) {
				firstClassesIndex = i;
				break;
			}
		}
		if (firstClassesIndex === -1) {
			imports = classesAndImports;
		} else {
			imports = classesAndImports.slice(0, firstClassesIndex);
			classes = classesAndImports.slice(firstClassesIndex);
		}
		var importsArray = new ImportsArray(imports);
		//var enclosing = initFunction.last(packageString);
		//this.last = enclosing;
		var _enclosing;
		Object.defineProperty(this, 'last', {
			get: function() {
				if ( ! _enclosing) _enclosing = initFunction.last(packageString);
				return _enclosing;
			}
		});
		for (var i=0, len=classes.length; i<len; i++) {
			var eachClass = classes[i];
			eachClass.package = packageString;
			//enclosing[eachClass.name] = eachClass;
			eachClass.setImportsArray(importsArray);
		}
		this.classes = classes;
	};

	initFunction.prototype.classNamesTree = function() {
		var tree = {};
		for (var i=0, len=this.classes.length; i<len; i++) {
			var clazz = this.classes[i];
			tree[clazz.name] = clazz.namesTree()[clazz.name];
		}
		return tree;
	};

	initFunction.last = function(param) {
		var result = window['globalJ2JS']['classpath'];
		if (!param) return result;
		var packageArr = param;
		if (typeof(param) == 'string') {
			packageArr = param.split(/\./);
		}
		for (var i=0, len=packageArr.length; i<len; i++) {
			var packageName = packageArr[i];
			if (!result.hasOwnProperty(packageName)) {
				result[packageName] = {};
			}
			result = result[packageName];
		}
		return result;
	};
	return initFunction;
})();

module.exports = SourceFile;
