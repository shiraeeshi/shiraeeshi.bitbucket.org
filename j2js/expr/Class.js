var window = global;

var Visibility = require('../Visibility.js');
var Field = require('./Field.js');
var StaticInitBlock = require('./StaticInitBlock.js');


var Klass = (function() {

	var initFunction = function ClassInitFunction(visibility, isStatic, isFinal, isAbstract, name, body, superClass, interfaces, genericNames) {
		this.visibility = visibility;
		this.isStatic = isStatic;
		this.isFinal = isFinal;
		this.isAbstract = isAbstract;
		this.name = name;
		this.members = mapMembers(body, this);
		this.superClassStr = superClass;
		this.interfaces = interfaces || [];
		if (genericNames) {
			this.genericNames = genericNames;
			this.isGeneric = true;
		}

		var _enclosing;
		Object.defineProperty(this, 'enclosingPackage', {
			get: function() {
				if ( ! _enclosing) _enclosing = require('./Package.js').last(this.package);
				return _enclosing;
			}
		});

		var _superClass;
		Object.defineProperty(this, 'superClass', {
			get: function() {
				if ( ! _superClass) {
					if (this.superClassStr) {
						_superClass = initFunction.forName(this.superClassStr);
					} else {
						_superClass = globalJ2JS.mainParent.prototype.__clazz;
					}
				}
				return _superClass;
			}
		});
	};

	initFunction.isPrimitive = function(name) {
		var primitiveNames = [ 'char', 'boolean', 'byte', 'short', 'int', 'long', 'float', 'double' ];
		return primitiveNames.indexOf(name) >= 0;
	};

	initFunction.isInstanceOfPrimitive = function(value, primitiveTypeName) {
		if (primitiveTypeName == 'char') {
			return typeof(value) == 'string' && value.length == 1;
		}
		if (primitiveTypeName == 'boolean') {
			return typeof(value) == 'boolean';
		}
		if (primitiveTypeName == 'byte' ||
			primitiveTypeName == 'short' ||
			primitiveTypeName == 'int' ||
			primitiveTypeName == 'long') {

			return Number.isInteger(value);
		}
		if (primitiveTypeName == 'float' || primitiveTypeName == 'double') {
			return typeof(value) == 'number';
		}
	};

	initFunction.isBuiltin = function(name) {
		if (initFunction.isPrimitive(name)) return true;
		
		var builtinNames = [
			'Character',
			'Boolean',
			'Byte',
			'Short',
			'Integer',
			'Long',
			'Float',
			'Double',
			'String',
			'func'];

		return builtinNames.indexOf(name) >= 0;
	};

	initFunction.isInstanceOfBuiltin = function(value, builtinTypeName) {
		if (initFunction.isPrimitive(builtinTypeName)) 
			return initFunction.isInstanceOfPrimitive(value, builtinTypeName);
		
		if (builtinTypeName == 'func') return value instanceof Function;
		if (builtinTypeName == 'String') return typeof value == 'string';
		
		throw new Error('not implemented yet'); // TODO
	};

	initFunction.typeNameOf = function(value) {
		if (value instanceof globalJ2JS.mainParent) {
			return Object.getPrototypeOf(value).__clazz.fullName;
		}
		
		if (typeof(value) == 'string' && value.length == 1) return 'char'; // TODO one-character-string or char?
		if (typeof(value) == 'boolean') return 'boolean';
		if (Number.isInteger(value)) return 'byte';
		if (typeof(value) == 'number') return 'float';

		if (typeof(value) == 'string') return 'String';
		if (value instanceof Function) return 'func';
		
		throw new Error('cannot find typeNameOf ' + value);
	};

	initFunction.forName = function(className) {
		var splitted = className.split(/\./);
		var node = globalJ2JS.namesTree;
		for (var i=0, len=splitted.length; i<len; i++) {
			var name = splitted[i];
			node = node[name];
		}
		return node.__j2js__parsedClass;
	};

	Object.defineProperty(initFunction.prototype, 'encloserInClasspath', {
		get: function() {
			if (this.outerClass) {
				var outer = this.outerClass;
				return outer.inClasspath;
			}
			return this.enclosingPackage;
		}
	});

	Object.defineProperty(initFunction.prototype, 'inClasspath', {
		get: function() {
			return this.encloserInClasspath[this.name];
		},
		set: function(val) {
			this.encloserInClasspath[this.name] = val;
		}
	});

	Object.defineProperty(initFunction.prototype, 'fullName', {
		get: function() {
			return this.package + '.' + this.name;
		}
	});

	var klassBuilder = require('../klass_utils/klassbuilder.js');

	initFunction.prototype.initStatic = function() {
		if (this.staticInitializationDone) return;
		var parent = this.superClass.inClasspath;
		if (parent.__j2js.isTemporary) {
			this.superClass.initStatic();
		}
		var temp = this.inClasspath;

		var real = klassBuilder.buildJS(this);

		makeTempSubclassOfReal(temp, real);
		injectReal(this, temp, real);

		this.staticInitializationDone = true;

		initStaticInner(this, temp, real);
//		if (this.outerClass) {
//			this.outerClass.initStatic();
//		}
	};

	var makeTempSubclassOfReal = function(temp, real) {
		var F = function() {};
		F.prototype = real.prototype;
		temp.prototype = new F(); // temp's children are also the real's children
	};

	var injectReal = function(instance, temp, real) {
		var name = instance.name;
		if (window[name] == temp) {
			window[name] = real;
		}
		instance.inClasspath = real;
	};

	var initStaticInner = function(instance, tempClass, realClass) {
		for (vis in instance.members.static.classes) {
			if ( ! instance.members.static.classes.hasOwnProperty(vis)) continue;
			for (className in instance.members.static.classes[vis]) {
				if ( ! instance.members.static.classes[vis].hasOwnProperty(className)) continue;
				var inner = instance.members.static.classes[vis][className];
				realClass[className] = tempClass[className];
				if (tempClass[className].__j2js.isTemporary) {
					inner.initStatic();
				}
			}
		}
		for (vis in instance.members.instance.classes) {
			if ( ! instance.members.instance.classes.hasOwnProperty(vis)) continue;
			for (className in instance.members.instance.classes[vis]) {
				if ( ! instance.members.instance.classes[vis].hasOwnProperty(className)) continue;
				var inner = instance.members.instance.classes[vis][className];
				realClass[className] = tempClass[className];
				if (tempClass[className].__j2js.isTemporary) {
					inner.initStatic();
				}
			}
		}
	};

	initFunction.prototype.isNestedIn = function(other) {
		if ( ! this.outerClass) return false;
		if (this.outerClass == other) return true;
		return this.outerClass.isNestedIn(other);
	};

	initFunction.prototype.is = function(other) {
		if ( ! other) throw new Error('Illegal (falsy) argument');
		var superClass = this.superClass;
		if (superClass == other) return true;
		if (superClass == globalJ2JS.mainParent) return false;
		return superClass.is(other);
	};

	initFunction.prototype.buildGenericInfo = function(args) {
		var result = {};
		for (var i=0, len=this.genericNames.length; i<len; i++) {
			var name = this.genericNames[i];
			result[name] = args[i];
		}
		return result;
	};
	
	initFunction.prototype.getPackageVisibleFields = function() {
		var fields = this.members.instance.fields;
		var packageVisibleFields = [];
		for (var i=fields.length-1; i>=0; i--) {
			var field = fields[i];
			if (field.isPackageVisible()) {
				packageVisibleFields.push(field);
			}
		}
		return packageVisibleFields;
	};
	initFunction.prototype.setImportsArray = function(importsArray) {
		this.importsArray = importsArray;
		var setImportsArrayToClass = function(clz) {
			clz.setImportsArray(importsArray);
		};
		if (this.members.static.classes) {
			foreachClass(this.members.static.classes, setImportsArrayToClass);
		}
		if (this.members.instance.classes) {
			foreachClass(this.members.instance.classes, setImportsArrayToClass);
		}
	};
	var foreachClass = function(classesByVisibility, act) {
		for (var vis in classesByVisibility) {
			if ( ! classesByVisibility[vis]) continue;
			for (var className in classesByVisibility[vis]) {
				if ( ! classesByVisibility[vis][className]) continue;
				var clz = classesByVisibility[vis][className];
				act(clz);
			}
		}
	};
	initFunction.prototype.linkToSuperclass = function() {
		var Package = require('./Package.js');
		if ( ! this.superClassStr) return window['globalJ2JS'].mainParent;

		if (this.superClassStr.indexOf('.') > 0) {
			var splitted = this.superClassStr.split(/\./);
			var pack = splitted.slice(0, -1);
			var className = splitted.slice(-1);
			return Package.last(pack)[className];
		}
		var imports = this.importsArray.imports;
		var enclosing = this.enclosingPackage;
		if (enclosing.hasOwnProperty(this.superClassStr)) {
			return enclosing[this.superClassStr];
		}
		for (var i=0, len=imports.length; i<len; i++) {
			var im = imports[i];
			if (im.isStatic) continue;
			if (im.className == this.superClassStr) {
				return Package.last(im.packageArray)[this.superClassStr];
			}
			if (im.className == '*') {
				var pack = Package.last(im.packageArray);
				if (pack.hasOwnProperty(this.superClassStr)) {
					return pack[this.superClassStr];
				}
			}
		}
		throw new Error('superclass not found');
	};

	var ClassNameNode = function(parsedClass) {
		this.__j2js__parsedClass = parsedClass;
	};

	initFunction.prototype.namesTree = function() {
		var tree = {};
		var node = new ClassNameNode(this);
		tree[this.name] = node;
		if (this.members.static.classes) {
			insertNestedClassesNamesIntoNode(node, this.members.static.classes);
		}
		if (this.members.instance.classes) {
			insertNestedClassesNamesIntoNode(node, this.members.instance.classes);
		}
		return tree;
	};

	var insertNestedClassesNamesIntoNode = function(node, classes) {
		for (var vis in classes) {
			if ( ! classes.hasOwnProperty(vis)) continue;
			for (var className in classes[vis]) {
				if ( ! classes[vis].hasOwnProperty(className)) continue;
				var nestedClass = classes[vis][className];
				node[className] = nestedClass.namesTree()[className];
			}
		}
	};

	var mapMembers = function(members, parsedClass) {
		var Method = require('./Method.js');
		members = members || [];
		var result = {
			'static': {
				'methods': {}, 'fields': [] // and lazy 'initBlock', 'classes'
			},
			'instance': {
				'methods': {}, 'fields': [], 'constructors': {} // and lazy 'initBlock', 'classes'
			}
		};
		for (var staticOrNot in result) {
			if (!result.hasOwnProperty(staticOrNot)) continue;
			result[staticOrNot]['methods'][Visibility.PUBLIC] = {};
			result[staticOrNot]['methods'][Visibility.PROTECTED] = {};
			result[staticOrNot]['methods'][Visibility.PRIVATE] = {};
			result[staticOrNot]['methods'][Visibility.DEFAULT] = {};
		}
		result['instance']['constructors'][Visibility.PUBLIC] = [];
		result['instance']['constructors'][Visibility.PROTECTED] = [];
		result['instance']['constructors'][Visibility.PRIVATE] = [];
		result['instance']['constructors'][Visibility.DEFAULT] = [];
		for (var i=0, len=members.length; i<len; i++) {
			var member = members[i];
			if (member.isAbstract) {
				continue;
			}
			if (member.isConstructor) {
//				result['instance']['constructors'][member.visibility].push({
//					body: member.body,
//					guard: member.guard
//				});
				
				result['instance']['constructors'][member.visibility].push(member);
				continue;
			}
			var container;
			if (member.isStatic) container = result.static;
			else container = result.instance;
			if (member instanceof Method) {
				if ( ! container.methods[member.visibility][member.name]) {
					container.methods[member.visibility][member.name] = [];
				}
				container.methods[member.visibility][member.name].push(member);
			}
			else if (member instanceof Field) {
				var field = member;
//				container.fields.push({
//					name: field.name,
//					visibility: field.visibility,
//					value: field.value,
//					defaultValue: field.defaultValue()
//				});
				container.fields.push(field);
			}
			else if (member instanceof StaticInitBlock) {
				result.static.initBlock = member.body;
			}
			else if (member instanceof Function && member.length == 0) {
				result.instance.initBlock = member;
			}
			// TODO inner classes
			else if (member instanceof Klass) {
				if (!container.classes) container.classes = {};
				if (!container.classes[member.visibility]) container.classes[member.visibility] = {};
				container.classes[member.visibility][member.name] = member;
				member.outerClass = parsedClass;
			}
			else throw new Error("unknown class member type.");
		}
		injectDefaultConstructorIfNeeded(result.instance.constructors);
		return result;
	};
	var injectDefaultConstructorIfNeeded = function(constructors) {
		var Method = require('./Method.js');
		for (var vis in constructors) {
			if ( ! constructors.hasOwnProperty(vis)) continue;
			if (constructors[vis].length != 0) return;
		}
//		constructors[Visibility.PUBLIC].push({
//			body: function() {uber();},
//			guard: function() {return arguments.length == 0;}
//		});
		constructors[Visibility.PUBLIC].push(
			new Method(Visibility.PUBLIC, false, false, false, false, undefined, '', 
				[], function() {uber();}, true));
	};
	initFunction.prototype.isPublic = function() {
		return this.visibility == Visibility.PUBLIC;
	};
	initFunction.prototype.isProtected = function() {
		return this.visibility == Visibility.PROTECTED;
	};
	initFunction.prototype.isPrivate = function() {
		return this.visibility == Visibility.PRIVATE;
	};
	initFunction.prototype.isPackageVisible = function() {
		return this.visibility == Visibility.DEFAULT;
	};
	return initFunction;
})();

module.exports = Klass;
