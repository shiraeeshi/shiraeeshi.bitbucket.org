var window = global;

var Visibility = require('../Visibility.js');
var Method = require('./Method.js');
var Field = require('./Field.js');
var StaticInitBlock = require('./StaticInitBlock.js');
var Package = require('./Package.js');


var Klass = (function() {
	var initFunction = function ClassInitFunction(visibility, isStatic, isFinal, isAbstract, name, body, superClass, interfaces) {
		this.visibility = visibility;
		this.isStatic = isStatic;
		this.isFinal = isFinal;
		this.isAbstract = isAbstract;
		this.name = name;
		this.members = mapMembers(body, this);
		this.superClass = superClass;
		this.interfaces = interfaces;

	};
	initFunction.prototype.initStatic = function() {
		var package = this.enclosingPackage;
		var name = this.name;
		var isTemp = package[name].__j2jsinfo_is_temp;
		if ( ! isTemp) {
			return;
		}
		var temp = package[name];

		var klassBuilder = require('../klass_utils/klassbuilder.js');
		var real = klassBuilder.buildJS(this);

		var F = function() {};
		F.prototype = real.prototype;
		temp.prototype = new F(); // temp's children are also the real's children
		if (window[name] == package[name]) {
			window[name] = real;
		}
		package[name] = real;
	};
	initFunction.prototype.getPackageVisibleFields = function() {
		var fields = this.members.instance.fields;
		var packageVisibleFields = [];
		for (var i=fields.length-1; i>=0; i--) {
			var field = fields[i];
			if (field.isPackageVisible()) {
				packageVisibleFields.push(field);
			}
		}
		return packageVisibleFields;
	};
	initFunction.prototype.linkToSuperclass = function() {
		if ( ! this.superClass) return window['globalJ2JS'].mainParent;

		if (this.superClass.indexOf('.') > 0) {
			var splitted = this.superClass.split(/\./);
			var pack = splitted.slice(0, -1);
			var className = splitted.slice(-1);
			return Package.last(pack)[className];
		}
		var imports = this.importsArray.imports;
		var enclosing = this.enclosingPackage;
		if (enclosing.hasOwnProperty(this.superClass)) {
			return enclosing[this.superClass];
		}
		for (var i=0, len=imports.length; i<len; i++) {
			var im = imports[i];
			if (im.isStatic) continue;
			if (im.className == this.superClass) {
				return Package.last(im.packageArray)[this.superClass];
			}
			if (im.className == '*') {
				var pack = Package.last(im.packageArray);
				if (pack.hasOwnProperty(this.superClass)) {
					return pack[this.superClass];
				}
			}
		}
		throw new Error('superclass not found');
	};
	var mapMembers = function(members, parsedClass) {
		members = members || [];
		var result = {
			'static': {
				'methods': {}, 'fields': [] // and lazy 'initBlock', 'classes'
			},
			'instance': {
				'methods': {}, 'fields': [], 'constructors': {} // and lazy 'initBlock', 'classes'
			}
		};
		for (var staticOrNot in result) {
			if (!result.hasOwnProperty(staticOrNot)) continue;
			result[staticOrNot]['methods'][Visibility.PUBLIC] = {};
			result[staticOrNot]['methods'][Visibility.PROTECTED] = {};
			result[staticOrNot]['methods'][Visibility.PRIVATE] = {};
			result[staticOrNot]['methods'][Visibility.DEFAULT] = {};
		}
		result['instance']['constructors'][Visibility.PUBLIC] = [];
		result['instance']['constructors'][Visibility.PROTECTED] = [];
		result['instance']['constructors'][Visibility.PRIVATE] = [];
		result['instance']['constructors'][Visibility.DEFAULT] = [];
		for (var i=0, len=members.length; i<len; i++) {
			var member = members[i];
			if (member.isAbstract) {
				continue;
			}
			if (member.isConstructor) {
				result['instance']['constructors'][member.visibility].push({
					body: member.body,
					guard: member.guard
				});
				continue;
			}
			var container;
			if (member.isStatic) container = result.static;
			else container = result.instance;
			if (member instanceof Method) container.methods[member.visibility][member.name] = member;
			else if (member instanceof Field) {
				var field = member;
//				container.fields.push({
//					name: field.name,
//					visibility: field.visibility,
//					value: field.value,
//					defaultValue: field.defaultValue()
//				});
				container.fields.push(field);
			}
			else if (member instanceof StaticInitBlock) {
				result.static.initBlock = member.body;
			}
			else if (member instanceof Function && member.length == 0) {
				result.instance.initBlock = member;
			}
			// TODO inner classes
			else if (member instanceof Klass) {
				if (!container.classes) container.classes = {};
				container.classes[member.visibility][member.name] = member;
				member.outer = parsedClass;
			}
			else throw new Error("unknown class member type.");
		}
		return result;
	};
	initFunction.prototype.isPublic = function() {
		return this.visibility == Visibility.PUBLIC;
	};
	initFunction.prototype.isProtected = function() {
		return this.visibility == Visibility.PROTECTED;
	};
	initFunction.prototype.isPrivate = function() {
		return this.visibility == Visibility.PRIVATE;
	};
	initFunction.prototype.isPackageVisible = function() {
		return this.visibility == Visibility.DEFAULT;
	};
	return initFunction;
})();

module.exports = Klass;
