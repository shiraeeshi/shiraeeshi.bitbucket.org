var Interface = require('./Interface.js');

var InterfaceExprBuilder = (function() {
	var initFunction = function InterfaceExprBuilderInitFunction(visibility, isStatic, name) {
		var isPublic, isProtected, isPrivate;
		var superInterface;

		var bodyBuilder = function() {
			var body = Array.prototype.slice.call(arguments);
			return new Interface(visibility, isStatic, name, body, superInterface);
		};
		bodyBuilder.extends = function(super_interface) {
			if (superInterface !== undefined) {
				throw new Error('illegal "extends" invokation');
			}
			if (typeof super_interface !== 'string') {
				throw new Error('"extends" method expects string argument');
			}
			superInterface = super_interface;
			return bodyBuilder;
		};
		return bodyBuilder;
	};
	return initFunction;
})();

module.exports = InterfaceExprBuilder;
