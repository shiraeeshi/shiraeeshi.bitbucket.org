
var Import = (function() {
	var initFunction = function ImportInitFunction(importString, isStatic) {
		if (importString.length == 0) {
			throw new Error('empty importString');
		}
		this.importString = importString;
		this.isStatic = isStatic;
		split(this);
	};

	var split = function(instance) {
		var splitted = instance.importString.split(/\./);
		var packageArray;
		var className;
		var funcName;
		if (instance.isStatic) {
			instance.packageArray = splitted.slice(0, -2);
			instance.className = splitted[splitted.length-2]; // TODO nested classes
			instance.funcName = splitted[splitted.length-1];
		} else {
			instance.packageArray = splitted.slice(0, -1);
			instance.className = splitted[splitted.length-1]; // TODO nested classes
		}
	};
	return initFunction;
})();

module.exports = Import;
