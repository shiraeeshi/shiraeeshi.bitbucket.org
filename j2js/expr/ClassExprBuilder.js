var Class = require('./Class.js');
var window = global;

var ClassExprBuilder = (function() {
	var initFunction = function ClassExprBuilderInitFunction(visibility, isStatic, isFinal, isAbstract, name) {
		var isPublic, isProtected, isPrivate;
		var superClass;
		var interfaces;

		var genericNames;
		var oldValuesWhenInjectGenericNames;

		var clazz = function() {
			var args = Array.prototype.slice.call(arguments);
			if (oldValuesWhenInjectGenericNames) {
					uninjectGenericNames(oldValuesWhenInjectGenericNames);
			} else if (args.length == 1 && args[0] instanceof Array) { // generic class declaration case
				if (superClass || interfaces || oldValuesWhenInjectGenericNames)
					throw new Error("illegal args: " + args);
				genericNames = args[0];
				oldValuesWhenInjectGenericNames = injectGenericNames(genericNames);
				return clazz;
			}
			var body = args;
			return new Class(visibility, isStatic, isFinal, isAbstract, name, body, superClass, interfaces, genericNames);
		};
		var instance = clazz;
		instance.extends = function(super_class) {
			if (superClass !== undefined || interfaces != undefined) {
				throw new Error('illegal "extends" invokation');
			}
			if (typeof super_class !== 'string') {
				throw new Error('"extends" method expects string argument');
			}
			superClass = super_class;
			return clazz;
		};
		instance.implements = function() {
			if (interfaces !== undefined) {
				throw new Error('illegal "implements" invokation');
			}
			interfaces = Array.prototype.slice.call(arguments);
			return clazz;
		};
		return instance;
	};
	return initFunction;
})();

var GenericName = require('./GenericName');

var injectGenericNames = function(genericNames) {
	var oldValues = {};
	genericNames.forEach(function(name) {
		oldValues[name] = window[name];
		window[name] = new GenericName(name);
	});
	return oldValues;
};
var uninjectGenericNames = function(oldValuesHolder) {
	Object.getOwnPropertyNames(oldValuesHolder).forEach(function(name) {
		window[name] = oldValuesHolder[name];
	});
};

module.exports = ClassExprBuilder;
