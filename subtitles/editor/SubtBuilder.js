function SubtBuilder() {
	var replics = [];
	var current_index = 0;
	var current_replic = null;

	var changeCurrentReplic = function(replic) {
		if (current_replic != null) {
			current_replic.removeListener(this);
		}
		current_replic = replic;
		if (current_replic != null) {
			current_replic.addListener(this);
		}
	};
	var instance_to_return = {
		update: function(new_replic_state) {//the method of replic's listener
			this.notifyCurrentReplicFieldsListeners();
		},
		next: function() {
			if (current_index >= replics.length-1) {
				throw new RangeError('cannot move to next replic: current replic is last');
			}
			current_index++;
			var replic = replics[current_index];
			changeCurrentReplic.call(this, replic);
			this.notifyCurrentReplicIndexListeners();
			return replic;
		},
		prev: function() {
			if (current_index <= 0) {
				throw new RangeError('cannot move to prev replic: current replic is first');
			}
			current_index--;
			var replic = replics[current_index];
			changeCurrentReplic.call(this, replic);
			this.notifyCurrentReplicIndexListeners();
			return replic;
		},
		insertBefore: function() {
			var replic = new Replic();
			replics.splice(current_index, 0, replic);
			this.notifyCurrentReplicIndexListeners();
			changeCurrentReplic.call(this, replic);
			return replic;
		},
		push: function() {
			var replic = new Replic();
			replics.push(replic);
			current_index = replics.length-1;
			this.notifyCurrentReplicIndexListeners();
			changeCurrentReplic.call(this, replic);
			return replic;
		},
		removeCurrent: function() {
			if (replics.length == 0) {
				return;
			}
			replics.splice(current_index, 1);
			if (replics.length == 0) {
				current_index = 0;
				changeCurrentReplic.call(this, null);
				this.notifyCurrentReplicIndexListeners();
				return;
			}
			if (current_index >= replics.length) {
				current_index = replics.length-1;
			}
			var replic = replics[current_index];
			changeCurrentReplic.call(this, replic);
			this.notifyCurrentReplicIndexListeners();
			return replic;
		},
		getCurrentReplic: function() {
			if (replics.length == 0) {
				return null;
			}
			return replics[current_index];
		},
		getReplics: function() {
			for (var i=0; i<replics.length; i++) {
				replics[i].setNum(i+1);
			}
			return replics;
		},
		getTimedeltaPrev: function() {
			if (current_index==0){
				throw new RangeError("can't get timedelta prev: current replic is first");
			}
			var prev_replic_end = replics[current_index-1].getTiming().end_time.getTime();
			var current_replic_start = replics[current_index].getTiming().start_time.getTime();
			return current_replic_start - prev_replic_end;
		},
		getTimedeltaNext: function() {
			if (current_index==replics.length-1){
				throw new RangeError("can't get timedelta next: current replic is last");
			}
			var current_replic_end = replics[current_index].getTiming().end_time.getTime();
			var next_replic_start = replics[current_index+1].getTiming().start_time.getTime();
			return next_replic_start - current_replic_end;
		},	
		setTimedeltaPrev: function(timedelta) {
			if (current_index == 0) {
				throw new RangeError('cannot set timedelta prev: current replic is first');
			}
			var prev_replic_end = replics[current_index-1].getTiming().end_time.getTime();
			var new_start = new Date(prev_replic_end + timedelta);
			var current_replic = replics[current_index];
			var duration = current_replic.getTiming().getDuration();
			current_replic.getTiming().start_time = new_start;
			var new_end = new Date(current_replic.getTiming().start_time.getTime() + duration);
			current_replic.getTiming().end_time = new_end;
			this.notifyCurrentReplicFieldsListeners();
		},
		setTimedeltaNext: function(timedelta) {
			if (current_index == replics.length-1) {
				throw new RangeError('cannot set timedelta next: current replic is last');
			}
			var next_replic_start = replics[current_index+1].getTiming().start_time.getTime();
			var new_end = new Date(next_replic_start - timedelta);
			var current_replic = replics[current_index];
			var duration = current_replic.getTiming().getDuration();
			current_replic.getTiming().end_time = new_end;
			var new_start = new Date(current_replic.getTiming().end_time.getTime() - duration);
			current_replic.getTiming().start_time = new_start;
			this.notifyCurrentReplicFieldsListeners();
		}
	};
	(function(obj){
		var listeners = [];
		obj.addCurrentReplicIndexListener = function(listener) {
			listeners.push(listener);
		};
		obj.removeCurrentReplicIndexListener = function(listener) {
			var index = listeners.indexOf(listener);
			if (index>=0 && index<= listeners.length) {
				listeners.splice(index, 1);
			}
		};
		obj.notifyCurrentReplicIndexListeners = function() {
			for (var i=listeners.length-1; i>=0; i--) {
				listeners[i].update(obj);
			}
		};
	 })(instance_to_return);

	(function(obj){
		var listeners = [];
		obj.addCurrentReplicFieldsListener = function(listener) {
			listeners.push(listener);
		};
		obj.removeCurrentReplicFieldsListener = function(listener) {
			var index = listeners.indexOf(listener);
			if (index>=0 && index<= listeners.length) {
				listeners.splice(index, 1);
			}
		};
		obj.notifyCurrentReplicFieldsListeners = function() {
			for (var i=listeners.length-1; i>=0; i--) {
				listeners[i].update(obj);
			}
		};
	 })(instance_to_return);

	return instance_to_return;
}


