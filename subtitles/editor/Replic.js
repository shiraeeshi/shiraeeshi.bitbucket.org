function Replic(num_param, timing_as_string, text_param) {
	var num = null, timing = null, text = null;
	if (num !== undefined && timing_as_string !== undefined && text !== undefined) {
		num = num_param;
		timing = new Timing(timing_as_string);
		text = text_param;
	} else {
		timing = Timing.zero();
	}
	var instance_to_return = {
		copyDuration: function(other_replic) {
			var duration_to_copy = other_replic.getTiming().getDuration();
			timing.end_time = new Date(timing.start_time.getTime() + duration_to_copy);
			this.notifyListeners();
		},
		getNum: function() {return num},
		setNum: function(num_param) {num = num_param;this.notifyListeners();},
		getTiming: function() {return timing},
		setTiming: function(timing_param) {timing = timing_param;this.notifyListeners();},
		getText: function() {return text},
		setText: function(text_param) {text = text_param;this.notifyListeners();}
	};
	(function(obj){
		var listeners = [];
		obj.addListener = function(listener) {
			listeners.push(listener);
		};
		obj.removeListener = function(listener) {
			var index = listeners.indexOf(listener);
			if (index>=0 && index<= listeners.length) {
				listeners.splice(index, 1);
			}
		};
		obj.notifyListeners = function() {
			for (var i=listeners.length-1; i>=0; i--) {
				listeners[i].update(obj);
			}
		};
	 })(instance_to_return);
	instance_to_return.toString = function() {
		return num + '\n' + timing + '\n' + text;
	};		
	return instance_to_return;
}

