var Gadget = (function() {
	// static variable/property
	var static_counter = 0,
		NewGadget;
	
	// this will become the 
	// new constructor implementation
	NewGadget = function() {
		static_counter++;
		
		// [my] private instance variables
		var name, uid;
		// [my] private functions
		var sum = function(a,b) {return a+b;};
		
		this.varA = 'smth';
		this.varB = 'smth';
		this.methodA = function() {
			// some functionality
			// ...
			// can invoke private functions
			var summa = sum(7,5);
			console.log('sum >> ' + summa)
		};
	};
	
	// prototype
	var proto = {};
	NewGadget.prototype = proto;
	
	// a privileged method
	NewGadget.prototype.getLastId = function() {
		return static_counter;
	}
	
	// overwrite the constructor
	return NewGadget;

})(); // execute immediately
