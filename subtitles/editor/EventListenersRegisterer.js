function EventListenersRegisterer() {
	var parsers = [new SubtParser(),new SubtParser()];
	var iters = [new ReplicsIterator(),new ReplicsIterator()];
	var builder = new SubtBuilder();

	var fileSelectionHandler = function(evt) {
		var files = evt.target.files; // FileList object
		// files is a FileList of File objects.
		for (var i = 0, f; f = files[i]; i++) {
			var parentElement = null;
			if (i == 0) {
				parentElement = document.getElementById('first');
			} else if (i == 1) {
				parentElement = document.getElementById('second');
			}
			if (parentElement == null) continue;
			parentElement.getElementsByClassName('name')[0].innerHTML = f.name;

			var encoding = parentElement.getElementsByClassName('encoding')[0].value;
			parsers[i].setFile(f);
			parsers[i].setEncoding(encoding);
		}
	};
	
	function ReplicListener(parent_element_id){
		var parent_id = parent_element_id;
		return {
			update: function(iterator) {
				var replic = iterator.getCurrentReplic();
				var parent_element = document.getElementById(parent_id);
				var output_element = parent_element.getElementsByClassName('file-content')[0];
				var prev_timedelta_button = parent_element.getElementsByClassName('copy-timedelta-prev')[0];
				var duration_button = parent_element.getElementsByClassName('copy-duration')[0];
				var next_timedelta_button = parent_element.getElementsByClassName('copy-timedelta-next')[0];
				var undef = 'undefined';
				if (replic == null) {
					output_element.value = '';
					prev_timedelta_button.value = undef;
					duration_button.value = undef;
					next_timedelta_button.value = undef;
					return;
				}
				var replic_string = replic.getNum() + '\n' + replic.getTiming() + '\n' + replic.getText();
				var timedelta_prev_value;
				try {
					timedelta_prev_value = iterator.getTimedeltaPrev();
				} catch(range_error) {
					timedelta_prev_value = undef;
				} 
				var current_timing = iterator.getCurrentReplic().getTiming();
				var duration_value = current_timing.getDuration();
				
				var timedelta_next_value;
				try {
					timedelta_next_value = iterator.getTimedeltaNext();
				} catch(range_error) {
					timedelta_next_value = undef;
				}
				output_element.value = replic_string;
				prev_timedelta_button.value = timedelta_prev_value;
				duration_button.value = duration_value;
				next_timedelta_button.value = timedelta_next_value;
			}
		};
	};
	var first_listener = new ReplicListener('first');
	var second_listener = new ReplicListener('second');
	iters[0].addCurrentReplicListener(first_listener);
	iters[1].addCurrentReplicListener(second_listener);
	
	var resultListener = {
		update: function(subt_builder) {
			var current_replic = subt_builder.getCurrentReplic();
			var result_parent = document.getElementById('result');
			var result_textarea = result_parent.getElementsByClassName('file-content')[0];
			var timedelta_prev_output = result_parent.getElementsByClassName('timedelta-prev')[0];
			var duration_output = result_parent.getElementsByClassName('duration')[0];
			var timedelta_next_output = result_parent.getElementsByClassName('timedelta-next')[0];
			var undef = 'undefined';
			if (current_replic == null) {
				result_textarea.value = '';
				timedelta_prev_output.value = undef;
				duration_output.value = undef;
				timedelta_next_output.value = undef;
				return;
			}
			result_textarea.value = current_replic.getTiming() + '\n' + current_replic.getText();
			var timedelta_prev;
			try {
				timedelta_prev = subt_builder.getTimedeltaPrev();
			} catch (range_error) {
				timedelta_prev = undef;
			}
			var timedelta_next;
			try {
				timedelta_next = subt_builder.getTimedeltaNext();
			} catch (range_error) {
				timedelta_next = undef;
			}
			timedelta_prev_output.value = timedelta_prev;
			duration_output.value = current_replic.getTiming().getDuration();
			timedelta_next_output.value = timedelta_next;
		}
	};
	builder.addCurrentReplicIndexListener(resultListener);
	builder.addCurrentReplicFieldsListener(resultListener);

	var showListener = function(clickEvent) {
		var replics = builder.getReplics();
		var text = replics.join('\n\n');
		document.getElementById('result').getElementsByClassName('file-content')[0].value = text;
	};
	var worker = new Worker(new ReplicsBuilderController(parsers, iters, builder));
	var macros_recorder_wrapper = new MacrosRecorderWrapper(worker);
	return {
		activateListeners: function() {
			document.getElementById('btnupload').addEventListener('change', fileSelectionHandler, false);
			var firstParent = document.getElementById('first');
			firstParent.getElementsByClassName('parse')[0].addEventListener('click', 
				function(eve){
					worker.parse(1);
					macros_recorder_wrapper.record(eve, 1);
				}, false);
			firstParent.getElementsByClassName('encoding')[0].addEventListener('change', function(evt){var encoding = evt.target.value;worker.encodingChange(1, encoding);}, false);
			firstParent.getElementsByClassName('next')[0].addEventListener('click', function(eve){worker.next(1);macros_recorder_wrapper.record(eve, 1);}, false);
			firstParent.getElementsByClassName('prev')[0].addEventListener('click', function(eve){worker.prev(1);macros_recorder_wrapper.record(eve, 1);}, false);
			firstParent.getElementsByClassName('copy-timing')[0].addEventListener('click', function(eve){worker.copyTiming(1);macros_recorder_wrapper.record(eve, 1);}, false);
			firstParent.getElementsByClassName('copy-text')[0].addEventListener('click', function(eve){worker.copyText(1);macros_recorder_wrapper.record(eve, 1);}, false);
			firstParent.getElementsByClassName('copy-timedelta-prev')[0].addEventListener('click', function(eve){worker.copyTimeDeltaPrev(1);macros_recorder_wrapper.record(eve, 1);}, false);
			firstParent.getElementsByClassName('copy-duration')[0].addEventListener('click', function(eve){worker.copyDuration(1);macros_recorder_wrapper.record(eve, 1);}, false);
			firstParent.getElementsByClassName('copy-timedelta-next')[0].addEventListener('click', function(eve){worker.copyTimeDeltaNext(1);macros_recorder_wrapper.record(eve, 1);}, false);
			var secondParent = document.getElementById('second');
			secondParent.getElementsByClassName('parse')[0].addEventListener('click', function(eve){worker.parse(2);macros_recorder_wrapper.record(eve, 2);}, false);
			secondParent.getElementsByClassName('encoding')[0].addEventListener('change', function(evt){var encoding = evt.target.value;worker.encodingChange(2, encoding);}, false);
			secondParent.getElementsByClassName('next')[0].addEventListener('click', function(eve){worker.next(2);macros_recorder_wrapper.record(eve, 2);}, false);
			secondParent.getElementsByClassName('prev')[0].addEventListener('click', function(eve){worker.prev(2);macros_recorder_wrapper.record(eve, 2);}, false);
			secondParent.getElementsByClassName('copy-timing')[0].addEventListener('click', function(eve){worker.copyTiming(2);macros_recorder_wrapper.record(eve, 2);}, false);
			secondParent.getElementsByClassName('copy-text')[0].addEventListener('click', function(eve){worker.copyText(2);macros_recorder_wrapper.record(eve, 2);}, false);
			secondParent.getElementsByClassName('copy-timedelta-prev')[0].addEventListener('click', function(eve){worker.copyTimeDeltaPrev(2);macros_recorder_wrapper.record(eve, 2);}, false);
			secondParent.getElementsByClassName('copy-duration')[0].addEventListener('click', function(eve){worker.copyDuration(2);macros_recorder_wrapper.record(eve, 2);}, false);
			secondParent.getElementsByClassName('copy-timedelta-next')[0].addEventListener('click', function(eve){worker.copyTimeDeltaNext(2);macros_recorder_wrapper.record(eve, 2);}, false);
			var resultParent = document.getElementById('result');
			resultParent.getElementsByClassName('next')[0].addEventListener('click', function(eve) {worker.nextReplicInBuilder();macros_recorder_wrapper.record(eve);}, false);
			resultParent.getElementsByClassName('prev')[0].addEventListener('click', function(eve) {worker.prevReplicInBuilder();macros_recorder_wrapper.record(eve);}, false);
			resultParent.getElementsByClassName('push')[0].addEventListener('click', function(eve){worker.push();macros_recorder_wrapper.record(eve);}, false);
			resultParent.getElementsByClassName('insert-before')[0].addEventListener('click', function(eve){worker.insertBefore();macros_recorder_wrapper.record(eve);}, false);
			resultParent.getElementsByClassName('remove-current')[0].addEventListener('click', function(eve){worker.removeCurrentReplicInBuilder();macros_recorder_wrapper.record(eve);}, false);
			resultParent.getElementsByClassName('show')[0].addEventListener('click', showListener, false);
			resultParent.getElementsByClassName('set-start')[0].addEventListener('click', function(eve){var time_as_string = resultParent.getElementsByClassName('time-to-set')[0].value;try{worker.setStartTiming(time_as_string);}catch (err) {alert('error: ' + err);}macros_recorder_wrapper.record(eve);}, false);
			resultParent.getElementsByClassName('set-end')[0].addEventListener('click', function(eve){var time_as_string = resultParent.getElementsByClassName('time-to-set')[0].value;try{worker.setEndTiming(time_as_string);}catch (err) {alert('error: ' + err);}macros_recorder_wrapper.record(eve);}, false);
			document.getElementById('record-macros').addEventListener('click', function() {macros_recorder_wrapper.start();}, false);
			document.getElementById('recording-done').addEventListener('click', function() {macros_recorder_wrapper.end();}, false);
			document.getElementById('cancel-recording').addEventListener('click', function() {macros_recorder_wrapper.cancel();}, false);
			document.getElementsByTagName('body')[0].addEventListener('keydown', macros_recorder_wrapper.getHotkeysListener(), false);
		}
	};
}

function Worker(controller) {
	var instance = {};
	var macros_recorder = {};
	macros_recorder.commands = [];
	var active_worker;
	for (key in controller) {
		(function(function_name){
			instance[function_name] = function() {
				active_worker[function_name].apply(active_worker, arguments);
			};
		})(key);
	}
	instance.startRecordingMacros = function() {active_worker = macros_recorder;};
	instance.endRecordingMacros = function() {
		active_worker = controller;
		var commands_to_return = macros_recorder.commands;
		macros_recorder.commands = [];
		return commands_to_return;
	};
	for (key in controller) {
		(function(function_name){
			macros_recorder[function_name] = function() {
				var args = arguments;
				this.commands.push(function(){
					controller[function_name].apply(controller, args);
				});
			};
		})(key);
	}
	active_worker = controller;
	return instance;
}

function MacrosRecorderWrapper(worker) {

	var isRecording = false;

	var recorded_buttons_view = {
		update: function(records_array) {
			var button_names = [];
			button_names = records_array.map(function(record, index){return index+1 + ') ' + (record.filenum!==undefined?'[file ' + record.filenum + '] ':'[result] ') + record.button.name;});	
			var text = button_names.join('\n');
			document.getElementById('result').getElementsByClassName('file-content')[0].value = text;
		}
	};
	var recorded_buttons_accumulator = (function(){
		var recorded_buttons = [];
		return {
			pushFileRelatedButton: function(filenum, button){
				recorded_buttons.push({filenum:filenum,button:button});
			},
	    		pushResultRelatedButton: function(button) {
				recorded_buttons.push({button:button});
			},
	    		reset: function() {recorded_buttons = [];},
	    		getRecords: function() {return recorded_buttons;}
		};
	})();
	var hotkey_handlers = [];
	var hotkey = null;

	var hotkey2string = function(hotkey) {
		return (hotkey.key_shift ? 'shift + ' : '') +
			(hotkey.key_ctrl ? 'ctrl + ' : '') +
			(hotkey.key_alt ? 'alt + ' : '') +
			(hotkey.key_meta ? 'meta + ' : '') +
			' (' + hotkey.keyCode + ')';
		
	};

	document.getElementById('macros-hotkey').addEventListener('keydown', function(key_event) {
		key_event.preventDefault();
		var keyCode = key_event.keyCode;
		var character = String.fromCharCode(key_event.keyCode || key_event.charCode);
		hotkey = {
			keyCode: keyCode,
			key_shift: key_event.shiftKey,
			key_ctrl: key_event.ctrlKey,
			key_alt: key_event.altKey,
			key_meta: key_event.metaKey
		};
		key_event.target.value = (hotkey.key_shift ? 'shift + ' : '') +
			(hotkey.key_ctrl ? 'ctrl + ' : '') +
			(hotkey.key_alt ? 'alt + ' : '') +
			(hotkey.key_meta ? 'meta + ' : '') +
			character + ' (' + hotkey.keyCode + ')';
		return false;
	}, false);

	return {
		start: function() {
			isRecording = true;
			hotkey = null;
			worker.startRecordingMacros();
			document.getElementById('macros-hotkey').value = '';
			document.getElementById('macros-name-box-container').style.display = 'block';
			document.getElementById('macros-buttons').style.display = 'none';
			document.getElementById('macros-button-name').focus();
		},
		end: function() {
			isRecording = false;
			var commands = worker.endRecordingMacros();
			var handler = function() {
				for (var i=0, len=commands.length;i<len;i++) {
					var command = commands[i];
					command();
				}
			}
			if (hotkey !== null) {
				var hotkey_as_key = hotkey2string(hotkey);
				hotkey_handlers[hotkey_as_key] = handler;
			}
			hotkey = null;
			var button = document.createElement('input');
			button.type = 'button';
			var button_name = document.getElementById('macros-button-name').value;
			button.value = button_name || 'macros';
			button.style.margin = '5px';
			button.addEventListener('click', handler, false);
			document.getElementById('macros-buttons').appendChild(button);
			recorded_buttons_accumulator.reset();

			document.getElementById('macros-button-name').value = '';
			document.getElementById('macros-name-box-container').style.display = 'none';
			document.getElementById('macros-buttons').style.display = 'block';
		
		},
		cancel: function() {
			isRecording = false;
			hotkey = null;
			worker.endRecordingMacros();
			recorded_buttons_accumulator.reset();
			document.getElementById('macros-button-name').value = '';
			document.getElementById('macros-name-box-container').style.display = 'none';
			document.getElementById('macros-buttons').style.display = 'block';
		},
		record: function(eve, filenum) {
			if ( ! isRecording) {
				return;
			}
			var button = eve.target;
			if (filenum !== undefined) {
				recorded_buttons_accumulator.pushFileRelatedButton(filenum, button);
			} else {
				recorded_buttons_accumulator.pushResultRelatedButton(button);
			}
			var recorded_buttons = recorded_buttons_accumulator.getRecords();
			recorded_buttons_view.update(recorded_buttons); 
		},
		getHotkeysListener: function() {
			return function(key_event) {
				if (isRecording) {
					return;
				}
				var keyCode = key_event.keyCode;
				var hotkey = {
					keyCode: keyCode,
					key_shift: key_event.shiftKey,
					key_ctrl: key_event.ctrlKey,
					key_alt: key_event.altKey,
					key_meta: key_event.metaKey
				};
				var hotkey_as_key = hotkey2string(hotkey);
				var handler = hotkey_handlers[hotkey_as_key];
				if (handler) {
					key_event.preventDefault();
					handler.apply();
				}
			};
		}
	}
}
