function ReplicsIterator(replics_array) {
	var replics = replics_array || [];
	var current_index = 0;

	var instance_to_return = {
		next: function() {
			if (replics.length == 0) {
				throw new RangeError('cannot move to next replic: replics list is empty');
			}
			if (current_index >= replics.length-1) {
				throw new RangeError('cannot move to next replic: current replic is last');
			}
			current_index++;
			this.notifyCurrentReplicListeners();
			return replics[current_index];
		},
		prev: function() {
			if (replics.length == 0) {
				throw new RangeError('cannot move to prev replic: replics list is empty');
			}
			if (current_index <= 0) {
				throw new RangeError('cannot move to prev replic: current replic is first');
			}
			current_index--;
			this.notifyCurrentReplicListeners();
			return replics[current_index];
		},
		getCurrentReplic: function() {return replics[current_index];},
		setReplics: function(replics_param) {replics = replics_param;current_index=0;this.notifyCurrentReplicListeners();},
		getTimedeltaPrev: function() {
			if (current_index==0){
				throw new RangeError("can't get timedelta prev: current replic is first");
			}
			var prev_replic_end = replics[current_index-1].getTiming().end_time.getTime();
			var current_replic_start = replics[current_index].getTiming().start_time.getTime();
			return current_replic_start - prev_replic_end;
		},
		getTimedeltaNext: function() {
			if (current_index==replics.length-1){
				throw new RangeError("can't get timedelta next: current replic is last");
			}
			var current_replic_end = replics[current_index].getTiming().end_time.getTime();
			var next_replic_start = replics[current_index+1].getTiming().start_time.getTime();
			return next_replic_start - current_replic_end;
		}
	};
	(function(obj){
		var listeners = [];
		obj.addCurrentReplicListener = function(listener) {
			listeners.push(listener);
		};
		obj.removeCurrentReplicListener = function(listener) {
			var index = listeners.indexOf(listener);
			if (index>=0 && index<= listeners.length) {
				listeners.splice(index, 1);
			}
		};
		obj.notifyCurrentReplicListeners = function() {
			for (var i=listeners.length-1; i>=0; i--) {
				listeners[i].update(obj);
			}
		};
	 })(instance_to_return);
	return instance_to_return;
}


