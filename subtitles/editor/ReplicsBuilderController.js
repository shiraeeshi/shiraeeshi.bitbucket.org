function ReplicsBuilderController(parsers_param, iterators_param, builder_param) {
	var parsers = parsers_param;
	var iters = iterators_param;
	var builder = builder_param;

	for (var i=0; i<parsers.length; i++) {
		(function(parsernum){
			parsers[parsernum].onParse = function(parser) {
				var iterator = iters[parsernum];
				iterator.setReplics(parser.getReplics());
			};
		 })(i);
	}

	var setTimingPoint = function(is_start, time_as_string) {
		var timing_point = Timing.parse_point(time_as_string);
		var replic = builder.getCurrentReplic();
		if (replic == null) {
			console.log('cannot set ' + (is_start?'start':'end') + ': current replic is null');
			return;
		}
		var timing = replic.getTiming().copy();
		if (is_start) {
			timing.start_time = timing_point;
		} else {
			timing.end_time = timing_point;
		}
		replic.setTiming(timing);
	};

	return {
		parse: function(filenum) {parsers[filenum-1].parse();},
		encodingChange: function(filenum, encoding) {parsers[filenum-1].setEncoding(encoding);},
		next: function(filenum) {iters[filenum-1].next();},
		prev: function(filenum) {iters[filenum-1].prev();},
		push: function() {builder.push();},
		insertBefore: function() {builder.insertBefore();},
		setStartTiming: function(start_as_string) {setTimingPoint(true,start_as_string);},
		setEndTiming: function(end_as_string) {setTimingPoint(false,end_as_string);},
		removeCurrentReplicInBuilder: function() {builder.removeCurrent();},
		nextReplicInBuilder: function() {builder.next();},
		prevReplicInBuilder: function() {builder.prev();},
		copyTiming: function(filenum) {builder.getCurrentReplic().setTiming(iters[filenum-1].getCurrentReplic().getTiming().copy()); },
		copyText: function(filenum) {builder.getCurrentReplic().setText(iters[filenum-1].getCurrentReplic().getText()); },
		copyTimeDeltaPrev: function(filenum) {
			var timedelta_prev = iters[filenum-1].getTimedeltaPrev();
			builder.setTimedeltaPrev(timedelta_prev);
		},
		copyDuration: function(filenum) {
			builder.getCurrentReplic().copyDuration(iters[filenum-1].getCurrentReplic());
		},
		copyTimeDeltaNext: function(filenum) {
			var timedelta_next = iters[filenum-1].getTimedeltaNext();
			builder.setTimedeltaNext(timedelta_next);
		}

	};
}


