function SubtParser() {
	var file = null; 
	var encoding = null;
	var replics = [];
	
	var reggie = /(?:\s*)^(\d+)(?:\s*)^(\d{1,2}:\d{2}:\d{2},\d{2,6} --> \d{1,2}:\d{2}:\d{2},\d{2,6})(?:\s*)^([\s\S]*)/m;

	var readfile = function(e) {
		// Print the contents of the file
		var text = e.target.result;

		var lines = text.split(/\r\n/g); // tolerate both Windows and Unix linebreaks
		console.log("lines count: " + lines.length);
		replics = [];
		var replic_as_string = '';
		for(var i = 0; i < lines.length; i++) {
			var line = lines[i];
			if (line.length > 0) {
				replic_as_string += '\n' + line;
				continue;
			}
			var groups = replic_as_string.match(reggie);
			if (groups == null) {
				console.log('bad replic_as_string: ' + replic_as_string);
				continue;
			}
			var num = parseInt(groups[1]);
			var timing_as_string = groups[2];
			var text = groups[3];
			replics.push( new Replic(num, timing_as_string, text) );
			replic_as_string = '';
		}
	};


	return {
		setFile: function(fileParam) {file=fileParam;},
		setEncoding: function(encodingParam) {encoding=encodingParam;},
		getReplics: function() {return replics;},
		getEncodingChangeListener: function() {
			var self = this;
			return function(eve) {
				var encod = eve.target.value;
				self.setEncoding(encod);
			}
		},
		parse: function() {
			var self = this;
			var reader = new FileReader();
			reader.onload = function(e) {
				readfile(e);
				if (self.onParse) {
					self.onParse(self);
				}
			}
			reader.readAsText(file, encoding);
		},

	};
}


