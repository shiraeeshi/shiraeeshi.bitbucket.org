function Timing(string_input) {
	if (string_input == undefined) {
		this.start_time = null;
		this.end_time = null;
		return;
	}
	var points = string_input.split(' --> ')
	this.start_time = Timing.parse_point(points[0]);
	this.end_time = Timing.parse_point(points[1]);
}
Timing.zero = function() {
	var timing = new Timing();
	timing.start_time = new Date(0);
	timing.end_time = new Date(0);
	return timing;
};
Timing.parse_point = function(point_as_string) {
	var reggie = /(\d{1,2}):(\d{2}):(\d{2}),(\d{2,6})/;
	var time = point_as_string.match(reggie);
	var dat = new Date(0);
	dat.setUTCHours( parseInt(time[1]) );
	dat.setUTCMinutes( parseInt(time[2]) );
	dat.setUTCSeconds( parseInt(time[3]) );
	var millis_as_string = time[4];
	while (millis_as_string.length < 3) {
		millis_as_string += '0';
	}
	if (millis_as_string.length > 3) {
		millis_as_string = millis_as_string.substring(0,3);
	}
	dat.setUTCMilliseconds( parseInt(millis_as_string) );
	return dat;
}; 
Timing.prototype.getDuration = function() {
	var end_time = this.end_time.getTime();
	var start_time = this.start_time.getTime();
	return end_time - start_time;
};
Timing.prototype.copy = function() {
	var timing = new Timing();
	timing.start_time = new Date( this.start_time.getTime() );	
	timing.end_time = new Date( this.end_time.getTime() );	
	return timing;
};
Timing.prototype.toString = function() {
	var time_point_to_string = function(time_point) {
		return time_point.getUTCHours() + ':' +
			('0' + time_point.getUTCMinutes()).slice(-2) + ':' +
			('0' + time_point.getUTCSeconds()).slice(-2) + ',' +
			(time_point.getUTCMilliseconds() / 1000 + '00').slice(2, 5);
	};
	return time_point_to_string(this.start_time) + ' --> ' + time_point_to_string(this.end_time);
};

